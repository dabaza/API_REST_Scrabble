
![Imagen de inicio](./imagenes/ImagenInicio.jpg "Logo Incio documento")

<p style="text-align: right;"> 

**ALUMNO:** 

</p>

<p style="text-align: right;"> 

**David Barberá Zahonero**

</p>

**Índice de contenido**

<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

- [PERSONAS.](#personas)
- [MODELO CONCEPTUAL (FLUJOGRAMA).](#modelo-conceptual-flujograma)
- [MAPA WEB](#mapa-web)
- [SCRABLE (FLUJOGRAMA)](#scrable-flujograma)
- [BOCETOS (WIREFRAMES)](#bocetos-wireframes)
    - [INFORMACIÓN IMPRENTA (VISTA PC)](#informaciÓn-imprenta-vista-pc)
    - [INFORMACIÓN IMPRENTA (VISTA TABLET)](#informaciÓn-imprenta-vista-tablet)
    - [INFORMACIÓN IMPRENTA (VISTA MÓVIL)](#informaciÓn-imprenta-vista-mÓvil)
    - [TABLERO SCRABBLE (VISTA PC)](#tablero-scrabble-vista-pc)
    - [TABLERO SCRABBLE (VISTA TABLET)](#tablero-scrabble-vista-tablet)
    - [TABLERO SCRABBLE (VISTA MOVIL)](#tablero-scrabble-vista-movil)
    - [DASHBOARD (VISTA PC)](#dashboard-vista-pc)
    - [DASHBOARD (VISTA TABLET)](#dashboard-vista-tablet)
    - [DASHBOARD (VISTA MÓVIL)](#dashboard-vista-mÓvil)

<!-- /code_chunk_output -->

# PERSONAS.

**Nombre:** Beatriz
**Edad:**20 años
**Ocupación:** Estudiante 1er curso del Grado de Educación Secundaria..
 
**Descripción:**
Beatriz es soltera y ha superado la Educación Secundaria post-obligatoria con una nota media de Notable. Como es normal en personas de su generación, está muy familiarizada con las nuevas tecnologías y accede a recursos de Internet por diferentes plataformas (Tablet, Smartphone, PC).
**Escenario:**
Son las 20:15 y Beatriz ha estado durante un par de horas repasando todo lo estudiado hoy en la facultad y ha de presentar un trabajo relacionado con la historia de la imprenta. Ha decidido hacer una pausa y necesita desconectar. Ella es una apasionada del juego del Scrabble y conoce que en la web de su antiguo instituto tienen una página dedicada a la imprenta que tiene es su contenido una sección donde hay una plataforma para jugar al juego que tanto le gusta.
___
**Nombre:** Andrés
**Edad: 48** años
**Ocupación:** Profesor del departamento de Castellano. 
**Descripción:**
Andrés está casado y tiene una hija de 14 años y un hijo de 9 años. Está familiarizado con las nuevas tecnologías y le gusta utilizarlas como recurso y complemento para la enseñanza. 
**Escenario:**
Son las 08:00 de un viernes. Después de una semana dura por que ha estado cargada de exámenes. Andrés observa que sus alumnos, en general, están saturados y con el rostro mostrando cansancio. Se le ocurre proponer una actividad lúdica y a su vez educativa. Decide organizar espontáneamente una especie de “batalla de las palabras”. Propone a sus alumnos que se conecten a la página del centro y que accedan a la página web del aniversario de la invención de la imprenta y accedan al juego del Scrabble para, por grupos de X alumnos, jugar una partida. Como incentivo, a las mejores puntuaciones de cada grupo obtendrán 0,5 puntos que se tendrán en cuenta en los criterios de evaluación.
___
**Nombre:** Amparo
**Edad:** 62 años
**Ocupación:** Ama de casa.
 
**Descripción:**
Amparo está casada y tiene 3 hijos, todos casados y con hijos, de los cuales tiene 4 nietos/as. Uno de 16 años, otro de 14 años, una nieta de 11 años y otra de 10 años. Dispone de Smartphone y desde hace poco tiempo está aprendiendo a utilizarlo para conectarse a Internet. Poco a poco se está familiarizando con esta plataforma.

**Escenario:**
Son las 17:30. Amparo acaba de llegar a casa después de recoger del colegio a su nieta Andrea, de 10 años. Hoy su nieta se está haciendo la remolona a la hora de tomarse la merienda y Amparo, para procurar que su nieta meriende bien, le dice a la esta que si  come toda la merienda, después jugarán las dos al juego del escrabble que hay en la página web del instituto de su nieto Juan, de 16 años. La niña con su Tablet y la abuela con su móvil. Obviamente, a la niña le gusta mucho jugar con su abuela y se comió la merienda al momento.
___
**Nombre:** Víctor
**Edad:** 11 años
**Ocupación:** Alumno de 6º curso de Educación Primaria.

**Descripción:**
Víctor es un niño cuya familia está compuesta además de sus padres por una hermana de 16 años de edad. A Víctor, como a la mayoría de niños de su edad, le encantan las nuevas tecnologías y le gusta mucho investigar y aprender con su Tablet, jugando a juegos educativos y a otros no tanto. En fin, lo que es normal a su edad. Le encanta mucho los juegos de palabras como el Apalabrados y el Scrabble, se ha aficionado a estos por influencia de su hermana, que le ha enseñado a jugar y muchas veces juegan juntos.

**Escenario:**
Es domingo por la mañana y Víctor, que es un niño muy aplicado y no tiene deberes por que ya los realizó el viernes por la tarde. Como de costumbre, es el primero que abre los ojos para levantarse. Momento que aprovecha para encender su Tablet y conectarse a la página del instituto de su hermano, donde juega al Scrabble.
___
# MODELO CONCEPTUAL (FLUJOGRAMA).

![Modelo conceptual](./imagenes/Modelo_Conceptual.png "Modelo concpetual")

# MAPA WEB

![Mapa Web](./imagenes/Mapa_Web.png "Mapa Web")

# SCRABLE (FLUJOGRAMA)

![Mapa Web](./imagenes/flujogramascrabble.png "Mapa Web")

# BOCETOS (WIREFRAMES)

## INFORMACIÓN IMPRENTA (VISTA PC)

![Información imprenta](./imagenes/boceto_area_info.jpg "Vista PC")

## INFORMACIÓN IMPRENTA (VISTA TABLET)

![Información imprenta](./imagenes/boceto_area_info_tablet.jpg "Vista Móvil")

## INFORMACIÓN IMPRENTA (VISTA MÓVIL)

![Información imprenta](./imagenes/boceto_area_info_movil.jpg "Vista Móvil")

## TABLERO SCRABBLE (VISTA PC)

![Área Scrabble](./imagenes/area_Scrabble_pc.jpg "Vista PC")

## TABLERO SCRABBLE (VISTA TABLET)

![Área Scrabble](./imagenes/area_scrabble_tablet.jpg "Vista Tablet")

## TABLERO SCRABBLE (VISTA MOVIL)

![Área Scrabble](./imagenes/area_scrabble_movil.jpg "Vista Móvil")

## DASHBOARD (VISTA PC)

![Área Dashboard](./imagenes/Dashboard_pc.jpg "Vista PC")

## DASHBOARD (VISTA TABLET)

![Área Dashboard](./imagenes/Dashboard_tablet.jpg "Vista PC")

## DASHBOARD (VISTA MÓVIL)

![Área Dashboard](./imagenes/dashboard_movil.jpg "Vista PC")
