$(function() {
  //var menuNav = $('#menuNav');
  var linkGutenberg = $('#linkGutenberg');
  var linkSubir = $('#subir');
  var linkTrabajos = $('#linkTrabajos');
  var linkDifusion = $('#linkDifusion');
  var linkLibros = $('#linkLibros');
  var linkImprentaValencia = $('#linkImprentaValencia');
  var spmComponedor = $('#componedor'); // referimos elemento del li "componedor"
  var imgComponedor = $('#imgComponedor'); // referimos el elemento img del componedor
  var spmEntintador = $('#entintador'); // referimos elemento del li "entintador"
  var imgEntintador = $('#imgEntintador'); // referimos el elemento img del entintador
  var spmTirador = $('#tirador'); // referimos elemento del li "tirador"
  var imgTirador = $('#imgTirador'); // referimos el elemento img del tirador
  var imgEuropa11 = $('#imgEuropa11'); // referimos el elemento img de mapa de europa
  var imgEuropa11_2 = $('#imgEuropa11_2'); // referimos el elemento img de mapa de europa
  var spnMainz = $('#mainz');
  var spnMainz_2 = $('#mainz_2');
  var spnVenecia = $('#venecia');
  var spnVenecia_2 = $('#venecia_2');
  var spnParis = $('#paris');
  var spnParis_2 = $('#paris_2');
  var spnBrujas = $('#brujas');
  var spnBrujas_2 = $('#brujas_2');
  var spnSegovia = $('#segovia');
  var spnSegovia_2 = $('#segovia_2');
  var spnValencia = $('#valencia');
  var spnValencia_2 = $('#valencia_2');
  var spnWestminster = $('#westminster');
  var spnWestminster_2 = $('#westminster_2');
  var btnRegistro = $('#botonRegistro'); // boton del formulario de login para ir a la página de registro
  var btnPass = $('#botonPass'); // boton del formulario de login para ir a la página de reset pass

  // al cargar la página elementos que no están de inicio en el flujo de la página.
  //menuNav.addClass("quitar"); // menu de navegación
  linkSubir.addClass('quitar'); // span para subir el scroll

  // comprobamos si se accede a info scrabble para posicionar el scroll en la posición deseada.
  var URLactual = window.location.toString();
  var sitio = '';
  if (URLactual.indexOf('scrabble/') >= 0) {
    linkSubir.removeClass('quitar');
  } else if (URLactual.indexOf('gutenberg') >= 0) {
    linkSubir.removeClass('quitar');
  } else {
    linkSubir.addClass('quitar');
  }

  if (
    URLactual.indexOf('scrabble') < 0 &&
    URLactual.indexOf('dashboard') < 0 &&
    URLactual.indexOf('login') < 0 &&
    URLactual.indexOf('register') < 0
  ) {
    linkDifusion.removeClass('quitar');
    linkDifusion.parent().removeClass('quitar');
    linkImprentaValencia.removeClass('quitar');
    linkImprentaValencia.parent().removeClass('quitar');
    linkTrabajos.removeClass('quitar');
    linkTrabajos.parent().removeClass('quitar');
    linkLibros.removeClass('quitar');
    linkLibros.parent().removeClass('quitar');
  } else {
    linkDifusion.addClass('quitar');
    linkDifusion.parent().addClass('quitar');
    linkImprentaValencia.addClass('quitar');
    linkImprentaValencia.parent().addClass('quitar');
    linkTrabajos.addClass('quitar');
    linkTrabajos.parent().addClass('quitar');
    linkLibros.addClass('quitar');
    linkLibros.parent().addClass('quitar');
  }
  console.log('Escrable' + URLactual.indexOf('scrabble'));
  console.log('Dahs' + URLactual.indexOf('Dahsboard'));
  console.log('login' + URLactual.indexOf('login'));
  console.log('register' + URLactual.indexOf('register'));

  // manejador del evento scroll de la ventana
  $(window).scroll(function() {
    // fija la barra de navegación en un momento determinado
    // if ($(this).scrollTop() >= 900) {
    //     menuNav.removeClass("quitarMenu");
    //     menuNav.removeClass("quitar");
    //     menuNav.addClass("mostrarMenu");
    //     //menuNav.addClass("fijarCabecera");
    //     $(".miLogo").addClass("miLogoPequeño").removeClass("miLogo");
    //     $("#gutenberg").addClass("gutenbergScroll");
    //     //$("#scrabbleInfo").addClass("gutenbergScroll");
    // } else {
    //     menuNav.removeClass("mostrarMenu");
    //     menuNav.addClass("quitarMenu");
    //     setTimeout(function() {
    //         menuNav.addClass("quitar");
    //     }, 3000);
    //     //$("header").removeClass("fijarCabecera");
    //     $(".miLogoPequeño").addClass("miLogo").removeClass("miLogoPequeño");
    //     $("#gutenberg").removeClass("gutenbergScroll");
    //     //$("#scrabbleInfo").removeClass("gutenbergScroll");
    // }

    // muestra u oculta span subir
    if ($(this).scrollTop() > 950) {
      linkSubir.removeClass('quitar');
    } else {
      linkSubir.addClass('quitar');
    }

    // inicio de animación cuando va a aparecer el contenido por pantalla
    $('.animation').each(function() {
      var imagePos = $(this).offset().top;

      var topOfWindow = $(window).scrollTop();
      if (imagePos < topOfWindow + 700) {
        // si la posición del contenido sobre el top es menor a 600
        $(this).addClass('slideUp'); // efecto que aparece la imagen con movimiento hacia arriba
      }
    });

    // inicio de animación cuando va a aparecer el contenido por pantalla
    $('.animationRight').each(function() {
      var imagePos = $(this).offset().top;

      var topOfWindow = $(window).scrollTop();
      if (imagePos < topOfWindow + 700) {
        // si la posición del contenido sobre el top es menor a 600
        $(this).addClass('slideRight'); // efecto que aparece la imagen con movimiento hacia arriba
      }
    });

    // inicio de animación cuando va a aparecer el contenido por pantalla
    $('.animationLeft').each(function() {
      var imagePos = $(this).offset().top;

      var topOfWindow = $(window).scrollTop();
      if (imagePos < topOfWindow + 700) {
        // si la posición del contenido sobre el top es menor a 600
        $(this).addClass('slideLeft'); // efecto que aparece la imagen con movimiento hacia arriba
      }
    });
  });

  // manejador del evento click de los enlaces del menú referentes a info imprenta
  // localiza la posición del destino en el dom y posiciona la página en su posición
  // se realiza ajuste para que el título de la sección no quede debajo del menú de navegación
  // if (sitio != 'scrabble') {
  //   linkGutenberg.attr('href', '#gutenberg');

  // linkGutenberg.click(function() {
  //   $('html, body').animate(
  //     { scrollTop: $('#gutenberg').offset().top - 120 },
  //     'slow'
  //   );
  // });
  // }

  $('a[href^="#"]').click(function() {
    var destino = $(this.hash);
    if (destino.length == 0) {
      destino = $('a[name="' + this.hash.substr(1) + '"]');
    }
    if (destino.length == 0) {
      destino = $('html');
    }

    $('html, body').animate({ scrollTop: destino.offset().top - 120 }, 'slow');
  });

  // manjejador del evento hover en el li del componedor
  spmComponedor.hover(
    function() {
      imgComponedor.attr('src', 'img/08b_Trabajadores_imprenta_1.png');
    },
    function() {
      imgComponedor.attr('src', 'img/08_Trabajadores_imprenta_1.png');
    }
  );
  // manjejador del evento hover la imagen del componedor
  imgComponedor.hover(
    function() {
      $(this).attr('src', 'img/08b_Trabajadores_imprenta_1.png');
    },
    function() {
      $(this).attr('src', 'img/08_Trabajadores_imprenta_1.png ');
    }
  );

  spmEntintador.hover(
    function() {
      imgEntintador.attr('src', 'img/09b_Trabajadores_imprenta_2.jpg');
    },
    function() {
      imgEntintador.attr('src', 'img/09_Trabajadores_imprenta_2.jpg');
    }
  );

  imgEntintador.hover(
    function() {
      $(this).attr('src', 'img/09b_Trabajadores_imprenta_2.jpg');
    },
    function() {
      $(this).attr('src', 'img/09_Trabajadores_imprenta_2.jpg');
    }
  );

  spmTirador.hover(
    function() {
      imgTirador.attr('src', 'img/10b_Trabajadores_imprenta_3.jpg');
    },
    function() {
      imgTirador.attr('src', 'img/10_Trabajadores_imprenta_3.jpg');
    }
  );

  imgTirador.hover(
    function() {
      $(this).attr('src', 'img/10b_Trabajadores_imprenta_3.jpg');
    },
    function() {
      $(this).attr('src', 'img/10_Trabajadores_imprenta_3.jpg');
    }
  );

  imgEuropa11.hover(
    function() {
      $(this).attr('src', 'img/11_b_Europa_e_imprenta.png');
    },
    function() {
      $(this).attr('src', 'img/11_transparente.png');
    }
  );

  imgEuropa11_2.hover(
    function() {
      $(this).attr('src', 'img/11_b_Europa_e_imprenta.png');
    },
    function() {
      $(this).attr('src', 'img/11_transparente.png');
    }
  );

  spnMainz.hover(
    function() {
      $(imgEuropa11).attr('src', 'img/11_maguncia.png');
    },
    function() {
      $(imgEuropa11).attr('src', 'img/11_transparente.png');
    }
  );

  spnVenecia.hover(
    function() {
      $(imgEuropa11).attr('src', 'img/11_venecia.png');
    },
    function() {
      $(imgEuropa11).attr('src', 'img/11_transparente.png');
    }
  );

  spnParis.hover(
    function() {
      $(imgEuropa11).attr('src', 'img/11_paris.png');
    },
    function() {
      $(imgEuropa11).attr('src', 'img/11_transparente.png');
    }
  );

  spnBrujas.hover(
    function() {
      $(imgEuropa11).attr('src', 'img/11_brujas.png');
    },
    function() {
      $(imgEuropa11).attr('src', 'img/11_transparente.png');
    }
  );

  spnSegovia.hover(
    function() {
      $(imgEuropa11).attr('src', 'img/11_segovia.png');
    },
    function() {
      $(imgEuropa11).attr('src', 'img/11_transparente.png');
    }
  );

  spnValencia.hover(
    function() {
      $(imgEuropa11).attr('src', 'img/11_valencia.png');
    },
    function() {
      $(imgEuropa11).attr('src', 'img/11_transparente.png');
    }
  );

  spnWestminster.hover(
    function() {
      $(imgEuropa11).attr('src', 'img/11_westminster.png');
    },
    function() {
      $(imgEuropa11).attr('src', 'img/11_transparente.png');
    }
  );

  spnMainz_2.hover(
    function() {
      $(imgEuropa11_2).attr('src', 'img/11_maguncia.png');
    },
    function() {
      $(imgEuropa11_2).attr('src', 'img/11_transparente.png');
    }
  );

  spnVenecia_2.hover(
    function() {
      $(imgEuropa11_2).attr('src', 'img/11_venecia.png');
    },
    function() {
      $(imgEuropa11_2).attr('src', 'img/11_transparente.png');
    }
  );

  spnParis_2.hover(
    function() {
      $(imgEuropa11_2).attr('src', 'img/11_paris.png');
    },
    function() {
      $(imgEuropa11_2).attr('src', 'img/11_transparente.png');
    }
  );

  spnBrujas_2.hover(
    function() {
      $(imgEuropa11_2).attr('src', 'img/11_brujas.png');
    },
    function() {
      $(imgEuropa11_2).attr('src', 'img/11_transparente.png');
    }
  );

  spnSegovia_2.hover(
    function() {
      $(imgEuropa11_2).attr('src', 'img/11_segovia.png');
    },
    function() {
      $(imgEuropa11_2).attr('src', 'img/11_transparente.png');
    }
  );

  spnValencia_2.hover(
    function() {
      $(imgEuropa11_2).attr('src', 'img/11_valencia.png');
    },
    function() {
      $(imgEuropa11_2).attr('src', 'img/11_transparente.png');
    }
  );

  spnWestminster_2.hover(
    function() {
      $(imgEuropa11_2).attr('src', 'img/11_westminster.png');
    },
    function() {
      $(imgEuropa11_2).attr('src', 'img/11_transparente.png');
    }
  );

  for (let i = 2; i < 6; i++) {
    $('#img1' + i).hover(
      function() {
        $(this).attr('class', 'imgLink');
      },
      function() {
        $(this).removeAttr('class');
      }
    );
  }

  // manejador de los sliders
  $('.carousel').carousel({
    interval: 3000,
    pause: 'hover',
  });

  // manejador del evento click del elemento subir que realiza scroll al inicio de la página
  $('#subir').click(function() {
    //referimos el elemento
    $('html, body').animate({ scrollTop: 850 }, 'slow'); //seleccionamos etiquetas,clase o identificador destino, creamos animación hacia top de la página.
  });

  // manejador el evento click del boton para acceder a la página de regstro
  btnRegistro.click(function() {
    location.href = './register'; // url de la página de registro
  });

  // manejador el evento click del boton para acceder a la página de reset pass
  btnPass.click(function() {
    location.href = './password/reset'; // url de la página de registro
  });

  // inicializamos todos los toolstips de la página
  $('[data-toggle="tooltip"]').tooltip();
});
