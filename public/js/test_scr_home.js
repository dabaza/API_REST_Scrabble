/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 139);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1,eval)("this");
} catch(e) {
	// This works if the window reference is available
	if(typeof window === "object")
		g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),
/* 1 */
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function(useSourceMap) {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		return this.map(function (item) {
			var content = cssWithMappingToString(item, useSourceMap);
			if(item[2]) {
				return "@media " + item[2] + "{" + content + "}";
			} else {
				return content;
			}
		}).join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};

function cssWithMappingToString(item, useSourceMap) {
	var content = item[1] || '';
	var cssMapping = item[3];
	if (!cssMapping) {
		return content;
	}

	if (useSourceMap && typeof btoa === 'function') {
		var sourceMapping = toComment(cssMapping);
		var sourceURLs = cssMapping.sources.map(function (source) {
			return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */'
		});

		return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
	}

	return [content].join('\n');
}

// Adapted from convert-source-map (MIT)
function toComment(sourceMap) {
	// eslint-disable-next-line no-undef
	var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
	var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;

	return '/*# ' + data + ' */';
}


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/

var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

var listToStyles = __webpack_require__(18)

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}
var options = null
var ssrIdKey = 'data-vue-ssr-id'

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

module.exports = function (parentId, list, _isProduction, _options) {
  isProduction = _isProduction

  options = _options || {}

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[' + ssrIdKey + '~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }
  if (options.ssrId) {
    styleElement.setAttribute(ssrIdKey, obj.id)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),
/* 3 */
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file.
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate

    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(process) {/**
 * This is the web browser implementation of `debug()`.
 *
 * Expose `debug()` as the module.
 */

exports = module.exports = __webpack_require__(48);
exports.log = log;
exports.formatArgs = formatArgs;
exports.save = save;
exports.load = load;
exports.useColors = useColors;
exports.storage = 'undefined' != typeof chrome
               && 'undefined' != typeof chrome.storage
                  ? chrome.storage.local
                  : localstorage();

/**
 * Colors.
 */

exports.colors = [
  '#0000CC', '#0000FF', '#0033CC', '#0033FF', '#0066CC', '#0066FF', '#0099CC',
  '#0099FF', '#00CC00', '#00CC33', '#00CC66', '#00CC99', '#00CCCC', '#00CCFF',
  '#3300CC', '#3300FF', '#3333CC', '#3333FF', '#3366CC', '#3366FF', '#3399CC',
  '#3399FF', '#33CC00', '#33CC33', '#33CC66', '#33CC99', '#33CCCC', '#33CCFF',
  '#6600CC', '#6600FF', '#6633CC', '#6633FF', '#66CC00', '#66CC33', '#9900CC',
  '#9900FF', '#9933CC', '#9933FF', '#99CC00', '#99CC33', '#CC0000', '#CC0033',
  '#CC0066', '#CC0099', '#CC00CC', '#CC00FF', '#CC3300', '#CC3333', '#CC3366',
  '#CC3399', '#CC33CC', '#CC33FF', '#CC6600', '#CC6633', '#CC9900', '#CC9933',
  '#CCCC00', '#CCCC33', '#FF0000', '#FF0033', '#FF0066', '#FF0099', '#FF00CC',
  '#FF00FF', '#FF3300', '#FF3333', '#FF3366', '#FF3399', '#FF33CC', '#FF33FF',
  '#FF6600', '#FF6633', '#FF9900', '#FF9933', '#FFCC00', '#FFCC33'
];

/**
 * Currently only WebKit-based Web Inspectors, Firefox >= v31,
 * and the Firebug extension (any Firefox version) are known
 * to support "%c" CSS customizations.
 *
 * TODO: add a `localStorage` variable to explicitly enable/disable colors
 */

function useColors() {
  // NB: In an Electron preload script, document will be defined but not fully
  // initialized. Since we know we're in Chrome, we'll just detect this case
  // explicitly
  if (typeof window !== 'undefined' && window.process && window.process.type === 'renderer') {
    return true;
  }

  // Internet Explorer and Edge do not support colors.
  if (typeof navigator !== 'undefined' && navigator.userAgent && navigator.userAgent.toLowerCase().match(/(edge|trident)\/(\d+)/)) {
    return false;
  }

  // is webkit? http://stackoverflow.com/a/16459606/376773
  // document is undefined in react-native: https://github.com/facebook/react-native/pull/1632
  return (typeof document !== 'undefined' && document.documentElement && document.documentElement.style && document.documentElement.style.WebkitAppearance) ||
    // is firebug? http://stackoverflow.com/a/398120/376773
    (typeof window !== 'undefined' && window.console && (window.console.firebug || (window.console.exception && window.console.table))) ||
    // is firefox >= v31?
    // https://developer.mozilla.org/en-US/docs/Tools/Web_Console#Styling_messages
    (typeof navigator !== 'undefined' && navigator.userAgent && navigator.userAgent.toLowerCase().match(/firefox\/(\d+)/) && parseInt(RegExp.$1, 10) >= 31) ||
    // double check webkit in userAgent just in case we are in a worker
    (typeof navigator !== 'undefined' && navigator.userAgent && navigator.userAgent.toLowerCase().match(/applewebkit\/(\d+)/));
}

/**
 * Map %j to `JSON.stringify()`, since no Web Inspectors do that by default.
 */

exports.formatters.j = function(v) {
  try {
    return JSON.stringify(v);
  } catch (err) {
    return '[UnexpectedJSONParseError]: ' + err.message;
  }
};


/**
 * Colorize log arguments if enabled.
 *
 * @api public
 */

function formatArgs(args) {
  var useColors = this.useColors;

  args[0] = (useColors ? '%c' : '')
    + this.namespace
    + (useColors ? ' %c' : ' ')
    + args[0]
    + (useColors ? '%c ' : ' ')
    + '+' + exports.humanize(this.diff);

  if (!useColors) return;

  var c = 'color: ' + this.color;
  args.splice(1, 0, c, 'color: inherit')

  // the final "%c" is somewhat tricky, because there could be other
  // arguments passed either before or after the %c, so we need to
  // figure out the correct index to insert the CSS into
  var index = 0;
  var lastC = 0;
  args[0].replace(/%[a-zA-Z%]/g, function(match) {
    if ('%%' === match) return;
    index++;
    if ('%c' === match) {
      // we only are interested in the *last* %c
      // (the user may have provided their own)
      lastC = index;
    }
  });

  args.splice(lastC, 0, c);
}

/**
 * Invokes `console.log()` when available.
 * No-op when `console.log` is not a "function".
 *
 * @api public
 */

function log() {
  // this hackery is required for IE8/9, where
  // the `console.log` function doesn't have 'apply'
  return 'object' === typeof console
    && console.log
    && Function.prototype.apply.call(console.log, console, arguments);
}

/**
 * Save `namespaces`.
 *
 * @param {String} namespaces
 * @api private
 */

function save(namespaces) {
  try {
    if (null == namespaces) {
      exports.storage.removeItem('debug');
    } else {
      exports.storage.debug = namespaces;
    }
  } catch(e) {}
}

/**
 * Load `namespaces`.
 *
 * @return {String} returns the previously persisted debug modes
 * @api private
 */

function load() {
  var r;
  try {
    r = exports.storage.debug;
  } catch(e) {}

  // If debug isn't set in LS, and we're in Electron, try to load $DEBUG
  if (!r && typeof process !== 'undefined' && 'env' in process) {
    r = Object({"MIX_PUSHER_APP_KEY":"","MIX_PUSHER_APP_CLUSTER":"mt1","NODE_ENV":"development"}).DEBUG;
  }

  return r;
}

/**
 * Enable namespaces listed in `localStorage.debug` initially.
 */

exports.enable(load());

/**
 * Localstorage attempts to return the localstorage.
 *
 * This is necessary because safari throws
 * when a user disables cookies/localstorage
 * and you attempt to access it.
 *
 * @return {LocalStorage}
 * @api private
 */

function localstorage() {
  try {
    return window.localStorage;
  } catch (e) {}
}

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(13)))

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {


/**
 * Expose `Emitter`.
 */

if (true) {
  module.exports = Emitter;
}

/**
 * Initialize a new `Emitter`.
 *
 * @api public
 */

function Emitter(obj) {
  if (obj) return mixin(obj);
};

/**
 * Mixin the emitter properties.
 *
 * @param {Object} obj
 * @return {Object}
 * @api private
 */

function mixin(obj) {
  for (var key in Emitter.prototype) {
    obj[key] = Emitter.prototype[key];
  }
  return obj;
}

/**
 * Listen on the given `event` with `fn`.
 *
 * @param {String} event
 * @param {Function} fn
 * @return {Emitter}
 * @api public
 */

Emitter.prototype.on =
Emitter.prototype.addEventListener = function(event, fn){
  this._callbacks = this._callbacks || {};
  (this._callbacks['$' + event] = this._callbacks['$' + event] || [])
    .push(fn);
  return this;
};

/**
 * Adds an `event` listener that will be invoked a single
 * time then automatically removed.
 *
 * @param {String} event
 * @param {Function} fn
 * @return {Emitter}
 * @api public
 */

Emitter.prototype.once = function(event, fn){
  function on() {
    this.off(event, on);
    fn.apply(this, arguments);
  }

  on.fn = fn;
  this.on(event, on);
  return this;
};

/**
 * Remove the given callback for `event` or all
 * registered callbacks.
 *
 * @param {String} event
 * @param {Function} fn
 * @return {Emitter}
 * @api public
 */

Emitter.prototype.off =
Emitter.prototype.removeListener =
Emitter.prototype.removeAllListeners =
Emitter.prototype.removeEventListener = function(event, fn){
  this._callbacks = this._callbacks || {};

  // all
  if (0 == arguments.length) {
    this._callbacks = {};
    return this;
  }

  // specific event
  var callbacks = this._callbacks['$' + event];
  if (!callbacks) return this;

  // remove all handlers
  if (1 == arguments.length) {
    delete this._callbacks['$' + event];
    return this;
  }

  // remove specific handler
  var cb;
  for (var i = 0; i < callbacks.length; i++) {
    cb = callbacks[i];
    if (cb === fn || cb.fn === fn) {
      callbacks.splice(i, 1);
      break;
    }
  }
  return this;
};

/**
 * Emit `event` with the given args.
 *
 * @param {String} event
 * @param {Mixed} ...
 * @return {Emitter}
 */

Emitter.prototype.emit = function(event){
  this._callbacks = this._callbacks || {};
  var args = [].slice.call(arguments, 1)
    , callbacks = this._callbacks['$' + event];

  if (callbacks) {
    callbacks = callbacks.slice(0);
    for (var i = 0, len = callbacks.length; i < len; ++i) {
      callbacks[i].apply(this, args);
    }
  }

  return this;
};

/**
 * Return array of callbacks for `event`.
 *
 * @param {String} event
 * @return {Array}
 * @api public
 */

Emitter.prototype.listeners = function(event){
  this._callbacks = this._callbacks || {};
  return this._callbacks['$' + event] || [];
};

/**
 * Check if this emitter has `event` handlers.
 *
 * @param {String} event
 * @return {Boolean}
 * @api public
 */

Emitter.prototype.hasListeners = function(event){
  return !! this.listeners(event).length;
};


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {/**
 * Module dependencies.
 */

var keys = __webpack_require__(55);
var hasBinary = __webpack_require__(37);
var sliceBuffer = __webpack_require__(61);
var after = __webpack_require__(62);
var utf8 = __webpack_require__(63);

var base64encoder;
if (global && global.ArrayBuffer) {
  base64encoder = __webpack_require__(64);
}

/**
 * Check if we are running an android browser. That requires us to use
 * ArrayBuffer with polling transports...
 *
 * http://ghinda.net/jpeg-blob-ajax-android/
 */

var isAndroid = typeof navigator !== 'undefined' && /Android/i.test(navigator.userAgent);

/**
 * Check if we are running in PhantomJS.
 * Uploading a Blob with PhantomJS does not work correctly, as reported here:
 * https://github.com/ariya/phantomjs/issues/11395
 * @type boolean
 */
var isPhantomJS = typeof navigator !== 'undefined' && /PhantomJS/i.test(navigator.userAgent);

/**
 * When true, avoids using Blobs to encode payloads.
 * @type boolean
 */
var dontSendBlobs = isAndroid || isPhantomJS;

/**
 * Current protocol version.
 */

exports.protocol = 3;

/**
 * Packet types.
 */

var packets = exports.packets = {
    open:     0    // non-ws
  , close:    1    // non-ws
  , ping:     2
  , pong:     3
  , message:  4
  , upgrade:  5
  , noop:     6
};

var packetslist = keys(packets);

/**
 * Premade error packet.
 */

var err = { type: 'error', data: 'parser error' };

/**
 * Create a blob api even for blob builder when vendor prefixes exist
 */

var Blob = __webpack_require__(65);

/**
 * Encodes a packet.
 *
 *     <packet type id> [ <data> ]
 *
 * Example:
 *
 *     5hello world
 *     3
 *     4
 *
 * Binary is encoded in an identical principle
 *
 * @api private
 */

exports.encodePacket = function (packet, supportsBinary, utf8encode, callback) {
  if (typeof supportsBinary === 'function') {
    callback = supportsBinary;
    supportsBinary = false;
  }

  if (typeof utf8encode === 'function') {
    callback = utf8encode;
    utf8encode = null;
  }

  var data = (packet.data === undefined)
    ? undefined
    : packet.data.buffer || packet.data;

  if (global.ArrayBuffer && data instanceof ArrayBuffer) {
    return encodeArrayBuffer(packet, supportsBinary, callback);
  } else if (Blob && data instanceof global.Blob) {
    return encodeBlob(packet, supportsBinary, callback);
  }

  // might be an object with { base64: true, data: dataAsBase64String }
  if (data && data.base64) {
    return encodeBase64Object(packet, callback);
  }

  // Sending data as a utf-8 string
  var encoded = packets[packet.type];

  // data fragment is optional
  if (undefined !== packet.data) {
    encoded += utf8encode ? utf8.encode(String(packet.data), { strict: false }) : String(packet.data);
  }

  return callback('' + encoded);

};

function encodeBase64Object(packet, callback) {
  // packet data is an object { base64: true, data: dataAsBase64String }
  var message = 'b' + exports.packets[packet.type] + packet.data.data;
  return callback(message);
}

/**
 * Encode packet helpers for binary types
 */

function encodeArrayBuffer(packet, supportsBinary, callback) {
  if (!supportsBinary) {
    return exports.encodeBase64Packet(packet, callback);
  }

  var data = packet.data;
  var contentArray = new Uint8Array(data);
  var resultBuffer = new Uint8Array(1 + data.byteLength);

  resultBuffer[0] = packets[packet.type];
  for (var i = 0; i < contentArray.length; i++) {
    resultBuffer[i+1] = contentArray[i];
  }

  return callback(resultBuffer.buffer);
}

function encodeBlobAsArrayBuffer(packet, supportsBinary, callback) {
  if (!supportsBinary) {
    return exports.encodeBase64Packet(packet, callback);
  }

  var fr = new FileReader();
  fr.onload = function() {
    packet.data = fr.result;
    exports.encodePacket(packet, supportsBinary, true, callback);
  };
  return fr.readAsArrayBuffer(packet.data);
}

function encodeBlob(packet, supportsBinary, callback) {
  if (!supportsBinary) {
    return exports.encodeBase64Packet(packet, callback);
  }

  if (dontSendBlobs) {
    return encodeBlobAsArrayBuffer(packet, supportsBinary, callback);
  }

  var length = new Uint8Array(1);
  length[0] = packets[packet.type];
  var blob = new Blob([length.buffer, packet.data]);

  return callback(blob);
}

/**
 * Encodes a packet with binary data in a base64 string
 *
 * @param {Object} packet, has `type` and `data`
 * @return {String} base64 encoded message
 */

exports.encodeBase64Packet = function(packet, callback) {
  var message = 'b' + exports.packets[packet.type];
  if (Blob && packet.data instanceof global.Blob) {
    var fr = new FileReader();
    fr.onload = function() {
      var b64 = fr.result.split(',')[1];
      callback(message + b64);
    };
    return fr.readAsDataURL(packet.data);
  }

  var b64data;
  try {
    b64data = String.fromCharCode.apply(null, new Uint8Array(packet.data));
  } catch (e) {
    // iPhone Safari doesn't let you apply with typed arrays
    var typed = new Uint8Array(packet.data);
    var basic = new Array(typed.length);
    for (var i = 0; i < typed.length; i++) {
      basic[i] = typed[i];
    }
    b64data = String.fromCharCode.apply(null, basic);
  }
  message += global.btoa(b64data);
  return callback(message);
};

/**
 * Decodes a packet. Changes format to Blob if requested.
 *
 * @return {Object} with `type` and `data` (if any)
 * @api private
 */

exports.decodePacket = function (data, binaryType, utf8decode) {
  if (data === undefined) {
    return err;
  }
  // String data
  if (typeof data === 'string') {
    if (data.charAt(0) === 'b') {
      return exports.decodeBase64Packet(data.substr(1), binaryType);
    }

    if (utf8decode) {
      data = tryDecode(data);
      if (data === false) {
        return err;
      }
    }
    var type = data.charAt(0);

    if (Number(type) != type || !packetslist[type]) {
      return err;
    }

    if (data.length > 1) {
      return { type: packetslist[type], data: data.substring(1) };
    } else {
      return { type: packetslist[type] };
    }
  }

  var asArray = new Uint8Array(data);
  var type = asArray[0];
  var rest = sliceBuffer(data, 1);
  if (Blob && binaryType === 'blob') {
    rest = new Blob([rest]);
  }
  return { type: packetslist[type], data: rest };
};

function tryDecode(data) {
  try {
    data = utf8.decode(data, { strict: false });
  } catch (e) {
    return false;
  }
  return data;
}

/**
 * Decodes a packet encoded in a base64 string
 *
 * @param {String} base64 encoded message
 * @return {Object} with `type` and `data` (if any)
 */

exports.decodeBase64Packet = function(msg, binaryType) {
  var type = packetslist[msg.charAt(0)];
  if (!base64encoder) {
    return { type: type, data: { base64: true, data: msg.substr(1) } };
  }

  var data = base64encoder.decode(msg.substr(1));

  if (binaryType === 'blob' && Blob) {
    data = new Blob([data]);
  }

  return { type: type, data: data };
};

/**
 * Encodes multiple messages (payload).
 *
 *     <length>:data
 *
 * Example:
 *
 *     11:hello world2:hi
 *
 * If any contents are binary, they will be encoded as base64 strings. Base64
 * encoded strings are marked with a b before the length specifier
 *
 * @param {Array} packets
 * @api private
 */

exports.encodePayload = function (packets, supportsBinary, callback) {
  if (typeof supportsBinary === 'function') {
    callback = supportsBinary;
    supportsBinary = null;
  }

  var isBinary = hasBinary(packets);

  if (supportsBinary && isBinary) {
    if (Blob && !dontSendBlobs) {
      return exports.encodePayloadAsBlob(packets, callback);
    }

    return exports.encodePayloadAsArrayBuffer(packets, callback);
  }

  if (!packets.length) {
    return callback('0:');
  }

  function setLengthHeader(message) {
    return message.length + ':' + message;
  }

  function encodeOne(packet, doneCallback) {
    exports.encodePacket(packet, !isBinary ? false : supportsBinary, false, function(message) {
      doneCallback(null, setLengthHeader(message));
    });
  }

  map(packets, encodeOne, function(err, results) {
    return callback(results.join(''));
  });
};

/**
 * Async array map using after
 */

function map(ary, each, done) {
  var result = new Array(ary.length);
  var next = after(ary.length, done);

  var eachWithIndex = function(i, el, cb) {
    each(el, function(error, msg) {
      result[i] = msg;
      cb(error, result);
    });
  };

  for (var i = 0; i < ary.length; i++) {
    eachWithIndex(i, ary[i], next);
  }
}

/*
 * Decodes data when a payload is maybe expected. Possible binary contents are
 * decoded from their base64 representation
 *
 * @param {String} data, callback method
 * @api public
 */

exports.decodePayload = function (data, binaryType, callback) {
  if (typeof data !== 'string') {
    return exports.decodePayloadAsBinary(data, binaryType, callback);
  }

  if (typeof binaryType === 'function') {
    callback = binaryType;
    binaryType = null;
  }

  var packet;
  if (data === '') {
    // parser error - ignoring payload
    return callback(err, 0, 1);
  }

  var length = '', n, msg;

  for (var i = 0, l = data.length; i < l; i++) {
    var chr = data.charAt(i);

    if (chr !== ':') {
      length += chr;
      continue;
    }

    if (length === '' || (length != (n = Number(length)))) {
      // parser error - ignoring payload
      return callback(err, 0, 1);
    }

    msg = data.substr(i + 1, n);

    if (length != msg.length) {
      // parser error - ignoring payload
      return callback(err, 0, 1);
    }

    if (msg.length) {
      packet = exports.decodePacket(msg, binaryType, false);

      if (err.type === packet.type && err.data === packet.data) {
        // parser error in individual packet - ignoring payload
        return callback(err, 0, 1);
      }

      var ret = callback(packet, i + n, l);
      if (false === ret) return;
    }

    // advance cursor
    i += n;
    length = '';
  }

  if (length !== '') {
    // parser error - ignoring payload
    return callback(err, 0, 1);
  }

};

/**
 * Encodes multiple messages (payload) as binary.
 *
 * <1 = binary, 0 = string><number from 0-9><number from 0-9>[...]<number
 * 255><data>
 *
 * Example:
 * 1 3 255 1 2 3, if the binary contents are interpreted as 8 bit integers
 *
 * @param {Array} packets
 * @return {ArrayBuffer} encoded payload
 * @api private
 */

exports.encodePayloadAsArrayBuffer = function(packets, callback) {
  if (!packets.length) {
    return callback(new ArrayBuffer(0));
  }

  function encodeOne(packet, doneCallback) {
    exports.encodePacket(packet, true, true, function(data) {
      return doneCallback(null, data);
    });
  }

  map(packets, encodeOne, function(err, encodedPackets) {
    var totalLength = encodedPackets.reduce(function(acc, p) {
      var len;
      if (typeof p === 'string'){
        len = p.length;
      } else {
        len = p.byteLength;
      }
      return acc + len.toString().length + len + 2; // string/binary identifier + separator = 2
    }, 0);

    var resultArray = new Uint8Array(totalLength);

    var bufferIndex = 0;
    encodedPackets.forEach(function(p) {
      var isString = typeof p === 'string';
      var ab = p;
      if (isString) {
        var view = new Uint8Array(p.length);
        for (var i = 0; i < p.length; i++) {
          view[i] = p.charCodeAt(i);
        }
        ab = view.buffer;
      }

      if (isString) { // not true binary
        resultArray[bufferIndex++] = 0;
      } else { // true binary
        resultArray[bufferIndex++] = 1;
      }

      var lenStr = ab.byteLength.toString();
      for (var i = 0; i < lenStr.length; i++) {
        resultArray[bufferIndex++] = parseInt(lenStr[i]);
      }
      resultArray[bufferIndex++] = 255;

      var view = new Uint8Array(ab);
      for (var i = 0; i < view.length; i++) {
        resultArray[bufferIndex++] = view[i];
      }
    });

    return callback(resultArray.buffer);
  });
};

/**
 * Encode as Blob
 */

exports.encodePayloadAsBlob = function(packets, callback) {
  function encodeOne(packet, doneCallback) {
    exports.encodePacket(packet, true, true, function(encoded) {
      var binaryIdentifier = new Uint8Array(1);
      binaryIdentifier[0] = 1;
      if (typeof encoded === 'string') {
        var view = new Uint8Array(encoded.length);
        for (var i = 0; i < encoded.length; i++) {
          view[i] = encoded.charCodeAt(i);
        }
        encoded = view.buffer;
        binaryIdentifier[0] = 0;
      }

      var len = (encoded instanceof ArrayBuffer)
        ? encoded.byteLength
        : encoded.size;

      var lenStr = len.toString();
      var lengthAry = new Uint8Array(lenStr.length + 1);
      for (var i = 0; i < lenStr.length; i++) {
        lengthAry[i] = parseInt(lenStr[i]);
      }
      lengthAry[lenStr.length] = 255;

      if (Blob) {
        var blob = new Blob([binaryIdentifier.buffer, lengthAry.buffer, encoded]);
        doneCallback(null, blob);
      }
    });
  }

  map(packets, encodeOne, function(err, results) {
    return callback(new Blob(results));
  });
};

/*
 * Decodes data when a payload is maybe expected. Strings are decoded by
 * interpreting each byte as a key code for entries marked to start with 0. See
 * description of encodePayloadAsBinary
 *
 * @param {ArrayBuffer} data, callback method
 * @api public
 */

exports.decodePayloadAsBinary = function (data, binaryType, callback) {
  if (typeof binaryType === 'function') {
    callback = binaryType;
    binaryType = null;
  }

  var bufferTail = data;
  var buffers = [];

  while (bufferTail.byteLength > 0) {
    var tailArray = new Uint8Array(bufferTail);
    var isString = tailArray[0] === 0;
    var msgLength = '';

    for (var i = 1; ; i++) {
      if (tailArray[i] === 255) break;

      // 310 = char length of Number.MAX_VALUE
      if (msgLength.length > 310) {
        return callback(err, 0, 1);
      }

      msgLength += tailArray[i];
    }

    bufferTail = sliceBuffer(bufferTail, 2 + msgLength.length);
    msgLength = parseInt(msgLength);

    var msg = sliceBuffer(bufferTail, 0, msgLength);
    if (isString) {
      try {
        msg = String.fromCharCode.apply(null, new Uint8Array(msg));
      } catch (e) {
        // iPhone Safari doesn't let you apply to typed arrays
        var typed = new Uint8Array(msg);
        msg = '';
        for (var i = 0; i < typed.length; i++) {
          msg += String.fromCharCode(typed[i]);
        }
      }
    }

    buffers.push(msg);
    bufferTail = sliceBuffer(bufferTail, msgLength);
  }

  var total = buffers.length;
  buffers.forEach(function(buffer, i) {
    callback(exports.decodePacket(buffer, binaryType, true), i, total);
  });
};

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(0)))

/***/ }),
/* 7 */,
/* 8 */
/***/ (function(module, exports) {

/**
 * Compiles a querystring
 * Returns string representation of the object
 *
 * @param {Object}
 * @api private
 */

exports.encode = function (obj) {
  var str = '';

  for (var i in obj) {
    if (obj.hasOwnProperty(i)) {
      if (str.length) str += '&';
      str += encodeURIComponent(i) + '=' + encodeURIComponent(obj[i]);
    }
  }

  return str;
};

/**
 * Parses a simple querystring into an object
 *
 * @param {String} qs
 * @api private
 */

exports.decode = function(qs){
  var qry = {};
  var pairs = qs.split('&');
  for (var i = 0, l = pairs.length; i < l; i++) {
    var pair = pairs[i].split('=');
    qry[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
  }
  return qry;
};


/***/ }),
/* 9 */
/***/ (function(module, exports) {


module.exports = function(a, b){
  var fn = function(){};
  fn.prototype = b.prototype;
  a.prototype = new fn;
  a.prototype.constructor = a;
};

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {


/**
 * Module dependencies.
 */

var debug = __webpack_require__(4)('socket.io-parser');
var Emitter = __webpack_require__(5);
var binary = __webpack_require__(50);
var isArray = __webpack_require__(32);
var isBuf = __webpack_require__(33);

/**
 * Protocol version.
 *
 * @api public
 */

exports.protocol = 4;

/**
 * Packet types.
 *
 * @api public
 */

exports.types = [
  'CONNECT',
  'DISCONNECT',
  'EVENT',
  'ACK',
  'ERROR',
  'BINARY_EVENT',
  'BINARY_ACK'
];

/**
 * Packet type `connect`.
 *
 * @api public
 */

exports.CONNECT = 0;

/**
 * Packet type `disconnect`.
 *
 * @api public
 */

exports.DISCONNECT = 1;

/**
 * Packet type `event`.
 *
 * @api public
 */

exports.EVENT = 2;

/**
 * Packet type `ack`.
 *
 * @api public
 */

exports.ACK = 3;

/**
 * Packet type `error`.
 *
 * @api public
 */

exports.ERROR = 4;

/**
 * Packet type 'binary event'
 *
 * @api public
 */

exports.BINARY_EVENT = 5;

/**
 * Packet type `binary ack`. For acks with binary arguments.
 *
 * @api public
 */

exports.BINARY_ACK = 6;

/**
 * Encoder constructor.
 *
 * @api public
 */

exports.Encoder = Encoder;

/**
 * Decoder constructor.
 *
 * @api public
 */

exports.Decoder = Decoder;

/**
 * A socket.io Encoder instance
 *
 * @api public
 */

function Encoder() {}

var ERROR_PACKET = exports.ERROR + '"encode error"';

/**
 * Encode a packet as a single string if non-binary, or as a
 * buffer sequence, depending on packet type.
 *
 * @param {Object} obj - packet object
 * @param {Function} callback - function to handle encodings (likely engine.write)
 * @return Calls callback with Array of encodings
 * @api public
 */

Encoder.prototype.encode = function(obj, callback){
  debug('encoding packet %j', obj);

  if (exports.BINARY_EVENT === obj.type || exports.BINARY_ACK === obj.type) {
    encodeAsBinary(obj, callback);
  } else {
    var encoding = encodeAsString(obj);
    callback([encoding]);
  }
};

/**
 * Encode packet as string.
 *
 * @param {Object} packet
 * @return {String} encoded
 * @api private
 */

function encodeAsString(obj) {

  // first is type
  var str = '' + obj.type;

  // attachments if we have them
  if (exports.BINARY_EVENT === obj.type || exports.BINARY_ACK === obj.type) {
    str += obj.attachments + '-';
  }

  // if we have a namespace other than `/`
  // we append it followed by a comma `,`
  if (obj.nsp && '/' !== obj.nsp) {
    str += obj.nsp + ',';
  }

  // immediately followed by the id
  if (null != obj.id) {
    str += obj.id;
  }

  // json data
  if (null != obj.data) {
    var payload = tryStringify(obj.data);
    if (payload !== false) {
      str += payload;
    } else {
      return ERROR_PACKET;
    }
  }

  debug('encoded %j as %s', obj, str);
  return str;
}

function tryStringify(str) {
  try {
    return JSON.stringify(str);
  } catch(e){
    return false;
  }
}

/**
 * Encode packet as 'buffer sequence' by removing blobs, and
 * deconstructing packet into object with placeholders and
 * a list of buffers.
 *
 * @param {Object} packet
 * @return {Buffer} encoded
 * @api private
 */

function encodeAsBinary(obj, callback) {

  function writeEncoding(bloblessData) {
    var deconstruction = binary.deconstructPacket(bloblessData);
    var pack = encodeAsString(deconstruction.packet);
    var buffers = deconstruction.buffers;

    buffers.unshift(pack); // add packet info to beginning of data list
    callback(buffers); // write all the buffers
  }

  binary.removeBlobs(obj, writeEncoding);
}

/**
 * A socket.io Decoder instance
 *
 * @return {Object} decoder
 * @api public
 */

function Decoder() {
  this.reconstructor = null;
}

/**
 * Mix in `Emitter` with Decoder.
 */

Emitter(Decoder.prototype);

/**
 * Decodes an ecoded packet string into packet JSON.
 *
 * @param {String} obj - encoded packet
 * @return {Object} packet
 * @api public
 */

Decoder.prototype.add = function(obj) {
  var packet;
  if (typeof obj === 'string') {
    packet = decodeString(obj);
    if (exports.BINARY_EVENT === packet.type || exports.BINARY_ACK === packet.type) { // binary packet's json
      this.reconstructor = new BinaryReconstructor(packet);

      // no attachments, labeled binary but no binary data to follow
      if (this.reconstructor.reconPack.attachments === 0) {
        this.emit('decoded', packet);
      }
    } else { // non-binary full packet
      this.emit('decoded', packet);
    }
  }
  else if (isBuf(obj) || obj.base64) { // raw binary data
    if (!this.reconstructor) {
      throw new Error('got binary data when not reconstructing a packet');
    } else {
      packet = this.reconstructor.takeBinaryData(obj);
      if (packet) { // received final buffer
        this.reconstructor = null;
        this.emit('decoded', packet);
      }
    }
  }
  else {
    throw new Error('Unknown type: ' + obj);
  }
};

/**
 * Decode a packet String (JSON data)
 *
 * @param {String} str
 * @return {Object} packet
 * @api private
 */

function decodeString(str) {
  var i = 0;
  // look up type
  var p = {
    type: Number(str.charAt(0))
  };

  if (null == exports.types[p.type]) {
    return error('unknown packet type ' + p.type);
  }

  // look up attachments if type binary
  if (exports.BINARY_EVENT === p.type || exports.BINARY_ACK === p.type) {
    var buf = '';
    while (str.charAt(++i) !== '-') {
      buf += str.charAt(i);
      if (i == str.length) break;
    }
    if (buf != Number(buf) || str.charAt(i) !== '-') {
      throw new Error('Illegal attachments');
    }
    p.attachments = Number(buf);
  }

  // look up namespace (if any)
  if ('/' === str.charAt(i + 1)) {
    p.nsp = '';
    while (++i) {
      var c = str.charAt(i);
      if (',' === c) break;
      p.nsp += c;
      if (i === str.length) break;
    }
  } else {
    p.nsp = '/';
  }

  // look up id
  var next = str.charAt(i + 1);
  if ('' !== next && Number(next) == next) {
    p.id = '';
    while (++i) {
      var c = str.charAt(i);
      if (null == c || Number(c) != c) {
        --i;
        break;
      }
      p.id += str.charAt(i);
      if (i === str.length) break;
    }
    p.id = Number(p.id);
  }

  // look up json data
  if (str.charAt(++i)) {
    var payload = tryParse(str.substr(i));
    var isPayloadValid = payload !== false && (p.type === exports.ERROR || isArray(payload));
    if (isPayloadValid) {
      p.data = payload;
    } else {
      return error('invalid payload');
    }
  }

  debug('decoded %s as %j', str, p);
  return p;
}

function tryParse(str) {
  try {
    return JSON.parse(str);
  } catch(e){
    return false;
  }
}

/**
 * Deallocates a parser's resources
 *
 * @api public
 */

Decoder.prototype.destroy = function() {
  if (this.reconstructor) {
    this.reconstructor.finishedReconstruction();
  }
};

/**
 * A manager of a binary event's 'buffer sequence'. Should
 * be constructed whenever a packet of type BINARY_EVENT is
 * decoded.
 *
 * @param {Object} packet
 * @return {BinaryReconstructor} initialized reconstructor
 * @api private
 */

function BinaryReconstructor(packet) {
  this.reconPack = packet;
  this.buffers = [];
}

/**
 * Method to be called when binary data received from connection
 * after a BINARY_EVENT packet.
 *
 * @param {Buffer | ArrayBuffer} binData - the raw binary data received
 * @return {null | Object} returns null if more binary data is expected or
 *   a reconstructed packet object if all buffers have been received.
 * @api private
 */

BinaryReconstructor.prototype.takeBinaryData = function(binData) {
  this.buffers.push(binData);
  if (this.buffers.length === this.reconPack.attachments) { // done with buffer list
    var packet = binary.reconstructPacket(this.reconPack, this.buffers);
    this.finishedReconstruction();
    return packet;
  }
  return null;
};

/**
 * Cleans up binary packet reconstruction variables.
 *
 * @api private
 */

BinaryReconstructor.prototype.finishedReconstruction = function() {
  this.reconPack = null;
  this.buffers = [];
};

function error(msg) {
  return {
    type: exports.ERROR,
    data: 'parser error: ' + msg
  };
}


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {// browser shim for xmlhttprequest module

var hasCORS = __webpack_require__(53);

module.exports = function (opts) {
  var xdomain = opts.xdomain;

  // scheme must be same when usign XDomainRequest
  // http://blogs.msdn.com/b/ieinternals/archive/2010/05/13/xdomainrequest-restrictions-limitations-and-workarounds.aspx
  var xscheme = opts.xscheme;

  // XDomainRequest has a flow of not sending cookie, therefore it should be disabled as a default.
  // https://github.com/Automattic/engine.io-client/pull/217
  var enablesXDR = opts.enablesXDR;

  // XMLHttpRequest can be disabled on IE
  try {
    if ('undefined' !== typeof XMLHttpRequest && (!xdomain || hasCORS)) {
      return new XMLHttpRequest();
    }
  } catch (e) { }

  // Use XDomainRequest for IE8 if enablesXDR is true
  // because loading bar keeps flashing when using jsonp-polling
  // https://github.com/yujiosaka/socke.io-ie8-loading-example
  try {
    if ('undefined' !== typeof XDomainRequest && !xscheme && enablesXDR) {
      return new XDomainRequest();
    }
  } catch (e) { }

  if (!xdomain) {
    try {
      return new global[['Active'].concat('Object').join('X')]('Microsoft.XMLHTTP');
    } catch (e) { }
  }
};

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(0)))

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

/**
 * Module dependencies.
 */

var parser = __webpack_require__(6);
var Emitter = __webpack_require__(5);

/**
 * Module exports.
 */

module.exports = Transport;

/**
 * Transport abstract constructor.
 *
 * @param {Object} options.
 * @api private
 */

function Transport (opts) {
  this.path = opts.path;
  this.hostname = opts.hostname;
  this.port = opts.port;
  this.secure = opts.secure;
  this.query = opts.query;
  this.timestampParam = opts.timestampParam;
  this.timestampRequests = opts.timestampRequests;
  this.readyState = '';
  this.agent = opts.agent || false;
  this.socket = opts.socket;
  this.enablesXDR = opts.enablesXDR;

  // SSL options for Node.js client
  this.pfx = opts.pfx;
  this.key = opts.key;
  this.passphrase = opts.passphrase;
  this.cert = opts.cert;
  this.ca = opts.ca;
  this.ciphers = opts.ciphers;
  this.rejectUnauthorized = opts.rejectUnauthorized;
  this.forceNode = opts.forceNode;

  // other options for Node.js client
  this.extraHeaders = opts.extraHeaders;
  this.localAddress = opts.localAddress;
}

/**
 * Mix in `Emitter`.
 */

Emitter(Transport.prototype);

/**
 * Emits an error.
 *
 * @param {String} str
 * @return {Transport} for chaining
 * @api public
 */

Transport.prototype.onError = function (msg, desc) {
  var err = new Error(msg);
  err.type = 'TransportError';
  err.description = desc;
  this.emit('error', err);
  return this;
};

/**
 * Opens the transport.
 *
 * @api public
 */

Transport.prototype.open = function () {
  if ('closed' === this.readyState || '' === this.readyState) {
    this.readyState = 'opening';
    this.doOpen();
  }

  return this;
};

/**
 * Closes the transport.
 *
 * @api private
 */

Transport.prototype.close = function () {
  if ('opening' === this.readyState || 'open' === this.readyState) {
    this.doClose();
    this.onClose();
  }

  return this;
};

/**
 * Sends multiple packets.
 *
 * @param {Array} packets
 * @api private
 */

Transport.prototype.send = function (packets) {
  if ('open' === this.readyState) {
    this.write(packets);
  } else {
    throw new Error('Transport not open');
  }
};

/**
 * Called upon open
 *
 * @api private
 */

Transport.prototype.onOpen = function () {
  this.readyState = 'open';
  this.writable = true;
  this.emit('open');
};

/**
 * Called with data.
 *
 * @param {String} data
 * @api private
 */

Transport.prototype.onData = function (data) {
  var packet = parser.decodePacket(data, this.socket.binaryType);
  this.onPacket(packet);
};

/**
 * Called with a decoded packet.
 */

Transport.prototype.onPacket = function (packet) {
  this.emit('packet', packet);
};

/**
 * Called upon close.
 *
 * @api private
 */

Transport.prototype.onClose = function () {
  this.readyState = 'closed';
  this.emit('close');
};


/***/ }),
/* 13 */
/***/ (function(module, exports) {

// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(16)
}
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(19)
/* template */
var __vue_template__ = __webpack_require__(30)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-04fdc5a3"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/CardContainerComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-04fdc5a3", Component.options)
  } else {
    hotAPI.reload("data-v-04fdc5a3", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 15 */
/***/ (function(module, exports) {

module.exports = function(module) {
	if(!module.webpackPolyfill) {
		module.deprecate = function() {};
		module.paths = [];
		// module.parent = undefined by default
		if(!module.children) module.children = [];
		Object.defineProperty(module, "loaded", {
			enumerable: true,
			get: function() {
				return module.l;
			}
		});
		Object.defineProperty(module, "id", {
			enumerable: true,
			get: function() {
				return module.i;
			}
		});
		module.webpackPolyfill = 1;
	}
	return module;
};


/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(17);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("6103ce9f", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-04fdc5a3\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./CardContainerComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-04fdc5a3\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./CardContainerComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(1)(false);
// imports


// module
exports.push([module.i, "/* Area de mensajes */\n.card-container[data-v-04fdc5a3] {\n  text-align: center;\n  min-width: 100%;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: start;\n      -ms-flex-align: start;\n          align-items: flex-start;\n  overflow-x: auto;\n}\n", ""]);

// exports


/***/ }),
/* 18 */
/***/ (function(module, exports) {

/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
module.exports = function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}


/***/ }),
/* 19 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__OutputCardComponent__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__OutputCardComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__OutputCardComponent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__BladeVariableComponent__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__BladeVariableComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__BladeVariableComponent__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'card-container-component', /* que sea siempre compuesto con - par evitar colisiones con otros tag HTMHL5 */
    props: {
        cards: { type: Array, required: false },
        variables: { type: Array, required: false }
    },
    components: {
        OutputCardComponent: __WEBPACK_IMPORTED_MODULE_0__OutputCardComponent___default.a, BladeVariableComponent: __WEBPACK_IMPORTED_MODULE_1__BladeVariableComponent___default.a
    }
});

/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(21)
}
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(23)
/* template */
var __vue_template__ = __webpack_require__(24)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-109490e5"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/OutputCardComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-109490e5", Component.options)
  } else {
    hotAPI.reload("data-v-109490e5", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(22);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("7dfbebec", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-109490e5\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./OutputCardComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-109490e5\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./OutputCardComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(1)(false);
// imports


// module
exports.push([module.i, "\n.output-card[data-v-109490e5] {\n  background-color: #27322c;\n  min-width: 400px;\n  max-width: 600px;\n  margin: 15px;\n}\n.output-card .info[data-v-109490e5] {\n    font-size: 1em;\n    padding-top: 0.4em;\n    margin-bottom: 0.65em;\n}\n.output-card .type[data-v-109490e5] {\n    float: right;\n    color: #4f90a7;\n    padding-right: 0.6em;\n    font-weight: 300;\n}\n.output-card .number[data-v-109490e5] {\n    float: left;\n    color: #6f61aa;\n    padding-left: 0.4em;\n    font-weight: 700;\n}\n.output-card .data[data-v-109490e5] {\n    padding-left: 0.4em;\n    text-align: left;\n    clear: both;\n}\n.output-card .data p[data-v-109490e5] {\n      margin-top: 0.6em;\n      margin-bottom: 0.3em;\n}\n.output-card .data .status[data-v-109490e5] {\n      color: #18dd4a;\n      padding-left: 0.9em;\n}\n.output-card .data .error[data-v-109490e5] {\n      color: #df2914ee;\n}\n.output-card .output[data-v-109490e5] {\n    color: #9b9ba5;\n    padding-left: 0.9em;\n}\n.output-card .output *[data-v-109490e5] {\n      color: #9b9ba5;\n}\n.output-card .output table[data-v-109490e5] {\n      border-collapse: collapse;\n      width: 80%;\n}\n.output-card .output td[data-v-109490e5] {\n      text-align: left;\n}\n", ""]);

// exports


/***/ }),
/* 23 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'output-card-component', /* que sea siempre compuesto con - par evitar colisiones con otros tag HTMHL5 */
    props: {
        pcard: {
            type: Object,
            required: true
        }
    },
    /* si hubieran datos internos, que no es el caso
    data () {   // en un componente SIEMPRE tiene que ser una función 
        return { var1: 'hola' }
    }*/
    mounted: function mounted() {
        console.log('OutputCardComponent montado.');
    }
});

/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "output-card" }, [
    _c("div", { staticClass: "info number" }, [
      _vm._v(_vm._s(_vm.pcard.order))
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "info type" }, [
      _vm._v(" " + _vm._s(_vm.pcard.type))
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "data" }, [
      _c("p", [_vm._v("Estado")]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "status", class: { error: _vm.pcard.errorCode > 204 } },
        [
          _vm._v(
            "\n            " +
              _vm._s(_vm.pcard.errorCode) +
              " " +
              _vm._s(_vm.pcard.statusCode) +
              "\n        "
          )
        ]
      ),
      _vm._v(" "),
      _c("p", [_vm._v("Salida")]),
      _vm._v(" "),
      _c("div", { staticClass: "output" }, [
        _vm.pcard.output instanceof Object
          ? _c("div", { staticClass: "json" }, [
              _c("pre", [_vm._v(_vm._s(_vm.pcard.output))])
            ])
          : _c("div", { staticClass: "string" }, [
              _vm._v(_vm._s(_vm.pcard.output))
            ])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-109490e5", module.exports)
  }
}

/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(26)
}
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(28)
/* template */
var __vue_template__ = __webpack_require__(29)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-4289d320"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/BladeVariableComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4289d320", Component.options)
  } else {
    hotAPI.reload("data-v-4289d320", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(27);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("44aba37f", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4289d320\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./BladeVariableComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4289d320\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./BladeVariableComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(1)(false);
// imports


// module
exports.push([module.i, "\n.output-variable[data-v-4289d320] {\n  background-color: #27322c;\n  min-width: 650px;\n  font-size: 1rem;\n  margin: 0 15px;\n  padding: 0.7em 4px;\n}\n.output-variable .value[data-v-4289d320] {\n    color: #6f61aa;\n    font-weight: 700;\n}\n.output-variable table[data-v-4289d320] {\n    border-spacing: 0.25em 0px;\n    border-collapse: separate;\n}\n.output-variable td[data-v-4289d320] {\n    text-align: center;\n}\n", ""]);

// exports


/***/ }),
/* 28 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'blade-variable-component', /* que sea siempre compuesto con - para evitar colisiones con otros tag HTMHL5 */
    props: {
        pvar: {
            type: Object,
            required: true
        }
    },
    filters: {
        reducido: function reducido(val) {
            if (val.length < 10) return val;else return val.substring(0, 4) + "..." + val.substring(val.length - 3, val.length);
        }
    },
    mounted: function mounted() {
        console.log('BladeVariableComponent montado.');
    }
});

/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "output-variable" }, [
    _vm.pvar instanceof Object
      ? _c("div", { staticClass: "json" }, [
          _c(
            "table",
            _vm._l(_vm.pvar, function(value, key) {
              return _c("td", { key: key }, [
                _c("tr", { staticClass: "key" }, [
                  _vm._v(_vm._s(_vm._f("reducido")(key)))
                ]),
                _vm._v(" "),
                _c("tr", { staticClass: "value" }, [
                  _vm._v(_vm._s(value === null ? "-" : value))
                ])
              ])
            })
          )
        ])
      : _c("div", { staticClass: "string" }, [_vm._v(_vm._s(_vm.pvar))])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-4289d320", module.exports)
  }
}

/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "card-container" },
    [
      _vm._l(_vm.cards, function(card) {
        return _vm.cards
          ? _c("output-card-component", {
              key: card.order,
              attrs: { pcard: card }
            })
          : _vm._e()
      }),
      _vm._v(" "),
      _vm._l(_vm.variables, function(variable) {
        return _vm.variables
          ? _c("blade-variable-component", {
              key: variable.order,
              attrs: { pvar: variable }
            })
          : _vm._e()
      })
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-04fdc5a3", module.exports)
  }
}

/***/ }),
/* 31 */
/***/ (function(module, exports) {

/**
 * Parses an URI
 *
 * @author Steven Levithan <stevenlevithan.com> (MIT license)
 * @api private
 */

var re = /^(?:(?![^:@]+:[^:@\/]*@)(http|https|ws|wss):\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?((?:[a-f0-9]{0,4}:){2,7}[a-f0-9]{0,4}|[^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/;

var parts = [
    'source', 'protocol', 'authority', 'userInfo', 'user', 'password', 'host', 'port', 'relative', 'path', 'directory', 'file', 'query', 'anchor'
];

module.exports = function parseuri(str) {
    var src = str,
        b = str.indexOf('['),
        e = str.indexOf(']');

    if (b != -1 && e != -1) {
        str = str.substring(0, b) + str.substring(b, e).replace(/:/g, ';') + str.substring(e, str.length);
    }

    var m = re.exec(str || ''),
        uri = {},
        i = 14;

    while (i--) {
        uri[parts[i]] = m[i] || '';
    }

    if (b != -1 && e != -1) {
        uri.source = src;
        uri.host = uri.host.substring(1, uri.host.length - 1).replace(/;/g, ':');
        uri.authority = uri.authority.replace('[', '').replace(']', '').replace(/;/g, ':');
        uri.ipv6uri = true;
    }

    return uri;
};


/***/ }),
/* 32 */
/***/ (function(module, exports) {

var toString = {}.toString;

module.exports = Array.isArray || function (arr) {
  return toString.call(arr) == '[object Array]';
};


/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {
module.exports = isBuf;

var withNativeBuffer = typeof global.Buffer === 'function' && typeof global.Buffer.isBuffer === 'function';
var withNativeArrayBuffer = typeof global.ArrayBuffer === 'function';

var isView = (function () {
  if (withNativeArrayBuffer && typeof global.ArrayBuffer.isView === 'function') {
    return global.ArrayBuffer.isView;
  } else {
    return function (obj) { return obj.buffer instanceof global.ArrayBuffer; };
  }
})();

/**
 * Returns true if obj is a buffer or an arraybuffer.
 *
 * @api private
 */

function isBuf(obj) {
  return (withNativeBuffer && global.Buffer.isBuffer(obj)) ||
          (withNativeArrayBuffer && (obj instanceof global.ArrayBuffer || isView(obj)));
}

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(0)))

/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {


/**
 * Module dependencies.
 */

var eio = __webpack_require__(51);
var Socket = __webpack_require__(40);
var Emitter = __webpack_require__(5);
var parser = __webpack_require__(10);
var on = __webpack_require__(41);
var bind = __webpack_require__(42);
var debug = __webpack_require__(4)('socket.io-client:manager');
var indexOf = __webpack_require__(39);
var Backoff = __webpack_require__(70);

/**
 * IE6+ hasOwnProperty
 */

var has = Object.prototype.hasOwnProperty;

/**
 * Module exports
 */

module.exports = Manager;

/**
 * `Manager` constructor.
 *
 * @param {String} engine instance or engine uri/opts
 * @param {Object} options
 * @api public
 */

function Manager (uri, opts) {
  if (!(this instanceof Manager)) return new Manager(uri, opts);
  if (uri && ('object' === typeof uri)) {
    opts = uri;
    uri = undefined;
  }
  opts = opts || {};

  opts.path = opts.path || '/socket.io';
  this.nsps = {};
  this.subs = [];
  this.opts = opts;
  this.reconnection(opts.reconnection !== false);
  this.reconnectionAttempts(opts.reconnectionAttempts || Infinity);
  this.reconnectionDelay(opts.reconnectionDelay || 1000);
  this.reconnectionDelayMax(opts.reconnectionDelayMax || 5000);
  this.randomizationFactor(opts.randomizationFactor || 0.5);
  this.backoff = new Backoff({
    min: this.reconnectionDelay(),
    max: this.reconnectionDelayMax(),
    jitter: this.randomizationFactor()
  });
  this.timeout(null == opts.timeout ? 20000 : opts.timeout);
  this.readyState = 'closed';
  this.uri = uri;
  this.connecting = [];
  this.lastPing = null;
  this.encoding = false;
  this.packetBuffer = [];
  var _parser = opts.parser || parser;
  this.encoder = new _parser.Encoder();
  this.decoder = new _parser.Decoder();
  this.autoConnect = opts.autoConnect !== false;
  if (this.autoConnect) this.open();
}

/**
 * Propagate given event to sockets and emit on `this`
 *
 * @api private
 */

Manager.prototype.emitAll = function () {
  this.emit.apply(this, arguments);
  for (var nsp in this.nsps) {
    if (has.call(this.nsps, nsp)) {
      this.nsps[nsp].emit.apply(this.nsps[nsp], arguments);
    }
  }
};

/**
 * Update `socket.id` of all sockets
 *
 * @api private
 */

Manager.prototype.updateSocketIds = function () {
  for (var nsp in this.nsps) {
    if (has.call(this.nsps, nsp)) {
      this.nsps[nsp].id = this.generateId(nsp);
    }
  }
};

/**
 * generate `socket.id` for the given `nsp`
 *
 * @param {String} nsp
 * @return {String}
 * @api private
 */

Manager.prototype.generateId = function (nsp) {
  return (nsp === '/' ? '' : (nsp + '#')) + this.engine.id;
};

/**
 * Mix in `Emitter`.
 */

Emitter(Manager.prototype);

/**
 * Sets the `reconnection` config.
 *
 * @param {Boolean} true/false if it should automatically reconnect
 * @return {Manager} self or value
 * @api public
 */

Manager.prototype.reconnection = function (v) {
  if (!arguments.length) return this._reconnection;
  this._reconnection = !!v;
  return this;
};

/**
 * Sets the reconnection attempts config.
 *
 * @param {Number} max reconnection attempts before giving up
 * @return {Manager} self or value
 * @api public
 */

Manager.prototype.reconnectionAttempts = function (v) {
  if (!arguments.length) return this._reconnectionAttempts;
  this._reconnectionAttempts = v;
  return this;
};

/**
 * Sets the delay between reconnections.
 *
 * @param {Number} delay
 * @return {Manager} self or value
 * @api public
 */

Manager.prototype.reconnectionDelay = function (v) {
  if (!arguments.length) return this._reconnectionDelay;
  this._reconnectionDelay = v;
  this.backoff && this.backoff.setMin(v);
  return this;
};

Manager.prototype.randomizationFactor = function (v) {
  if (!arguments.length) return this._randomizationFactor;
  this._randomizationFactor = v;
  this.backoff && this.backoff.setJitter(v);
  return this;
};

/**
 * Sets the maximum delay between reconnections.
 *
 * @param {Number} delay
 * @return {Manager} self or value
 * @api public
 */

Manager.prototype.reconnectionDelayMax = function (v) {
  if (!arguments.length) return this._reconnectionDelayMax;
  this._reconnectionDelayMax = v;
  this.backoff && this.backoff.setMax(v);
  return this;
};

/**
 * Sets the connection timeout. `false` to disable
 *
 * @return {Manager} self or value
 * @api public
 */

Manager.prototype.timeout = function (v) {
  if (!arguments.length) return this._timeout;
  this._timeout = v;
  return this;
};

/**
 * Starts trying to reconnect if reconnection is enabled and we have not
 * started reconnecting yet
 *
 * @api private
 */

Manager.prototype.maybeReconnectOnOpen = function () {
  // Only try to reconnect if it's the first time we're connecting
  if (!this.reconnecting && this._reconnection && this.backoff.attempts === 0) {
    // keeps reconnection from firing twice for the same reconnection loop
    this.reconnect();
  }
};

/**
 * Sets the current transport `socket`.
 *
 * @param {Function} optional, callback
 * @return {Manager} self
 * @api public
 */

Manager.prototype.open =
Manager.prototype.connect = function (fn, opts) {
  debug('readyState %s', this.readyState);
  if (~this.readyState.indexOf('open')) return this;

  debug('opening %s', this.uri);
  this.engine = eio(this.uri, this.opts);
  var socket = this.engine;
  var self = this;
  this.readyState = 'opening';
  this.skipReconnect = false;

  // emit `open`
  var openSub = on(socket, 'open', function () {
    self.onopen();
    fn && fn();
  });

  // emit `connect_error`
  var errorSub = on(socket, 'error', function (data) {
    debug('connect_error');
    self.cleanup();
    self.readyState = 'closed';
    self.emitAll('connect_error', data);
    if (fn) {
      var err = new Error('Connection error');
      err.data = data;
      fn(err);
    } else {
      // Only do this if there is no fn to handle the error
      self.maybeReconnectOnOpen();
    }
  });

  // emit `connect_timeout`
  if (false !== this._timeout) {
    var timeout = this._timeout;
    debug('connect attempt will timeout after %d', timeout);

    // set timer
    var timer = setTimeout(function () {
      debug('connect attempt timed out after %d', timeout);
      openSub.destroy();
      socket.close();
      socket.emit('error', 'timeout');
      self.emitAll('connect_timeout', timeout);
    }, timeout);

    this.subs.push({
      destroy: function () {
        clearTimeout(timer);
      }
    });
  }

  this.subs.push(openSub);
  this.subs.push(errorSub);

  return this;
};

/**
 * Called upon transport open.
 *
 * @api private
 */

Manager.prototype.onopen = function () {
  debug('open');

  // clear old subs
  this.cleanup();

  // mark as open
  this.readyState = 'open';
  this.emit('open');

  // add new subs
  var socket = this.engine;
  this.subs.push(on(socket, 'data', bind(this, 'ondata')));
  this.subs.push(on(socket, 'ping', bind(this, 'onping')));
  this.subs.push(on(socket, 'pong', bind(this, 'onpong')));
  this.subs.push(on(socket, 'error', bind(this, 'onerror')));
  this.subs.push(on(socket, 'close', bind(this, 'onclose')));
  this.subs.push(on(this.decoder, 'decoded', bind(this, 'ondecoded')));
};

/**
 * Called upon a ping.
 *
 * @api private
 */

Manager.prototype.onping = function () {
  this.lastPing = new Date();
  this.emitAll('ping');
};

/**
 * Called upon a packet.
 *
 * @api private
 */

Manager.prototype.onpong = function () {
  this.emitAll('pong', new Date() - this.lastPing);
};

/**
 * Called with data.
 *
 * @api private
 */

Manager.prototype.ondata = function (data) {
  this.decoder.add(data);
};

/**
 * Called when parser fully decodes a packet.
 *
 * @api private
 */

Manager.prototype.ondecoded = function (packet) {
  this.emit('packet', packet);
};

/**
 * Called upon socket error.
 *
 * @api private
 */

Manager.prototype.onerror = function (err) {
  debug('error', err);
  this.emitAll('error', err);
};

/**
 * Creates a new socket for the given `nsp`.
 *
 * @return {Socket}
 * @api public
 */

Manager.prototype.socket = function (nsp, opts) {
  var socket = this.nsps[nsp];
  if (!socket) {
    socket = new Socket(this, nsp, opts);
    this.nsps[nsp] = socket;
    var self = this;
    socket.on('connecting', onConnecting);
    socket.on('connect', function () {
      socket.id = self.generateId(nsp);
    });

    if (this.autoConnect) {
      // manually call here since connecting event is fired before listening
      onConnecting();
    }
  }

  function onConnecting () {
    if (!~indexOf(self.connecting, socket)) {
      self.connecting.push(socket);
    }
  }

  return socket;
};

/**
 * Called upon a socket close.
 *
 * @param {Socket} socket
 */

Manager.prototype.destroy = function (socket) {
  var index = indexOf(this.connecting, socket);
  if (~index) this.connecting.splice(index, 1);
  if (this.connecting.length) return;

  this.close();
};

/**
 * Writes a packet.
 *
 * @param {Object} packet
 * @api private
 */

Manager.prototype.packet = function (packet) {
  debug('writing packet %j', packet);
  var self = this;
  if (packet.query && packet.type === 0) packet.nsp += '?' + packet.query;

  if (!self.encoding) {
    // encode, then write to engine with result
    self.encoding = true;
    this.encoder.encode(packet, function (encodedPackets) {
      for (var i = 0; i < encodedPackets.length; i++) {
        self.engine.write(encodedPackets[i], packet.options);
      }
      self.encoding = false;
      self.processPacketQueue();
    });
  } else { // add packet to the queue
    self.packetBuffer.push(packet);
  }
};

/**
 * If packet buffer is non-empty, begins encoding the
 * next packet in line.
 *
 * @api private
 */

Manager.prototype.processPacketQueue = function () {
  if (this.packetBuffer.length > 0 && !this.encoding) {
    var pack = this.packetBuffer.shift();
    this.packet(pack);
  }
};

/**
 * Clean up transport subscriptions and packet buffer.
 *
 * @api private
 */

Manager.prototype.cleanup = function () {
  debug('cleanup');

  var subsLength = this.subs.length;
  for (var i = 0; i < subsLength; i++) {
    var sub = this.subs.shift();
    sub.destroy();
  }

  this.packetBuffer = [];
  this.encoding = false;
  this.lastPing = null;

  this.decoder.destroy();
};

/**
 * Close the current socket.
 *
 * @api private
 */

Manager.prototype.close =
Manager.prototype.disconnect = function () {
  debug('disconnect');
  this.skipReconnect = true;
  this.reconnecting = false;
  if ('opening' === this.readyState) {
    // `onclose` will not fire because
    // an open event never happened
    this.cleanup();
  }
  this.backoff.reset();
  this.readyState = 'closed';
  if (this.engine) this.engine.close();
};

/**
 * Called upon engine close.
 *
 * @api private
 */

Manager.prototype.onclose = function (reason) {
  debug('onclose');

  this.cleanup();
  this.backoff.reset();
  this.readyState = 'closed';
  this.emit('close', reason);

  if (this._reconnection && !this.skipReconnect) {
    this.reconnect();
  }
};

/**
 * Attempt a reconnection.
 *
 * @api private
 */

Manager.prototype.reconnect = function () {
  if (this.reconnecting || this.skipReconnect) return this;

  var self = this;

  if (this.backoff.attempts >= this._reconnectionAttempts) {
    debug('reconnect failed');
    this.backoff.reset();
    this.emitAll('reconnect_failed');
    this.reconnecting = false;
  } else {
    var delay = this.backoff.duration();
    debug('will wait %dms before reconnect attempt', delay);

    this.reconnecting = true;
    var timer = setTimeout(function () {
      if (self.skipReconnect) return;

      debug('attempting reconnect');
      self.emitAll('reconnect_attempt', self.backoff.attempts);
      self.emitAll('reconnecting', self.backoff.attempts);

      // check again for the case socket closed in above events
      if (self.skipReconnect) return;

      self.open(function (err) {
        if (err) {
          debug('reconnect attempt error');
          self.reconnecting = false;
          self.reconnect();
          self.emitAll('reconnect_error', err.data);
        } else {
          debug('reconnect success');
          self.onreconnect();
        }
      });
    }, delay);

    this.subs.push({
      destroy: function () {
        clearTimeout(timer);
      }
    });
  }
};

/**
 * Called upon successful reconnect.
 *
 * @api private
 */

Manager.prototype.onreconnect = function () {
  var attempt = this.backoff.attempts;
  this.reconnecting = false;
  this.backoff.reset();
  this.updateSocketIds();
  this.emitAll('reconnect', attempt);
};


/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {/**
 * Module dependencies
 */

var XMLHttpRequest = __webpack_require__(11);
var XHR = __webpack_require__(54);
var JSONP = __webpack_require__(66);
var websocket = __webpack_require__(67);

/**
 * Export transports.
 */

exports.polling = polling;
exports.websocket = websocket;

/**
 * Polling transport polymorphic constructor.
 * Decides on xhr vs jsonp based on feature detection.
 *
 * @api private
 */

function polling (opts) {
  var xhr;
  var xd = false;
  var xs = false;
  var jsonp = false !== opts.jsonp;

  if (global.location) {
    var isSSL = 'https:' === location.protocol;
    var port = location.port;

    // some user agents have empty `location.port`
    if (!port) {
      port = isSSL ? 443 : 80;
    }

    xd = opts.hostname !== location.hostname || port !== opts.port;
    xs = opts.secure !== isSSL;
  }

  opts.xdomain = xd;
  opts.xscheme = xs;
  xhr = new XMLHttpRequest(opts);

  if ('open' in xhr && !opts.forceJSONP) {
    return new XHR(opts);
  } else {
    if (!jsonp) throw new Error('JSONP disabled');
    return new JSONP(opts);
  }
}

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(0)))

/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

/**
 * Module dependencies.
 */

var Transport = __webpack_require__(12);
var parseqs = __webpack_require__(8);
var parser = __webpack_require__(6);
var inherit = __webpack_require__(9);
var yeast = __webpack_require__(38);
var debug = __webpack_require__(4)('engine.io-client:polling');

/**
 * Module exports.
 */

module.exports = Polling;

/**
 * Is XHR2 supported?
 */

var hasXHR2 = (function () {
  var XMLHttpRequest = __webpack_require__(11);
  var xhr = new XMLHttpRequest({ xdomain: false });
  return null != xhr.responseType;
})();

/**
 * Polling interface.
 *
 * @param {Object} opts
 * @api private
 */

function Polling (opts) {
  var forceBase64 = (opts && opts.forceBase64);
  if (!hasXHR2 || forceBase64) {
    this.supportsBinary = false;
  }
  Transport.call(this, opts);
}

/**
 * Inherits from Transport.
 */

inherit(Polling, Transport);

/**
 * Transport name.
 */

Polling.prototype.name = 'polling';

/**
 * Opens the socket (triggers polling). We write a PING message to determine
 * when the transport is open.
 *
 * @api private
 */

Polling.prototype.doOpen = function () {
  this.poll();
};

/**
 * Pauses polling.
 *
 * @param {Function} callback upon buffers are flushed and transport is paused
 * @api private
 */

Polling.prototype.pause = function (onPause) {
  var self = this;

  this.readyState = 'pausing';

  function pause () {
    debug('paused');
    self.readyState = 'paused';
    onPause();
  }

  if (this.polling || !this.writable) {
    var total = 0;

    if (this.polling) {
      debug('we are currently polling - waiting to pause');
      total++;
      this.once('pollComplete', function () {
        debug('pre-pause polling complete');
        --total || pause();
      });
    }

    if (!this.writable) {
      debug('we are currently writing - waiting to pause');
      total++;
      this.once('drain', function () {
        debug('pre-pause writing complete');
        --total || pause();
      });
    }
  } else {
    pause();
  }
};

/**
 * Starts polling cycle.
 *
 * @api public
 */

Polling.prototype.poll = function () {
  debug('polling');
  this.polling = true;
  this.doPoll();
  this.emit('poll');
};

/**
 * Overloads onData to detect payloads.
 *
 * @api private
 */

Polling.prototype.onData = function (data) {
  var self = this;
  debug('polling got data %s', data);
  var callback = function (packet, index, total) {
    // if its the first message we consider the transport open
    if ('opening' === self.readyState) {
      self.onOpen();
    }

    // if its a close packet, we close the ongoing requests
    if ('close' === packet.type) {
      self.onClose();
      return false;
    }

    // otherwise bypass onData and handle the message
    self.onPacket(packet);
  };

  // decode payload
  parser.decodePayload(data, this.socket.binaryType, callback);

  // if an event did not trigger closing
  if ('closed' !== this.readyState) {
    // if we got data we're not polling
    this.polling = false;
    this.emit('pollComplete');

    if ('open' === this.readyState) {
      this.poll();
    } else {
      debug('ignoring poll - transport state "%s"', this.readyState);
    }
  }
};

/**
 * For polling, send a close packet.
 *
 * @api private
 */

Polling.prototype.doClose = function () {
  var self = this;

  function close () {
    debug('writing close packet');
    self.write([{ type: 'close' }]);
  }

  if ('open' === this.readyState) {
    debug('transport open - closing');
    close();
  } else {
    // in case we're trying to close while
    // handshaking is in progress (GH-164)
    debug('transport not open - deferring close');
    this.once('open', close);
  }
};

/**
 * Writes a packets payload.
 *
 * @param {Array} data packets
 * @param {Function} drain callback
 * @api private
 */

Polling.prototype.write = function (packets) {
  var self = this;
  this.writable = false;
  var callbackfn = function () {
    self.writable = true;
    self.emit('drain');
  };

  parser.encodePayload(packets, this.supportsBinary, function (data) {
    self.doWrite(data, callbackfn);
  });
};

/**
 * Generates uri for connection.
 *
 * @api private
 */

Polling.prototype.uri = function () {
  var query = this.query || {};
  var schema = this.secure ? 'https' : 'http';
  var port = '';

  // cache busting is forced
  if (false !== this.timestampRequests) {
    query[this.timestampParam] = yeast();
  }

  if (!this.supportsBinary && !query.sid) {
    query.b64 = 1;
  }

  query = parseqs.encode(query);

  // avoid port if default for schema
  if (this.port && (('https' === schema && Number(this.port) !== 443) ||
     ('http' === schema && Number(this.port) !== 80))) {
    port = ':' + this.port;
  }

  // prepend ? to query
  if (query.length) {
    query = '?' + query;
  }

  var ipv6 = this.hostname.indexOf(':') !== -1;
  return schema + '://' + (ipv6 ? '[' + this.hostname + ']' : this.hostname) + port + this.path + query;
};


/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(Buffer) {/* global Blob File */

/*
 * Module requirements.
 */

var isArray = __webpack_require__(60);

var toString = Object.prototype.toString;
var withNativeBlob = typeof Blob === 'function' ||
                        typeof Blob !== 'undefined' && toString.call(Blob) === '[object BlobConstructor]';
var withNativeFile = typeof File === 'function' ||
                        typeof File !== 'undefined' && toString.call(File) === '[object FileConstructor]';

/**
 * Module exports.
 */

module.exports = hasBinary;

/**
 * Checks for binary data.
 *
 * Supports Buffer, ArrayBuffer, Blob and File.
 *
 * @param {Object} anything
 * @api public
 */

function hasBinary (obj) {
  if (!obj || typeof obj !== 'object') {
    return false;
  }

  if (isArray(obj)) {
    for (var i = 0, l = obj.length; i < l; i++) {
      if (hasBinary(obj[i])) {
        return true;
      }
    }
    return false;
  }

  if ((typeof Buffer === 'function' && Buffer.isBuffer && Buffer.isBuffer(obj)) ||
    (typeof ArrayBuffer === 'function' && obj instanceof ArrayBuffer) ||
    (withNativeBlob && obj instanceof Blob) ||
    (withNativeFile && obj instanceof File)
  ) {
    return true;
  }

  // see: https://github.com/Automattic/has-binary/pull/4
  if (obj.toJSON && typeof obj.toJSON === 'function' && arguments.length === 1) {
    return hasBinary(obj.toJSON(), true);
  }

  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key) && hasBinary(obj[key])) {
      return true;
    }
  }

  return false;
}

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(56).Buffer))

/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_'.split('')
  , length = 64
  , map = {}
  , seed = 0
  , i = 0
  , prev;

/**
 * Return a string representing the specified number.
 *
 * @param {Number} num The number to convert.
 * @returns {String} The string representation of the number.
 * @api public
 */
function encode(num) {
  var encoded = '';

  do {
    encoded = alphabet[num % length] + encoded;
    num = Math.floor(num / length);
  } while (num > 0);

  return encoded;
}

/**
 * Return the integer value specified by the given string.
 *
 * @param {String} str The string to convert.
 * @returns {Number} The integer value represented by the string.
 * @api public
 */
function decode(str) {
  var decoded = 0;

  for (i = 0; i < str.length; i++) {
    decoded = decoded * length + map[str.charAt(i)];
  }

  return decoded;
}

/**
 * Yeast: A tiny growing id generator.
 *
 * @returns {String} A unique id.
 * @api public
 */
function yeast() {
  var now = encode(+new Date());

  if (now !== prev) return seed = 0, prev = now;
  return now +'.'+ encode(seed++);
}

//
// Map each character to its index.
//
for (; i < length; i++) map[alphabet[i]] = i;

//
// Expose the `yeast`, `encode` and `decode` functions.
//
yeast.encode = encode;
yeast.decode = decode;
module.exports = yeast;


/***/ }),
/* 39 */
/***/ (function(module, exports) {


var indexOf = [].indexOf;

module.exports = function(arr, obj){
  if (indexOf) return arr.indexOf(obj);
  for (var i = 0; i < arr.length; ++i) {
    if (arr[i] === obj) return i;
  }
  return -1;
};

/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {


/**
 * Module dependencies.
 */

var parser = __webpack_require__(10);
var Emitter = __webpack_require__(5);
var toArray = __webpack_require__(69);
var on = __webpack_require__(41);
var bind = __webpack_require__(42);
var debug = __webpack_require__(4)('socket.io-client:socket');
var parseqs = __webpack_require__(8);
var hasBin = __webpack_require__(37);

/**
 * Module exports.
 */

module.exports = exports = Socket;

/**
 * Internal events (blacklisted).
 * These events can't be emitted by the user.
 *
 * @api private
 */

var events = {
  connect: 1,
  connect_error: 1,
  connect_timeout: 1,
  connecting: 1,
  disconnect: 1,
  error: 1,
  reconnect: 1,
  reconnect_attempt: 1,
  reconnect_failed: 1,
  reconnect_error: 1,
  reconnecting: 1,
  ping: 1,
  pong: 1
};

/**
 * Shortcut to `Emitter#emit`.
 */

var emit = Emitter.prototype.emit;

/**
 * `Socket` constructor.
 *
 * @api public
 */

function Socket (io, nsp, opts) {
  this.io = io;
  this.nsp = nsp;
  this.json = this; // compat
  this.ids = 0;
  this.acks = {};
  this.receiveBuffer = [];
  this.sendBuffer = [];
  this.connected = false;
  this.disconnected = true;
  this.flags = {};
  if (opts && opts.query) {
    this.query = opts.query;
  }
  if (this.io.autoConnect) this.open();
}

/**
 * Mix in `Emitter`.
 */

Emitter(Socket.prototype);

/**
 * Subscribe to open, close and packet events
 *
 * @api private
 */

Socket.prototype.subEvents = function () {
  if (this.subs) return;

  var io = this.io;
  this.subs = [
    on(io, 'open', bind(this, 'onopen')),
    on(io, 'packet', bind(this, 'onpacket')),
    on(io, 'close', bind(this, 'onclose'))
  ];
};

/**
 * "Opens" the socket.
 *
 * @api public
 */

Socket.prototype.open =
Socket.prototype.connect = function () {
  if (this.connected) return this;

  this.subEvents();
  this.io.open(); // ensure open
  if ('open' === this.io.readyState) this.onopen();
  this.emit('connecting');
  return this;
};

/**
 * Sends a `message` event.
 *
 * @return {Socket} self
 * @api public
 */

Socket.prototype.send = function () {
  var args = toArray(arguments);
  args.unshift('message');
  this.emit.apply(this, args);
  return this;
};

/**
 * Override `emit`.
 * If the event is in `events`, it's emitted normally.
 *
 * @param {String} event name
 * @return {Socket} self
 * @api public
 */

Socket.prototype.emit = function (ev) {
  if (events.hasOwnProperty(ev)) {
    emit.apply(this, arguments);
    return this;
  }

  var args = toArray(arguments);
  var packet = {
    type: (this.flags.binary !== undefined ? this.flags.binary : hasBin(args)) ? parser.BINARY_EVENT : parser.EVENT,
    data: args
  };

  packet.options = {};
  packet.options.compress = !this.flags || false !== this.flags.compress;

  // event ack callback
  if ('function' === typeof args[args.length - 1]) {
    debug('emitting packet with ack id %d', this.ids);
    this.acks[this.ids] = args.pop();
    packet.id = this.ids++;
  }

  if (this.connected) {
    this.packet(packet);
  } else {
    this.sendBuffer.push(packet);
  }

  this.flags = {};

  return this;
};

/**
 * Sends a packet.
 *
 * @param {Object} packet
 * @api private
 */

Socket.prototype.packet = function (packet) {
  packet.nsp = this.nsp;
  this.io.packet(packet);
};

/**
 * Called upon engine `open`.
 *
 * @api private
 */

Socket.prototype.onopen = function () {
  debug('transport is open - connecting');

  // write connect packet if necessary
  if ('/' !== this.nsp) {
    if (this.query) {
      var query = typeof this.query === 'object' ? parseqs.encode(this.query) : this.query;
      debug('sending connect packet with query %s', query);
      this.packet({type: parser.CONNECT, query: query});
    } else {
      this.packet({type: parser.CONNECT});
    }
  }
};

/**
 * Called upon engine `close`.
 *
 * @param {String} reason
 * @api private
 */

Socket.prototype.onclose = function (reason) {
  debug('close (%s)', reason);
  this.connected = false;
  this.disconnected = true;
  delete this.id;
  this.emit('disconnect', reason);
};

/**
 * Called with socket packet.
 *
 * @param {Object} packet
 * @api private
 */

Socket.prototype.onpacket = function (packet) {
  var sameNamespace = packet.nsp === this.nsp;
  var rootNamespaceError = packet.type === parser.ERROR && packet.nsp === '/';

  if (!sameNamespace && !rootNamespaceError) return;

  switch (packet.type) {
    case parser.CONNECT:
      this.onconnect();
      break;

    case parser.EVENT:
      this.onevent(packet);
      break;

    case parser.BINARY_EVENT:
      this.onevent(packet);
      break;

    case parser.ACK:
      this.onack(packet);
      break;

    case parser.BINARY_ACK:
      this.onack(packet);
      break;

    case parser.DISCONNECT:
      this.ondisconnect();
      break;

    case parser.ERROR:
      this.emit('error', packet.data);
      break;
  }
};

/**
 * Called upon a server event.
 *
 * @param {Object} packet
 * @api private
 */

Socket.prototype.onevent = function (packet) {
  var args = packet.data || [];
  debug('emitting event %j', args);

  if (null != packet.id) {
    debug('attaching ack callback to event');
    args.push(this.ack(packet.id));
  }

  if (this.connected) {
    emit.apply(this, args);
  } else {
    this.receiveBuffer.push(args);
  }
};

/**
 * Produces an ack callback to emit with an event.
 *
 * @api private
 */

Socket.prototype.ack = function (id) {
  var self = this;
  var sent = false;
  return function () {
    // prevent double callbacks
    if (sent) return;
    sent = true;
    var args = toArray(arguments);
    debug('sending ack %j', args);

    self.packet({
      type: hasBin(args) ? parser.BINARY_ACK : parser.ACK,
      id: id,
      data: args
    });
  };
};

/**
 * Called upon a server acknowlegement.
 *
 * @param {Object} packet
 * @api private
 */

Socket.prototype.onack = function (packet) {
  var ack = this.acks[packet.id];
  if ('function' === typeof ack) {
    debug('calling ack %s with %j', packet.id, packet.data);
    ack.apply(this, packet.data);
    delete this.acks[packet.id];
  } else {
    debug('bad ack %s', packet.id);
  }
};

/**
 * Called upon server connect.
 *
 * @api private
 */

Socket.prototype.onconnect = function () {
  this.connected = true;
  this.disconnected = false;
  this.emit('connect');
  this.emitBuffered();
};

/**
 * Emit buffered events (received and emitted).
 *
 * @api private
 */

Socket.prototype.emitBuffered = function () {
  var i;
  for (i = 0; i < this.receiveBuffer.length; i++) {
    emit.apply(this, this.receiveBuffer[i]);
  }
  this.receiveBuffer = [];

  for (i = 0; i < this.sendBuffer.length; i++) {
    this.packet(this.sendBuffer[i]);
  }
  this.sendBuffer = [];
};

/**
 * Called upon server disconnect.
 *
 * @api private
 */

Socket.prototype.ondisconnect = function () {
  debug('server disconnect (%s)', this.nsp);
  this.destroy();
  this.onclose('io server disconnect');
};

/**
 * Called upon forced client/server side disconnections,
 * this method ensures the manager stops tracking us and
 * that reconnections don't get triggered for this.
 *
 * @api private.
 */

Socket.prototype.destroy = function () {
  if (this.subs) {
    // clean subscriptions to avoid reconnections
    for (var i = 0; i < this.subs.length; i++) {
      this.subs[i].destroy();
    }
    this.subs = null;
  }

  this.io.destroy(this);
};

/**
 * Disconnects the socket manually.
 *
 * @return {Socket} self
 * @api public
 */

Socket.prototype.close =
Socket.prototype.disconnect = function () {
  if (this.connected) {
    debug('performing disconnect (%s)', this.nsp);
    this.packet({ type: parser.DISCONNECT });
  }

  // remove socket from pool
  this.destroy();

  if (this.connected) {
    // fire events
    this.onclose('io client disconnect');
  }
  return this;
};

/**
 * Sets the compress flag.
 *
 * @param {Boolean} if `true`, compresses the sending data
 * @return {Socket} self
 * @api public
 */

Socket.prototype.compress = function (compress) {
  this.flags.compress = compress;
  return this;
};

/**
 * Sets the binary flag
 *
 * @param {Boolean} whether the emitted data contains binary
 * @return {Socket} self
 * @api public
 */

Socket.prototype.binary = function (binary) {
  this.flags.binary = binary;
  return this;
};


/***/ }),
/* 41 */
/***/ (function(module, exports) {


/**
 * Module exports.
 */

module.exports = on;

/**
 * Helper for subscriptions.
 *
 * @param {Object|EventEmitter} obj with `Emitter` mixin or `EventEmitter`
 * @param {String} event name
 * @param {Function} callback
 * @api public
 */

function on (obj, ev, fn) {
  obj.on(ev, fn);
  return {
    destroy: function () {
      obj.removeListener(ev, fn);
    }
  };
}


/***/ }),
/* 42 */
/***/ (function(module, exports) {

/**
 * Slice reference.
 */

var slice = [].slice;

/**
 * Bind `obj` to `fn`.
 *
 * @param {Object} obj
 * @param {Function|String} fn or string
 * @return {Function}
 * @api public
 */

module.exports = function(obj, fn){
  if ('string' == typeof fn) fn = obj[fn];
  if ('function' != typeof fn) throw new Error('bind() requires a function');
  var args = slice.call(arguments, 2);
  return function(){
    return fn.apply(obj, args.concat(slice.call(arguments)));
  }
};


/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(71)
}
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(73)
/* template */
var __vue_template__ = __webpack_require__(74)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-03567f37"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/ModalWindowComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-03567f37", Component.options)
  } else {
    hotAPI.reload("data-v-03567f37", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 44 */,
/* 45 */
/***/ (function(module, exports) {

var asyncGenerator = function () {
  function AwaitValue(value) {
    this.value = value;
  }

  function AsyncGenerator(gen) {
    var front, back;

    function send(key, arg) {
      return new Promise(function (resolve, reject) {
        var request = {
          key: key,
          arg: arg,
          resolve: resolve,
          reject: reject,
          next: null
        };

        if (back) {
          back = back.next = request;
        } else {
          front = back = request;
          resume(key, arg);
        }
      });
    }

    function resume(key, arg) {
      try {
        var result = gen[key](arg);
        var value = result.value;

        if (value instanceof AwaitValue) {
          Promise.resolve(value.value).then(function (arg) {
            resume("next", arg);
          }, function (arg) {
            resume("throw", arg);
          });
        } else {
          settle(result.done ? "return" : "normal", result.value);
        }
      } catch (err) {
        settle("throw", err);
      }
    }

    function settle(type, value) {
      switch (type) {
        case "return":
          front.resolve({
            value: value,
            done: true
          });
          break;

        case "throw":
          front.reject(value);
          break;

        default:
          front.resolve({
            value: value,
            done: false
          });
          break;
      }

      front = front.next;

      if (front) {
        resume(front.key, front.arg);
      } else {
        back = null;
      }
    }

    this._invoke = send;

    if (typeof gen.return !== "function") {
      this.return = undefined;
    }
  }

  if (typeof Symbol === "function" && Symbol.asyncIterator) {
    AsyncGenerator.prototype[Symbol.asyncIterator] = function () {
      return this;
    };
  }

  AsyncGenerator.prototype.next = function (arg) {
    return this._invoke("next", arg);
  };

  AsyncGenerator.prototype.throw = function (arg) {
    return this._invoke("throw", arg);
  };

  AsyncGenerator.prototype.return = function (arg) {
    return this._invoke("return", arg);
  };

  return {
    wrap: function (fn) {
      return function () {
        return new AsyncGenerator(fn.apply(this, arguments));
      };
    },
    await: function (value) {
      return new AwaitValue(value);
    }
  };
}();

var classCallCheck = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

var createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

var inherits = function (subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
};

var possibleConstructorReturn = function (self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
};

var Connector = function () {
    function Connector(options) {
        classCallCheck(this, Connector);

        this._defaultOptions = {
            auth: {
                headers: {}
            },
            authEndpoint: '/broadcasting/auth',
            broadcaster: 'pusher',
            csrfToken: null,
            host: null,
            key: null,
            namespace: 'App.Events'
        };
        this.setOptions(options);
        this.connect();
    }

    createClass(Connector, [{
        key: 'setOptions',
        value: function setOptions(options) {
            this.options = _extends(this._defaultOptions, options);
            if (this.csrfToken()) {
                this.options.auth.headers['X-CSRF-TOKEN'] = this.csrfToken();
            }
            return options;
        }
    }, {
        key: 'csrfToken',
        value: function csrfToken() {
            var selector = void 0;
            if (typeof window !== 'undefined' && window['Laravel'] && window['Laravel'].csrfToken) {
                return window['Laravel'].csrfToken;
            } else if (this.options.csrfToken) {
                return this.options.csrfToken;
            } else if (typeof document !== 'undefined' && (selector = document.querySelector('meta[name="csrf-token"]'))) {
                return selector.getAttribute('content');
            }
            return null;
        }
    }]);
    return Connector;
}();

var Channel = function () {
    function Channel() {
        classCallCheck(this, Channel);
    }

    createClass(Channel, [{
        key: 'notification',
        value: function notification(callback) {
            return this.listen('.Illuminate\\Notifications\\Events\\BroadcastNotificationCreated', callback);
        }
    }, {
        key: 'listenForWhisper',
        value: function listenForWhisper(event, callback) {
            return this.listen('.client-' + event, callback);
        }
    }]);
    return Channel;
}();

var EventFormatter = function () {
    function EventFormatter(namespace) {
        classCallCheck(this, EventFormatter);

        this.setNamespace(namespace);
    }

    createClass(EventFormatter, [{
        key: 'format',
        value: function format(event) {
            if (event.charAt(0) === '.' || event.charAt(0) === '\\') {
                return event.substr(1);
            } else if (this.namespace) {
                event = this.namespace + '.' + event;
            }
            return event.replace(/\./g, '\\');
        }
    }, {
        key: 'setNamespace',
        value: function setNamespace(value) {
            this.namespace = value;
        }
    }]);
    return EventFormatter;
}();

var PusherChannel = function (_Channel) {
    inherits(PusherChannel, _Channel);

    function PusherChannel(pusher, name, options) {
        classCallCheck(this, PusherChannel);

        var _this = possibleConstructorReturn(this, (PusherChannel.__proto__ || Object.getPrototypeOf(PusherChannel)).call(this));

        _this.name = name;
        _this.pusher = pusher;
        _this.options = options;
        _this.eventFormatter = new EventFormatter(_this.options.namespace);
        _this.subscribe();
        return _this;
    }

    createClass(PusherChannel, [{
        key: 'subscribe',
        value: function subscribe() {
            this.subscription = this.pusher.subscribe(this.name);
        }
    }, {
        key: 'unsubscribe',
        value: function unsubscribe() {
            this.pusher.unsubscribe(this.name);
        }
    }, {
        key: 'listen',
        value: function listen(event, callback) {
            this.on(this.eventFormatter.format(event), callback);
            return this;
        }
    }, {
        key: 'stopListening',
        value: function stopListening(event) {
            this.subscription.unbind(this.eventFormatter.format(event));
            return this;
        }
    }, {
        key: 'on',
        value: function on(event, callback) {
            this.subscription.bind(event, callback);
            return this;
        }
    }]);
    return PusherChannel;
}(Channel);

var PusherPrivateChannel = function (_PusherChannel) {
    inherits(PusherPrivateChannel, _PusherChannel);

    function PusherPrivateChannel() {
        classCallCheck(this, PusherPrivateChannel);
        return possibleConstructorReturn(this, (PusherPrivateChannel.__proto__ || Object.getPrototypeOf(PusherPrivateChannel)).apply(this, arguments));
    }

    createClass(PusherPrivateChannel, [{
        key: 'whisper',
        value: function whisper(eventName, data) {
            this.pusher.channels.channels[this.name].trigger('client-' + eventName, data);
            return this;
        }
    }]);
    return PusherPrivateChannel;
}(PusherChannel);

var PusherPresenceChannel = function (_PusherChannel) {
    inherits(PusherPresenceChannel, _PusherChannel);

    function PusherPresenceChannel() {
        classCallCheck(this, PusherPresenceChannel);
        return possibleConstructorReturn(this, (PusherPresenceChannel.__proto__ || Object.getPrototypeOf(PusherPresenceChannel)).apply(this, arguments));
    }

    createClass(PusherPresenceChannel, [{
        key: 'here',
        value: function here(callback) {
            this.on('pusher:subscription_succeeded', function (data) {
                callback(Object.keys(data.members).map(function (k) {
                    return data.members[k];
                }));
            });
            return this;
        }
    }, {
        key: 'joining',
        value: function joining(callback) {
            this.on('pusher:member_added', function (member) {
                callback(member.info);
            });
            return this;
        }
    }, {
        key: 'leaving',
        value: function leaving(callback) {
            this.on('pusher:member_removed', function (member) {
                callback(member.info);
            });
            return this;
        }
    }, {
        key: 'whisper',
        value: function whisper(eventName, data) {
            this.pusher.channels.channels[this.name].trigger('client-' + eventName, data);
            return this;
        }
    }]);
    return PusherPresenceChannel;
}(PusherChannel);

var SocketIoChannel = function (_Channel) {
    inherits(SocketIoChannel, _Channel);

    function SocketIoChannel(socket, name, options) {
        classCallCheck(this, SocketIoChannel);

        var _this = possibleConstructorReturn(this, (SocketIoChannel.__proto__ || Object.getPrototypeOf(SocketIoChannel)).call(this));

        _this.events = {};
        _this.name = name;
        _this.socket = socket;
        _this.options = options;
        _this.eventFormatter = new EventFormatter(_this.options.namespace);
        _this.subscribe();
        _this.configureReconnector();
        return _this;
    }

    createClass(SocketIoChannel, [{
        key: 'subscribe',
        value: function subscribe() {
            this.socket.emit('subscribe', {
                channel: this.name,
                auth: this.options.auth || {}
            });
        }
    }, {
        key: 'unsubscribe',
        value: function unsubscribe() {
            this.unbind();
            this.socket.emit('unsubscribe', {
                channel: this.name,
                auth: this.options.auth || {}
            });
        }
    }, {
        key: 'listen',
        value: function listen(event, callback) {
            this.on(this.eventFormatter.format(event), callback);
            return this;
        }
    }, {
        key: 'on',
        value: function on(event, callback) {
            var _this2 = this;

            var listener = function listener(channel, data) {
                if (_this2.name == channel) {
                    callback(data);
                }
            };
            this.socket.on(event, listener);
            this.bind(event, listener);
        }
    }, {
        key: 'configureReconnector',
        value: function configureReconnector() {
            var _this3 = this;

            var listener = function listener() {
                _this3.subscribe();
            };
            this.socket.on('reconnect', listener);
            this.bind('reconnect', listener);
        }
    }, {
        key: 'bind',
        value: function bind(event, callback) {
            this.events[event] = this.events[event] || [];
            this.events[event].push(callback);
        }
    }, {
        key: 'unbind',
        value: function unbind() {
            var _this4 = this;

            Object.keys(this.events).forEach(function (event) {
                _this4.events[event].forEach(function (callback) {
                    _this4.socket.removeListener(event, callback);
                });
                delete _this4.events[event];
            });
        }
    }]);
    return SocketIoChannel;
}(Channel);

var SocketIoPrivateChannel = function (_SocketIoChannel) {
    inherits(SocketIoPrivateChannel, _SocketIoChannel);

    function SocketIoPrivateChannel() {
        classCallCheck(this, SocketIoPrivateChannel);
        return possibleConstructorReturn(this, (SocketIoPrivateChannel.__proto__ || Object.getPrototypeOf(SocketIoPrivateChannel)).apply(this, arguments));
    }

    createClass(SocketIoPrivateChannel, [{
        key: 'whisper',
        value: function whisper(eventName, data) {
            this.socket.emit('client event', {
                channel: this.name,
                event: 'client-' + eventName,
                data: data
            });
            return this;
        }
    }]);
    return SocketIoPrivateChannel;
}(SocketIoChannel);

var SocketIoPresenceChannel = function (_SocketIoPrivateChann) {
    inherits(SocketIoPresenceChannel, _SocketIoPrivateChann);

    function SocketIoPresenceChannel() {
        classCallCheck(this, SocketIoPresenceChannel);
        return possibleConstructorReturn(this, (SocketIoPresenceChannel.__proto__ || Object.getPrototypeOf(SocketIoPresenceChannel)).apply(this, arguments));
    }

    createClass(SocketIoPresenceChannel, [{
        key: 'here',
        value: function here(callback) {
            this.on('presence:subscribed', function (members) {
                callback(members.map(function (m) {
                    return m.user_info;
                }));
            });
            return this;
        }
    }, {
        key: 'joining',
        value: function joining(callback) {
            this.on('presence:joining', function (member) {
                return callback(member.user_info);
            });
            return this;
        }
    }, {
        key: 'leaving',
        value: function leaving(callback) {
            this.on('presence:leaving', function (member) {
                return callback(member.user_info);
            });
            return this;
        }
    }]);
    return SocketIoPresenceChannel;
}(SocketIoPrivateChannel);

var NullChannel = function (_Channel) {
    inherits(NullChannel, _Channel);

    function NullChannel() {
        classCallCheck(this, NullChannel);
        return possibleConstructorReturn(this, (NullChannel.__proto__ || Object.getPrototypeOf(NullChannel)).apply(this, arguments));
    }

    createClass(NullChannel, [{
        key: 'subscribe',
        value: function subscribe() {}
    }, {
        key: 'unsubscribe',
        value: function unsubscribe() {}
    }, {
        key: 'listen',
        value: function listen(event, callback) {
            return this;
        }
    }, {
        key: 'stopListening',
        value: function stopListening(event) {
            return this;
        }
    }, {
        key: 'on',
        value: function on(event, callback) {
            return this;
        }
    }]);
    return NullChannel;
}(Channel);

var NullPrivateChannel = function (_NullChannel) {
    inherits(NullPrivateChannel, _NullChannel);

    function NullPrivateChannel() {
        classCallCheck(this, NullPrivateChannel);
        return possibleConstructorReturn(this, (NullPrivateChannel.__proto__ || Object.getPrototypeOf(NullPrivateChannel)).apply(this, arguments));
    }

    createClass(NullPrivateChannel, [{
        key: 'whisper',
        value: function whisper(eventName, data) {
            return this;
        }
    }]);
    return NullPrivateChannel;
}(NullChannel);

var NullPresenceChannel = function (_NullChannel) {
    inherits(NullPresenceChannel, _NullChannel);

    function NullPresenceChannel() {
        classCallCheck(this, NullPresenceChannel);
        return possibleConstructorReturn(this, (NullPresenceChannel.__proto__ || Object.getPrototypeOf(NullPresenceChannel)).apply(this, arguments));
    }

    createClass(NullPresenceChannel, [{
        key: 'here',
        value: function here(callback) {
            return this;
        }
    }, {
        key: 'joining',
        value: function joining(callback) {
            return this;
        }
    }, {
        key: 'leaving',
        value: function leaving(callback) {
            return this;
        }
    }, {
        key: 'whisper',
        value: function whisper(eventName, data) {
            return this;
        }
    }]);
    return NullPresenceChannel;
}(NullChannel);

var PusherConnector = function (_Connector) {
    inherits(PusherConnector, _Connector);

    function PusherConnector() {
        var _ref;

        classCallCheck(this, PusherConnector);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        var _this = possibleConstructorReturn(this, (_ref = PusherConnector.__proto__ || Object.getPrototypeOf(PusherConnector)).call.apply(_ref, [this].concat(args)));

        _this.channels = {};
        return _this;
    }

    createClass(PusherConnector, [{
        key: 'connect',
        value: function connect() {
            this.pusher = new Pusher(this.options.key, this.options);
        }
    }, {
        key: 'listen',
        value: function listen(name, event, callback) {
            return this.channel(name).listen(event, callback);
        }
    }, {
        key: 'channel',
        value: function channel(name) {
            if (!this.channels[name]) {
                this.channels[name] = new PusherChannel(this.pusher, name, this.options);
            }
            return this.channels[name];
        }
    }, {
        key: 'privateChannel',
        value: function privateChannel(name) {
            if (!this.channels['private-' + name]) {
                this.channels['private-' + name] = new PusherPrivateChannel(this.pusher, 'private-' + name, this.options);
            }
            return this.channels['private-' + name];
        }
    }, {
        key: 'presenceChannel',
        value: function presenceChannel(name) {
            if (!this.channels['presence-' + name]) {
                this.channels['presence-' + name] = new PusherPresenceChannel(this.pusher, 'presence-' + name, this.options);
            }
            return this.channels['presence-' + name];
        }
    }, {
        key: 'leave',
        value: function leave(name) {
            var _this2 = this;

            var channels = [name, 'private-' + name, 'presence-' + name];
            channels.forEach(function (name, index) {
                if (_this2.channels[name]) {
                    _this2.channels[name].unsubscribe();
                    delete _this2.channels[name];
                }
            });
        }
    }, {
        key: 'socketId',
        value: function socketId() {
            return this.pusher.connection.socket_id;
        }
    }, {
        key: 'disconnect',
        value: function disconnect() {
            this.pusher.disconnect();
        }
    }]);
    return PusherConnector;
}(Connector);

var SocketIoConnector = function (_Connector) {
    inherits(SocketIoConnector, _Connector);

    function SocketIoConnector() {
        var _ref;

        classCallCheck(this, SocketIoConnector);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        var _this = possibleConstructorReturn(this, (_ref = SocketIoConnector.__proto__ || Object.getPrototypeOf(SocketIoConnector)).call.apply(_ref, [this].concat(args)));

        _this.channels = {};
        return _this;
    }

    createClass(SocketIoConnector, [{
        key: 'connect',
        value: function connect() {
            var io = this.getSocketIO();
            this.socket = io(this.options.host, this.options);
            return this.socket;
        }
    }, {
        key: 'getSocketIO',
        value: function getSocketIO() {
            if (typeof io !== 'undefined') {
                return io;
            }
            if (this.options.client !== 'undefined') {
                return this.options.client;
            }
            throw new Error('Socket.io client not found. Should be globally available or passed via options.client');
        }
    }, {
        key: 'listen',
        value: function listen(name, event, callback) {
            return this.channel(name).listen(event, callback);
        }
    }, {
        key: 'channel',
        value: function channel(name) {
            if (!this.channels[name]) {
                this.channels[name] = new SocketIoChannel(this.socket, name, this.options);
            }
            return this.channels[name];
        }
    }, {
        key: 'privateChannel',
        value: function privateChannel(name) {
            if (!this.channels['private-' + name]) {
                this.channels['private-' + name] = new SocketIoPrivateChannel(this.socket, 'private-' + name, this.options);
            }
            return this.channels['private-' + name];
        }
    }, {
        key: 'presenceChannel',
        value: function presenceChannel(name) {
            if (!this.channels['presence-' + name]) {
                this.channels['presence-' + name] = new SocketIoPresenceChannel(this.socket, 'presence-' + name, this.options);
            }
            return this.channels['presence-' + name];
        }
    }, {
        key: 'leave',
        value: function leave(name) {
            var _this2 = this;

            var channels = [name, 'private-' + name, 'presence-' + name];
            channels.forEach(function (name) {
                if (_this2.channels[name]) {
                    _this2.channels[name].unsubscribe();
                    delete _this2.channels[name];
                }
            });
        }
    }, {
        key: 'socketId',
        value: function socketId() {
            return this.socket.id;
        }
    }, {
        key: 'disconnect',
        value: function disconnect() {
            this.socket.disconnect();
        }
    }]);
    return SocketIoConnector;
}(Connector);

var NullConnector = function (_Connector) {
    inherits(NullConnector, _Connector);

    function NullConnector() {
        var _ref;

        classCallCheck(this, NullConnector);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        var _this = possibleConstructorReturn(this, (_ref = NullConnector.__proto__ || Object.getPrototypeOf(NullConnector)).call.apply(_ref, [this].concat(args)));

        _this.channels = {};
        return _this;
    }

    createClass(NullConnector, [{
        key: 'connect',
        value: function connect() {}
    }, {
        key: 'listen',
        value: function listen(name, event, callback) {
            return new NullChannel();
        }
    }, {
        key: 'channel',
        value: function channel(name) {
            return new NullChannel();
        }
    }, {
        key: 'privateChannel',
        value: function privateChannel(name) {
            return new NullPrivateChannel();
        }
    }, {
        key: 'presenceChannel',
        value: function presenceChannel(name) {
            return new NullPresenceChannel();
        }
    }, {
        key: 'leave',
        value: function leave(name) {}
    }, {
        key: 'socketId',
        value: function socketId() {
            return 'fake-socket-id';
        }
    }, {
        key: 'disconnect',
        value: function disconnect() {}
    }]);
    return NullConnector;
}(Connector);

var Echo = function () {
    function Echo(options) {
        classCallCheck(this, Echo);

        this.options = options;
        if (typeof Vue === 'function' && Vue.http) {
            this.registerVueRequestInterceptor();
        }
        if (typeof axios === 'function') {
            this.registerAxiosRequestInterceptor();
        }
        if (typeof jQuery === 'function') {
            this.registerjQueryAjaxSetup();
        }
        if (this.options.broadcaster == 'pusher') {
            this.connector = new PusherConnector(this.options);
        } else if (this.options.broadcaster == 'socket.io') {
            this.connector = new SocketIoConnector(this.options);
        } else if (this.options.broadcaster == 'null') {
            this.connector = new NullConnector(this.options);
        }
    }

    createClass(Echo, [{
        key: 'registerVueRequestInterceptor',
        value: function registerVueRequestInterceptor() {
            var _this = this;

            Vue.http.interceptors.push(function (request, next) {
                if (_this.socketId()) {
                    request.headers.set('X-Socket-ID', _this.socketId());
                }
                next();
            });
        }
    }, {
        key: 'registerAxiosRequestInterceptor',
        value: function registerAxiosRequestInterceptor() {
            var _this2 = this;

            axios.interceptors.request.use(function (config) {
                if (_this2.socketId()) {
                    config.headers['X-Socket-Id'] = _this2.socketId();
                }
                return config;
            });
        }
    }, {
        key: 'registerjQueryAjaxSetup',
        value: function registerjQueryAjaxSetup() {
            var _this3 = this;

            if (typeof jQuery.ajax != 'undefined') {
                jQuery.ajaxSetup({
                    beforeSend: function beforeSend(xhr) {
                        if (_this3.socketId()) {
                            xhr.setRequestHeader('X-Socket-Id', _this3.socketId());
                        }
                    }
                });
            }
        }
    }, {
        key: 'listen',
        value: function listen(channel, event, callback) {
            return this.connector.listen(channel, event, callback);
        }
    }, {
        key: 'channel',
        value: function channel(_channel) {
            return this.connector.channel(_channel);
        }
    }, {
        key: 'private',
        value: function _private(channel) {
            return this.connector.privateChannel(channel);
        }
    }, {
        key: 'join',
        value: function join(channel) {
            return this.connector.presenceChannel(channel);
        }
    }, {
        key: 'leave',
        value: function leave(channel) {
            this.connector.leave(channel);
        }
    }, {
        key: 'socketId',
        value: function socketId() {
            return this.connector.socketId();
        }
    }, {
        key: 'disconnect',
        value: function disconnect() {
            this.connector.disconnect();
        }
    }]);
    return Echo;
}();

module.exports = Echo;

/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {


/**
 * Module dependencies.
 */

var url = __webpack_require__(47);
var parser = __webpack_require__(10);
var Manager = __webpack_require__(34);
var debug = __webpack_require__(4)('socket.io-client');

/**
 * Module exports.
 */

module.exports = exports = lookup;

/**
 * Managers cache.
 */

var cache = exports.managers = {};

/**
 * Looks up an existing `Manager` for multiplexing.
 * If the user summons:
 *
 *   `io('http://localhost/a');`
 *   `io('http://localhost/b');`
 *
 * We reuse the existing instance based on same scheme/port/host,
 * and we initialize sockets for each namespace.
 *
 * @api public
 */

function lookup (uri, opts) {
  if (typeof uri === 'object') {
    opts = uri;
    uri = undefined;
  }

  opts = opts || {};

  var parsed = url(uri);
  var source = parsed.source;
  var id = parsed.id;
  var path = parsed.path;
  var sameNamespace = cache[id] && path in cache[id].nsps;
  var newConnection = opts.forceNew || opts['force new connection'] ||
                      false === opts.multiplex || sameNamespace;

  var io;

  if (newConnection) {
    debug('ignoring socket cache for %s', source);
    io = Manager(source, opts);
  } else {
    if (!cache[id]) {
      debug('new io instance for %s', source);
      cache[id] = Manager(source, opts);
    }
    io = cache[id];
  }
  if (parsed.query && !opts.query) {
    opts.query = parsed.query;
  }
  return io.socket(parsed.path, opts);
}

/**
 * Protocol version.
 *
 * @api public
 */

exports.protocol = parser.protocol;

/**
 * `connect`.
 *
 * @param {String} uri
 * @api public
 */

exports.connect = lookup;

/**
 * Expose constructors for standalone build.
 *
 * @api public
 */

exports.Manager = __webpack_require__(34);
exports.Socket = __webpack_require__(40);


/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {
/**
 * Module dependencies.
 */

var parseuri = __webpack_require__(31);
var debug = __webpack_require__(4)('socket.io-client:url');

/**
 * Module exports.
 */

module.exports = url;

/**
 * URL parser.
 *
 * @param {String} url
 * @param {Object} An object meant to mimic window.location.
 *                 Defaults to window.location.
 * @api public
 */

function url (uri, loc) {
  var obj = uri;

  // default to window.location
  loc = loc || global.location;
  if (null == uri) uri = loc.protocol + '//' + loc.host;

  // relative path support
  if ('string' === typeof uri) {
    if ('/' === uri.charAt(0)) {
      if ('/' === uri.charAt(1)) {
        uri = loc.protocol + uri;
      } else {
        uri = loc.host + uri;
      }
    }

    if (!/^(https?|wss?):\/\//.test(uri)) {
      debug('protocol-less url %s', uri);
      if ('undefined' !== typeof loc) {
        uri = loc.protocol + '//' + uri;
      } else {
        uri = 'https://' + uri;
      }
    }

    // parse
    debug('parse %s', uri);
    obj = parseuri(uri);
  }

  // make sure we treat `localhost:80` and `localhost` equally
  if (!obj.port) {
    if (/^(http|ws)$/.test(obj.protocol)) {
      obj.port = '80';
    } else if (/^(http|ws)s$/.test(obj.protocol)) {
      obj.port = '443';
    }
  }

  obj.path = obj.path || '/';

  var ipv6 = obj.host.indexOf(':') !== -1;
  var host = ipv6 ? '[' + obj.host + ']' : obj.host;

  // define unique id
  obj.id = obj.protocol + '://' + host + ':' + obj.port;
  // define href
  obj.href = obj.protocol + '://' + host + (loc && loc.port === obj.port ? '' : (':' + obj.port));

  return obj;
}

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(0)))

/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {


/**
 * This is the common logic for both the Node.js and web browser
 * implementations of `debug()`.
 *
 * Expose `debug()` as the module.
 */

exports = module.exports = createDebug.debug = createDebug['default'] = createDebug;
exports.coerce = coerce;
exports.disable = disable;
exports.enable = enable;
exports.enabled = enabled;
exports.humanize = __webpack_require__(49);

/**
 * Active `debug` instances.
 */
exports.instances = [];

/**
 * The currently active debug mode names, and names to skip.
 */

exports.names = [];
exports.skips = [];

/**
 * Map of special "%n" handling functions, for the debug "format" argument.
 *
 * Valid key names are a single, lower or upper-case letter, i.e. "n" and "N".
 */

exports.formatters = {};

/**
 * Select a color.
 * @param {String} namespace
 * @return {Number}
 * @api private
 */

function selectColor(namespace) {
  var hash = 0, i;

  for (i in namespace) {
    hash  = ((hash << 5) - hash) + namespace.charCodeAt(i);
    hash |= 0; // Convert to 32bit integer
  }

  return exports.colors[Math.abs(hash) % exports.colors.length];
}

/**
 * Create a debugger with the given `namespace`.
 *
 * @param {String} namespace
 * @return {Function}
 * @api public
 */

function createDebug(namespace) {

  var prevTime;

  function debug() {
    // disabled?
    if (!debug.enabled) return;

    var self = debug;

    // set `diff` timestamp
    var curr = +new Date();
    var ms = curr - (prevTime || curr);
    self.diff = ms;
    self.prev = prevTime;
    self.curr = curr;
    prevTime = curr;

    // turn the `arguments` into a proper Array
    var args = new Array(arguments.length);
    for (var i = 0; i < args.length; i++) {
      args[i] = arguments[i];
    }

    args[0] = exports.coerce(args[0]);

    if ('string' !== typeof args[0]) {
      // anything else let's inspect with %O
      args.unshift('%O');
    }

    // apply any `formatters` transformations
    var index = 0;
    args[0] = args[0].replace(/%([a-zA-Z%])/g, function(match, format) {
      // if we encounter an escaped % then don't increase the array index
      if (match === '%%') return match;
      index++;
      var formatter = exports.formatters[format];
      if ('function' === typeof formatter) {
        var val = args[index];
        match = formatter.call(self, val);

        // now we need to remove `args[index]` since it's inlined in the `format`
        args.splice(index, 1);
        index--;
      }
      return match;
    });

    // apply env-specific formatting (colors, etc.)
    exports.formatArgs.call(self, args);

    var logFn = debug.log || exports.log || console.log.bind(console);
    logFn.apply(self, args);
  }

  debug.namespace = namespace;
  debug.enabled = exports.enabled(namespace);
  debug.useColors = exports.useColors();
  debug.color = selectColor(namespace);
  debug.destroy = destroy;

  // env-specific initialization logic for debug instances
  if ('function' === typeof exports.init) {
    exports.init(debug);
  }

  exports.instances.push(debug);

  return debug;
}

function destroy () {
  var index = exports.instances.indexOf(this);
  if (index !== -1) {
    exports.instances.splice(index, 1);
    return true;
  } else {
    return false;
  }
}

/**
 * Enables a debug mode by namespaces. This can include modes
 * separated by a colon and wildcards.
 *
 * @param {String} namespaces
 * @api public
 */

function enable(namespaces) {
  exports.save(namespaces);

  exports.names = [];
  exports.skips = [];

  var i;
  var split = (typeof namespaces === 'string' ? namespaces : '').split(/[\s,]+/);
  var len = split.length;

  for (i = 0; i < len; i++) {
    if (!split[i]) continue; // ignore empty strings
    namespaces = split[i].replace(/\*/g, '.*?');
    if (namespaces[0] === '-') {
      exports.skips.push(new RegExp('^' + namespaces.substr(1) + '$'));
    } else {
      exports.names.push(new RegExp('^' + namespaces + '$'));
    }
  }

  for (i = 0; i < exports.instances.length; i++) {
    var instance = exports.instances[i];
    instance.enabled = exports.enabled(instance.namespace);
  }
}

/**
 * Disable debug output.
 *
 * @api public
 */

function disable() {
  exports.enable('');
}

/**
 * Returns true if the given mode name is enabled, false otherwise.
 *
 * @param {String} name
 * @return {Boolean}
 * @api public
 */

function enabled(name) {
  if (name[name.length - 1] === '*') {
    return true;
  }
  var i, len;
  for (i = 0, len = exports.skips.length; i < len; i++) {
    if (exports.skips[i].test(name)) {
      return false;
    }
  }
  for (i = 0, len = exports.names.length; i < len; i++) {
    if (exports.names[i].test(name)) {
      return true;
    }
  }
  return false;
}

/**
 * Coerce `val`.
 *
 * @param {Mixed} val
 * @return {Mixed}
 * @api private
 */

function coerce(val) {
  if (val instanceof Error) return val.stack || val.message;
  return val;
}


/***/ }),
/* 49 */
/***/ (function(module, exports) {

/**
 * Helpers.
 */

var s = 1000;
var m = s * 60;
var h = m * 60;
var d = h * 24;
var y = d * 365.25;

/**
 * Parse or format the given `val`.
 *
 * Options:
 *
 *  - `long` verbose formatting [false]
 *
 * @param {String|Number} val
 * @param {Object} [options]
 * @throws {Error} throw an error if val is not a non-empty string or a number
 * @return {String|Number}
 * @api public
 */

module.exports = function(val, options) {
  options = options || {};
  var type = typeof val;
  if (type === 'string' && val.length > 0) {
    return parse(val);
  } else if (type === 'number' && isNaN(val) === false) {
    return options.long ? fmtLong(val) : fmtShort(val);
  }
  throw new Error(
    'val is not a non-empty string or a valid number. val=' +
      JSON.stringify(val)
  );
};

/**
 * Parse the given `str` and return milliseconds.
 *
 * @param {String} str
 * @return {Number}
 * @api private
 */

function parse(str) {
  str = String(str);
  if (str.length > 100) {
    return;
  }
  var match = /^((?:\d+)?\.?\d+) *(milliseconds?|msecs?|ms|seconds?|secs?|s|minutes?|mins?|m|hours?|hrs?|h|days?|d|years?|yrs?|y)?$/i.exec(
    str
  );
  if (!match) {
    return;
  }
  var n = parseFloat(match[1]);
  var type = (match[2] || 'ms').toLowerCase();
  switch (type) {
    case 'years':
    case 'year':
    case 'yrs':
    case 'yr':
    case 'y':
      return n * y;
    case 'days':
    case 'day':
    case 'd':
      return n * d;
    case 'hours':
    case 'hour':
    case 'hrs':
    case 'hr':
    case 'h':
      return n * h;
    case 'minutes':
    case 'minute':
    case 'mins':
    case 'min':
    case 'm':
      return n * m;
    case 'seconds':
    case 'second':
    case 'secs':
    case 'sec':
    case 's':
      return n * s;
    case 'milliseconds':
    case 'millisecond':
    case 'msecs':
    case 'msec':
    case 'ms':
      return n;
    default:
      return undefined;
  }
}

/**
 * Short format for `ms`.
 *
 * @param {Number} ms
 * @return {String}
 * @api private
 */

function fmtShort(ms) {
  if (ms >= d) {
    return Math.round(ms / d) + 'd';
  }
  if (ms >= h) {
    return Math.round(ms / h) + 'h';
  }
  if (ms >= m) {
    return Math.round(ms / m) + 'm';
  }
  if (ms >= s) {
    return Math.round(ms / s) + 's';
  }
  return ms + 'ms';
}

/**
 * Long format for `ms`.
 *
 * @param {Number} ms
 * @return {String}
 * @api private
 */

function fmtLong(ms) {
  return plural(ms, d, 'day') ||
    plural(ms, h, 'hour') ||
    plural(ms, m, 'minute') ||
    plural(ms, s, 'second') ||
    ms + ' ms';
}

/**
 * Pluralization helper.
 */

function plural(ms, n, name) {
  if (ms < n) {
    return;
  }
  if (ms < n * 1.5) {
    return Math.floor(ms / n) + ' ' + name;
  }
  return Math.ceil(ms / n) + ' ' + name + 's';
}


/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {/*global Blob,File*/

/**
 * Module requirements
 */

var isArray = __webpack_require__(32);
var isBuf = __webpack_require__(33);
var toString = Object.prototype.toString;
var withNativeBlob = typeof global.Blob === 'function' || toString.call(global.Blob) === '[object BlobConstructor]';
var withNativeFile = typeof global.File === 'function' || toString.call(global.File) === '[object FileConstructor]';

/**
 * Replaces every Buffer | ArrayBuffer in packet with a numbered placeholder.
 * Anything with blobs or files should be fed through removeBlobs before coming
 * here.
 *
 * @param {Object} packet - socket.io event packet
 * @return {Object} with deconstructed packet and list of buffers
 * @api public
 */

exports.deconstructPacket = function(packet) {
  var buffers = [];
  var packetData = packet.data;
  var pack = packet;
  pack.data = _deconstructPacket(packetData, buffers);
  pack.attachments = buffers.length; // number of binary 'attachments'
  return {packet: pack, buffers: buffers};
};

function _deconstructPacket(data, buffers) {
  if (!data) return data;

  if (isBuf(data)) {
    var placeholder = { _placeholder: true, num: buffers.length };
    buffers.push(data);
    return placeholder;
  } else if (isArray(data)) {
    var newData = new Array(data.length);
    for (var i = 0; i < data.length; i++) {
      newData[i] = _deconstructPacket(data[i], buffers);
    }
    return newData;
  } else if (typeof data === 'object' && !(data instanceof Date)) {
    var newData = {};
    for (var key in data) {
      newData[key] = _deconstructPacket(data[key], buffers);
    }
    return newData;
  }
  return data;
}

/**
 * Reconstructs a binary packet from its placeholder packet and buffers
 *
 * @param {Object} packet - event packet with placeholders
 * @param {Array} buffers - binary buffers to put in placeholder positions
 * @return {Object} reconstructed packet
 * @api public
 */

exports.reconstructPacket = function(packet, buffers) {
  packet.data = _reconstructPacket(packet.data, buffers);
  packet.attachments = undefined; // no longer useful
  return packet;
};

function _reconstructPacket(data, buffers) {
  if (!data) return data;

  if (data && data._placeholder) {
    return buffers[data.num]; // appropriate buffer (should be natural order anyway)
  } else if (isArray(data)) {
    for (var i = 0; i < data.length; i++) {
      data[i] = _reconstructPacket(data[i], buffers);
    }
  } else if (typeof data === 'object') {
    for (var key in data) {
      data[key] = _reconstructPacket(data[key], buffers);
    }
  }

  return data;
}

/**
 * Asynchronously removes Blobs or Files from data via
 * FileReader's readAsArrayBuffer method. Used before encoding
 * data as msgpack. Calls callback with the blobless data.
 *
 * @param {Object} data
 * @param {Function} callback
 * @api private
 */

exports.removeBlobs = function(data, callback) {
  function _removeBlobs(obj, curKey, containingObject) {
    if (!obj) return obj;

    // convert any blob
    if ((withNativeBlob && obj instanceof Blob) ||
        (withNativeFile && obj instanceof File)) {
      pendingBlobs++;

      // async filereader
      var fileReader = new FileReader();
      fileReader.onload = function() { // this.result == arraybuffer
        if (containingObject) {
          containingObject[curKey] = this.result;
        }
        else {
          bloblessData = this.result;
        }

        // if nothing pending its callback time
        if(! --pendingBlobs) {
          callback(bloblessData);
        }
      };

      fileReader.readAsArrayBuffer(obj); // blob -> arraybuffer
    } else if (isArray(obj)) { // handle array
      for (var i = 0; i < obj.length; i++) {
        _removeBlobs(obj[i], i, obj);
      }
    } else if (typeof obj === 'object' && !isBuf(obj)) { // and object
      for (var key in obj) {
        _removeBlobs(obj[key], key, obj);
      }
    }
  }

  var pendingBlobs = 0;
  var bloblessData = data;
  _removeBlobs(bloblessData);
  if (!pendingBlobs) {
    callback(bloblessData);
  }
};

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(0)))

/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {


module.exports = __webpack_require__(52);

/**
 * Exports parser
 *
 * @api public
 *
 */
module.exports.parser = __webpack_require__(6);


/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {/**
 * Module dependencies.
 */

var transports = __webpack_require__(35);
var Emitter = __webpack_require__(5);
var debug = __webpack_require__(4)('engine.io-client:socket');
var index = __webpack_require__(39);
var parser = __webpack_require__(6);
var parseuri = __webpack_require__(31);
var parseqs = __webpack_require__(8);

/**
 * Module exports.
 */

module.exports = Socket;

/**
 * Socket constructor.
 *
 * @param {String|Object} uri or options
 * @param {Object} options
 * @api public
 */

function Socket (uri, opts) {
  if (!(this instanceof Socket)) return new Socket(uri, opts);

  opts = opts || {};

  if (uri && 'object' === typeof uri) {
    opts = uri;
    uri = null;
  }

  if (uri) {
    uri = parseuri(uri);
    opts.hostname = uri.host;
    opts.secure = uri.protocol === 'https' || uri.protocol === 'wss';
    opts.port = uri.port;
    if (uri.query) opts.query = uri.query;
  } else if (opts.host) {
    opts.hostname = parseuri(opts.host).host;
  }

  this.secure = null != opts.secure ? opts.secure
    : (global.location && 'https:' === location.protocol);

  if (opts.hostname && !opts.port) {
    // if no port is specified manually, use the protocol default
    opts.port = this.secure ? '443' : '80';
  }

  this.agent = opts.agent || false;
  this.hostname = opts.hostname ||
    (global.location ? location.hostname : 'localhost');
  this.port = opts.port || (global.location && location.port
      ? location.port
      : (this.secure ? 443 : 80));
  this.query = opts.query || {};
  if ('string' === typeof this.query) this.query = parseqs.decode(this.query);
  this.upgrade = false !== opts.upgrade;
  this.path = (opts.path || '/engine.io').replace(/\/$/, '') + '/';
  this.forceJSONP = !!opts.forceJSONP;
  this.jsonp = false !== opts.jsonp;
  this.forceBase64 = !!opts.forceBase64;
  this.enablesXDR = !!opts.enablesXDR;
  this.timestampParam = opts.timestampParam || 't';
  this.timestampRequests = opts.timestampRequests;
  this.transports = opts.transports || ['polling', 'websocket'];
  this.transportOptions = opts.transportOptions || {};
  this.readyState = '';
  this.writeBuffer = [];
  this.prevBufferLen = 0;
  this.policyPort = opts.policyPort || 843;
  this.rememberUpgrade = opts.rememberUpgrade || false;
  this.binaryType = null;
  this.onlyBinaryUpgrades = opts.onlyBinaryUpgrades;
  this.perMessageDeflate = false !== opts.perMessageDeflate ? (opts.perMessageDeflate || {}) : false;

  if (true === this.perMessageDeflate) this.perMessageDeflate = {};
  if (this.perMessageDeflate && null == this.perMessageDeflate.threshold) {
    this.perMessageDeflate.threshold = 1024;
  }

  // SSL options for Node.js client
  this.pfx = opts.pfx || null;
  this.key = opts.key || null;
  this.passphrase = opts.passphrase || null;
  this.cert = opts.cert || null;
  this.ca = opts.ca || null;
  this.ciphers = opts.ciphers || null;
  this.rejectUnauthorized = opts.rejectUnauthorized === undefined ? true : opts.rejectUnauthorized;
  this.forceNode = !!opts.forceNode;

  // other options for Node.js client
  var freeGlobal = typeof global === 'object' && global;
  if (freeGlobal.global === freeGlobal) {
    if (opts.extraHeaders && Object.keys(opts.extraHeaders).length > 0) {
      this.extraHeaders = opts.extraHeaders;
    }

    if (opts.localAddress) {
      this.localAddress = opts.localAddress;
    }
  }

  // set on handshake
  this.id = null;
  this.upgrades = null;
  this.pingInterval = null;
  this.pingTimeout = null;

  // set on heartbeat
  this.pingIntervalTimer = null;
  this.pingTimeoutTimer = null;

  this.open();
}

Socket.priorWebsocketSuccess = false;

/**
 * Mix in `Emitter`.
 */

Emitter(Socket.prototype);

/**
 * Protocol version.
 *
 * @api public
 */

Socket.protocol = parser.protocol; // this is an int

/**
 * Expose deps for legacy compatibility
 * and standalone browser access.
 */

Socket.Socket = Socket;
Socket.Transport = __webpack_require__(12);
Socket.transports = __webpack_require__(35);
Socket.parser = __webpack_require__(6);

/**
 * Creates transport of the given type.
 *
 * @param {String} transport name
 * @return {Transport}
 * @api private
 */

Socket.prototype.createTransport = function (name) {
  debug('creating transport "%s"', name);
  var query = clone(this.query);

  // append engine.io protocol identifier
  query.EIO = parser.protocol;

  // transport name
  query.transport = name;

  // per-transport options
  var options = this.transportOptions[name] || {};

  // session id if we already have one
  if (this.id) query.sid = this.id;

  var transport = new transports[name]({
    query: query,
    socket: this,
    agent: options.agent || this.agent,
    hostname: options.hostname || this.hostname,
    port: options.port || this.port,
    secure: options.secure || this.secure,
    path: options.path || this.path,
    forceJSONP: options.forceJSONP || this.forceJSONP,
    jsonp: options.jsonp || this.jsonp,
    forceBase64: options.forceBase64 || this.forceBase64,
    enablesXDR: options.enablesXDR || this.enablesXDR,
    timestampRequests: options.timestampRequests || this.timestampRequests,
    timestampParam: options.timestampParam || this.timestampParam,
    policyPort: options.policyPort || this.policyPort,
    pfx: options.pfx || this.pfx,
    key: options.key || this.key,
    passphrase: options.passphrase || this.passphrase,
    cert: options.cert || this.cert,
    ca: options.ca || this.ca,
    ciphers: options.ciphers || this.ciphers,
    rejectUnauthorized: options.rejectUnauthorized || this.rejectUnauthorized,
    perMessageDeflate: options.perMessageDeflate || this.perMessageDeflate,
    extraHeaders: options.extraHeaders || this.extraHeaders,
    forceNode: options.forceNode || this.forceNode,
    localAddress: options.localAddress || this.localAddress,
    requestTimeout: options.requestTimeout || this.requestTimeout,
    protocols: options.protocols || void (0)
  });

  return transport;
};

function clone (obj) {
  var o = {};
  for (var i in obj) {
    if (obj.hasOwnProperty(i)) {
      o[i] = obj[i];
    }
  }
  return o;
}

/**
 * Initializes transport to use and starts probe.
 *
 * @api private
 */
Socket.prototype.open = function () {
  var transport;
  if (this.rememberUpgrade && Socket.priorWebsocketSuccess && this.transports.indexOf('websocket') !== -1) {
    transport = 'websocket';
  } else if (0 === this.transports.length) {
    // Emit error on next tick so it can be listened to
    var self = this;
    setTimeout(function () {
      self.emit('error', 'No transports available');
    }, 0);
    return;
  } else {
    transport = this.transports[0];
  }
  this.readyState = 'opening';

  // Retry with the next transport if the transport is disabled (jsonp: false)
  try {
    transport = this.createTransport(transport);
  } catch (e) {
    this.transports.shift();
    this.open();
    return;
  }

  transport.open();
  this.setTransport(transport);
};

/**
 * Sets the current transport. Disables the existing one (if any).
 *
 * @api private
 */

Socket.prototype.setTransport = function (transport) {
  debug('setting transport %s', transport.name);
  var self = this;

  if (this.transport) {
    debug('clearing existing transport %s', this.transport.name);
    this.transport.removeAllListeners();
  }

  // set up transport
  this.transport = transport;

  // set up transport listeners
  transport
  .on('drain', function () {
    self.onDrain();
  })
  .on('packet', function (packet) {
    self.onPacket(packet);
  })
  .on('error', function (e) {
    self.onError(e);
  })
  .on('close', function () {
    self.onClose('transport close');
  });
};

/**
 * Probes a transport.
 *
 * @param {String} transport name
 * @api private
 */

Socket.prototype.probe = function (name) {
  debug('probing transport "%s"', name);
  var transport = this.createTransport(name, { probe: 1 });
  var failed = false;
  var self = this;

  Socket.priorWebsocketSuccess = false;

  function onTransportOpen () {
    if (self.onlyBinaryUpgrades) {
      var upgradeLosesBinary = !this.supportsBinary && self.transport.supportsBinary;
      failed = failed || upgradeLosesBinary;
    }
    if (failed) return;

    debug('probe transport "%s" opened', name);
    transport.send([{ type: 'ping', data: 'probe' }]);
    transport.once('packet', function (msg) {
      if (failed) return;
      if ('pong' === msg.type && 'probe' === msg.data) {
        debug('probe transport "%s" pong', name);
        self.upgrading = true;
        self.emit('upgrading', transport);
        if (!transport) return;
        Socket.priorWebsocketSuccess = 'websocket' === transport.name;

        debug('pausing current transport "%s"', self.transport.name);
        self.transport.pause(function () {
          if (failed) return;
          if ('closed' === self.readyState) return;
          debug('changing transport and sending upgrade packet');

          cleanup();

          self.setTransport(transport);
          transport.send([{ type: 'upgrade' }]);
          self.emit('upgrade', transport);
          transport = null;
          self.upgrading = false;
          self.flush();
        });
      } else {
        debug('probe transport "%s" failed', name);
        var err = new Error('probe error');
        err.transport = transport.name;
        self.emit('upgradeError', err);
      }
    });
  }

  function freezeTransport () {
    if (failed) return;

    // Any callback called by transport should be ignored since now
    failed = true;

    cleanup();

    transport.close();
    transport = null;
  }

  // Handle any error that happens while probing
  function onerror (err) {
    var error = new Error('probe error: ' + err);
    error.transport = transport.name;

    freezeTransport();

    debug('probe transport "%s" failed because of error: %s', name, err);

    self.emit('upgradeError', error);
  }

  function onTransportClose () {
    onerror('transport closed');
  }

  // When the socket is closed while we're probing
  function onclose () {
    onerror('socket closed');
  }

  // When the socket is upgraded while we're probing
  function onupgrade (to) {
    if (transport && to.name !== transport.name) {
      debug('"%s" works - aborting "%s"', to.name, transport.name);
      freezeTransport();
    }
  }

  // Remove all listeners on the transport and on self
  function cleanup () {
    transport.removeListener('open', onTransportOpen);
    transport.removeListener('error', onerror);
    transport.removeListener('close', onTransportClose);
    self.removeListener('close', onclose);
    self.removeListener('upgrading', onupgrade);
  }

  transport.once('open', onTransportOpen);
  transport.once('error', onerror);
  transport.once('close', onTransportClose);

  this.once('close', onclose);
  this.once('upgrading', onupgrade);

  transport.open();
};

/**
 * Called when connection is deemed open.
 *
 * @api public
 */

Socket.prototype.onOpen = function () {
  debug('socket open');
  this.readyState = 'open';
  Socket.priorWebsocketSuccess = 'websocket' === this.transport.name;
  this.emit('open');
  this.flush();

  // we check for `readyState` in case an `open`
  // listener already closed the socket
  if ('open' === this.readyState && this.upgrade && this.transport.pause) {
    debug('starting upgrade probes');
    for (var i = 0, l = this.upgrades.length; i < l; i++) {
      this.probe(this.upgrades[i]);
    }
  }
};

/**
 * Handles a packet.
 *
 * @api private
 */

Socket.prototype.onPacket = function (packet) {
  if ('opening' === this.readyState || 'open' === this.readyState ||
      'closing' === this.readyState) {
    debug('socket receive: type "%s", data "%s"', packet.type, packet.data);

    this.emit('packet', packet);

    // Socket is live - any packet counts
    this.emit('heartbeat');

    switch (packet.type) {
      case 'open':
        this.onHandshake(JSON.parse(packet.data));
        break;

      case 'pong':
        this.setPing();
        this.emit('pong');
        break;

      case 'error':
        var err = new Error('server error');
        err.code = packet.data;
        this.onError(err);
        break;

      case 'message':
        this.emit('data', packet.data);
        this.emit('message', packet.data);
        break;
    }
  } else {
    debug('packet received with socket readyState "%s"', this.readyState);
  }
};

/**
 * Called upon handshake completion.
 *
 * @param {Object} handshake obj
 * @api private
 */

Socket.prototype.onHandshake = function (data) {
  this.emit('handshake', data);
  this.id = data.sid;
  this.transport.query.sid = data.sid;
  this.upgrades = this.filterUpgrades(data.upgrades);
  this.pingInterval = data.pingInterval;
  this.pingTimeout = data.pingTimeout;
  this.onOpen();
  // In case open handler closes socket
  if ('closed' === this.readyState) return;
  this.setPing();

  // Prolong liveness of socket on heartbeat
  this.removeListener('heartbeat', this.onHeartbeat);
  this.on('heartbeat', this.onHeartbeat);
};

/**
 * Resets ping timeout.
 *
 * @api private
 */

Socket.prototype.onHeartbeat = function (timeout) {
  clearTimeout(this.pingTimeoutTimer);
  var self = this;
  self.pingTimeoutTimer = setTimeout(function () {
    if ('closed' === self.readyState) return;
    self.onClose('ping timeout');
  }, timeout || (self.pingInterval + self.pingTimeout));
};

/**
 * Pings server every `this.pingInterval` and expects response
 * within `this.pingTimeout` or closes connection.
 *
 * @api private
 */

Socket.prototype.setPing = function () {
  var self = this;
  clearTimeout(self.pingIntervalTimer);
  self.pingIntervalTimer = setTimeout(function () {
    debug('writing ping packet - expecting pong within %sms', self.pingTimeout);
    self.ping();
    self.onHeartbeat(self.pingTimeout);
  }, self.pingInterval);
};

/**
* Sends a ping packet.
*
* @api private
*/

Socket.prototype.ping = function () {
  var self = this;
  this.sendPacket('ping', function () {
    self.emit('ping');
  });
};

/**
 * Called on `drain` event
 *
 * @api private
 */

Socket.prototype.onDrain = function () {
  this.writeBuffer.splice(0, this.prevBufferLen);

  // setting prevBufferLen = 0 is very important
  // for example, when upgrading, upgrade packet is sent over,
  // and a nonzero prevBufferLen could cause problems on `drain`
  this.prevBufferLen = 0;

  if (0 === this.writeBuffer.length) {
    this.emit('drain');
  } else {
    this.flush();
  }
};

/**
 * Flush write buffers.
 *
 * @api private
 */

Socket.prototype.flush = function () {
  if ('closed' !== this.readyState && this.transport.writable &&
    !this.upgrading && this.writeBuffer.length) {
    debug('flushing %d packets in socket', this.writeBuffer.length);
    this.transport.send(this.writeBuffer);
    // keep track of current length of writeBuffer
    // splice writeBuffer and callbackBuffer on `drain`
    this.prevBufferLen = this.writeBuffer.length;
    this.emit('flush');
  }
};

/**
 * Sends a message.
 *
 * @param {String} message.
 * @param {Function} callback function.
 * @param {Object} options.
 * @return {Socket} for chaining.
 * @api public
 */

Socket.prototype.write =
Socket.prototype.send = function (msg, options, fn) {
  this.sendPacket('message', msg, options, fn);
  return this;
};

/**
 * Sends a packet.
 *
 * @param {String} packet type.
 * @param {String} data.
 * @param {Object} options.
 * @param {Function} callback function.
 * @api private
 */

Socket.prototype.sendPacket = function (type, data, options, fn) {
  if ('function' === typeof data) {
    fn = data;
    data = undefined;
  }

  if ('function' === typeof options) {
    fn = options;
    options = null;
  }

  if ('closing' === this.readyState || 'closed' === this.readyState) {
    return;
  }

  options = options || {};
  options.compress = false !== options.compress;

  var packet = {
    type: type,
    data: data,
    options: options
  };
  this.emit('packetCreate', packet);
  this.writeBuffer.push(packet);
  if (fn) this.once('flush', fn);
  this.flush();
};

/**
 * Closes the connection.
 *
 * @api private
 */

Socket.prototype.close = function () {
  if ('opening' === this.readyState || 'open' === this.readyState) {
    this.readyState = 'closing';

    var self = this;

    if (this.writeBuffer.length) {
      this.once('drain', function () {
        if (this.upgrading) {
          waitForUpgrade();
        } else {
          close();
        }
      });
    } else if (this.upgrading) {
      waitForUpgrade();
    } else {
      close();
    }
  }

  function close () {
    self.onClose('forced close');
    debug('socket closing - telling transport to close');
    self.transport.close();
  }

  function cleanupAndClose () {
    self.removeListener('upgrade', cleanupAndClose);
    self.removeListener('upgradeError', cleanupAndClose);
    close();
  }

  function waitForUpgrade () {
    // wait for upgrade to finish since we can't send packets while pausing a transport
    self.once('upgrade', cleanupAndClose);
    self.once('upgradeError', cleanupAndClose);
  }

  return this;
};

/**
 * Called upon transport error
 *
 * @api private
 */

Socket.prototype.onError = function (err) {
  debug('socket error %j', err);
  Socket.priorWebsocketSuccess = false;
  this.emit('error', err);
  this.onClose('transport error', err);
};

/**
 * Called upon transport close.
 *
 * @api private
 */

Socket.prototype.onClose = function (reason, desc) {
  if ('opening' === this.readyState || 'open' === this.readyState || 'closing' === this.readyState) {
    debug('socket close with reason: "%s"', reason);
    var self = this;

    // clear timers
    clearTimeout(this.pingIntervalTimer);
    clearTimeout(this.pingTimeoutTimer);

    // stop event from firing again for transport
    this.transport.removeAllListeners('close');

    // ensure transport won't stay open
    this.transport.close();

    // ignore further transport communication
    this.transport.removeAllListeners();

    // set ready state
    this.readyState = 'closed';

    // clear session id
    this.id = null;

    // emit close event
    this.emit('close', reason, desc);

    // clean buffers after, so users can still
    // grab the buffers on `close` event
    self.writeBuffer = [];
    self.prevBufferLen = 0;
  }
};

/**
 * Filters upgrades, returning only those matching client transports.
 *
 * @param {Array} server upgrades
 * @api private
 *
 */

Socket.prototype.filterUpgrades = function (upgrades) {
  var filteredUpgrades = [];
  for (var i = 0, j = upgrades.length; i < j; i++) {
    if (~index(this.transports, upgrades[i])) filteredUpgrades.push(upgrades[i]);
  }
  return filteredUpgrades;
};

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(0)))

/***/ }),
/* 53 */
/***/ (function(module, exports) {


/**
 * Module exports.
 *
 * Logic borrowed from Modernizr:
 *
 *   - https://github.com/Modernizr/Modernizr/blob/master/feature-detects/cors.js
 */

try {
  module.exports = typeof XMLHttpRequest !== 'undefined' &&
    'withCredentials' in new XMLHttpRequest();
} catch (err) {
  // if XMLHttp support is disabled in IE then it will throw
  // when trying to create
  module.exports = false;
}


/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {/**
 * Module requirements.
 */

var XMLHttpRequest = __webpack_require__(11);
var Polling = __webpack_require__(36);
var Emitter = __webpack_require__(5);
var inherit = __webpack_require__(9);
var debug = __webpack_require__(4)('engine.io-client:polling-xhr');

/**
 * Module exports.
 */

module.exports = XHR;
module.exports.Request = Request;

/**
 * Empty function
 */

function empty () {}

/**
 * XHR Polling constructor.
 *
 * @param {Object} opts
 * @api public
 */

function XHR (opts) {
  Polling.call(this, opts);
  this.requestTimeout = opts.requestTimeout;
  this.extraHeaders = opts.extraHeaders;

  if (global.location) {
    var isSSL = 'https:' === location.protocol;
    var port = location.port;

    // some user agents have empty `location.port`
    if (!port) {
      port = isSSL ? 443 : 80;
    }

    this.xd = opts.hostname !== global.location.hostname ||
      port !== opts.port;
    this.xs = opts.secure !== isSSL;
  }
}

/**
 * Inherits from Polling.
 */

inherit(XHR, Polling);

/**
 * XHR supports binary
 */

XHR.prototype.supportsBinary = true;

/**
 * Creates a request.
 *
 * @param {String} method
 * @api private
 */

XHR.prototype.request = function (opts) {
  opts = opts || {};
  opts.uri = this.uri();
  opts.xd = this.xd;
  opts.xs = this.xs;
  opts.agent = this.agent || false;
  opts.supportsBinary = this.supportsBinary;
  opts.enablesXDR = this.enablesXDR;

  // SSL options for Node.js client
  opts.pfx = this.pfx;
  opts.key = this.key;
  opts.passphrase = this.passphrase;
  opts.cert = this.cert;
  opts.ca = this.ca;
  opts.ciphers = this.ciphers;
  opts.rejectUnauthorized = this.rejectUnauthorized;
  opts.requestTimeout = this.requestTimeout;

  // other options for Node.js client
  opts.extraHeaders = this.extraHeaders;

  return new Request(opts);
};

/**
 * Sends data.
 *
 * @param {String} data to send.
 * @param {Function} called upon flush.
 * @api private
 */

XHR.prototype.doWrite = function (data, fn) {
  var isBinary = typeof data !== 'string' && data !== undefined;
  var req = this.request({ method: 'POST', data: data, isBinary: isBinary });
  var self = this;
  req.on('success', fn);
  req.on('error', function (err) {
    self.onError('xhr post error', err);
  });
  this.sendXhr = req;
};

/**
 * Starts a poll cycle.
 *
 * @api private
 */

XHR.prototype.doPoll = function () {
  debug('xhr poll');
  var req = this.request();
  var self = this;
  req.on('data', function (data) {
    self.onData(data);
  });
  req.on('error', function (err) {
    self.onError('xhr poll error', err);
  });
  this.pollXhr = req;
};

/**
 * Request constructor
 *
 * @param {Object} options
 * @api public
 */

function Request (opts) {
  this.method = opts.method || 'GET';
  this.uri = opts.uri;
  this.xd = !!opts.xd;
  this.xs = !!opts.xs;
  this.async = false !== opts.async;
  this.data = undefined !== opts.data ? opts.data : null;
  this.agent = opts.agent;
  this.isBinary = opts.isBinary;
  this.supportsBinary = opts.supportsBinary;
  this.enablesXDR = opts.enablesXDR;
  this.requestTimeout = opts.requestTimeout;

  // SSL options for Node.js client
  this.pfx = opts.pfx;
  this.key = opts.key;
  this.passphrase = opts.passphrase;
  this.cert = opts.cert;
  this.ca = opts.ca;
  this.ciphers = opts.ciphers;
  this.rejectUnauthorized = opts.rejectUnauthorized;

  // other options for Node.js client
  this.extraHeaders = opts.extraHeaders;

  this.create();
}

/**
 * Mix in `Emitter`.
 */

Emitter(Request.prototype);

/**
 * Creates the XHR object and sends the request.
 *
 * @api private
 */

Request.prototype.create = function () {
  var opts = { agent: this.agent, xdomain: this.xd, xscheme: this.xs, enablesXDR: this.enablesXDR };

  // SSL options for Node.js client
  opts.pfx = this.pfx;
  opts.key = this.key;
  opts.passphrase = this.passphrase;
  opts.cert = this.cert;
  opts.ca = this.ca;
  opts.ciphers = this.ciphers;
  opts.rejectUnauthorized = this.rejectUnauthorized;

  var xhr = this.xhr = new XMLHttpRequest(opts);
  var self = this;

  try {
    debug('xhr open %s: %s', this.method, this.uri);
    xhr.open(this.method, this.uri, this.async);
    try {
      if (this.extraHeaders) {
        xhr.setDisableHeaderCheck && xhr.setDisableHeaderCheck(true);
        for (var i in this.extraHeaders) {
          if (this.extraHeaders.hasOwnProperty(i)) {
            xhr.setRequestHeader(i, this.extraHeaders[i]);
          }
        }
      }
    } catch (e) {}

    if ('POST' === this.method) {
      try {
        if (this.isBinary) {
          xhr.setRequestHeader('Content-type', 'application/octet-stream');
        } else {
          xhr.setRequestHeader('Content-type', 'text/plain;charset=UTF-8');
        }
      } catch (e) {}
    }

    try {
      xhr.setRequestHeader('Accept', '*/*');
    } catch (e) {}

    // ie6 check
    if ('withCredentials' in xhr) {
      xhr.withCredentials = true;
    }

    if (this.requestTimeout) {
      xhr.timeout = this.requestTimeout;
    }

    if (this.hasXDR()) {
      xhr.onload = function () {
        self.onLoad();
      };
      xhr.onerror = function () {
        self.onError(xhr.responseText);
      };
    } else {
      xhr.onreadystatechange = function () {
        if (xhr.readyState === 2) {
          try {
            var contentType = xhr.getResponseHeader('Content-Type');
            if (self.supportsBinary && contentType === 'application/octet-stream') {
              xhr.responseType = 'arraybuffer';
            }
          } catch (e) {}
        }
        if (4 !== xhr.readyState) return;
        if (200 === xhr.status || 1223 === xhr.status) {
          self.onLoad();
        } else {
          // make sure the `error` event handler that's user-set
          // does not throw in the same tick and gets caught here
          setTimeout(function () {
            self.onError(xhr.status);
          }, 0);
        }
      };
    }

    debug('xhr data %s', this.data);
    xhr.send(this.data);
  } catch (e) {
    // Need to defer since .create() is called directly fhrom the constructor
    // and thus the 'error' event can only be only bound *after* this exception
    // occurs.  Therefore, also, we cannot throw here at all.
    setTimeout(function () {
      self.onError(e);
    }, 0);
    return;
  }

  if (global.document) {
    this.index = Request.requestsCount++;
    Request.requests[this.index] = this;
  }
};

/**
 * Called upon successful response.
 *
 * @api private
 */

Request.prototype.onSuccess = function () {
  this.emit('success');
  this.cleanup();
};

/**
 * Called if we have data.
 *
 * @api private
 */

Request.prototype.onData = function (data) {
  this.emit('data', data);
  this.onSuccess();
};

/**
 * Called upon error.
 *
 * @api private
 */

Request.prototype.onError = function (err) {
  this.emit('error', err);
  this.cleanup(true);
};

/**
 * Cleans up house.
 *
 * @api private
 */

Request.prototype.cleanup = function (fromError) {
  if ('undefined' === typeof this.xhr || null === this.xhr) {
    return;
  }
  // xmlhttprequest
  if (this.hasXDR()) {
    this.xhr.onload = this.xhr.onerror = empty;
  } else {
    this.xhr.onreadystatechange = empty;
  }

  if (fromError) {
    try {
      this.xhr.abort();
    } catch (e) {}
  }

  if (global.document) {
    delete Request.requests[this.index];
  }

  this.xhr = null;
};

/**
 * Called upon load.
 *
 * @api private
 */

Request.prototype.onLoad = function () {
  var data;
  try {
    var contentType;
    try {
      contentType = this.xhr.getResponseHeader('Content-Type');
    } catch (e) {}
    if (contentType === 'application/octet-stream') {
      data = this.xhr.response || this.xhr.responseText;
    } else {
      data = this.xhr.responseText;
    }
  } catch (e) {
    this.onError(e);
  }
  if (null != data) {
    this.onData(data);
  }
};

/**
 * Check if it has XDomainRequest.
 *
 * @api private
 */

Request.prototype.hasXDR = function () {
  return 'undefined' !== typeof global.XDomainRequest && !this.xs && this.enablesXDR;
};

/**
 * Aborts the request.
 *
 * @api public
 */

Request.prototype.abort = function () {
  this.cleanup();
};

/**
 * Aborts pending requests when unloading the window. This is needed to prevent
 * memory leaks (e.g. when using IE) and to ensure that no spurious error is
 * emitted.
 */

Request.requestsCount = 0;
Request.requests = {};

if (global.document) {
  if (global.attachEvent) {
    global.attachEvent('onunload', unloadHandler);
  } else if (global.addEventListener) {
    global.addEventListener('beforeunload', unloadHandler, false);
  }
}

function unloadHandler () {
  for (var i in Request.requests) {
    if (Request.requests.hasOwnProperty(i)) {
      Request.requests[i].abort();
    }
  }
}

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(0)))

/***/ }),
/* 55 */
/***/ (function(module, exports) {


/**
 * Gets the keys for an object.
 *
 * @return {Array} keys
 * @api private
 */

module.exports = Object.keys || function keys (obj){
  var arr = [];
  var has = Object.prototype.hasOwnProperty;

  for (var i in obj) {
    if (has.call(obj, i)) {
      arr.push(i);
    }
  }
  return arr;
};


/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {/*!
 * The buffer module from node.js, for the browser.
 *
 * @author   Feross Aboukhadijeh <feross@feross.org> <http://feross.org>
 * @license  MIT
 */
/* eslint-disable no-proto */



var base64 = __webpack_require__(57)
var ieee754 = __webpack_require__(58)
var isArray = __webpack_require__(59)

exports.Buffer = Buffer
exports.SlowBuffer = SlowBuffer
exports.INSPECT_MAX_BYTES = 50

/**
 * If `Buffer.TYPED_ARRAY_SUPPORT`:
 *   === true    Use Uint8Array implementation (fastest)
 *   === false   Use Object implementation (most compatible, even IE6)
 *
 * Browsers that support typed arrays are IE 10+, Firefox 4+, Chrome 7+, Safari 5.1+,
 * Opera 11.6+, iOS 4.2+.
 *
 * Due to various browser bugs, sometimes the Object implementation will be used even
 * when the browser supports typed arrays.
 *
 * Note:
 *
 *   - Firefox 4-29 lacks support for adding new properties to `Uint8Array` instances,
 *     See: https://bugzilla.mozilla.org/show_bug.cgi?id=695438.
 *
 *   - Chrome 9-10 is missing the `TypedArray.prototype.subarray` function.
 *
 *   - IE10 has a broken `TypedArray.prototype.subarray` function which returns arrays of
 *     incorrect length in some situations.

 * We detect these buggy browsers and set `Buffer.TYPED_ARRAY_SUPPORT` to `false` so they
 * get the Object implementation, which is slower but behaves correctly.
 */
Buffer.TYPED_ARRAY_SUPPORT = global.TYPED_ARRAY_SUPPORT !== undefined
  ? global.TYPED_ARRAY_SUPPORT
  : typedArraySupport()

/*
 * Export kMaxLength after typed array support is determined.
 */
exports.kMaxLength = kMaxLength()

function typedArraySupport () {
  try {
    var arr = new Uint8Array(1)
    arr.__proto__ = {__proto__: Uint8Array.prototype, foo: function () { return 42 }}
    return arr.foo() === 42 && // typed array instances can be augmented
        typeof arr.subarray === 'function' && // chrome 9-10 lack `subarray`
        arr.subarray(1, 1).byteLength === 0 // ie10 has broken `subarray`
  } catch (e) {
    return false
  }
}

function kMaxLength () {
  return Buffer.TYPED_ARRAY_SUPPORT
    ? 0x7fffffff
    : 0x3fffffff
}

function createBuffer (that, length) {
  if (kMaxLength() < length) {
    throw new RangeError('Invalid typed array length')
  }
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    // Return an augmented `Uint8Array` instance, for best performance
    that = new Uint8Array(length)
    that.__proto__ = Buffer.prototype
  } else {
    // Fallback: Return an object instance of the Buffer class
    if (that === null) {
      that = new Buffer(length)
    }
    that.length = length
  }

  return that
}

/**
 * The Buffer constructor returns instances of `Uint8Array` that have their
 * prototype changed to `Buffer.prototype`. Furthermore, `Buffer` is a subclass of
 * `Uint8Array`, so the returned instances will have all the node `Buffer` methods
 * and the `Uint8Array` methods. Square bracket notation works as expected -- it
 * returns a single octet.
 *
 * The `Uint8Array` prototype remains unmodified.
 */

function Buffer (arg, encodingOrOffset, length) {
  if (!Buffer.TYPED_ARRAY_SUPPORT && !(this instanceof Buffer)) {
    return new Buffer(arg, encodingOrOffset, length)
  }

  // Common case.
  if (typeof arg === 'number') {
    if (typeof encodingOrOffset === 'string') {
      throw new Error(
        'If encoding is specified then the first argument must be a string'
      )
    }
    return allocUnsafe(this, arg)
  }
  return from(this, arg, encodingOrOffset, length)
}

Buffer.poolSize = 8192 // not used by this implementation

// TODO: Legacy, not needed anymore. Remove in next major version.
Buffer._augment = function (arr) {
  arr.__proto__ = Buffer.prototype
  return arr
}

function from (that, value, encodingOrOffset, length) {
  if (typeof value === 'number') {
    throw new TypeError('"value" argument must not be a number')
  }

  if (typeof ArrayBuffer !== 'undefined' && value instanceof ArrayBuffer) {
    return fromArrayBuffer(that, value, encodingOrOffset, length)
  }

  if (typeof value === 'string') {
    return fromString(that, value, encodingOrOffset)
  }

  return fromObject(that, value)
}

/**
 * Functionally equivalent to Buffer(arg, encoding) but throws a TypeError
 * if value is a number.
 * Buffer.from(str[, encoding])
 * Buffer.from(array)
 * Buffer.from(buffer)
 * Buffer.from(arrayBuffer[, byteOffset[, length]])
 **/
Buffer.from = function (value, encodingOrOffset, length) {
  return from(null, value, encodingOrOffset, length)
}

if (Buffer.TYPED_ARRAY_SUPPORT) {
  Buffer.prototype.__proto__ = Uint8Array.prototype
  Buffer.__proto__ = Uint8Array
  if (typeof Symbol !== 'undefined' && Symbol.species &&
      Buffer[Symbol.species] === Buffer) {
    // Fix subarray() in ES2016. See: https://github.com/feross/buffer/pull/97
    Object.defineProperty(Buffer, Symbol.species, {
      value: null,
      configurable: true
    })
  }
}

function assertSize (size) {
  if (typeof size !== 'number') {
    throw new TypeError('"size" argument must be a number')
  } else if (size < 0) {
    throw new RangeError('"size" argument must not be negative')
  }
}

function alloc (that, size, fill, encoding) {
  assertSize(size)
  if (size <= 0) {
    return createBuffer(that, size)
  }
  if (fill !== undefined) {
    // Only pay attention to encoding if it's a string. This
    // prevents accidentally sending in a number that would
    // be interpretted as a start offset.
    return typeof encoding === 'string'
      ? createBuffer(that, size).fill(fill, encoding)
      : createBuffer(that, size).fill(fill)
  }
  return createBuffer(that, size)
}

/**
 * Creates a new filled Buffer instance.
 * alloc(size[, fill[, encoding]])
 **/
Buffer.alloc = function (size, fill, encoding) {
  return alloc(null, size, fill, encoding)
}

function allocUnsafe (that, size) {
  assertSize(size)
  that = createBuffer(that, size < 0 ? 0 : checked(size) | 0)
  if (!Buffer.TYPED_ARRAY_SUPPORT) {
    for (var i = 0; i < size; ++i) {
      that[i] = 0
    }
  }
  return that
}

/**
 * Equivalent to Buffer(num), by default creates a non-zero-filled Buffer instance.
 * */
Buffer.allocUnsafe = function (size) {
  return allocUnsafe(null, size)
}
/**
 * Equivalent to SlowBuffer(num), by default creates a non-zero-filled Buffer instance.
 */
Buffer.allocUnsafeSlow = function (size) {
  return allocUnsafe(null, size)
}

function fromString (that, string, encoding) {
  if (typeof encoding !== 'string' || encoding === '') {
    encoding = 'utf8'
  }

  if (!Buffer.isEncoding(encoding)) {
    throw new TypeError('"encoding" must be a valid string encoding')
  }

  var length = byteLength(string, encoding) | 0
  that = createBuffer(that, length)

  var actual = that.write(string, encoding)

  if (actual !== length) {
    // Writing a hex string, for example, that contains invalid characters will
    // cause everything after the first invalid character to be ignored. (e.g.
    // 'abxxcd' will be treated as 'ab')
    that = that.slice(0, actual)
  }

  return that
}

function fromArrayLike (that, array) {
  var length = array.length < 0 ? 0 : checked(array.length) | 0
  that = createBuffer(that, length)
  for (var i = 0; i < length; i += 1) {
    that[i] = array[i] & 255
  }
  return that
}

function fromArrayBuffer (that, array, byteOffset, length) {
  array.byteLength // this throws if `array` is not a valid ArrayBuffer

  if (byteOffset < 0 || array.byteLength < byteOffset) {
    throw new RangeError('\'offset\' is out of bounds')
  }

  if (array.byteLength < byteOffset + (length || 0)) {
    throw new RangeError('\'length\' is out of bounds')
  }

  if (byteOffset === undefined && length === undefined) {
    array = new Uint8Array(array)
  } else if (length === undefined) {
    array = new Uint8Array(array, byteOffset)
  } else {
    array = new Uint8Array(array, byteOffset, length)
  }

  if (Buffer.TYPED_ARRAY_SUPPORT) {
    // Return an augmented `Uint8Array` instance, for best performance
    that = array
    that.__proto__ = Buffer.prototype
  } else {
    // Fallback: Return an object instance of the Buffer class
    that = fromArrayLike(that, array)
  }
  return that
}

function fromObject (that, obj) {
  if (Buffer.isBuffer(obj)) {
    var len = checked(obj.length) | 0
    that = createBuffer(that, len)

    if (that.length === 0) {
      return that
    }

    obj.copy(that, 0, 0, len)
    return that
  }

  if (obj) {
    if ((typeof ArrayBuffer !== 'undefined' &&
        obj.buffer instanceof ArrayBuffer) || 'length' in obj) {
      if (typeof obj.length !== 'number' || isnan(obj.length)) {
        return createBuffer(that, 0)
      }
      return fromArrayLike(that, obj)
    }

    if (obj.type === 'Buffer' && isArray(obj.data)) {
      return fromArrayLike(that, obj.data)
    }
  }

  throw new TypeError('First argument must be a string, Buffer, ArrayBuffer, Array, or array-like object.')
}

function checked (length) {
  // Note: cannot use `length < kMaxLength()` here because that fails when
  // length is NaN (which is otherwise coerced to zero.)
  if (length >= kMaxLength()) {
    throw new RangeError('Attempt to allocate Buffer larger than maximum ' +
                         'size: 0x' + kMaxLength().toString(16) + ' bytes')
  }
  return length | 0
}

function SlowBuffer (length) {
  if (+length != length) { // eslint-disable-line eqeqeq
    length = 0
  }
  return Buffer.alloc(+length)
}

Buffer.isBuffer = function isBuffer (b) {
  return !!(b != null && b._isBuffer)
}

Buffer.compare = function compare (a, b) {
  if (!Buffer.isBuffer(a) || !Buffer.isBuffer(b)) {
    throw new TypeError('Arguments must be Buffers')
  }

  if (a === b) return 0

  var x = a.length
  var y = b.length

  for (var i = 0, len = Math.min(x, y); i < len; ++i) {
    if (a[i] !== b[i]) {
      x = a[i]
      y = b[i]
      break
    }
  }

  if (x < y) return -1
  if (y < x) return 1
  return 0
}

Buffer.isEncoding = function isEncoding (encoding) {
  switch (String(encoding).toLowerCase()) {
    case 'hex':
    case 'utf8':
    case 'utf-8':
    case 'ascii':
    case 'latin1':
    case 'binary':
    case 'base64':
    case 'ucs2':
    case 'ucs-2':
    case 'utf16le':
    case 'utf-16le':
      return true
    default:
      return false
  }
}

Buffer.concat = function concat (list, length) {
  if (!isArray(list)) {
    throw new TypeError('"list" argument must be an Array of Buffers')
  }

  if (list.length === 0) {
    return Buffer.alloc(0)
  }

  var i
  if (length === undefined) {
    length = 0
    for (i = 0; i < list.length; ++i) {
      length += list[i].length
    }
  }

  var buffer = Buffer.allocUnsafe(length)
  var pos = 0
  for (i = 0; i < list.length; ++i) {
    var buf = list[i]
    if (!Buffer.isBuffer(buf)) {
      throw new TypeError('"list" argument must be an Array of Buffers')
    }
    buf.copy(buffer, pos)
    pos += buf.length
  }
  return buffer
}

function byteLength (string, encoding) {
  if (Buffer.isBuffer(string)) {
    return string.length
  }
  if (typeof ArrayBuffer !== 'undefined' && typeof ArrayBuffer.isView === 'function' &&
      (ArrayBuffer.isView(string) || string instanceof ArrayBuffer)) {
    return string.byteLength
  }
  if (typeof string !== 'string') {
    string = '' + string
  }

  var len = string.length
  if (len === 0) return 0

  // Use a for loop to avoid recursion
  var loweredCase = false
  for (;;) {
    switch (encoding) {
      case 'ascii':
      case 'latin1':
      case 'binary':
        return len
      case 'utf8':
      case 'utf-8':
      case undefined:
        return utf8ToBytes(string).length
      case 'ucs2':
      case 'ucs-2':
      case 'utf16le':
      case 'utf-16le':
        return len * 2
      case 'hex':
        return len >>> 1
      case 'base64':
        return base64ToBytes(string).length
      default:
        if (loweredCase) return utf8ToBytes(string).length // assume utf8
        encoding = ('' + encoding).toLowerCase()
        loweredCase = true
    }
  }
}
Buffer.byteLength = byteLength

function slowToString (encoding, start, end) {
  var loweredCase = false

  // No need to verify that "this.length <= MAX_UINT32" since it's a read-only
  // property of a typed array.

  // This behaves neither like String nor Uint8Array in that we set start/end
  // to their upper/lower bounds if the value passed is out of range.
  // undefined is handled specially as per ECMA-262 6th Edition,
  // Section 13.3.3.7 Runtime Semantics: KeyedBindingInitialization.
  if (start === undefined || start < 0) {
    start = 0
  }
  // Return early if start > this.length. Done here to prevent potential uint32
  // coercion fail below.
  if (start > this.length) {
    return ''
  }

  if (end === undefined || end > this.length) {
    end = this.length
  }

  if (end <= 0) {
    return ''
  }

  // Force coersion to uint32. This will also coerce falsey/NaN values to 0.
  end >>>= 0
  start >>>= 0

  if (end <= start) {
    return ''
  }

  if (!encoding) encoding = 'utf8'

  while (true) {
    switch (encoding) {
      case 'hex':
        return hexSlice(this, start, end)

      case 'utf8':
      case 'utf-8':
        return utf8Slice(this, start, end)

      case 'ascii':
        return asciiSlice(this, start, end)

      case 'latin1':
      case 'binary':
        return latin1Slice(this, start, end)

      case 'base64':
        return base64Slice(this, start, end)

      case 'ucs2':
      case 'ucs-2':
      case 'utf16le':
      case 'utf-16le':
        return utf16leSlice(this, start, end)

      default:
        if (loweredCase) throw new TypeError('Unknown encoding: ' + encoding)
        encoding = (encoding + '').toLowerCase()
        loweredCase = true
    }
  }
}

// The property is used by `Buffer.isBuffer` and `is-buffer` (in Safari 5-7) to detect
// Buffer instances.
Buffer.prototype._isBuffer = true

function swap (b, n, m) {
  var i = b[n]
  b[n] = b[m]
  b[m] = i
}

Buffer.prototype.swap16 = function swap16 () {
  var len = this.length
  if (len % 2 !== 0) {
    throw new RangeError('Buffer size must be a multiple of 16-bits')
  }
  for (var i = 0; i < len; i += 2) {
    swap(this, i, i + 1)
  }
  return this
}

Buffer.prototype.swap32 = function swap32 () {
  var len = this.length
  if (len % 4 !== 0) {
    throw new RangeError('Buffer size must be a multiple of 32-bits')
  }
  for (var i = 0; i < len; i += 4) {
    swap(this, i, i + 3)
    swap(this, i + 1, i + 2)
  }
  return this
}

Buffer.prototype.swap64 = function swap64 () {
  var len = this.length
  if (len % 8 !== 0) {
    throw new RangeError('Buffer size must be a multiple of 64-bits')
  }
  for (var i = 0; i < len; i += 8) {
    swap(this, i, i + 7)
    swap(this, i + 1, i + 6)
    swap(this, i + 2, i + 5)
    swap(this, i + 3, i + 4)
  }
  return this
}

Buffer.prototype.toString = function toString () {
  var length = this.length | 0
  if (length === 0) return ''
  if (arguments.length === 0) return utf8Slice(this, 0, length)
  return slowToString.apply(this, arguments)
}

Buffer.prototype.equals = function equals (b) {
  if (!Buffer.isBuffer(b)) throw new TypeError('Argument must be a Buffer')
  if (this === b) return true
  return Buffer.compare(this, b) === 0
}

Buffer.prototype.inspect = function inspect () {
  var str = ''
  var max = exports.INSPECT_MAX_BYTES
  if (this.length > 0) {
    str = this.toString('hex', 0, max).match(/.{2}/g).join(' ')
    if (this.length > max) str += ' ... '
  }
  return '<Buffer ' + str + '>'
}

Buffer.prototype.compare = function compare (target, start, end, thisStart, thisEnd) {
  if (!Buffer.isBuffer(target)) {
    throw new TypeError('Argument must be a Buffer')
  }

  if (start === undefined) {
    start = 0
  }
  if (end === undefined) {
    end = target ? target.length : 0
  }
  if (thisStart === undefined) {
    thisStart = 0
  }
  if (thisEnd === undefined) {
    thisEnd = this.length
  }

  if (start < 0 || end > target.length || thisStart < 0 || thisEnd > this.length) {
    throw new RangeError('out of range index')
  }

  if (thisStart >= thisEnd && start >= end) {
    return 0
  }
  if (thisStart >= thisEnd) {
    return -1
  }
  if (start >= end) {
    return 1
  }

  start >>>= 0
  end >>>= 0
  thisStart >>>= 0
  thisEnd >>>= 0

  if (this === target) return 0

  var x = thisEnd - thisStart
  var y = end - start
  var len = Math.min(x, y)

  var thisCopy = this.slice(thisStart, thisEnd)
  var targetCopy = target.slice(start, end)

  for (var i = 0; i < len; ++i) {
    if (thisCopy[i] !== targetCopy[i]) {
      x = thisCopy[i]
      y = targetCopy[i]
      break
    }
  }

  if (x < y) return -1
  if (y < x) return 1
  return 0
}

// Finds either the first index of `val` in `buffer` at offset >= `byteOffset`,
// OR the last index of `val` in `buffer` at offset <= `byteOffset`.
//
// Arguments:
// - buffer - a Buffer to search
// - val - a string, Buffer, or number
// - byteOffset - an index into `buffer`; will be clamped to an int32
// - encoding - an optional encoding, relevant is val is a string
// - dir - true for indexOf, false for lastIndexOf
function bidirectionalIndexOf (buffer, val, byteOffset, encoding, dir) {
  // Empty buffer means no match
  if (buffer.length === 0) return -1

  // Normalize byteOffset
  if (typeof byteOffset === 'string') {
    encoding = byteOffset
    byteOffset = 0
  } else if (byteOffset > 0x7fffffff) {
    byteOffset = 0x7fffffff
  } else if (byteOffset < -0x80000000) {
    byteOffset = -0x80000000
  }
  byteOffset = +byteOffset  // Coerce to Number.
  if (isNaN(byteOffset)) {
    // byteOffset: it it's undefined, null, NaN, "foo", etc, search whole buffer
    byteOffset = dir ? 0 : (buffer.length - 1)
  }

  // Normalize byteOffset: negative offsets start from the end of the buffer
  if (byteOffset < 0) byteOffset = buffer.length + byteOffset
  if (byteOffset >= buffer.length) {
    if (dir) return -1
    else byteOffset = buffer.length - 1
  } else if (byteOffset < 0) {
    if (dir) byteOffset = 0
    else return -1
  }

  // Normalize val
  if (typeof val === 'string') {
    val = Buffer.from(val, encoding)
  }

  // Finally, search either indexOf (if dir is true) or lastIndexOf
  if (Buffer.isBuffer(val)) {
    // Special case: looking for empty string/buffer always fails
    if (val.length === 0) {
      return -1
    }
    return arrayIndexOf(buffer, val, byteOffset, encoding, dir)
  } else if (typeof val === 'number') {
    val = val & 0xFF // Search for a byte value [0-255]
    if (Buffer.TYPED_ARRAY_SUPPORT &&
        typeof Uint8Array.prototype.indexOf === 'function') {
      if (dir) {
        return Uint8Array.prototype.indexOf.call(buffer, val, byteOffset)
      } else {
        return Uint8Array.prototype.lastIndexOf.call(buffer, val, byteOffset)
      }
    }
    return arrayIndexOf(buffer, [ val ], byteOffset, encoding, dir)
  }

  throw new TypeError('val must be string, number or Buffer')
}

function arrayIndexOf (arr, val, byteOffset, encoding, dir) {
  var indexSize = 1
  var arrLength = arr.length
  var valLength = val.length

  if (encoding !== undefined) {
    encoding = String(encoding).toLowerCase()
    if (encoding === 'ucs2' || encoding === 'ucs-2' ||
        encoding === 'utf16le' || encoding === 'utf-16le') {
      if (arr.length < 2 || val.length < 2) {
        return -1
      }
      indexSize = 2
      arrLength /= 2
      valLength /= 2
      byteOffset /= 2
    }
  }

  function read (buf, i) {
    if (indexSize === 1) {
      return buf[i]
    } else {
      return buf.readUInt16BE(i * indexSize)
    }
  }

  var i
  if (dir) {
    var foundIndex = -1
    for (i = byteOffset; i < arrLength; i++) {
      if (read(arr, i) === read(val, foundIndex === -1 ? 0 : i - foundIndex)) {
        if (foundIndex === -1) foundIndex = i
        if (i - foundIndex + 1 === valLength) return foundIndex * indexSize
      } else {
        if (foundIndex !== -1) i -= i - foundIndex
        foundIndex = -1
      }
    }
  } else {
    if (byteOffset + valLength > arrLength) byteOffset = arrLength - valLength
    for (i = byteOffset; i >= 0; i--) {
      var found = true
      for (var j = 0; j < valLength; j++) {
        if (read(arr, i + j) !== read(val, j)) {
          found = false
          break
        }
      }
      if (found) return i
    }
  }

  return -1
}

Buffer.prototype.includes = function includes (val, byteOffset, encoding) {
  return this.indexOf(val, byteOffset, encoding) !== -1
}

Buffer.prototype.indexOf = function indexOf (val, byteOffset, encoding) {
  return bidirectionalIndexOf(this, val, byteOffset, encoding, true)
}

Buffer.prototype.lastIndexOf = function lastIndexOf (val, byteOffset, encoding) {
  return bidirectionalIndexOf(this, val, byteOffset, encoding, false)
}

function hexWrite (buf, string, offset, length) {
  offset = Number(offset) || 0
  var remaining = buf.length - offset
  if (!length) {
    length = remaining
  } else {
    length = Number(length)
    if (length > remaining) {
      length = remaining
    }
  }

  // must be an even number of digits
  var strLen = string.length
  if (strLen % 2 !== 0) throw new TypeError('Invalid hex string')

  if (length > strLen / 2) {
    length = strLen / 2
  }
  for (var i = 0; i < length; ++i) {
    var parsed = parseInt(string.substr(i * 2, 2), 16)
    if (isNaN(parsed)) return i
    buf[offset + i] = parsed
  }
  return i
}

function utf8Write (buf, string, offset, length) {
  return blitBuffer(utf8ToBytes(string, buf.length - offset), buf, offset, length)
}

function asciiWrite (buf, string, offset, length) {
  return blitBuffer(asciiToBytes(string), buf, offset, length)
}

function latin1Write (buf, string, offset, length) {
  return asciiWrite(buf, string, offset, length)
}

function base64Write (buf, string, offset, length) {
  return blitBuffer(base64ToBytes(string), buf, offset, length)
}

function ucs2Write (buf, string, offset, length) {
  return blitBuffer(utf16leToBytes(string, buf.length - offset), buf, offset, length)
}

Buffer.prototype.write = function write (string, offset, length, encoding) {
  // Buffer#write(string)
  if (offset === undefined) {
    encoding = 'utf8'
    length = this.length
    offset = 0
  // Buffer#write(string, encoding)
  } else if (length === undefined && typeof offset === 'string') {
    encoding = offset
    length = this.length
    offset = 0
  // Buffer#write(string, offset[, length][, encoding])
  } else if (isFinite(offset)) {
    offset = offset | 0
    if (isFinite(length)) {
      length = length | 0
      if (encoding === undefined) encoding = 'utf8'
    } else {
      encoding = length
      length = undefined
    }
  // legacy write(string, encoding, offset, length) - remove in v0.13
  } else {
    throw new Error(
      'Buffer.write(string, encoding, offset[, length]) is no longer supported'
    )
  }

  var remaining = this.length - offset
  if (length === undefined || length > remaining) length = remaining

  if ((string.length > 0 && (length < 0 || offset < 0)) || offset > this.length) {
    throw new RangeError('Attempt to write outside buffer bounds')
  }

  if (!encoding) encoding = 'utf8'

  var loweredCase = false
  for (;;) {
    switch (encoding) {
      case 'hex':
        return hexWrite(this, string, offset, length)

      case 'utf8':
      case 'utf-8':
        return utf8Write(this, string, offset, length)

      case 'ascii':
        return asciiWrite(this, string, offset, length)

      case 'latin1':
      case 'binary':
        return latin1Write(this, string, offset, length)

      case 'base64':
        // Warning: maxLength not taken into account in base64Write
        return base64Write(this, string, offset, length)

      case 'ucs2':
      case 'ucs-2':
      case 'utf16le':
      case 'utf-16le':
        return ucs2Write(this, string, offset, length)

      default:
        if (loweredCase) throw new TypeError('Unknown encoding: ' + encoding)
        encoding = ('' + encoding).toLowerCase()
        loweredCase = true
    }
  }
}

Buffer.prototype.toJSON = function toJSON () {
  return {
    type: 'Buffer',
    data: Array.prototype.slice.call(this._arr || this, 0)
  }
}

function base64Slice (buf, start, end) {
  if (start === 0 && end === buf.length) {
    return base64.fromByteArray(buf)
  } else {
    return base64.fromByteArray(buf.slice(start, end))
  }
}

function utf8Slice (buf, start, end) {
  end = Math.min(buf.length, end)
  var res = []

  var i = start
  while (i < end) {
    var firstByte = buf[i]
    var codePoint = null
    var bytesPerSequence = (firstByte > 0xEF) ? 4
      : (firstByte > 0xDF) ? 3
      : (firstByte > 0xBF) ? 2
      : 1

    if (i + bytesPerSequence <= end) {
      var secondByte, thirdByte, fourthByte, tempCodePoint

      switch (bytesPerSequence) {
        case 1:
          if (firstByte < 0x80) {
            codePoint = firstByte
          }
          break
        case 2:
          secondByte = buf[i + 1]
          if ((secondByte & 0xC0) === 0x80) {
            tempCodePoint = (firstByte & 0x1F) << 0x6 | (secondByte & 0x3F)
            if (tempCodePoint > 0x7F) {
              codePoint = tempCodePoint
            }
          }
          break
        case 3:
          secondByte = buf[i + 1]
          thirdByte = buf[i + 2]
          if ((secondByte & 0xC0) === 0x80 && (thirdByte & 0xC0) === 0x80) {
            tempCodePoint = (firstByte & 0xF) << 0xC | (secondByte & 0x3F) << 0x6 | (thirdByte & 0x3F)
            if (tempCodePoint > 0x7FF && (tempCodePoint < 0xD800 || tempCodePoint > 0xDFFF)) {
              codePoint = tempCodePoint
            }
          }
          break
        case 4:
          secondByte = buf[i + 1]
          thirdByte = buf[i + 2]
          fourthByte = buf[i + 3]
          if ((secondByte & 0xC0) === 0x80 && (thirdByte & 0xC0) === 0x80 && (fourthByte & 0xC0) === 0x80) {
            tempCodePoint = (firstByte & 0xF) << 0x12 | (secondByte & 0x3F) << 0xC | (thirdByte & 0x3F) << 0x6 | (fourthByte & 0x3F)
            if (tempCodePoint > 0xFFFF && tempCodePoint < 0x110000) {
              codePoint = tempCodePoint
            }
          }
      }
    }

    if (codePoint === null) {
      // we did not generate a valid codePoint so insert a
      // replacement char (U+FFFD) and advance only 1 byte
      codePoint = 0xFFFD
      bytesPerSequence = 1
    } else if (codePoint > 0xFFFF) {
      // encode to utf16 (surrogate pair dance)
      codePoint -= 0x10000
      res.push(codePoint >>> 10 & 0x3FF | 0xD800)
      codePoint = 0xDC00 | codePoint & 0x3FF
    }

    res.push(codePoint)
    i += bytesPerSequence
  }

  return decodeCodePointsArray(res)
}

// Based on http://stackoverflow.com/a/22747272/680742, the browser with
// the lowest limit is Chrome, with 0x10000 args.
// We go 1 magnitude less, for safety
var MAX_ARGUMENTS_LENGTH = 0x1000

function decodeCodePointsArray (codePoints) {
  var len = codePoints.length
  if (len <= MAX_ARGUMENTS_LENGTH) {
    return String.fromCharCode.apply(String, codePoints) // avoid extra slice()
  }

  // Decode in chunks to avoid "call stack size exceeded".
  var res = ''
  var i = 0
  while (i < len) {
    res += String.fromCharCode.apply(
      String,
      codePoints.slice(i, i += MAX_ARGUMENTS_LENGTH)
    )
  }
  return res
}

function asciiSlice (buf, start, end) {
  var ret = ''
  end = Math.min(buf.length, end)

  for (var i = start; i < end; ++i) {
    ret += String.fromCharCode(buf[i] & 0x7F)
  }
  return ret
}

function latin1Slice (buf, start, end) {
  var ret = ''
  end = Math.min(buf.length, end)

  for (var i = start; i < end; ++i) {
    ret += String.fromCharCode(buf[i])
  }
  return ret
}

function hexSlice (buf, start, end) {
  var len = buf.length

  if (!start || start < 0) start = 0
  if (!end || end < 0 || end > len) end = len

  var out = ''
  for (var i = start; i < end; ++i) {
    out += toHex(buf[i])
  }
  return out
}

function utf16leSlice (buf, start, end) {
  var bytes = buf.slice(start, end)
  var res = ''
  for (var i = 0; i < bytes.length; i += 2) {
    res += String.fromCharCode(bytes[i] + bytes[i + 1] * 256)
  }
  return res
}

Buffer.prototype.slice = function slice (start, end) {
  var len = this.length
  start = ~~start
  end = end === undefined ? len : ~~end

  if (start < 0) {
    start += len
    if (start < 0) start = 0
  } else if (start > len) {
    start = len
  }

  if (end < 0) {
    end += len
    if (end < 0) end = 0
  } else if (end > len) {
    end = len
  }

  if (end < start) end = start

  var newBuf
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    newBuf = this.subarray(start, end)
    newBuf.__proto__ = Buffer.prototype
  } else {
    var sliceLen = end - start
    newBuf = new Buffer(sliceLen, undefined)
    for (var i = 0; i < sliceLen; ++i) {
      newBuf[i] = this[i + start]
    }
  }

  return newBuf
}

/*
 * Need to make sure that buffer isn't trying to write out of bounds.
 */
function checkOffset (offset, ext, length) {
  if ((offset % 1) !== 0 || offset < 0) throw new RangeError('offset is not uint')
  if (offset + ext > length) throw new RangeError('Trying to access beyond buffer length')
}

Buffer.prototype.readUIntLE = function readUIntLE (offset, byteLength, noAssert) {
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) checkOffset(offset, byteLength, this.length)

  var val = this[offset]
  var mul = 1
  var i = 0
  while (++i < byteLength && (mul *= 0x100)) {
    val += this[offset + i] * mul
  }

  return val
}

Buffer.prototype.readUIntBE = function readUIntBE (offset, byteLength, noAssert) {
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) {
    checkOffset(offset, byteLength, this.length)
  }

  var val = this[offset + --byteLength]
  var mul = 1
  while (byteLength > 0 && (mul *= 0x100)) {
    val += this[offset + --byteLength] * mul
  }

  return val
}

Buffer.prototype.readUInt8 = function readUInt8 (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 1, this.length)
  return this[offset]
}

Buffer.prototype.readUInt16LE = function readUInt16LE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 2, this.length)
  return this[offset] | (this[offset + 1] << 8)
}

Buffer.prototype.readUInt16BE = function readUInt16BE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 2, this.length)
  return (this[offset] << 8) | this[offset + 1]
}

Buffer.prototype.readUInt32LE = function readUInt32LE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)

  return ((this[offset]) |
      (this[offset + 1] << 8) |
      (this[offset + 2] << 16)) +
      (this[offset + 3] * 0x1000000)
}

Buffer.prototype.readUInt32BE = function readUInt32BE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)

  return (this[offset] * 0x1000000) +
    ((this[offset + 1] << 16) |
    (this[offset + 2] << 8) |
    this[offset + 3])
}

Buffer.prototype.readIntLE = function readIntLE (offset, byteLength, noAssert) {
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) checkOffset(offset, byteLength, this.length)

  var val = this[offset]
  var mul = 1
  var i = 0
  while (++i < byteLength && (mul *= 0x100)) {
    val += this[offset + i] * mul
  }
  mul *= 0x80

  if (val >= mul) val -= Math.pow(2, 8 * byteLength)

  return val
}

Buffer.prototype.readIntBE = function readIntBE (offset, byteLength, noAssert) {
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) checkOffset(offset, byteLength, this.length)

  var i = byteLength
  var mul = 1
  var val = this[offset + --i]
  while (i > 0 && (mul *= 0x100)) {
    val += this[offset + --i] * mul
  }
  mul *= 0x80

  if (val >= mul) val -= Math.pow(2, 8 * byteLength)

  return val
}

Buffer.prototype.readInt8 = function readInt8 (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 1, this.length)
  if (!(this[offset] & 0x80)) return (this[offset])
  return ((0xff - this[offset] + 1) * -1)
}

Buffer.prototype.readInt16LE = function readInt16LE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 2, this.length)
  var val = this[offset] | (this[offset + 1] << 8)
  return (val & 0x8000) ? val | 0xFFFF0000 : val
}

Buffer.prototype.readInt16BE = function readInt16BE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 2, this.length)
  var val = this[offset + 1] | (this[offset] << 8)
  return (val & 0x8000) ? val | 0xFFFF0000 : val
}

Buffer.prototype.readInt32LE = function readInt32LE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)

  return (this[offset]) |
    (this[offset + 1] << 8) |
    (this[offset + 2] << 16) |
    (this[offset + 3] << 24)
}

Buffer.prototype.readInt32BE = function readInt32BE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)

  return (this[offset] << 24) |
    (this[offset + 1] << 16) |
    (this[offset + 2] << 8) |
    (this[offset + 3])
}

Buffer.prototype.readFloatLE = function readFloatLE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)
  return ieee754.read(this, offset, true, 23, 4)
}

Buffer.prototype.readFloatBE = function readFloatBE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)
  return ieee754.read(this, offset, false, 23, 4)
}

Buffer.prototype.readDoubleLE = function readDoubleLE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 8, this.length)
  return ieee754.read(this, offset, true, 52, 8)
}

Buffer.prototype.readDoubleBE = function readDoubleBE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 8, this.length)
  return ieee754.read(this, offset, false, 52, 8)
}

function checkInt (buf, value, offset, ext, max, min) {
  if (!Buffer.isBuffer(buf)) throw new TypeError('"buffer" argument must be a Buffer instance')
  if (value > max || value < min) throw new RangeError('"value" argument is out of bounds')
  if (offset + ext > buf.length) throw new RangeError('Index out of range')
}

Buffer.prototype.writeUIntLE = function writeUIntLE (value, offset, byteLength, noAssert) {
  value = +value
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) {
    var maxBytes = Math.pow(2, 8 * byteLength) - 1
    checkInt(this, value, offset, byteLength, maxBytes, 0)
  }

  var mul = 1
  var i = 0
  this[offset] = value & 0xFF
  while (++i < byteLength && (mul *= 0x100)) {
    this[offset + i] = (value / mul) & 0xFF
  }

  return offset + byteLength
}

Buffer.prototype.writeUIntBE = function writeUIntBE (value, offset, byteLength, noAssert) {
  value = +value
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) {
    var maxBytes = Math.pow(2, 8 * byteLength) - 1
    checkInt(this, value, offset, byteLength, maxBytes, 0)
  }

  var i = byteLength - 1
  var mul = 1
  this[offset + i] = value & 0xFF
  while (--i >= 0 && (mul *= 0x100)) {
    this[offset + i] = (value / mul) & 0xFF
  }

  return offset + byteLength
}

Buffer.prototype.writeUInt8 = function writeUInt8 (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 1, 0xff, 0)
  if (!Buffer.TYPED_ARRAY_SUPPORT) value = Math.floor(value)
  this[offset] = (value & 0xff)
  return offset + 1
}

function objectWriteUInt16 (buf, value, offset, littleEndian) {
  if (value < 0) value = 0xffff + value + 1
  for (var i = 0, j = Math.min(buf.length - offset, 2); i < j; ++i) {
    buf[offset + i] = (value & (0xff << (8 * (littleEndian ? i : 1 - i)))) >>>
      (littleEndian ? i : 1 - i) * 8
  }
}

Buffer.prototype.writeUInt16LE = function writeUInt16LE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 2, 0xffff, 0)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value & 0xff)
    this[offset + 1] = (value >>> 8)
  } else {
    objectWriteUInt16(this, value, offset, true)
  }
  return offset + 2
}

Buffer.prototype.writeUInt16BE = function writeUInt16BE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 2, 0xffff, 0)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value >>> 8)
    this[offset + 1] = (value & 0xff)
  } else {
    objectWriteUInt16(this, value, offset, false)
  }
  return offset + 2
}

function objectWriteUInt32 (buf, value, offset, littleEndian) {
  if (value < 0) value = 0xffffffff + value + 1
  for (var i = 0, j = Math.min(buf.length - offset, 4); i < j; ++i) {
    buf[offset + i] = (value >>> (littleEndian ? i : 3 - i) * 8) & 0xff
  }
}

Buffer.prototype.writeUInt32LE = function writeUInt32LE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 4, 0xffffffff, 0)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset + 3] = (value >>> 24)
    this[offset + 2] = (value >>> 16)
    this[offset + 1] = (value >>> 8)
    this[offset] = (value & 0xff)
  } else {
    objectWriteUInt32(this, value, offset, true)
  }
  return offset + 4
}

Buffer.prototype.writeUInt32BE = function writeUInt32BE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 4, 0xffffffff, 0)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value >>> 24)
    this[offset + 1] = (value >>> 16)
    this[offset + 2] = (value >>> 8)
    this[offset + 3] = (value & 0xff)
  } else {
    objectWriteUInt32(this, value, offset, false)
  }
  return offset + 4
}

Buffer.prototype.writeIntLE = function writeIntLE (value, offset, byteLength, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) {
    var limit = Math.pow(2, 8 * byteLength - 1)

    checkInt(this, value, offset, byteLength, limit - 1, -limit)
  }

  var i = 0
  var mul = 1
  var sub = 0
  this[offset] = value & 0xFF
  while (++i < byteLength && (mul *= 0x100)) {
    if (value < 0 && sub === 0 && this[offset + i - 1] !== 0) {
      sub = 1
    }
    this[offset + i] = ((value / mul) >> 0) - sub & 0xFF
  }

  return offset + byteLength
}

Buffer.prototype.writeIntBE = function writeIntBE (value, offset, byteLength, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) {
    var limit = Math.pow(2, 8 * byteLength - 1)

    checkInt(this, value, offset, byteLength, limit - 1, -limit)
  }

  var i = byteLength - 1
  var mul = 1
  var sub = 0
  this[offset + i] = value & 0xFF
  while (--i >= 0 && (mul *= 0x100)) {
    if (value < 0 && sub === 0 && this[offset + i + 1] !== 0) {
      sub = 1
    }
    this[offset + i] = ((value / mul) >> 0) - sub & 0xFF
  }

  return offset + byteLength
}

Buffer.prototype.writeInt8 = function writeInt8 (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 1, 0x7f, -0x80)
  if (!Buffer.TYPED_ARRAY_SUPPORT) value = Math.floor(value)
  if (value < 0) value = 0xff + value + 1
  this[offset] = (value & 0xff)
  return offset + 1
}

Buffer.prototype.writeInt16LE = function writeInt16LE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 2, 0x7fff, -0x8000)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value & 0xff)
    this[offset + 1] = (value >>> 8)
  } else {
    objectWriteUInt16(this, value, offset, true)
  }
  return offset + 2
}

Buffer.prototype.writeInt16BE = function writeInt16BE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 2, 0x7fff, -0x8000)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value >>> 8)
    this[offset + 1] = (value & 0xff)
  } else {
    objectWriteUInt16(this, value, offset, false)
  }
  return offset + 2
}

Buffer.prototype.writeInt32LE = function writeInt32LE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value & 0xff)
    this[offset + 1] = (value >>> 8)
    this[offset + 2] = (value >>> 16)
    this[offset + 3] = (value >>> 24)
  } else {
    objectWriteUInt32(this, value, offset, true)
  }
  return offset + 4
}

Buffer.prototype.writeInt32BE = function writeInt32BE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000)
  if (value < 0) value = 0xffffffff + value + 1
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value >>> 24)
    this[offset + 1] = (value >>> 16)
    this[offset + 2] = (value >>> 8)
    this[offset + 3] = (value & 0xff)
  } else {
    objectWriteUInt32(this, value, offset, false)
  }
  return offset + 4
}

function checkIEEE754 (buf, value, offset, ext, max, min) {
  if (offset + ext > buf.length) throw new RangeError('Index out of range')
  if (offset < 0) throw new RangeError('Index out of range')
}

function writeFloat (buf, value, offset, littleEndian, noAssert) {
  if (!noAssert) {
    checkIEEE754(buf, value, offset, 4, 3.4028234663852886e+38, -3.4028234663852886e+38)
  }
  ieee754.write(buf, value, offset, littleEndian, 23, 4)
  return offset + 4
}

Buffer.prototype.writeFloatLE = function writeFloatLE (value, offset, noAssert) {
  return writeFloat(this, value, offset, true, noAssert)
}

Buffer.prototype.writeFloatBE = function writeFloatBE (value, offset, noAssert) {
  return writeFloat(this, value, offset, false, noAssert)
}

function writeDouble (buf, value, offset, littleEndian, noAssert) {
  if (!noAssert) {
    checkIEEE754(buf, value, offset, 8, 1.7976931348623157E+308, -1.7976931348623157E+308)
  }
  ieee754.write(buf, value, offset, littleEndian, 52, 8)
  return offset + 8
}

Buffer.prototype.writeDoubleLE = function writeDoubleLE (value, offset, noAssert) {
  return writeDouble(this, value, offset, true, noAssert)
}

Buffer.prototype.writeDoubleBE = function writeDoubleBE (value, offset, noAssert) {
  return writeDouble(this, value, offset, false, noAssert)
}

// copy(targetBuffer, targetStart=0, sourceStart=0, sourceEnd=buffer.length)
Buffer.prototype.copy = function copy (target, targetStart, start, end) {
  if (!start) start = 0
  if (!end && end !== 0) end = this.length
  if (targetStart >= target.length) targetStart = target.length
  if (!targetStart) targetStart = 0
  if (end > 0 && end < start) end = start

  // Copy 0 bytes; we're done
  if (end === start) return 0
  if (target.length === 0 || this.length === 0) return 0

  // Fatal error conditions
  if (targetStart < 0) {
    throw new RangeError('targetStart out of bounds')
  }
  if (start < 0 || start >= this.length) throw new RangeError('sourceStart out of bounds')
  if (end < 0) throw new RangeError('sourceEnd out of bounds')

  // Are we oob?
  if (end > this.length) end = this.length
  if (target.length - targetStart < end - start) {
    end = target.length - targetStart + start
  }

  var len = end - start
  var i

  if (this === target && start < targetStart && targetStart < end) {
    // descending copy from end
    for (i = len - 1; i >= 0; --i) {
      target[i + targetStart] = this[i + start]
    }
  } else if (len < 1000 || !Buffer.TYPED_ARRAY_SUPPORT) {
    // ascending copy from start
    for (i = 0; i < len; ++i) {
      target[i + targetStart] = this[i + start]
    }
  } else {
    Uint8Array.prototype.set.call(
      target,
      this.subarray(start, start + len),
      targetStart
    )
  }

  return len
}

// Usage:
//    buffer.fill(number[, offset[, end]])
//    buffer.fill(buffer[, offset[, end]])
//    buffer.fill(string[, offset[, end]][, encoding])
Buffer.prototype.fill = function fill (val, start, end, encoding) {
  // Handle string cases:
  if (typeof val === 'string') {
    if (typeof start === 'string') {
      encoding = start
      start = 0
      end = this.length
    } else if (typeof end === 'string') {
      encoding = end
      end = this.length
    }
    if (val.length === 1) {
      var code = val.charCodeAt(0)
      if (code < 256) {
        val = code
      }
    }
    if (encoding !== undefined && typeof encoding !== 'string') {
      throw new TypeError('encoding must be a string')
    }
    if (typeof encoding === 'string' && !Buffer.isEncoding(encoding)) {
      throw new TypeError('Unknown encoding: ' + encoding)
    }
  } else if (typeof val === 'number') {
    val = val & 255
  }

  // Invalid ranges are not set to a default, so can range check early.
  if (start < 0 || this.length < start || this.length < end) {
    throw new RangeError('Out of range index')
  }

  if (end <= start) {
    return this
  }

  start = start >>> 0
  end = end === undefined ? this.length : end >>> 0

  if (!val) val = 0

  var i
  if (typeof val === 'number') {
    for (i = start; i < end; ++i) {
      this[i] = val
    }
  } else {
    var bytes = Buffer.isBuffer(val)
      ? val
      : utf8ToBytes(new Buffer(val, encoding).toString())
    var len = bytes.length
    for (i = 0; i < end - start; ++i) {
      this[i + start] = bytes[i % len]
    }
  }

  return this
}

// HELPER FUNCTIONS
// ================

var INVALID_BASE64_RE = /[^+\/0-9A-Za-z-_]/g

function base64clean (str) {
  // Node strips out invalid characters like \n and \t from the string, base64-js does not
  str = stringtrim(str).replace(INVALID_BASE64_RE, '')
  // Node converts strings with length < 2 to ''
  if (str.length < 2) return ''
  // Node allows for non-padded base64 strings (missing trailing ===), base64-js does not
  while (str.length % 4 !== 0) {
    str = str + '='
  }
  return str
}

function stringtrim (str) {
  if (str.trim) return str.trim()
  return str.replace(/^\s+|\s+$/g, '')
}

function toHex (n) {
  if (n < 16) return '0' + n.toString(16)
  return n.toString(16)
}

function utf8ToBytes (string, units) {
  units = units || Infinity
  var codePoint
  var length = string.length
  var leadSurrogate = null
  var bytes = []

  for (var i = 0; i < length; ++i) {
    codePoint = string.charCodeAt(i)

    // is surrogate component
    if (codePoint > 0xD7FF && codePoint < 0xE000) {
      // last char was a lead
      if (!leadSurrogate) {
        // no lead yet
        if (codePoint > 0xDBFF) {
          // unexpected trail
          if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
          continue
        } else if (i + 1 === length) {
          // unpaired lead
          if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
          continue
        }

        // valid lead
        leadSurrogate = codePoint

        continue
      }

      // 2 leads in a row
      if (codePoint < 0xDC00) {
        if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
        leadSurrogate = codePoint
        continue
      }

      // valid surrogate pair
      codePoint = (leadSurrogate - 0xD800 << 10 | codePoint - 0xDC00) + 0x10000
    } else if (leadSurrogate) {
      // valid bmp char, but last char was a lead
      if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
    }

    leadSurrogate = null

    // encode utf8
    if (codePoint < 0x80) {
      if ((units -= 1) < 0) break
      bytes.push(codePoint)
    } else if (codePoint < 0x800) {
      if ((units -= 2) < 0) break
      bytes.push(
        codePoint >> 0x6 | 0xC0,
        codePoint & 0x3F | 0x80
      )
    } else if (codePoint < 0x10000) {
      if ((units -= 3) < 0) break
      bytes.push(
        codePoint >> 0xC | 0xE0,
        codePoint >> 0x6 & 0x3F | 0x80,
        codePoint & 0x3F | 0x80
      )
    } else if (codePoint < 0x110000) {
      if ((units -= 4) < 0) break
      bytes.push(
        codePoint >> 0x12 | 0xF0,
        codePoint >> 0xC & 0x3F | 0x80,
        codePoint >> 0x6 & 0x3F | 0x80,
        codePoint & 0x3F | 0x80
      )
    } else {
      throw new Error('Invalid code point')
    }
  }

  return bytes
}

function asciiToBytes (str) {
  var byteArray = []
  for (var i = 0; i < str.length; ++i) {
    // Node's code seems to be doing this and not & 0x7F..
    byteArray.push(str.charCodeAt(i) & 0xFF)
  }
  return byteArray
}

function utf16leToBytes (str, units) {
  var c, hi, lo
  var byteArray = []
  for (var i = 0; i < str.length; ++i) {
    if ((units -= 2) < 0) break

    c = str.charCodeAt(i)
    hi = c >> 8
    lo = c % 256
    byteArray.push(lo)
    byteArray.push(hi)
  }

  return byteArray
}

function base64ToBytes (str) {
  return base64.toByteArray(base64clean(str))
}

function blitBuffer (src, dst, offset, length) {
  for (var i = 0; i < length; ++i) {
    if ((i + offset >= dst.length) || (i >= src.length)) break
    dst[i + offset] = src[i]
  }
  return i
}

function isnan (val) {
  return val !== val // eslint-disable-line no-self-compare
}

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(0)))

/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.byteLength = byteLength
exports.toByteArray = toByteArray
exports.fromByteArray = fromByteArray

var lookup = []
var revLookup = []
var Arr = typeof Uint8Array !== 'undefined' ? Uint8Array : Array

var code = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
for (var i = 0, len = code.length; i < len; ++i) {
  lookup[i] = code[i]
  revLookup[code.charCodeAt(i)] = i
}

// Support decoding URL-safe base64 strings, as Node.js does.
// See: https://en.wikipedia.org/wiki/Base64#URL_applications
revLookup['-'.charCodeAt(0)] = 62
revLookup['_'.charCodeAt(0)] = 63

function getLens (b64) {
  var len = b64.length

  if (len % 4 > 0) {
    throw new Error('Invalid string. Length must be a multiple of 4')
  }

  // Trim off extra bytes after placeholder bytes are found
  // See: https://github.com/beatgammit/base64-js/issues/42
  var validLen = b64.indexOf('=')
  if (validLen === -1) validLen = len

  var placeHoldersLen = validLen === len
    ? 0
    : 4 - (validLen % 4)

  return [validLen, placeHoldersLen]
}

// base64 is 4/3 + up to two characters of the original data
function byteLength (b64) {
  var lens = getLens(b64)
  var validLen = lens[0]
  var placeHoldersLen = lens[1]
  return ((validLen + placeHoldersLen) * 3 / 4) - placeHoldersLen
}

function _byteLength (b64, validLen, placeHoldersLen) {
  return ((validLen + placeHoldersLen) * 3 / 4) - placeHoldersLen
}

function toByteArray (b64) {
  var tmp
  var lens = getLens(b64)
  var validLen = lens[0]
  var placeHoldersLen = lens[1]

  var arr = new Arr(_byteLength(b64, validLen, placeHoldersLen))

  var curByte = 0

  // if there are placeholders, only get up to the last complete 4 chars
  var len = placeHoldersLen > 0
    ? validLen - 4
    : validLen

  for (var i = 0; i < len; i += 4) {
    tmp =
      (revLookup[b64.charCodeAt(i)] << 18) |
      (revLookup[b64.charCodeAt(i + 1)] << 12) |
      (revLookup[b64.charCodeAt(i + 2)] << 6) |
      revLookup[b64.charCodeAt(i + 3)]
    arr[curByte++] = (tmp >> 16) & 0xFF
    arr[curByte++] = (tmp >> 8) & 0xFF
    arr[curByte++] = tmp & 0xFF
  }

  if (placeHoldersLen === 2) {
    tmp =
      (revLookup[b64.charCodeAt(i)] << 2) |
      (revLookup[b64.charCodeAt(i + 1)] >> 4)
    arr[curByte++] = tmp & 0xFF
  }

  if (placeHoldersLen === 1) {
    tmp =
      (revLookup[b64.charCodeAt(i)] << 10) |
      (revLookup[b64.charCodeAt(i + 1)] << 4) |
      (revLookup[b64.charCodeAt(i + 2)] >> 2)
    arr[curByte++] = (tmp >> 8) & 0xFF
    arr[curByte++] = tmp & 0xFF
  }

  return arr
}

function tripletToBase64 (num) {
  return lookup[num >> 18 & 0x3F] +
    lookup[num >> 12 & 0x3F] +
    lookup[num >> 6 & 0x3F] +
    lookup[num & 0x3F]
}

function encodeChunk (uint8, start, end) {
  var tmp
  var output = []
  for (var i = start; i < end; i += 3) {
    tmp =
      ((uint8[i] << 16) & 0xFF0000) +
      ((uint8[i + 1] << 8) & 0xFF00) +
      (uint8[i + 2] & 0xFF)
    output.push(tripletToBase64(tmp))
  }
  return output.join('')
}

function fromByteArray (uint8) {
  var tmp
  var len = uint8.length
  var extraBytes = len % 3 // if we have 1 byte left, pad 2 bytes
  var parts = []
  var maxChunkLength = 16383 // must be multiple of 3

  // go through the array every three bytes, we'll deal with trailing stuff later
  for (var i = 0, len2 = len - extraBytes; i < len2; i += maxChunkLength) {
    parts.push(encodeChunk(
      uint8, i, (i + maxChunkLength) > len2 ? len2 : (i + maxChunkLength)
    ))
  }

  // pad the end with zeros, but make sure to not forget the extra bytes
  if (extraBytes === 1) {
    tmp = uint8[len - 1]
    parts.push(
      lookup[tmp >> 2] +
      lookup[(tmp << 4) & 0x3F] +
      '=='
    )
  } else if (extraBytes === 2) {
    tmp = (uint8[len - 2] << 8) + uint8[len - 1]
    parts.push(
      lookup[tmp >> 10] +
      lookup[(tmp >> 4) & 0x3F] +
      lookup[(tmp << 2) & 0x3F] +
      '='
    )
  }

  return parts.join('')
}


/***/ }),
/* 58 */
/***/ (function(module, exports) {

exports.read = function (buffer, offset, isLE, mLen, nBytes) {
  var e, m
  var eLen = (nBytes * 8) - mLen - 1
  var eMax = (1 << eLen) - 1
  var eBias = eMax >> 1
  var nBits = -7
  var i = isLE ? (nBytes - 1) : 0
  var d = isLE ? -1 : 1
  var s = buffer[offset + i]

  i += d

  e = s & ((1 << (-nBits)) - 1)
  s >>= (-nBits)
  nBits += eLen
  for (; nBits > 0; e = (e * 256) + buffer[offset + i], i += d, nBits -= 8) {}

  m = e & ((1 << (-nBits)) - 1)
  e >>= (-nBits)
  nBits += mLen
  for (; nBits > 0; m = (m * 256) + buffer[offset + i], i += d, nBits -= 8) {}

  if (e === 0) {
    e = 1 - eBias
  } else if (e === eMax) {
    return m ? NaN : ((s ? -1 : 1) * Infinity)
  } else {
    m = m + Math.pow(2, mLen)
    e = e - eBias
  }
  return (s ? -1 : 1) * m * Math.pow(2, e - mLen)
}

exports.write = function (buffer, value, offset, isLE, mLen, nBytes) {
  var e, m, c
  var eLen = (nBytes * 8) - mLen - 1
  var eMax = (1 << eLen) - 1
  var eBias = eMax >> 1
  var rt = (mLen === 23 ? Math.pow(2, -24) - Math.pow(2, -77) : 0)
  var i = isLE ? 0 : (nBytes - 1)
  var d = isLE ? 1 : -1
  var s = value < 0 || (value === 0 && 1 / value < 0) ? 1 : 0

  value = Math.abs(value)

  if (isNaN(value) || value === Infinity) {
    m = isNaN(value) ? 1 : 0
    e = eMax
  } else {
    e = Math.floor(Math.log(value) / Math.LN2)
    if (value * (c = Math.pow(2, -e)) < 1) {
      e--
      c *= 2
    }
    if (e + eBias >= 1) {
      value += rt / c
    } else {
      value += rt * Math.pow(2, 1 - eBias)
    }
    if (value * c >= 2) {
      e++
      c /= 2
    }

    if (e + eBias >= eMax) {
      m = 0
      e = eMax
    } else if (e + eBias >= 1) {
      m = ((value * c) - 1) * Math.pow(2, mLen)
      e = e + eBias
    } else {
      m = value * Math.pow(2, eBias - 1) * Math.pow(2, mLen)
      e = 0
    }
  }

  for (; mLen >= 8; buffer[offset + i] = m & 0xff, i += d, m /= 256, mLen -= 8) {}

  e = (e << mLen) | m
  eLen += mLen
  for (; eLen > 0; buffer[offset + i] = e & 0xff, i += d, e /= 256, eLen -= 8) {}

  buffer[offset + i - d] |= s * 128
}


/***/ }),
/* 59 */
/***/ (function(module, exports) {

var toString = {}.toString;

module.exports = Array.isArray || function (arr) {
  return toString.call(arr) == '[object Array]';
};


/***/ }),
/* 60 */
/***/ (function(module, exports) {

var toString = {}.toString;

module.exports = Array.isArray || function (arr) {
  return toString.call(arr) == '[object Array]';
};


/***/ }),
/* 61 */
/***/ (function(module, exports) {

/**
 * An abstraction for slicing an arraybuffer even when
 * ArrayBuffer.prototype.slice is not supported
 *
 * @api public
 */

module.exports = function(arraybuffer, start, end) {
  var bytes = arraybuffer.byteLength;
  start = start || 0;
  end = end || bytes;

  if (arraybuffer.slice) { return arraybuffer.slice(start, end); }

  if (start < 0) { start += bytes; }
  if (end < 0) { end += bytes; }
  if (end > bytes) { end = bytes; }

  if (start >= bytes || start >= end || bytes === 0) {
    return new ArrayBuffer(0);
  }

  var abv = new Uint8Array(arraybuffer);
  var result = new Uint8Array(end - start);
  for (var i = start, ii = 0; i < end; i++, ii++) {
    result[ii] = abv[i];
  }
  return result.buffer;
};


/***/ }),
/* 62 */
/***/ (function(module, exports) {

module.exports = after

function after(count, callback, err_cb) {
    var bail = false
    err_cb = err_cb || noop
    proxy.count = count

    return (count === 0) ? callback() : proxy

    function proxy(err, result) {
        if (proxy.count <= 0) {
            throw new Error('after called too many times')
        }
        --proxy.count

        // after first error, rest are passed to err_cb
        if (err) {
            bail = true
            callback(err)
            // future error callbacks will go to error handler
            callback = err_cb
        } else if (proxy.count === 0 && !bail) {
            callback(null, result)
        }
    }
}

function noop() {}


/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(module, global) {var __WEBPACK_AMD_DEFINE_RESULT__;/*! https://mths.be/utf8js v2.1.2 by @mathias */
;(function(root) {

	// Detect free variables `exports`
	var freeExports = typeof exports == 'object' && exports;

	// Detect free variable `module`
	var freeModule = typeof module == 'object' && module &&
		module.exports == freeExports && module;

	// Detect free variable `global`, from Node.js or Browserified code,
	// and use it as `root`
	var freeGlobal = typeof global == 'object' && global;
	if (freeGlobal.global === freeGlobal || freeGlobal.window === freeGlobal) {
		root = freeGlobal;
	}

	/*--------------------------------------------------------------------------*/

	var stringFromCharCode = String.fromCharCode;

	// Taken from https://mths.be/punycode
	function ucs2decode(string) {
		var output = [];
		var counter = 0;
		var length = string.length;
		var value;
		var extra;
		while (counter < length) {
			value = string.charCodeAt(counter++);
			if (value >= 0xD800 && value <= 0xDBFF && counter < length) {
				// high surrogate, and there is a next character
				extra = string.charCodeAt(counter++);
				if ((extra & 0xFC00) == 0xDC00) { // low surrogate
					output.push(((value & 0x3FF) << 10) + (extra & 0x3FF) + 0x10000);
				} else {
					// unmatched surrogate; only append this code unit, in case the next
					// code unit is the high surrogate of a surrogate pair
					output.push(value);
					counter--;
				}
			} else {
				output.push(value);
			}
		}
		return output;
	}

	// Taken from https://mths.be/punycode
	function ucs2encode(array) {
		var length = array.length;
		var index = -1;
		var value;
		var output = '';
		while (++index < length) {
			value = array[index];
			if (value > 0xFFFF) {
				value -= 0x10000;
				output += stringFromCharCode(value >>> 10 & 0x3FF | 0xD800);
				value = 0xDC00 | value & 0x3FF;
			}
			output += stringFromCharCode(value);
		}
		return output;
	}

	function checkScalarValue(codePoint, strict) {
		if (codePoint >= 0xD800 && codePoint <= 0xDFFF) {
			if (strict) {
				throw Error(
					'Lone surrogate U+' + codePoint.toString(16).toUpperCase() +
					' is not a scalar value'
				);
			}
			return false;
		}
		return true;
	}
	/*--------------------------------------------------------------------------*/

	function createByte(codePoint, shift) {
		return stringFromCharCode(((codePoint >> shift) & 0x3F) | 0x80);
	}

	function encodeCodePoint(codePoint, strict) {
		if ((codePoint & 0xFFFFFF80) == 0) { // 1-byte sequence
			return stringFromCharCode(codePoint);
		}
		var symbol = '';
		if ((codePoint & 0xFFFFF800) == 0) { // 2-byte sequence
			symbol = stringFromCharCode(((codePoint >> 6) & 0x1F) | 0xC0);
		}
		else if ((codePoint & 0xFFFF0000) == 0) { // 3-byte sequence
			if (!checkScalarValue(codePoint, strict)) {
				codePoint = 0xFFFD;
			}
			symbol = stringFromCharCode(((codePoint >> 12) & 0x0F) | 0xE0);
			symbol += createByte(codePoint, 6);
		}
		else if ((codePoint & 0xFFE00000) == 0) { // 4-byte sequence
			symbol = stringFromCharCode(((codePoint >> 18) & 0x07) | 0xF0);
			symbol += createByte(codePoint, 12);
			symbol += createByte(codePoint, 6);
		}
		symbol += stringFromCharCode((codePoint & 0x3F) | 0x80);
		return symbol;
	}

	function utf8encode(string, opts) {
		opts = opts || {};
		var strict = false !== opts.strict;

		var codePoints = ucs2decode(string);
		var length = codePoints.length;
		var index = -1;
		var codePoint;
		var byteString = '';
		while (++index < length) {
			codePoint = codePoints[index];
			byteString += encodeCodePoint(codePoint, strict);
		}
		return byteString;
	}

	/*--------------------------------------------------------------------------*/

	function readContinuationByte() {
		if (byteIndex >= byteCount) {
			throw Error('Invalid byte index');
		}

		var continuationByte = byteArray[byteIndex] & 0xFF;
		byteIndex++;

		if ((continuationByte & 0xC0) == 0x80) {
			return continuationByte & 0x3F;
		}

		// If we end up here, it’s not a continuation byte
		throw Error('Invalid continuation byte');
	}

	function decodeSymbol(strict) {
		var byte1;
		var byte2;
		var byte3;
		var byte4;
		var codePoint;

		if (byteIndex > byteCount) {
			throw Error('Invalid byte index');
		}

		if (byteIndex == byteCount) {
			return false;
		}

		// Read first byte
		byte1 = byteArray[byteIndex] & 0xFF;
		byteIndex++;

		// 1-byte sequence (no continuation bytes)
		if ((byte1 & 0x80) == 0) {
			return byte1;
		}

		// 2-byte sequence
		if ((byte1 & 0xE0) == 0xC0) {
			byte2 = readContinuationByte();
			codePoint = ((byte1 & 0x1F) << 6) | byte2;
			if (codePoint >= 0x80) {
				return codePoint;
			} else {
				throw Error('Invalid continuation byte');
			}
		}

		// 3-byte sequence (may include unpaired surrogates)
		if ((byte1 & 0xF0) == 0xE0) {
			byte2 = readContinuationByte();
			byte3 = readContinuationByte();
			codePoint = ((byte1 & 0x0F) << 12) | (byte2 << 6) | byte3;
			if (codePoint >= 0x0800) {
				return checkScalarValue(codePoint, strict) ? codePoint : 0xFFFD;
			} else {
				throw Error('Invalid continuation byte');
			}
		}

		// 4-byte sequence
		if ((byte1 & 0xF8) == 0xF0) {
			byte2 = readContinuationByte();
			byte3 = readContinuationByte();
			byte4 = readContinuationByte();
			codePoint = ((byte1 & 0x07) << 0x12) | (byte2 << 0x0C) |
				(byte3 << 0x06) | byte4;
			if (codePoint >= 0x010000 && codePoint <= 0x10FFFF) {
				return codePoint;
			}
		}

		throw Error('Invalid UTF-8 detected');
	}

	var byteArray;
	var byteCount;
	var byteIndex;
	function utf8decode(byteString, opts) {
		opts = opts || {};
		var strict = false !== opts.strict;

		byteArray = ucs2decode(byteString);
		byteCount = byteArray.length;
		byteIndex = 0;
		var codePoints = [];
		var tmp;
		while ((tmp = decodeSymbol(strict)) !== false) {
			codePoints.push(tmp);
		}
		return ucs2encode(codePoints);
	}

	/*--------------------------------------------------------------------------*/

	var utf8 = {
		'version': '2.1.2',
		'encode': utf8encode,
		'decode': utf8decode
	};

	// Some AMD build optimizers, like r.js, check for specific condition patterns
	// like the following:
	if (
		true
	) {
		!(__WEBPACK_AMD_DEFINE_RESULT__ = (function() {
			return utf8;
		}).call(exports, __webpack_require__, exports, module),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	}	else if (freeExports && !freeExports.nodeType) {
		if (freeModule) { // in Node.js or RingoJS v0.8.0+
			freeModule.exports = utf8;
		} else { // in Narwhal or RingoJS v0.7.0-
			var object = {};
			var hasOwnProperty = object.hasOwnProperty;
			for (var key in utf8) {
				hasOwnProperty.call(utf8, key) && (freeExports[key] = utf8[key]);
			}
		}
	} else { // in Rhino or a web browser
		root.utf8 = utf8;
	}

}(this));

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(15)(module), __webpack_require__(0)))

/***/ }),
/* 64 */
/***/ (function(module, exports) {

/*
 * base64-arraybuffer
 * https://github.com/niklasvh/base64-arraybuffer
 *
 * Copyright (c) 2012 Niklas von Hertzen
 * Licensed under the MIT license.
 */
(function(){
  "use strict";

  var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

  // Use a lookup table to find the index.
  var lookup = new Uint8Array(256);
  for (var i = 0; i < chars.length; i++) {
    lookup[chars.charCodeAt(i)] = i;
  }

  exports.encode = function(arraybuffer) {
    var bytes = new Uint8Array(arraybuffer),
    i, len = bytes.length, base64 = "";

    for (i = 0; i < len; i+=3) {
      base64 += chars[bytes[i] >> 2];
      base64 += chars[((bytes[i] & 3) << 4) | (bytes[i + 1] >> 4)];
      base64 += chars[((bytes[i + 1] & 15) << 2) | (bytes[i + 2] >> 6)];
      base64 += chars[bytes[i + 2] & 63];
    }

    if ((len % 3) === 2) {
      base64 = base64.substring(0, base64.length - 1) + "=";
    } else if (len % 3 === 1) {
      base64 = base64.substring(0, base64.length - 2) + "==";
    }

    return base64;
  };

  exports.decode =  function(base64) {
    var bufferLength = base64.length * 0.75,
    len = base64.length, i, p = 0,
    encoded1, encoded2, encoded3, encoded4;

    if (base64[base64.length - 1] === "=") {
      bufferLength--;
      if (base64[base64.length - 2] === "=") {
        bufferLength--;
      }
    }

    var arraybuffer = new ArrayBuffer(bufferLength),
    bytes = new Uint8Array(arraybuffer);

    for (i = 0; i < len; i+=4) {
      encoded1 = lookup[base64.charCodeAt(i)];
      encoded2 = lookup[base64.charCodeAt(i+1)];
      encoded3 = lookup[base64.charCodeAt(i+2)];
      encoded4 = lookup[base64.charCodeAt(i+3)];

      bytes[p++] = (encoded1 << 2) | (encoded2 >> 4);
      bytes[p++] = ((encoded2 & 15) << 4) | (encoded3 >> 2);
      bytes[p++] = ((encoded3 & 3) << 6) | (encoded4 & 63);
    }

    return arraybuffer;
  };
})();


/***/ }),
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {/**
 * Create a blob builder even when vendor prefixes exist
 */

var BlobBuilder = global.BlobBuilder
  || global.WebKitBlobBuilder
  || global.MSBlobBuilder
  || global.MozBlobBuilder;

/**
 * Check if Blob constructor is supported
 */

var blobSupported = (function() {
  try {
    var a = new Blob(['hi']);
    return a.size === 2;
  } catch(e) {
    return false;
  }
})();

/**
 * Check if Blob constructor supports ArrayBufferViews
 * Fails in Safari 6, so we need to map to ArrayBuffers there.
 */

var blobSupportsArrayBufferView = blobSupported && (function() {
  try {
    var b = new Blob([new Uint8Array([1,2])]);
    return b.size === 2;
  } catch(e) {
    return false;
  }
})();

/**
 * Check if BlobBuilder is supported
 */

var blobBuilderSupported = BlobBuilder
  && BlobBuilder.prototype.append
  && BlobBuilder.prototype.getBlob;

/**
 * Helper function that maps ArrayBufferViews to ArrayBuffers
 * Used by BlobBuilder constructor and old browsers that didn't
 * support it in the Blob constructor.
 */

function mapArrayBufferViews(ary) {
  for (var i = 0; i < ary.length; i++) {
    var chunk = ary[i];
    if (chunk.buffer instanceof ArrayBuffer) {
      var buf = chunk.buffer;

      // if this is a subarray, make a copy so we only
      // include the subarray region from the underlying buffer
      if (chunk.byteLength !== buf.byteLength) {
        var copy = new Uint8Array(chunk.byteLength);
        copy.set(new Uint8Array(buf, chunk.byteOffset, chunk.byteLength));
        buf = copy.buffer;
      }

      ary[i] = buf;
    }
  }
}

function BlobBuilderConstructor(ary, options) {
  options = options || {};

  var bb = new BlobBuilder();
  mapArrayBufferViews(ary);

  for (var i = 0; i < ary.length; i++) {
    bb.append(ary[i]);
  }

  return (options.type) ? bb.getBlob(options.type) : bb.getBlob();
};

function BlobConstructor(ary, options) {
  mapArrayBufferViews(ary);
  return new Blob(ary, options || {});
};

module.exports = (function() {
  if (blobSupported) {
    return blobSupportsArrayBufferView ? global.Blob : BlobConstructor;
  } else if (blobBuilderSupported) {
    return BlobBuilderConstructor;
  } else {
    return undefined;
  }
})();

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(0)))

/***/ }),
/* 66 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {
/**
 * Module requirements.
 */

var Polling = __webpack_require__(36);
var inherit = __webpack_require__(9);

/**
 * Module exports.
 */

module.exports = JSONPPolling;

/**
 * Cached regular expressions.
 */

var rNewline = /\n/g;
var rEscapedNewline = /\\n/g;

/**
 * Global JSONP callbacks.
 */

var callbacks;

/**
 * Noop.
 */

function empty () { }

/**
 * JSONP Polling constructor.
 *
 * @param {Object} opts.
 * @api public
 */

function JSONPPolling (opts) {
  Polling.call(this, opts);

  this.query = this.query || {};

  // define global callbacks array if not present
  // we do this here (lazily) to avoid unneeded global pollution
  if (!callbacks) {
    // we need to consider multiple engines in the same page
    if (!global.___eio) global.___eio = [];
    callbacks = global.___eio;
  }

  // callback identifier
  this.index = callbacks.length;

  // add callback to jsonp global
  var self = this;
  callbacks.push(function (msg) {
    self.onData(msg);
  });

  // append to query string
  this.query.j = this.index;

  // prevent spurious errors from being emitted when the window is unloaded
  if (global.document && global.addEventListener) {
    global.addEventListener('beforeunload', function () {
      if (self.script) self.script.onerror = empty;
    }, false);
  }
}

/**
 * Inherits from Polling.
 */

inherit(JSONPPolling, Polling);

/*
 * JSONP only supports binary as base64 encoded strings
 */

JSONPPolling.prototype.supportsBinary = false;

/**
 * Closes the socket.
 *
 * @api private
 */

JSONPPolling.prototype.doClose = function () {
  if (this.script) {
    this.script.parentNode.removeChild(this.script);
    this.script = null;
  }

  if (this.form) {
    this.form.parentNode.removeChild(this.form);
    this.form = null;
    this.iframe = null;
  }

  Polling.prototype.doClose.call(this);
};

/**
 * Starts a poll cycle.
 *
 * @api private
 */

JSONPPolling.prototype.doPoll = function () {
  var self = this;
  var script = document.createElement('script');

  if (this.script) {
    this.script.parentNode.removeChild(this.script);
    this.script = null;
  }

  script.async = true;
  script.src = this.uri();
  script.onerror = function (e) {
    self.onError('jsonp poll error', e);
  };

  var insertAt = document.getElementsByTagName('script')[0];
  if (insertAt) {
    insertAt.parentNode.insertBefore(script, insertAt);
  } else {
    (document.head || document.body).appendChild(script);
  }
  this.script = script;

  var isUAgecko = 'undefined' !== typeof navigator && /gecko/i.test(navigator.userAgent);

  if (isUAgecko) {
    setTimeout(function () {
      var iframe = document.createElement('iframe');
      document.body.appendChild(iframe);
      document.body.removeChild(iframe);
    }, 100);
  }
};

/**
 * Writes with a hidden iframe.
 *
 * @param {String} data to send
 * @param {Function} called upon flush.
 * @api private
 */

JSONPPolling.prototype.doWrite = function (data, fn) {
  var self = this;

  if (!this.form) {
    var form = document.createElement('form');
    var area = document.createElement('textarea');
    var id = this.iframeId = 'eio_iframe_' + this.index;
    var iframe;

    form.className = 'socketio';
    form.style.position = 'absolute';
    form.style.top = '-1000px';
    form.style.left = '-1000px';
    form.target = id;
    form.method = 'POST';
    form.setAttribute('accept-charset', 'utf-8');
    area.name = 'd';
    form.appendChild(area);
    document.body.appendChild(form);

    this.form = form;
    this.area = area;
  }

  this.form.action = this.uri();

  function complete () {
    initIframe();
    fn();
  }

  function initIframe () {
    if (self.iframe) {
      try {
        self.form.removeChild(self.iframe);
      } catch (e) {
        self.onError('jsonp polling iframe removal error', e);
      }
    }

    try {
      // ie6 dynamic iframes with target="" support (thanks Chris Lambacher)
      var html = '<iframe src="javascript:0" name="' + self.iframeId + '">';
      iframe = document.createElement(html);
    } catch (e) {
      iframe = document.createElement('iframe');
      iframe.name = self.iframeId;
      iframe.src = 'javascript:0';
    }

    iframe.id = self.iframeId;

    self.form.appendChild(iframe);
    self.iframe = iframe;
  }

  initIframe();

  // escape \n to prevent it from being converted into \r\n by some UAs
  // double escaping is required for escaped new lines because unescaping of new lines can be done safely on server-side
  data = data.replace(rEscapedNewline, '\\\n');
  this.area.value = data.replace(rNewline, '\\n');

  try {
    this.form.submit();
  } catch (e) {}

  if (this.iframe.attachEvent) {
    this.iframe.onreadystatechange = function () {
      if (self.iframe.readyState === 'complete') {
        complete();
      }
    };
  } else {
    this.iframe.onload = complete;
  }
};

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(0)))

/***/ }),
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {/**
 * Module dependencies.
 */

var Transport = __webpack_require__(12);
var parser = __webpack_require__(6);
var parseqs = __webpack_require__(8);
var inherit = __webpack_require__(9);
var yeast = __webpack_require__(38);
var debug = __webpack_require__(4)('engine.io-client:websocket');
var BrowserWebSocket = global.WebSocket || global.MozWebSocket;
var NodeWebSocket;
if (typeof window === 'undefined') {
  try {
    NodeWebSocket = __webpack_require__(68);
  } catch (e) { }
}

/**
 * Get either the `WebSocket` or `MozWebSocket` globals
 * in the browser or try to resolve WebSocket-compatible
 * interface exposed by `ws` for Node-like environment.
 */

var WebSocket = BrowserWebSocket;
if (!WebSocket && typeof window === 'undefined') {
  WebSocket = NodeWebSocket;
}

/**
 * Module exports.
 */

module.exports = WS;

/**
 * WebSocket transport constructor.
 *
 * @api {Object} connection options
 * @api public
 */

function WS (opts) {
  var forceBase64 = (opts && opts.forceBase64);
  if (forceBase64) {
    this.supportsBinary = false;
  }
  this.perMessageDeflate = opts.perMessageDeflate;
  this.usingBrowserWebSocket = BrowserWebSocket && !opts.forceNode;
  this.protocols = opts.protocols;
  if (!this.usingBrowserWebSocket) {
    WebSocket = NodeWebSocket;
  }
  Transport.call(this, opts);
}

/**
 * Inherits from Transport.
 */

inherit(WS, Transport);

/**
 * Transport name.
 *
 * @api public
 */

WS.prototype.name = 'websocket';

/*
 * WebSockets support binary
 */

WS.prototype.supportsBinary = true;

/**
 * Opens socket.
 *
 * @api private
 */

WS.prototype.doOpen = function () {
  if (!this.check()) {
    // let probe timeout
    return;
  }

  var uri = this.uri();
  var protocols = this.protocols;
  var opts = {
    agent: this.agent,
    perMessageDeflate: this.perMessageDeflate
  };

  // SSL options for Node.js client
  opts.pfx = this.pfx;
  opts.key = this.key;
  opts.passphrase = this.passphrase;
  opts.cert = this.cert;
  opts.ca = this.ca;
  opts.ciphers = this.ciphers;
  opts.rejectUnauthorized = this.rejectUnauthorized;
  if (this.extraHeaders) {
    opts.headers = this.extraHeaders;
  }
  if (this.localAddress) {
    opts.localAddress = this.localAddress;
  }

  try {
    this.ws = this.usingBrowserWebSocket ? (protocols ? new WebSocket(uri, protocols) : new WebSocket(uri)) : new WebSocket(uri, protocols, opts);
  } catch (err) {
    return this.emit('error', err);
  }

  if (this.ws.binaryType === undefined) {
    this.supportsBinary = false;
  }

  if (this.ws.supports && this.ws.supports.binary) {
    this.supportsBinary = true;
    this.ws.binaryType = 'nodebuffer';
  } else {
    this.ws.binaryType = 'arraybuffer';
  }

  this.addEventListeners();
};

/**
 * Adds event listeners to the socket
 *
 * @api private
 */

WS.prototype.addEventListeners = function () {
  var self = this;

  this.ws.onopen = function () {
    self.onOpen();
  };
  this.ws.onclose = function () {
    self.onClose();
  };
  this.ws.onmessage = function (ev) {
    self.onData(ev.data);
  };
  this.ws.onerror = function (e) {
    self.onError('websocket error', e);
  };
};

/**
 * Writes data to socket.
 *
 * @param {Array} array of packets.
 * @api private
 */

WS.prototype.write = function (packets) {
  var self = this;
  this.writable = false;

  // encodePacket efficient as it uses WS framing
  // no need for encodePayload
  var total = packets.length;
  for (var i = 0, l = total; i < l; i++) {
    (function (packet) {
      parser.encodePacket(packet, self.supportsBinary, function (data) {
        if (!self.usingBrowserWebSocket) {
          // always create a new object (GH-437)
          var opts = {};
          if (packet.options) {
            opts.compress = packet.options.compress;
          }

          if (self.perMessageDeflate) {
            var len = 'string' === typeof data ? global.Buffer.byteLength(data) : data.length;
            if (len < self.perMessageDeflate.threshold) {
              opts.compress = false;
            }
          }
        }

        // Sometimes the websocket has already been closed but the browser didn't
        // have a chance of informing us about it yet, in that case send will
        // throw an error
        try {
          if (self.usingBrowserWebSocket) {
            // TypeError is thrown when passing the second argument on Safari
            self.ws.send(data);
          } else {
            self.ws.send(data, opts);
          }
        } catch (e) {
          debug('websocket closed before onclose event');
        }

        --total || done();
      });
    })(packets[i]);
  }

  function done () {
    self.emit('flush');

    // fake drain
    // defer to next tick to allow Socket to clear writeBuffer
    setTimeout(function () {
      self.writable = true;
      self.emit('drain');
    }, 0);
  }
};

/**
 * Called upon close
 *
 * @api private
 */

WS.prototype.onClose = function () {
  Transport.prototype.onClose.call(this);
};

/**
 * Closes socket.
 *
 * @api private
 */

WS.prototype.doClose = function () {
  if (typeof this.ws !== 'undefined') {
    this.ws.close();
  }
};

/**
 * Generates uri for connection.
 *
 * @api private
 */

WS.prototype.uri = function () {
  var query = this.query || {};
  var schema = this.secure ? 'wss' : 'ws';
  var port = '';

  // avoid port if default for schema
  if (this.port && (('wss' === schema && Number(this.port) !== 443) ||
    ('ws' === schema && Number(this.port) !== 80))) {
    port = ':' + this.port;
  }

  // append timestamp to URI
  if (this.timestampRequests) {
    query[this.timestampParam] = yeast();
  }

  // communicate binary support capabilities
  if (!this.supportsBinary) {
    query.b64 = 1;
  }

  query = parseqs.encode(query);

  // prepend ? to query
  if (query.length) {
    query = '?' + query;
  }

  var ipv6 = this.hostname.indexOf(':') !== -1;
  return schema + '://' + (ipv6 ? '[' + this.hostname + ']' : this.hostname) + port + this.path + query;
};

/**
 * Feature detection for WebSocket.
 *
 * @return {Boolean} whether this transport is available.
 * @api public
 */

WS.prototype.check = function () {
  return !!WebSocket && !('__initialize' in WebSocket && this.name === WS.prototype.name);
};

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(0)))

/***/ }),
/* 68 */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),
/* 69 */
/***/ (function(module, exports) {

module.exports = toArray

function toArray(list, index) {
    var array = []

    index = index || 0

    for (var i = index || 0; i < list.length; i++) {
        array[i - index] = list[i]
    }

    return array
}


/***/ }),
/* 70 */
/***/ (function(module, exports) {


/**
 * Expose `Backoff`.
 */

module.exports = Backoff;

/**
 * Initialize backoff timer with `opts`.
 *
 * - `min` initial timeout in milliseconds [100]
 * - `max` max timeout [10000]
 * - `jitter` [0]
 * - `factor` [2]
 *
 * @param {Object} opts
 * @api public
 */

function Backoff(opts) {
  opts = opts || {};
  this.ms = opts.min || 100;
  this.max = opts.max || 10000;
  this.factor = opts.factor || 2;
  this.jitter = opts.jitter > 0 && opts.jitter <= 1 ? opts.jitter : 0;
  this.attempts = 0;
}

/**
 * Return the backoff duration.
 *
 * @return {Number}
 * @api public
 */

Backoff.prototype.duration = function(){
  var ms = this.ms * Math.pow(this.factor, this.attempts++);
  if (this.jitter) {
    var rand =  Math.random();
    var deviation = Math.floor(rand * this.jitter * ms);
    ms = (Math.floor(rand * 10) & 1) == 0  ? ms - deviation : ms + deviation;
  }
  return Math.min(ms, this.max) | 0;
};

/**
 * Reset the number of attempts.
 *
 * @api public
 */

Backoff.prototype.reset = function(){
  this.attempts = 0;
};

/**
 * Set the minimum duration
 *
 * @api public
 */

Backoff.prototype.setMin = function(min){
  this.ms = min;
};

/**
 * Set the maximum duration
 *
 * @api public
 */

Backoff.prototype.setMax = function(max){
  this.max = max;
};

/**
 * Set the jitter
 *
 * @api public
 */

Backoff.prototype.setJitter = function(jitter){
  this.jitter = jitter;
};



/***/ }),
/* 71 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(72);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("60944238", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-03567f37\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./ModalWindowComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-03567f37\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./ModalWindowComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 72 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(1)(false);
// imports


// module
exports.push([module.i, "\n.contenedor_modal[data-v-03567f37] {\n  background: rgba(0, 0, 0, 0.6);\n  position: fixed;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  z-index: 300;\n}\n.modal[data-v-03567f37] {\n  display: block;\n  padding: 10px;\n  max-width: 500px;\n  max-height: 200px;\n  margin: 150px auto;\n  background: #b2faf6;\n  -webkit-box-shadow: 0 0 10px black;\n          box-shadow: 0 0 10px black;\n}\n.modal_header[data-v-03567f37] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-family: Roboto, sans-serif;\n  font-size: 1.2rem;\n}\n.modal_header[data-v-03567f37]:nth-child(1) {\n    margin-left: 10px;\n}\n.modal_header span i[data-v-03567f37] {\n    background: #ff2e1a;\n    color: rgba(231, 231, 231, 0.9);\n    border-radius: 4px;\n}\n.modal_content[data-v-03567f37] {\n  display: grid;\n  grid-template-rows: 105px 50px;\n  grid-template-areas: \"contenido\" \"acciones\";\n}\n.modal_content span[data-v-03567f37] {\n    padding: 10px;\n    text-align: start;\n    font-family: Roboto, sans-serif;\n    font-size: 0.9rem;\n}\n.acciones[data-v-03567f37] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  padding: 10px;\n  grid-area: acciones;\n  -webkit-box-pack: end;\n      -ms-flex-pack: end;\n          justify-content: flex-end;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.acciones button[data-v-03567f37] {\n    padding: 0 4px;\n    border-radius: 4px;\n    font-family: Roboto, sans-serif;\n    font-size: 0.8rem;\n}\n.acciones button[data-v-03567f37]:hover {\n      font-weight: 800;\n}\n.visible[data-v-03567f37] {\n  visibility: visible;\n}\n.hidden[data-v-03567f37] {\n  visibility: hidden;\n}\n", ""]);

// exports


/***/ }),
/* 73 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "modal-window-component",
  props: ["window", "title", "message", "idg"],
  data: function data() {
    return {
      c_window: null,
      c_title: null,
      c_content: null,
      c_partida: false,
      c_idg: null
    };
  },

  methods: {
    gotoGame: function gotoGame(event, idg) {
      if (idg === undefined) {
        idg = this.c_idg;
      }
      var url = location.protocol + "//" + location.hostname + (location.port ? ":" + location.port : "");
      window.location.href = url + "/scrabble/game/" + idg;
    }
    // listenForBroadcast: function() {
    //   var vm = this;
    //   var argumento;
    //   if (this.c_game) {
    //     argumento = "game." + this.c_game.id + ".user." + this.c_user.id;
    //     console.log(
    //       "Escuchando en Modal canales: user." +
    //         this.c_user.id +
    //         ".game." +
    //         this.c_game.id
    //     );
    //   } else {
    //     argumento = "user." + this.c_user.id;
    //     console.log("Escuchando en Modal canales: user." + this.c_user.id);
    //   }
    //   Echo.private(argumento)
    //     .listen("AcceptedChallenge", e => {
    //       console.log("En modal: Partida generada");
    //       vm.c_title = "Generada partida";
    //       vm.c_content =
    //         "Generada partida " +
    //         e.gameId +
    //         "\nIdioma: " +
    //         e.lang +
    //         "\nOponente\n\tid: " +
    //         e.oppoId +
    //         "\n\tNombre: " +
    //         e.oppoName +
    //         "\n\tPaís: " +
    //         e.oppoCountry;
    //       vm.c_partida = true;
    //       this.requestChallengeId = e.gameId;
    //       vm.c_window = true;
    //     })
    //     .listen("OpponentThrow", e => {
    //       //vm.c_game.state = e.state;
    //       if (e.playerState == "pass") {
    //         console.log("En modal: Pasa turno");
    //         vm.c_title = "El contrincante ha pasado turno.";
    //         vm.c_content = "Te toca. Es la hora de colocar tu palabra.";
    //       } else if (e.playerState == "win") {
    //         vm.c_title = "Fin de la partida";
    //         vm.c_content = "Lo siento, has perdido";
    //       } else if (e.playerState == "return") {
    //         alert("El contrincante ha devuelto fichas");
    //         console.log("El contrincante ha devuelto fichas");
    //       } else {
    //         vm.content = "";
    //         vm.c_title = "Recibida tirada de oponente";
    //         vm.c_content += "Palabra: " + e.word + "\n";
    //         vm.c_content += "Puntuación de palabra: " + e.wordScore + "\n";
    //         vm.c_content += "Puntuación de oponente: " + e.playerScore + "\n";
    //         vm.c_content += "Ahora te toca a ti. ¡¡¡Ánimo!!!";
    //       }
    //       vm.c_window = true;
    //     })
    //     .listen("GiveupGame", e => {
    //       //vm.c_game.state = e.state;
    //       vm.c_title = "Partida finalizada.";
    //       vm.content = "El contrincante ha abandonado el juego";
    //       vm.c_window = true;
    //     });
    // }
  },
  created: function created() {
    this.c_window = this.window;
    this.c_title = this.tittle;
    this.c_content = this.message;
    this.c_idg = this.idg;
    this.c_window = true;
    // this.listenForBroadcast();
  },
  mounted: function mounted() {
    console.log("Ventana modal montada");
    console.log(this.c_window);
  },
  updated: function updated() {
    this.c_window = this.window;
  }
});

/***/ }),
/* 74 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.c_window
    ? _c("div", { staticClass: "contenedor_modal" }, [
        _c("div", { staticClass: "modal" }, [
          _c("div", { staticClass: "modal_header" }, [
            _c("span", [_vm._v(_vm._s(_vm.c_title))]),
            _vm._v(" "),
            _c(
              "span",
              {
                on: {
                  click: function($event) {
                    _vm.c_window = false
                  }
                }
              },
              [_c("i", { staticClass: "far fa-window-close" })]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "modal_content" }, [
            _c("span", [
              _vm._v("\n        " + _vm._s(_vm.c_content) + "\n        "),
              _c(
                "button",
                {
                  class: _vm.c_partida ? "visible" : "hidden",
                  on: { click: _vm.gotoGame }
                },
                [_vm._v("Ir a partida")]
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "acciones" }, [
              _c(
                "button",
                {
                  on: {
                    click: function($event) {
                      _vm.c_window = false
                    }
                  }
                },
                [_vm._v("Aceptar")]
              )
            ])
          ])
        ])
      ])
    : _vm._e()
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-03567f37", module.exports)
  }
}

/***/ }),
/* 75 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(76)
}
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = null
/* template */
var __vue_template__ = __webpack_require__(78)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-8651e08a"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/FooterComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-8651e08a", Component.options)
  } else {
    hotAPI.reload("data-v-8651e08a", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 76 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(77);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("55b91fce", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-8651e08a\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./FooterComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-8651e08a\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./FooterComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 77 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(1)(false);
// imports


// module
exports.push([module.i, "\n.foot[data-v-8651e08a] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.foot ul[data-v-8651e08a] {\n    position: relative;\n    margin: 0;\n    padding: 0;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -ms-flex-wrap: wrap;\n        flex-wrap: wrap;\n}\n.foot ul li[data-v-8651e08a] {\n    position: relative;\n    list-style: none;\n    width: 60px;\n    height: 60px;\n    margin: 10px 30px;\n    -webkit-transform: rotate(-30deg) skew(25deg);\n            transform: rotate(-30deg) skew(25deg);\n    background: rgba(16, 16, 16, 0.7);\n}\n@media screen and (max-width: 866px) {\n.foot ul li[data-v-8651e08a] {\n        width: 40px;\n        height: 40px;\n}\n}\n@media screen and (max-width: 530px) {\n.foot ul li[data-v-8651e08a] {\n        width: 29px;\n        height: 29px;\n}\n}\n.foot ul li span[data-v-8651e08a] {\n    position: absolute;\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n    background: rgba(16, 16, 16, 0.7);\n    -webkit-transition: 0.5s;\n    transition: 0.5s;\n    display: -webkit-box !important;\n    display: -ms-flexbox !important;\n    display: flex !important;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    color: #fff;\n    font-size: 30px !important;\n}\n@media screen and (max-width: 530px) {\n.foot ul li span[data-v-8651e08a] {\n        font-size: 20px !important;\n}\n}\n.foot ul li:hover span[data-v-8651e08a] {\n    -webkit-box-shadow: -1px 1px 1px rgba(0, 0, 0, 0.1);\n            box-shadow: -1px 1px 1px rgba(0, 0, 0, 0.1);\n}\n.foot ul li:hover span[data-v-8651e08a]:nth-child(5) {\n    -webkit-transform: translate(40px, -40px);\n            transform: translate(40px, -40px);\n    opacity: 1;\n}\n.foot ul li:hover span[data-v-8651e08a]:nth-child(4) {\n    -webkit-transform: translate(30px, -30px);\n            transform: translate(30px, -30px);\n    opacity: 0.8;\n}\n.foot ul li:hover span[data-v-8651e08a]:nth-child(3) {\n    -webkit-transform: translate(20px, -20px);\n            transform: translate(20px, -20px);\n    opacity: 0.6;\n}\n.foot ul li:hover span[data-v-8651e08a]:nth-child(2) {\n    -webkit-transform: translate(10px, -10px);\n            transform: translate(10px, -10px);\n    opacity: 0.4;\n}\n.foot ul li:hover span[data-v-8651e08a]:nth-child(1) {\n    -webkit-transform: translate(0, 0);\n            transform: translate(0, 0);\n    opacity: 0.2;\n}\n.foot ul li:nth-child(1) span[data-v-8651e08a] {\n    background: #3b5999;\n}\n.foot ul li:nth-child(2) span[data-v-8651e08a] {\n    background: #55acee;\n}\n.foot ul li:nth-child(3) span[data-v-8651e08a] {\n    background: #dd4b39;\n}\n.foot ul li:nth-child(4) span[data-v-8651e08a] {\n    background: #0077b5;\n}\n.foot ul li:nth-child(5) span[data-v-8651e08a] {\n    background: #e4405f;\n}\n", ""]);

// exports


/***/ }),
/* 78 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "foot" }, [
      _c("ul", [
        _c("li", [
          _c(
            "a",
            { attrs: { href: "https://www.facebook.com", target: "_blanc" } },
            [
              _c("span"),
              _vm._v(" "),
              _c("span"),
              _vm._v(" "),
              _c("span"),
              _vm._v(" "),
              _c("span"),
              _vm._v(" "),
              _c("span", {
                staticClass: "fab fa-facebook",
                attrs: { "aria-hidden": "true" }
              })
            ]
          )
        ]),
        _vm._v(" "),
        _c("li", [
          _c("a", { attrs: { href: "http://twitter.es", target: "_blanc" } }, [
            _c("span"),
            _vm._v(" "),
            _c("span"),
            _vm._v(" "),
            _c("span"),
            _vm._v(" "),
            _c("span"),
            _vm._v(" "),
            _c("span", {
              staticClass: "fab fa-twitter",
              attrs: { "aria-hidden": "true" }
            })
          ])
        ]),
        _vm._v(" "),
        _c("li", [
          _c(
            "a",
            {
              attrs: { href: "https://www.plus.google.com", target: "_blanc" }
            },
            [
              _c("span"),
              _vm._v(" "),
              _c("span"),
              _vm._v(" "),
              _c("span"),
              _vm._v(" "),
              _c("span"),
              _vm._v(" "),
              _c("span", {
                staticClass: "fab fa-google-plus",
                attrs: { "aria-hidden": "true" }
              })
            ]
          )
        ]),
        _vm._v(" "),
        _c("li", [
          _c(
            "a",
            { attrs: { href: "https://www.linkedin.es", target: "_blanc" } },
            [
              _c("span"),
              _vm._v(" "),
              _c("span"),
              _vm._v(" "),
              _c("span"),
              _vm._v(" "),
              _c("span"),
              _vm._v(" "),
              _c("span", {
                staticClass: "fab fa-linkedin",
                attrs: { "aria-hidden": "true" }
              })
            ]
          )
        ]),
        _vm._v(" "),
        _c("li", [
          _c(
            "a",
            { attrs: { href: "https://www.instagram.com", target: "_blanc" } },
            [
              _c("span"),
              _vm._v(" "),
              _c("span"),
              _vm._v(" "),
              _c("span"),
              _vm._v(" "),
              _c("span"),
              _vm._v(" "),
              _c("span", {
                staticClass: "fab fa-instagram",
                attrs: { "aria-hidden": "true" }
              })
            ]
          )
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-8651e08a", module.exports)
  }
}

/***/ }),
/* 79 */,
/* 80 */,
/* 81 */,
/* 82 */,
/* 83 */,
/* 84 */,
/* 85 */,
/* 86 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(149)
}
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(151)
/* template */
var __vue_template__ = __webpack_require__(152)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/UploadImageComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-57c3577a", Component.options)
  } else {
    hotAPI.reload("data-v-57c3577a", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 87 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(165)
}
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(167)
/* template */
var __vue_template__ = __webpack_require__(172)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-77c36763"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/DashboardContenedorComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-77c36763", Component.options)
  } else {
    hotAPI.reload("data-v-77c36763", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 88 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(168)
}
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(170)
/* template */
var __vue_template__ = __webpack_require__(171)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-32e2715e"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/RankingUsuarioComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-32e2715e", Component.options)
  } else {
    hotAPI.reload("data-v-32e2715e", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 89 */,
/* 90 */,
/* 91 */,
/* 92 */,
/* 93 */,
/* 94 */,
/* 95 */,
/* 96 */,
/* 97 */,
/* 98 */,
/* 99 */,
/* 100 */,
/* 101 */,
/* 102 */,
/* 103 */,
/* 104 */,
/* 105 */,
/* 106 */,
/* 107 */,
/* 108 */,
/* 109 */,
/* 110 */,
/* 111 */,
/* 112 */,
/* 113 */,
/* 114 */,
/* 115 */,
/* 116 */,
/* 117 */,
/* 118 */,
/* 119 */,
/* 120 */,
/* 121 */,
/* 122 */,
/* 123 */,
/* 124 */,
/* 125 */,
/* 126 */,
/* 127 */,
/* 128 */,
/* 129 */,
/* 130 */,
/* 131 */,
/* 132 */,
/* 133 */,
/* 134 */,
/* 135 */,
/* 136 */,
/* 137 */,
/* 138 */,
/* 139 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(140);


/***/ }),
/* 140 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_laravel_echo__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_laravel_echo___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_laravel_echo__);
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


window.io = __webpack_require__(46);

// Have this in case you stop running your laravel echo server
if (typeof io !== 'undefined') {
  window.Echo = new __WEBPACK_IMPORTED_MODULE_0_laravel_echo___default.a({
    broadcaster: 'socket.io',
    host: window.location.hostname + ':6001'
  });
}

Vue.component('dashboard-test-component', __webpack_require__(141));
Vue.component('datos-usuario-component', __webpack_require__(155));
Vue.component('estadisticas-usuario-component', __webpack_require__(160));
Vue.component('dashboard-contenedor-component', __webpack_require__(87));
Vue.component('ranking-usuario-component', __webpack_require__(88));
Vue.component('dashboard-infochallenges-component', __webpack_require__(173));
Vue.component('modal-window-component', __webpack_require__(43));
Vue.component('footer-component', __webpack_require__(75));
Vue.filter('fechaES', function (value) {
  var fecha = value;
  fecha = fecha.substring(0, 10).split('-').reverse().join('/');
  return fecha;
});

Vue.filter('firstWord', function (value) {
  if (!value) {
    return '';
  } else {
    var userName = [];
    var name = '';
    if (value.indexOf(' ') != -1) {
      userName = value.split(' ');
      name = userName[0];
    } else {
      name = value;
    }
    return name;
  }
});

var register = new Vue({
  el: '#app'
});

/***/ }),
/* 141 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(142)
}
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(144)
/* template */
var __vue_template__ = __webpack_require__(154)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-f2b5f464"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/DashboardTestComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-f2b5f464", Component.options)
  } else {
    hotAPI.reload("data-v-f2b5f464", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 142 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(143);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("df15a40c", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-f2b5f464\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./DashboardTestComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-f2b5f464\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./DashboardTestComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 143 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(1)(false);
// imports


// module
exports.push([module.i, "\n#dash[data-v-f2b5f464] {\n  max-width: 100%;\n  display: grid;\n  grid-gap: 10px;\n  grid-template-columns: 1fr 0.5fr 6fr;\n}\n#dash > div[data-v-f2b5f464]:nth-child(1) {\n    grid-column: 1 / 4;\n}\n#dash > div[data-v-f2b5f464]:nth-child(3) {\n    grid-column: 2 / 4;\n}\n.sidebar[data-v-f2b5f464] {\n  grid-column: 1 / 3;\n}\n.variables-title[data-v-f2b5f464] {\n  font-size: 1.6em;\n  padding: 0.5em;\n  font-weight: 500;\n  margin-left: 15%;\n}\n", ""]);

// exports


/***/ }),
/* 144 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__UserDataComponent__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__UserDataComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__UserDataComponent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__CardContainerComponent__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__CardContainerComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__CardContainerComponent__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  name: "dashboard-test-component" /* que sea siempre compuesto con - para evitar colisiones con otros tag HTMHL5 */
  , props: {
    user: { required: true, type: Object },
    avatar: { required: true, type: String },
    variables: { required: true, type: Array }
  },
  data: function data() {
    return {
      c_user: null,
      c_avatar: null,
      c_variables: null,
      c_cards: [],
      requestChallengeId: null,
      name: ""
    };
  },

  components: {
    CardContainerComponent: __WEBPACK_IMPORTED_MODULE_1__CardContainerComponent___default.a,
    UserDataComponent: __WEBPACK_IMPORTED_MODULE_0__UserDataComponent___default.a
  },
  methods: {
    onAvatarChange: function onAvatarChange(image) {
      this.c_avatar = image;
    },
    gotoGame: function gotoGame(event, idg) {
      if (idg === undefined) {
        idg = this.requestChallengeId;
      }
      var url = location.protocol + "//" + location.hostname + (location.port ? ":" + location.port : "");
      window.location.href = url + "/scrabble/game/" + idg;
    },
    updateProfile: function updateProfile() {
      var vm = this;
      // modifica los datos del usuario.
      // el JSON de ejemplo incuye todos los posibles datos a modificar
      // podría sólo indicarse uno
      return axios.put("/scrabble/user/update", {
        name: "Fred",
        country: "UK",
        favourite_language: "ES",
        new_password: "1234567",
        new_password_confirmation: "1234567",
        old_password: "12345"
      }).then(function (response) {
        console.log("Datos de usuario:", response);
        vm.createCard("Update Profile", response.status, response.statusText, response.data);

        /* IMPORTANTE: en caso de hacer cambio de password el servidor
                     hace un logout y por lo tanto conviene volver a la página de login
                     siempre se ejecuta
                     uso replace para que la página actual desaparezca del historial
                     y evitar la tecla back
                       setTimeout(function () {
                          window.location.replace('/scrabble/login');
                      }, 2000);
                  */
      }).catch(function (error) {
        console.log("ERROR: " + error);
        vm.createCard("Update Profile", error.response.status, error.response.statusText, error.response.data);
      });
    },
    requestChallenge: function requestChallenge() {
      var vm = this;
      // solicita una partida en español
      return axios.post("/scrabble/challenge/request/es/level/-4").then(function (response) {
        console.log("Reto creado:", response);
        vm.createCard("Request Challenge", response.status, response.statusText, response.data);
      }).catch(function (error) {
        console.log("ERROR: " + error);
        vm.createCard("Request Challenge", error.response.status, error.response.statusText, error.response.data);
      });
    },
    deleteNotification: function deleteNotification() {
      var vm = this;
      // pone a borrado el estado de una notificación (id=3)
      return axios.put("/scrabble/notification/9/update/delete").then(function (response) {
        console.log("Borrado notificación:", response);
        vm.createCard("Update Notification", response.status, response.statusText, response.data);
      }).catch(function (error) {
        console.log("ERROR: " + error);
        vm.createCard("Update Notification", error.response.status, error.response.statusText, error.response.data);
      });
    },
    userGames: function userGames() {
      var vm = this;
      // partidas del usuario paginadas, 3 por página
      return axios.get("/scrabble/user/games/3").then(function (response) {
        console.log("Partidas de usuario:", response);
        vm.createCard("User Games", response.status, response.statusText, response.data);
      }).catch(function (error) {
        console.log("ERROR: " + error);
        vm.createCard("User Games", error.response.status, error.response.statusText, error.response.data);
      });
    },
    userGamesPag2: function userGamesPag2() {
      var vm = this;
      // partidas del usuario paginadas, 3 por página
      return axios.get("/scrabble/user/games/3?page=2").then(function (response) {
        console.log("Partidas de usuario:", response);
        vm.createCard("User Games", response.status, response.statusText, response.data);
      }).catch(function (error) {
        console.log("ERROR: " + error);
        vm.createCard("User Games", error.response.status, error.response.statusText, error.response.data);
      });
    },
    removeUser: function removeUser() {
      var vm = this;
      // estadísiticas via AJAX
      return axios.delete("/scrabble/user/remove").then(function (response) {
        console.log("Eliminar cuenta:", response);
        vm.createCard("Remove User", response.status, response.statusText, response.data);
      }).catch(function (error) {
        console.log("ERROR: " + error);
        vm.createCard("Remove User", error.response.status, error.response.statusText, error.response.data);
      }).then(function () {
        // siempre se ejecuta
        // uso replace para que la página actual desaparezca del historial
        // y evitar la tecla back
        setTimeout(function () {
          window.location.replace("/scrabble/login");
        }, 2000);
      });
    },
    userStatistics: function userStatistics() {
      var vm = this;
      // estadísiticas via AJAX
      return axios.get("/scrabble/user/statistics").then(function (response) {
        console.log("Estadísticas:", response);
        vm.createCard("User Statistics", response.status, response.statusText, response.data);
      }).catch(function (error) {
        console.log("ERROR: " + error);
        vm.createCard("User Statistics", error.response.status, error.response.statusText, error.response.data);
      });
    },
    giveupGame: function giveupGame() {
      var vm = this;
      // abandona la partida 3 (no se comprueba si es jugador de la partida o no)
      return axios.post("/scrabble/game/12/giveup").then(function (response) {
        console.log("Abandonando partida:", response);
        vm.createCard("Giveup Game", response.status, response.statusText, response.data);
      }).catch(function (error) {
        console.log("ERROR: " + error);
        vm.createCard("Giveup Game", error.response.status, error.response.statusText, error.response.data);
      });
    },
    removeAvatar: function removeAvatar(event) {
      //  event.preventDefault(); no hace falta ya que lo he puesto en la llamada
      this.c_avatar = null;
      var vm = this;
      return axios.post("/scrabble/user/avatar/remove").then(function (response) {
        console.log("Elimina avatar respuesta:", response);
        vm.createCard("Remove Avatar", response.status, response.statusText, response.data);
      }).catch(function (error) {
        console.log("ERROR: " + error);
        vm.createCard("Remove Avatar", error.response.status, error.response.statusText, error.response.data);
      });
    },
    listUsers: function listUsers() {
      var vm = this;
      // solicita la lista de jugadores
      return axios.get("/scrabble/search/user/live", {
        params: {
          name: vm.name
        }
      }).then(function (response) {
        console.log("Lista usuarios: ", response);
        vm.createCard("Remove User", response.status, response.statusText, response.data);
      }).catch(function (error) {
        console.log("ERROR: " + error);
        vm.createCard("Lista usuarios", error.response.status, error.response.statusText, error.response.data);
      });
    },
    createCard: function createCard(title, status, statusText, data) {
      var newcard = {
        order: this.c_cards.length + 1,
        type: title,
        errorCode: status,
        statusCode: statusText,
        output: data
      };

      this.c_cards.push(newcard);
    },
    listenForBroadcast: function listenForBroadcast() {
      var _this = this;

      console.log("Escuchando canales: user" + this.c_user.id);
      Echo.private("user." + this.c_user.id).listen("AcceptedChallenge", function (e) {
        alert("Generada partida " + e.gameId + "\nIdioma: " + e.lang + "\nOponente\n\tid: " + e.oppoId + "\n\tNombre: " + e.oppoName + "\n\tPaís: " + e.oppoCountry + "\n\tAvatar: " + e.oppoAvatar);

        _this.requestChallengeId = e.gameId;

        console.log("Recibido");
      });
    }
  },
  created: function created() {
    this.c_user = this.user;
    this.c_variables = this.variables;

    if (!this.avatar) this.c_avatar = null;else this.c_avatar = this.avatar;

    this.listenForBroadcast();
  },
  mounted: function mounted() {
    console.log("DashboardComponent montado.");
  }
});

/***/ }),
/* 145 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(146)
}
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(148)
/* template */
var __vue_template__ = __webpack_require__(153)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-4de2657e"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/UserDataComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4de2657e", Component.options)
  } else {
    hotAPI.reload("data-v-4de2657e", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 146 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(147);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("42bdb156", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4de2657e\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./UserDataComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4de2657e\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./UserDataComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 147 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(1)(false);
// imports


// module
exports.push([module.i, "\n.user-data[data-v-4de2657e] {\n  background-color: #27322c;\n  width: 96%;\n  margin: 0px 2%;\n  display: grid;\n  grid-template: repeat(2, 40px)/200px repeat(3, 1fr);\n  grid-column-gap: 10px;\n  font-size: 1.2em;\n}\n.user-data #upload-image[data-v-4de2657e] {\n    grid-column: 1 / 2;\n    grid-row: 1 / 3;\n}\n.user-data .info[data-v-4de2657e] {\n    line-height: 40px;\n}\n.user-data .value[data-v-4de2657e] {\n    color: #6f61aa;\n    padding-left: 0.4em;\n    font-weight: 700;\n}\n.user-data .data[data-v-4de2657e] {\n    padding-left: 0.4em;\n    text-align: left;\n}\n", ""]);

// exports


/***/ }),
/* 148 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__UploadImageComponent__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__UploadImageComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__UploadImageComponent__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: "user-data-component" /* que sea siempre compuesto con - para evitar colisiones con otros tag HTMHL5 */
  , props: ["user", "avatar"],
  components: {
    UploadImageComponent: __WEBPACK_IMPORTED_MODULE_0__UploadImageComponent___default.a
  },
  methods: {
    onAvatarChange: function onAvatarChange(image) {
      this.$emit("avatar-change", image);
    }
  },
  mounted: function mounted() {
    console.log("UserDataComponent montado.");
  }
});

/***/ }),
/* 149 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(150);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("2e155048", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-57c3577a\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./UploadImageComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-57c3577a\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./UploadImageComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 150 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(1)(false);
// imports


// module
exports.push([module.i, "\n#upload-image {\n  position: relative;\n  border-radius: 25%;\n  -webkit-box-shadow: 15px 15px 15px -5px white;\n          box-shadow: 15px 15px 15px -5px white;\n}\n#upload-image img {\n    max-width: 100%;\n    max-height: 100%;\n    margin-left: auto;\n    margin-right: auto;\n    border: 10px solid white;\n    display: block;\n    border-radius: 25%;\n}\n@media screen and (max-width: 414px) {\n#upload-image img {\n        width: 140px;\n}\n}\n.dropbox {\n  border: 2px dashed grey;\n  /* the dash box */\n  background: lightcyan;\n  color: dimgray;\n  padding: 9px 10px 2px;\n  position: absolute;\n  top: 0px;\n  cursor: pointer;\n  border-radius: 25%;\n  left: 50%;\n  -webkit-transform: translateX(-50%);\n          transform: translateX(-50%);\n  max-width: 266px;\n  max-height: 266px;\n  min-width: 76px;\n  min-height: 76px;\n  width: 100%;\n  height: 100%;\n}\n.dropbox p {\n    font-size: 0.8em;\n    text-align: center;\n    padding: 5px 0 5px 0;\n    text-indent: 0;\n}\n.dropbox:hover {\n    background: lightblue;\n    /* when mouse over to the drop zone, change color */\n}\n.input-file {\n  opacity: 0;\n  /* invisible but it's there! */\n  width: 100%;\n  height: 100%;\n  position: absolute;\n  top: 0px;\n  left: 0px;\n  cursor: pointer;\n}\n.opacityon {\n  opacity: 0.4;\n}\n", ""]);

// exports


/***/ }),
/* 151 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* La manera más natural de realizar la actualización de la imagen del avatar sería
   la de modificar la propiedad image de dentro del método upload/post/then
        vm.image = response.data.path;
 El problema es que esto no es aconsejable. Cambiar una propiedad (es decir, un dato que
 viene del padre) en el hijo, es posible ya que las propiedades son objetos mutables,
 pero, realizar el cambio de esta manera puede ser muy confuso.
 Es por tanto una mala práctica (incluso salta un warning en la consola)
 La solucion radica en que todo los cambios se realicen en el mismo componente,
 es decir, en este caso la eliminación del avatar debería lanzarse desde
 este componente.
 En nuestro caso vamos a seguir manteniendo la separación para aprender más cosas.
 La solución obvia sería crea una variable interna, por ejemplo imageToRender, que inicializariamos
 con el valor de image yque seria la que renderizariamos. A la hora de actualizar en upload/post/then
 hariamos algo como
    vm.imageToRender = response.data.path;
 En este caso el problema vendria al eliminar el avatar desde el menú. ya que necesitamos actualizar el
 valor de imageToRender a null cuando esto ocurra, es decir, cuando la variable image valga null de nuevo.
 Para eso podemos usar los métodos watch.
 Los métodos watch se utilizan para cambiar el valor de un dato o propiedad porque haya cambiado otro.
 El nombre de la función es el nombre de la propiedad que quiero vigilar si ha cambiado
 Desgraciadamente, en este caso es sólo una solución parcial, ya que si el componente se monta con
 image=null cuando se borra el avatar desde el padre se envia image=null.. es decir, no cambia, así que
 no salta la función. (sólo se detectan cambios de valor)
 Sólo saltría en el caso de que al montar el componente SI tenga avatar, es decir image!=null
 Para solicionar este caso problemático la única solución que tenemos es que image pase a no ser
 null cuando se le asigna, es decir que la propiedad image cambie si cambia la imagen... parece que volvemos
 al principio
 La solución, y esta vez es definitiva, consiste en mandar un evento al abuelo (al dashboard), a través
 del padre (UserData) indicándole que la imagen ha cambiado y con que nuevo valor y que sea el dashboard
 el que cambie la propiedad y, por lo tanto, se actualice de manera automática en el componente UploadImage
*/
var STATUS_INITIAL = 0,
    STATUS_SAVING = 1,
    STATUS_SUCCESS = 2,
    STATUS_FAILED = 3;

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "upload-image-component", // que sea siempre compuesto con - para evitar colisiones con otros tag HTML5
  props: ["image", "editar"], // imagen por defecto
  data: function data() {
    return {
      currentStatus: null,
      uploadFieldName: "avatar"
    };
  },

  computed: {
    isInitial: function isInitial() {
      return this.currentStatus === STATUS_INITIAL;
    },
    isSaving: function isSaving() {
      return this.currentStatus === STATUS_SAVING;
    },
    isSuccess: function isSuccess() {
      return this.currentStatus === STATUS_SUCCESS;
    },
    isFailed: function isFailed() {
      return this.currentStatus === STATUS_FAILED;
    }
  },
  /*watch: {
        image: function (val) {
            // image sólo cambia si se elimina el avatar desde el padre
            this.imageToRender = val;
        }
    },*/
  methods: {
    reset: function reset() {
      // reset form to initial state
      this.currentStatus = STATUS_INITIAL;
    },
    upload: function upload(formData) {
      // no se accede a this desde dentro de las funciones then
      // me creo una variable v (viewmodel) para cambiar el ámbito
      // y así si poder acceder
      var vm = this;
      return axios.post("/scrabble/upload/avatar", formData).then(function (response) {
        vm.currentStatus = STATUS_SUCCESS;
        vm.$emit("avatar-change", response.data.path);
        vm.currentStatus = STATUS_INITIAL;
      }).catch(function (error) {
        vm.currentStatus = STATUS_FAILED;
        alert("ERROR: " + error);
      });
    },
    save: function save(formData) {
      // subimos los datos al servidor
      this.currentStatus = STATUS_SAVING;
      this.upload(formData);
    },
    filesChange: function filesChange(fieldName, fileList) {
      var formData = new FormData();

      if (!fileList.length) return;

      // se adjuntan los ficheros a formData
      Array.from(Array(fileList.length).keys()).map(function (x) {
        formData.append(fieldName, fileList[x], fileList[x].name);
      });

      // se graba
      this.save(formData);
    }
  },
  created: function created() {
    this.reset();
  },
  mounted: function mounted() {
    console.log("UploadImageComponent montado.");
  }
});

/***/ }),
/* 152 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { attrs: { id: "upload-image" } }, [
    _vm.image ? _c("img", { attrs: { src: _vm.image } }) : _vm._e(),
    _vm._v(" "),
    _vm.isInitial || _vm.isSaving
      ? _c(
          "form",
          { attrs: { enctype: "multipart/form-data", novalidate: "" } },
          [
            _c(
              "div",
              { class: { dropbox: _vm.editar, opacityon: _vm.image } },
              [
                _vm.editar
                  ? _c("input", {
                      staticClass: "input-file",
                      attrs: {
                        type: "file",
                        name: _vm.uploadFieldName,
                        disabled: _vm.isSaving,
                        accept: "image/*"
                      },
                      on: {
                        change: function($event) {
                          _vm.filesChange(
                            $event.target.name,
                            $event.target.files
                          )
                        }
                      }
                    })
                  : _vm._e(),
                _vm._v(" "),
                _vm.editar
                  ? _c("p", [
                      _vm._v("\n        Arrastra tu imagen\n        "),
                      _c("br"),
                      _vm._v("o pincha para buscarla\n      ")
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.isSaving
                  ? _c("p", [_vm._v("Subiendo imagen...")])
                  : _vm._e()
              ]
            )
          ]
        )
      : _vm._e()
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-57c3577a", module.exports)
  }
}

/***/ }),
/* 153 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "user-data" },
    [
      _c("upload-image-component", {
        attrs: { image: _vm.avatar },
        on: { "avatar-change": _vm.onAvatarChange }
      }),
      _vm._v(" "),
      _c("div", { staticClass: "info" }, [
        _c("span", { staticClass: "data" }, [_vm._v("ID:")]),
        _vm._v(" "),
        _c("span", { staticClass: "value" }, [_vm._v(_vm._s(_vm.user.id))])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "info" }, [
        _c("span", { staticClass: "data" }, [_vm._v("Usuario:")]),
        _vm._v(" "),
        _c("span", { staticClass: "value" }, [_vm._v(_vm._s(_vm.user.name))])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "info" }, [
        _c("span", { staticClass: "data" }, [_vm._v("Mail:")]),
        _vm._v(" "),
        _c("span", { staticClass: "value" }, [_vm._v(_vm._s(_vm.user.email))])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "info" }, [
        _c("span", { staticClass: "data" }, [_vm._v("País:")]),
        _vm._v(" "),
        _c("span", { staticClass: "value" }, [_vm._v(_vm._s(_vm.user.country))])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "info" }, [
        _c("span", { staticClass: "data" }, [_vm._v("Lenguaje favorito:")]),
        _vm._v(" "),
        _c("span", { staticClass: "value" }, [
          _vm._v(_vm._s(_vm.user.favourite_language))
        ])
      ]),
      _vm._v(" "),
      _vm.user.updated_at
        ? _c("div", { staticClass: "info" }, [
            _c("span", { staticClass: "data" }, [
              _vm._v("Última actualización:")
            ]),
            _vm._v(" "),
            _c("span", { staticClass: "value" }, [
              _vm._v(_vm._s(_vm.user.updated_at))
            ])
          ])
        : _c("div", { staticClass: "info" }, [
            _c("span", { staticClass: "data" }, [
              _vm._v("Última actualización:")
            ]),
            _vm._v(" "),
            _c("span", { staticClass: "value" }, [_vm._v("Nunca")])
          ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-4de2657e", module.exports)
  }
}

/***/ }),
/* 154 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { attrs: { id: "dash" } },
    [
      _c("user-data-component", {
        attrs: { user: _vm.c_user, avatar: _vm.c_avatar },
        on: { "avatar-change": _vm.onAvatarChange }
      }),
      _vm._v(" "),
      _c("p", [_vm._v(_vm._s(_vm.user.id))]),
      _vm._v(" "),
      _c("p", { staticClass: "variables-title" }, [_vm._v("Estadísticas")]),
      _vm._v(" "),
      _c("card-container-component", { attrs: { variables: _vm.c_variables } }),
      _vm._v(" "),
      _c("aside", { staticClass: "sidebar" }, [
        _c("br"),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("ul", [
          _vm._m(0),
          _vm._v(" "),
          _c("li", { staticClass: "input-menu upper-margin" }, [
            _c(
              "a",
              {
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                    return _vm.removeAvatar($event)
                  }
                }
              },
              [_vm._v("Elimina avatar")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "input-menu" }, [
            _c(
              "a",
              {
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                    return _vm.giveupGame($event)
                  }
                }
              },
              [_vm._v("Abandonar partida")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "input-menu" }, [
            _c(
              "a",
              {
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                    return _vm.userStatistics($event)
                  }
                }
              },
              [_vm._v("Estadísticas usuario")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "input-menu" }, [
            _c(
              "a",
              {
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                    return _vm.removeUser($event)
                  }
                }
              },
              [_vm._v("Eliminar cuenta")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "input-menu" }, [
            _c(
              "a",
              {
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                    return _vm.userGames($event)
                  }
                }
              },
              [_vm._v("Partidas del usuario (pag 1)")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "input-menu" }, [
            _c(
              "a",
              {
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                    return _vm.userGamesPag2($event)
                  }
                }
              },
              [_vm._v("Partidas del usuario (pag 2)")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "input-menu upper-margin" }, [
            _c(
              "a",
              {
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                    return _vm.deleteNotification($event)
                  }
                }
              },
              [_vm._v("Borrar notificación")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "input-menu" }, [
            _c(
              "a",
              {
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                    return _vm.updateProfile($event)
                  }
                }
              },
              [_vm._v("Modificar datos usuario")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "input-menu" }, [
            _c(
              "a",
              {
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                    return _vm.requestChallenge($event)
                  }
                }
              },
              [_vm._v("Solicitar partida")]
            )
          ]),
          _vm._v(" "),
          this.requestChallengeId
            ? _c("li", { staticClass: "input-menu" }, [
                _c(
                  "a",
                  {
                    attrs: { href: "#" },
                    on: {
                      click: function($event) {
                        $event.preventDefault()
                        return _vm.gotoGame($event)
                      }
                    }
                  },
                  [_vm._v("Ir a partida solicitada")]
                )
              ])
            : _c(
                "li",
                {
                  staticClass: "input-menu",
                  staticStyle: { "text-decoration": "line-through" }
                },
                [_c("a", [_vm._v("Ir a partida solicitada")])]
              ),
          _vm._v(" "),
          _c("li", { staticClass: "input-menu" }, [
            _c(
              "a",
              {
                attrs: {
                  title: "Partida preparada para ser jugada",
                  href: "#"
                },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                    _vm.gotoGame($event, 6)
                  }
                }
              },
              [_vm._v("Ir partida juego")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "input-menu" }, [
            _c(
              "a",
              {
                attrs: {
                  title: "Partida preparada para ser ganada TOME en D6",
                  href: "#"
                },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                    _vm.gotoGame($event, 7)
                  }
                }
              },
              [_vm._v("Ir partida ganar")]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "input-menu upper-margin" }, [
            _vm._v("\n        Lista jugadores\n        "),
            _c("div", { staticClass: "submenu" }, [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.name,
                    expression: "name"
                  }
                ],
                domProps: { value: _vm.name },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.name = $event.target.value
                  }
                }
              }),
              _vm._v(" "),
              _c("button", { on: { click: _vm.listUsers } }, [_vm._v("OK")])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("card-container-component", { attrs: { cards: _vm.c_cards } })
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "input-menu" }, [
      _c(
        "a",
        {
          attrs: {
            href: "#",
            onclick:
              "event.preventDefault(); document.getElementById('logout-form').submit();"
          }
        },
        [_vm._v("Logout")]
      )
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-f2b5f464", module.exports)
  }
}

/***/ }),
/* 155 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(156)
}
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(158)
/* template */
var __vue_template__ = __webpack_require__(159)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-238ea4a3"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/DatosUsuarioComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-238ea4a3", Component.options)
  } else {
    hotAPI.reload("data-v-238ea4a3", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 156 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(157);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("492fa604", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-238ea4a3\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./DatosUsuarioComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-238ea4a3\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./DatosUsuarioComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 157 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(1)(false);
// imports


// module
exports.push([module.i, "\n.userInfo_container[data-v-238ea4a3] {\n  display: grid;\n  grid-template-columns: repeat(12, 1fr);\n  grid-template-rows: repeat(5, 75px);\n  border-radius: 10px;\n  width: 100%;\n  z-index: -1;\n}\n.userInfo_container img[data-v-238ea4a3] {\n    grid-column: 1 / 13;\n    grid-row: 1 / 6;\n    width: 100%;\n    height: 100%;\n    z-index: 1;\n}\n.userInfo_container #userAvatar[data-v-238ea4a3] {\n    grid-column: 2 / 4;\n    grid-row: 1 / 2;\n    z-index: 3;\n}\n.userInfo_container #userInfo[data-v-238ea4a3] {\n    grid-column: 3 / 12;\n    grid-row: 2 / 5;\n    z-index: 2;\n}\n.userInfo_container .btn_container[data-v-238ea4a3] {\n    grid-column: 9 / 12;\n    grid-row: 5;\n    margin: auto auto;\n    justify-items: space-around;\n    z-index: 3;\n}\n@media screen and (max-width: 1486px) {\n.userInfo_container .btn_container[data-v-238ea4a3] {\n      grid-column: 7 / 12;\n      grid-row: 5;\n      margin: auto auto;\n      justify-items: space-around;\n      z-index: 3;\n}\n}\n@media screen and (max-width: 768px) {\n.userInfo_container #userAvatar[data-v-238ea4a3] {\n      grid-column: 6 / 9;\n      grid-row: 1 / 2;\n      z-index: 3;\n}\n}\n@media screen and (max-width: 613px) {\n.userInfo_container a > span[data-v-238ea4a3] {\n      display: none;\n}\n}\n@media screen and (max-width: 414px) {\n.userInfo_container #userAvatar[data-v-238ea4a3] {\n      grid-column: 5 / 10;\n      grid-row: 1 / 2;\n      z-index: 3;\n}\n.userInfo_container .btn_container[data-v-238ea4a3] {\n      grid-column: 3 / 12;\n      grid-row: 5;\n      margin: auto auto;\n      justify-items: space-around;\n      z-index: 3;\n}\n.userInfo_container a > span[data-v-238ea4a3] {\n      display: none;\n}\n}\n.gridUser[data-v-238ea4a3] {\n  display: grid;\n  grid-template-columns: 0.5fr repeat(3, 1fr);\n  grid-template-rows: repeat(2, 0.5fr);\n  background-color: #fff;\n  border-radius: 10px;\n  -webkit-box-shadow: 15px 15px 15px -5px #d1cbcb;\n          box-shadow: 15px 15px 15px -5px #d1cbcb;\n}\n.gridUser .infouser[data-v-238ea4a3] {\n    display: -ms-flex;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    padding-left: 10px;\n    padding-right: 10px;\n    font-family: Roboto, sans-serif;\n    font-size: 1rem;\n    font-weight: 800;\n}\n.gridUser #userCreatedAt[data-v-238ea4a3] {\n    grid-column: 2;\n    grid-row: 1;\n    border-bottom: 1px solid rgba(231, 231, 231, 0.5);\n}\n.gridUser #userName[data-v-238ea4a3] {\n    grid-column: 3;\n    grid-row: 1;\n    border-bottom: 1px solid rgba(231, 231, 231, 0.5);\n}\n.gridUser #userMail[data-v-238ea4a3] {\n    grid-column: 4;\n    grid-row: 1;\n    border-bottom: 1px solid rgba(231, 231, 231, 0.5);\n}\n.gridUser #userCountry[data-v-238ea4a3] {\n    grid-column: 2;\n    grid-row: 2;\n}\n.gridUser #userLanguage[data-v-238ea4a3] {\n    grid-column: 3;\n    grid-row: 2;\n}\n.gridUser #userState[data-v-238ea4a3] {\n    grid-column: 4;\n    grid-row: 2;\n}\n@media screen and (max-width: 1543px) {\n.gridUser #userMail[data-v-238ea4a3],\n    .gridUser #userName[data-v-238ea4a3] {\n      display: -ms-flex;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: normal;\n          -ms-flex-direction: column;\n              flex-direction: column;\n      -ms-flex-wrap: wrap;\n          flex-wrap: wrap;\n      -webkit-box-align: center;\n          -ms-flex-align: center;\n              align-items: center;\n      -webkit-box-pack: center;\n          -ms-flex-pack: center;\n              justify-content: center;\n}\n}\n@media screen and (max-width: 768px) {\n.gridUser[data-v-238ea4a3] {\n      grid-template-columns: repeat(3, 1fr);\n      grid-template-rows: repeat(4, 0.5fr);\n}\n.gridUser .infouser[data-v-238ea4a3] {\n        display: -ms-flex;\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-orient: vertical;\n        -webkit-box-direction: normal;\n            -ms-flex-direction: column;\n                flex-direction: column;\n        -ms-flex-wrap: wrap;\n            flex-wrap: wrap;\n        -webkit-box-align: center;\n            -ms-flex-align: center;\n                align-items: center;\n        -webkit-box-pack: center;\n            -ms-flex-pack: center;\n                justify-content: center;\n}\n.gridUser #userCreatedAt[data-v-238ea4a3] {\n        grid-column: 1;\n        grid-row: 3;\n}\n.gridUser #userName[data-v-238ea4a3] {\n        grid-column: 2;\n        grid-row: 3;\n}\n.gridUser #userMail[data-v-238ea4a3] {\n        grid-column: 3;\n        grid-row: 3;\n}\n.gridUser #userCountry[data-v-238ea4a3] {\n        grid-column: 1;\n        grid-row: 4;\n}\n.gridUser #userLanguage[data-v-238ea4a3] {\n        grid-column: 2;\n        grid-row: 4;\n}\n.gridUser #userState[data-v-238ea4a3] {\n        grid-column: 3;\n        grid-row: 4;\n}\n}\n@media screen and (max-width: 414px) {\n.gridUser[data-v-238ea4a3] {\n      grid-template-columns: repeat(2, 1fr);\n      grid-template-rows: repeat(5, 0.5fr);\n}\n.gridUser .infouser[data-v-238ea4a3] {\n        display: -ms-flex;\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-orient: vertical;\n        -webkit-box-direction: normal;\n            -ms-flex-direction: column;\n                flex-direction: column;\n        -ms-flex-wrap: wrap;\n            flex-wrap: wrap;\n        -webkit-box-align: center;\n            -ms-flex-align: center;\n                align-items: center;\n        -webkit-box-pack: center;\n            -ms-flex-pack: center;\n                justify-content: center;\n        border-bottom: 1px solid #d1cbcb;\n}\n.gridUser #userId[data-v-238ea4a3] {\n        grid-column: 1;\n        grid-row: 3;\n}\n.gridUser #userName[data-v-238ea4a3] {\n        grid-column: 2;\n        grid-row: 3;\n}\n.gridUser #userMail[data-v-238ea4a3] {\n        grid-column: 1;\n        grid-row: 4;\n}\n.gridUser #userCountry[data-v-238ea4a3] {\n        grid-column: 2;\n        grid-row: 4;\n}\n.gridUser #userLanguage[data-v-238ea4a3] {\n        grid-column: 1;\n        grid-row: 5;\n        border-bottom-left-radius: 10px;\n}\n.gridUser #userState[data-v-238ea4a3] {\n        grid-column: 2;\n        grid-row: 5;\n        border-bottom-right-radius: 10px;\n}\n}\n.avatar_container[data-v-238ea4a3] {\n  height: 100%;\n  width: 100%;\n  border-radius: 50%;\n}\n.boton_borrar[data-v-238ea4a3] {\n  display: inline-block;\n  cursor: pointer;\n  background-color: #027381;\n  color: #fff;\n  text-align: center;\n  border-radius: 10rem;\n  padding: 0.5rem 1.4rem;\n  margin-top: 10px;\n  height: 2rem;\n  font-size: 0.8rem;\n  line-height: 1;\n}\n.boton_borrar[data-v-238ea4a3]:before {\n    font-family: \"Font Awesome 5 free\";\n    content: \"\\F2ED\";\n    font-size: 15px;\n    padding-right: 2px;\n}\n.boton_borrar[data-v-238ea4a3]:hover {\n    background-color: #4ba2b0;\n    color: #000;\n    text-decoration: none;\n}\n.boton_borrar a[data-v-238ea4a3] {\n    text-decoration: none;\n}\n.boton_editar[data-v-238ea4a3] {\n  display: inline-block;\n  cursor: pointer;\n  background-color: #815502;\n  color: #fff;\n  text-align: center;\n  border-radius: 10rem;\n  padding: 0.5rem 1.4rem;\n  margin-top: 10px;\n  height: 2rem;\n  font-size: 0.8rem;\n  line-height: 1;\n}\n.boton_editar[data-v-238ea4a3]:before {\n    font-family: \"Font Awesome 5 free\";\n    content: \"\\F044\";\n    font-size: 15px;\n    padding-right: 2px;\n}\n.boton_editar[data-v-238ea4a3]:hover {\n    background-color: #b0904b;\n    color: #000;\n    text-decoration: none;\n}\n.boton_editar a[data-v-238ea4a3] {\n    text-decoration: none;\n}\n.boton_confirmar[data-v-238ea4a3] {\n  display: inline-block;\n  cursor: pointer;\n  background-color: #028102;\n  color: #fff;\n  text-align: center;\n  border-radius: 10rem;\n  padding: 0.5rem 1.4rem;\n  margin-top: 10px;\n  height: 2rem;\n  font-size: 0.8rem;\n  line-height: 1;\n}\n.boton_confirmar[data-v-238ea4a3]:before {\n    font-family: \"Font Awesome 5 free\";\n    content: \"\\F058\";\n    font-size: 15px;\n    padding-right: 2px;\n}\n.boton_confirmar[data-v-238ea4a3]:hover {\n    background-color: #57b04b;\n    color: #000;\n    text-decoration: none;\n}\n.boton_confirmar a[data-v-238ea4a3] {\n    text-decoration: none;\n}\n.rojo[data-v-238ea4a3] {\n  font-weight: bold;\n  color: red;\n}\n.inp[data-v-238ea4a3] {\n  position: relative;\n  margin: auto;\n  width: 100%;\n  max-width: 180px;\n}\n.inp .label[data-v-238ea4a3] {\n    position: absolute;\n    top: 16px;\n    left: 0;\n    font-family: Roboto, sans-serif;\n    font-size: 1rem;\n    color: #3e4046;\n    font-weight: 500;\n    -webkit-transform-origin: 0 0;\n            transform-origin: 0 0;\n    -webkit-transition: all 2s ease;\n    transition: all 2s ease;\n}\n.inp .border[data-v-238ea4a3] {\n    position: absolute;\n    bottom: 0;\n    left: 0;\n    height: 2px;\n    width: 100%;\n    background: #07f;\n    -webkit-transform: scale(0);\n            transform: scale(0);\n    -webkit-transform-origin: 0 0;\n            transform-origin: 0 0;\n    -webkit-transition: all 15s ease;\n    transition: all 15s ease;\n}\n.inp input[data-v-238ea4a3] {\n    -webkit-appearance: none;\n    width: 100%;\n    border: 0;\n    font-family: inherit;\n    padding: 12px 0;\n    height: 48px;\n    font-size: 1rem;\n    font-weight: 500;\n    border-bottom: 2px solid #808183;\n    background: none;\n    border-radius: 0;\n    color: #223254;\n    -webkit-transition: all 15s ease;\n    transition: all 15s ease;\n}\n.inp input[data-v-238ea4a3]:hover {\n      background: rgba(34, 50, 84, 0.03);\n}\n.inp input:not(:placeholder-shown) + span[data-v-238ea4a3] {\n      color: #5a667f;\n      -webkit-transform: translateY(-26px) scale(0.75);\n              transform: translateY(-26px) scale(0.75);\n}\n.inp input[data-v-238ea4a3]:focus {\n      background: none;\n      outline: none;\n}\n.inp input:focus + span[data-v-238ea4a3] {\n        color: #07f;\n        -webkit-transform: translateY(-26px) scale(0.75);\n                transform: translateY(-26px) scale(0.75);\n}\n.inp input:focus + span + .border[data-v-238ea4a3] {\n          -webkit-transform: scale(1);\n                  transform: scale(1);\n}\n.dato[data-v-238ea4a3],\n.valor[data-v-238ea4a3] {\n  margin-right: 4px;\n}\n", ""]);

// exports


/***/ }),
/* 158 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__UploadImageComponent__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__UploadImageComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__UploadImageComponent__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: "datos-usuario-component",
  props: {
    user: { required: true, type: Object },
    avatar: { required: true, type: String }
  },
  components: {
    UploadImageComponent: __WEBPACK_IMPORTED_MODULE_0__UploadImageComponent___default.a
  },
  data: function data() {
    return {
      c_user: null,
      c_avatar: null,
      editar: false,
      accion: "Editar"
    };
  },

  methods: {
    onAvatarChange: function onAvatarChange(image) {
      this.c_avatar = image;
      this.editar = !this.editar;
    },
    removeAvatar: function removeAvatar(event) {
      //  event.preventDefault(); no hace falta ya que lo he puesto en la llamada
      console.log("Entro en remove avatar");
      this.c_avatar = null;
      var vm = this;
      return axios.post("/scrabble/user/avatar/remove").then(function (response) {
        console.log("Elimina avatar respuesta:", response);
        alert("Avatar eliminado.");
        vm.editar = !vm.editar;
        //vm.createCard('Remove Avatar', response.status, response.statusText, response.data);
      }).catch(function (error) {
        console.log("ERROR: " + error);
        alert("Error:" + error);
        vm.editar = !vm.editar;
        //vm.createCard('Giveup Game', error.response.status, error.response.statusText, error.response.data);
      });
    },
    editAvatar: function editAvatar(event) {
      this.editar = !this.editar;
      if (!this.editar) {
        this.accion = "Editar";
        this.updateUser();
      } else {
        this.accion = "Confirmar";
      }
    },
    updateUser: function updateUser() {
      var vm = this;
      // modifica los datos del usuario.
      // el JSON de ejemplo incuye todos los posibles datos a modificar
      // podría sólo indicarse uno
      return axios.put("/scrabble/user/update", {
        name: vm.c_user.name,
        country: vm.c_user.country,
        favourite_language: vm.c_user.favourite_language
        // new_password: "1234567",
        // new_password_confirmation: "1234567",
        // old_password: "12345"
      }).then(function (response) {
        alert("Datos del usuario" + vm.c_user.name + " actualizados");

        /* IMPORTANTE: en caso de hacer cambio de password el servidor
                     hace un logout y por lo tanto conviene volver a la página de login
                     siempre se ejecuta
                     uso replace para que la página actual desaparezca del historial
                     y evitar la tecla back
                       setTimeout(function () {
                          window.location.replace('/scrabble/login');
                      }, 2000);
                  */
      }).catch(function (error) {
        console.log("ERROR: " + error);
        alert("ERROR: " + error);
      });
    }
  },
  created: function created() {
    this.c_user = this.user;
    if (!this.avatar) {
      this.c_avatar = "/storage/public/default_avatar.jpg";
      this.editar = false;
    } else {
      this.c_avatar = this.avatar;
      this.editar = false;
    }
  }
});

/***/ }),
/* 159 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "userInfo_container" }, [
    _c("img", {
      attrs: {
        id: "fondo",
        src: "../img/game-2790713_1920.jpg",
        title:
          "CC0 Creative Commons. Gratis para usos comerciales, No es necesario reconocimiento"
      }
    }),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "avatar_container", attrs: { id: "userAvatar" } },
      [
        _c("upload-image-component", {
          attrs: { editar: _vm.editar, image: _vm.c_avatar },
          on: { "avatar-change": _vm.onAvatarChange }
        })
      ],
      1
    ),
    _vm._v(" "),
    _c("div", { staticClass: "gridUser", attrs: { id: "userInfo" } }, [
      _c("div", { staticClass: "infouser", attrs: { id: "userCreatedAt" } }, [
        _c("span", { staticClass: "valor" }, [_vm._v("Jugador desde")]),
        _vm._v(" "),
        _c(
          "span",
          {
            staticClass: "valor",
            class: { rojo: _vm.c_user.created_at == null }
          },
          [
            _vm._v(
              _vm._s(
                _vm._f("fechaES")(
                  _vm.c_user.created_at == null
                    ? "se ignora"
                    : _vm.c_user.created_at
                )
              )
            )
          ]
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "infouser", attrs: { id: "userName" } }, [
        !_vm.editar
          ? _c("span", { staticClass: "dato" }, [_vm._v("Nombre:")])
          : _vm._e(),
        _vm._v(" "),
        !_vm.editar
          ? _c("span", { staticClass: "valor" }, [
              _vm._v(_vm._s(_vm.c_user.name))
            ])
          : _vm._e(),
        _vm._v(" "),
        _vm.editar
          ? _c("label", { staticClass: "inp", attrs: { for: "nombre" } }, [
              _vm.editar
                ? _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.c_user.name,
                        expression: "c_user.name"
                      }
                    ],
                    attrs: {
                      type: "text",
                      name: "nombre",
                      id: "nombre",
                      placeholder: "&nbsp;"
                    },
                    domProps: { value: _vm.c_user.name },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.c_user, "name", $event.target.value)
                      }
                    }
                  })
                : _vm._e(),
              _vm._v(" "),
              _c("span", { staticClass: "label" }, [_vm._v("Nombre:")]),
              _vm._v(" "),
              _c("span", { staticClass: "border" })
            ])
          : _vm._e()
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "infouser", attrs: { id: "userMail" } }, [
        _c("span", { staticClass: "dato" }, [_vm._v("Email:")]),
        _vm._v(" "),
        _c("span", { staticClass: "valor" }, [_vm._v(_vm._s(_vm.c_user.email))])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "infouser", attrs: { id: "userCountry" } }, [
        !_vm.editar
          ? _c("span", { staticClass: "dato" }, [_vm._v("País:")])
          : _vm._e(),
        _vm._v(" "),
        !_vm.editar
          ? _c("span", { staticClass: "valor" }, [
              _vm._v(_vm._s(_vm.c_user.country))
            ])
          : _vm._e(),
        _vm._v(" "),
        _vm.editar
          ? _c("label", { staticClass: "inp", attrs: { for: "pais" } }, [
              _vm.editar
                ? _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.c_user.country,
                        expression: "c_user.country"
                      }
                    ],
                    attrs: {
                      type: "text",
                      name: "pais",
                      id: "pais",
                      placeholder: "&nbsp;"
                    },
                    domProps: { value: _vm.c_user.country },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.c_user, "country", $event.target.value)
                      }
                    }
                  })
                : _vm._e(),
              _vm._v(" "),
              _c("span", { staticClass: "label" }, [_vm._v("País:")]),
              _vm._v(" "),
              _c("span", { staticClass: "border" })
            ])
          : _vm._e()
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "infouser", attrs: { id: "userLanguage" } }, [
        !_vm.editar
          ? _c("span", { staticClass: "dato" }, [_vm._v("Idioma:")])
          : _vm._e(),
        _vm._v(" "),
        !_vm.editar
          ? _c("span", { staticClass: "valor" }, [
              _vm._v(_vm._s(_vm.c_user.favourite_language))
            ])
          : _vm._e(),
        _vm._v(" "),
        _vm.editar
          ? _c("label", { staticClass: "inp", attrs: { for: "pais" } }, [
              _vm.editar
                ? _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.c_user.favourite_language,
                        expression: "c_user.favourite_language"
                      }
                    ],
                    attrs: {
                      type: "text",
                      name: "idioma",
                      id: "idioma",
                      placeholder: "&nbsp;"
                    },
                    domProps: { value: _vm.c_user.favourite_language },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(
                          _vm.c_user,
                          "favourite_language",
                          $event.target.value
                        )
                      }
                    }
                  })
                : _vm._e(),
              _vm._v(" "),
              _c("span", { staticClass: "label" }, [_vm._v("Idioma:")]),
              _vm._v(" "),
              _c("span", { staticClass: "border" })
            ])
          : _vm._e()
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "infouser", attrs: { id: "userState" } }, [
        _c("span", { staticClass: "dato" }, [_vm._v("Estado:")]),
        _vm._v(" "),
        _c("span", { class: { rojo: _vm.user.state === "offline" } }, [
          _vm._v(_vm._s(_vm.c_user.state))
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "btn_container" }, [
      _c(
        "a",
        {
          class: { boton_editar: !_vm.editar, boton_confirmar: _vm.editar },
          attrs: { href: "#" },
          on: {
            click: function($event) {
              $event.preventDefault()
              return _vm.editAvatar($event)
            }
          }
        },
        [_c("span", [_vm._v(_vm._s(_vm.accion))])]
      ),
      _vm._v(" "),
      _c(
        "a",
        {
          staticClass: "boton_borrar",
          attrs: { href: "#" },
          on: {
            click: function($event) {
              $event.preventDefault()
              return _vm.removeAvatar($event)
            }
          }
        },
        [_c("span", [_vm._v("Borrar Avatar")])]
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-238ea4a3", module.exports)
  }
}

/***/ }),
/* 160 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(161)
}
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(163)
/* template */
var __vue_template__ = __webpack_require__(164)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-06652f53"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/EstadisticasUsuarioComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-06652f53", Component.options)
  } else {
    hotAPI.reload("data-v-06652f53", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 161 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(162);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("3af1b8a0", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-06652f53\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./EstadisticasUsuarioComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-06652f53\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./EstadisticasUsuarioComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 162 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(1)(false);
// imports


// module
exports.push([module.i, "\n.user_statistics_container[data-v-06652f53],\n.user_statistics_container_empty[data-v-06652f53] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  background: #fff;\n  -ms-flex-line-pack: end;\n      align-content: flex-end;\n  -webkit-box-pack: start;\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n  justify-items: flex-start;\n  grid-gap: 5px 10px;\n  width: 100%;\n  height: 200px;\n}\n@media screen and (max-width: 1450px) {\n.user_statistics_container[data-v-06652f53] {\n    display: grid;\n    grid-auto-columns: 250px;\n    grid-template-rows: minmax(35px, 150px);\n    justify-items: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    grid-row-gap: 5px;\n    grid-template-areas: \"acciones\" \"idiomas\";\n    margin: auto 0;\n    height: 260px;\n}\n}\n@media screen and (max-width: 1450px) {\n.user_statistics_container_empty[data-v-06652f53] {\n    grid-template-columns: minmax(100%, 200px);\n    grid-template-rows: minmax(100%, 180px);\n    justify-items: center;\n    grid-row-gap: 5px;\n    grid-template-areas: \"idiomas\";\n}\n}\n.datosIdioma[data-v-06652f53] {\n  display: grid;\n  grid-template-columns: 1fr;\n  grid-template-rows: repeat(2, 0.5fr);\n  padding: 10px;\n  margin: 5px 10px 5px 3px;\n  border: 1px solid rgba(57, 59, 66, 0.7);\n  border-radius: 5px;\n  width: 150px;\n  grid-template-areas: \"titulo\" \"contenido\";\n  font-size: 0.9rem;\n}\n@media screen and (max-width: 1450px) {\n.datosIdioma[data-v-06652f53] {\n      margin: 5px 10px 5px 3px;\n}\n}\n.contenedorIdioma[data-v-06652f53] {\n  grid-area: idiomas;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: row;\n          flex-direction: row;\n  -ms-flex-line-pack: end;\n      align-content: flex-end;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n@media screen and (max-width: 1450px) {\n.contenedorIdioma[data-v-06652f53] {\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-line-pack: center;\n          align-content: center;\n      -webkit-box-align: space-between;\n          -ms-flex-align: space-between;\n              align-items: space-between;\n      justify-items: center;\n      -webkit-box-pack: center;\n          -ms-flex-pack: center;\n              justify-content: center;\n}\n}\n.cabecera[data-v-06652f53] {\n  grid-area: \"titulo\";\n  display: -ms-flex;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: end;\n      -ms-flex-pack: end;\n          justify-content: flex-end;\n  font-family: Roboto, sans-serif;\n  font-size: 0.8rem;\n  margin-bottom: 5px;\n}\n.principal[data-v-06652f53] {\n  grid-area: \"contenido\";\n  display: -ms-flex;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: end;\n      -ms-flex-align: end;\n          align-items: flex-end;\n}\n.ganadas[data-v-06652f53],\n.perdidas[data-v-06652f53] {\n  margin-right: 2px;\n  margin-left: 2px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -ms-flex-line-pack: center;\n      align-content: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  text-align: center;\n}\n.graphLose[data-v-06652f53],\n.graphWin[data-v-06652f53] {\n  padding: 2px 5px 0 5px;\n  border-top-right-radius: 5px;\n  border-top-left-radius: 5px;\n  -webkit-box-align: end;\n      -ms-flex-align: end;\n          align-items: flex-end;\n  width: 34px;\n  text-align: center;\n}\n.graphWin[data-v-06652f53] {\n  background-color: rgba(115, 189, 96, 0.7);\n  height: 100px;\n}\n.graphLose[data-v-06652f53] {\n  background-color: rgba(189, 96, 108, 0.7);\n  height: 85px;\n}\n.sin_datos[data-v-06652f53] {\n  color: red;\n  margin: 10px 0 10px 5px;\n}\n.userStats .dato img[data-v-06652f53] {\n  max-width: 30px;\n  max-height: 30px;\n  width: 100%;\n  height: 100%;\n}\n.acciones[data-v-06652f53] {\n  grid-area: acciones;\n}\n.acciones button[data-v-06652f53] {\n    width: 75px;\n    border-radius: 5px;\n    background: #000;\n    color: #fff;\n}\n.acciones button[data-v-06652f53]:hover {\n      background: rgba(231, 231, 231, 0.5);\n      color: #000;\n}\n", ""]);

// exports


/***/ }),
/* 163 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "estadisticas-usuario-component",
  props: ["user", "statistics"],
  data: function data() {
    return {
      c_user: null,
      c_statistics: null,
      level: 0,
      wins: 0,
      loses: 0,
      winsES: 0,
      losesES: 0,
      winsEN: 0,
      losesEN: 0,
      valores_totales: {},
      valores_idiomas: {},
      sin_datos: "",
      ganadas: "",
      perdidas: "",
      ganadasES: "",
      perdidasES: "",
      ganadasEN: "",
      perdidasEN: "",
      levelES: 0,
      levelEN: 0,
      alto: 15,
      nivelMedio: 0,
      c_title: "",
      c_message: ""
    };
  },

  computed: {
    valorStyleGanadas: function valorStyleGanadas() {
      if (this.wins > this.loses) {
        return this.ganadas = 100;
      } else {
        var proporcion = this.wins * 100 / this.loses;
        if (proporcion <= 22) {
          this.ganadas = 22;
        } else {
          this.ganadas = proporcion;
        }
        return this.ganadas;
      }
    },
    valorStylePerdidas: function valorStylePerdidas() {
      if (this.wins < this.loses) {
        return this.perdidas = 100;
      } else {
        var proporcion = this.loses * 100 / this.wins;
        if (proporcion <= 22) {
          this.proporcion = 22;
        } else {
          this.perdidas = proporcion;
        }
        return this.perdidas;
      }
    },
    valorStyleGanadasES: function valorStyleGanadasES() {
      if (this.winsES > this.losesES) {
        return this.ganadasES = 100;
      } else {
        var proporcion = this.winsES * 100 / this.losesES;
        console.log(proporcion);
        if (proporcion < 22) {
          this.ganadasES = 22;
        } else {
          this.ganadasES = proporcion;
        }
        return this.ganadasES;
      }
    },
    valorStylePerdidasES: function valorStylePerdidasES() {
      if (this.winsES < this.losesES) {
        return this.perdidasES = 100;
      } else {
        var proporcion = this.losesES * 100 / this.winsES;
        console.log(proporcion);
        if (proporcion < 22) {
          this.perdidasES = 22;
        } else {
          this.perdidasES = proporcion;
        }
        return this.perdidasES;
      }
    },
    valorStyleGanadasEN: function valorStyleGanadasEN() {
      if (this.winsEN > this.losesEN) {
        return this.ganadasEN = 100;
      } else {
        var proporcion = this.winsEN * 100 / this.losesEN;
        console.log(proporcion);
        if (proporcion < 22) {
          this.ganadasEN = 22;
        } else {
          this.ganadasEN = proporcion;
        }
        return this.ganadasEN;
      }
    },
    valorStylePerdidasEN: function valorStylePerdidasEN() {
      if (this.winsEN < this.losesEN) {
        return this.perdidasEN = 100;
      } else {
        var proporcion = this.losesEN * 100 / this.winsEN;
        console.log(proporcion);
        if (proporcion < 22) {
          this.perdidasEN = 22;
        } else {
          this.perdidasEN = proporcion;
        }
        return this.perdidasEN;
      }
    }
  },
  methods: {
    initialStatistics: function initialStatistics() {
      var _this = this;

      if (this.c_statistics.length > 0) {
        this.c_statistics.forEach(function (element) {
          switch (element.language_code) {
            case "es":
              _this.winsES = parseInt(element.won);
              _this.losesES = parseInt(element.lost);
              _this.levelES = parseInt(element.level);
              break;
            case "en":
              _this.winsEN = parseInt(element.won);
              _this.losesEN = parseInt(element.lost);
              _this.levelEN = parseInt(element.level);
              break;
          }

          _this.wins += parseInt(element.won);
          _this.loses += parseInt(element.lost);
        });
        this.level = parseInt(this.levelES + this.levelEN);
        this.nivelMedio = parseInt(this.level) / parseInt(this.c_statistics.length);
        this.valores_totales = {
          ganadas: this.wins,
          perdidas: this.loses
        };
        this.valores_idiomas = {
          ganadasES: this.winsES,
          perdidasES: this.losesES,
          ganadasEN: this.winsEN,
          perdidasEN: this.losesEN
        };
      } else {
        this.sin_datos = "No existen datos estadísticos del jugador " + this.c_user.name;
        console.log("Sin datos");
      }
    },
    userStatistics: function userStatistics() {
      var vm = this;
      // estadísiticas via AJAX
      return axios.get("/scrabble/user/statistics").then(function (response) {
        vm.initialStatistics();
        console.log("Estadísticas actualizadas");
        vm.c_title = "Acción realizada";
        vm.c_message = "Estadísticas actualizadas";
        Push.create(vm.c_title, {
          body: vm.c_message,
          icon: "/img/notify_success.png",
          timeout: 10000,
          onClick: function onClick() {
            vm.window.close();
          }
        });
      }).catch(function (error) {
        console.log("ERROR de estadisticas: " + error);
        vm.c_title = "Notificación de error";
        vm.c_message = "ERROR de estadisticas: " + error;
        Push.create(vm.c_title, {
          body: vm.c_message,
          icon: "/img/notify_error.png",
          timeout: 10000,
          onClick: function onClick() {
            vm.window.close();
          }
        });
        vm.sin_datos = "No hay datos estadísticos del usuario.";
      });
    },
    mandarDatos: function mandarDatos() {
      this.$emit("recibir-datos", Math.floor(this.nivelMedio), this.wins, this.loses, this.levelES, this.levelEN);
      console.log("mandar datos " + Math.floor(this.nivelMedio) + " " + this.wins + " " + this.loses + " " + this.levelES + " " + this.levelEN);
    }
    // listenForBroadcast: function() {
    //   console.log("Escuchando canales: user" + this.c_user.id);
    //   Echo.private("user." + this.c_user.id).listen("AcceptedChallenge", e => {
    //     alert(
    //       "Generada partida " +
    //         e.gameId +
    //         "\nIdioma: " +
    //         e.lang +
    //         "\nOponente\n\tid: " +
    //         e.oppoId +
    //         "\n\tNombre: " +
    //         e.oppoName +
    //         "\n\tPaís: " +
    //         e.oppoCountry +
    //         "\n\tAvatar: " +
    //         e.oppoAvatar
    //     );

    //     this.requestChallengeId = e.gameId;

    //     console.log("Recibido");
    //   });
    // }
  },
  created: function created() {
    this.c_user = this.user;
    this.c_statistics = this.statistics;
    this.initialStatistics();
    this.mandarDatos();
    // this.listenForBroadcast();
  },
  mounted: function mounted() {
    console.log("EstadisticasUsuarioComponent montado.");
  }
});

/***/ }),
/* 164 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      class:
        _vm.c_statistics.length > 1
          ? "user_statistics_container"
          : "user_statistics_container_empty"
    },
    [
      _vm.c_statistics.length > 1
        ? _c(
            "div",
            { staticClass: "contenedorIdioma" },
            _vm._l(_vm.c_statistics, function(estadisticas, index) {
              return _c(
                "div",
                {
                  key: index,
                  staticClass: "datosIdioma",
                  class: "idioma" + index
                },
                [
                  _c("div", { staticClass: "cabecera" }, [
                    _c(
                      "div",
                      { staticClass: "userStats", attrs: { id: "userLevel" } },
                      [
                        _c("span", { staticClass: "dato" }, [
                          estadisticas.language_code === "es"
                            ? _c("img", {
                                attrs: {
                                  src: "../img/espana.png",
                                  title: "Nivel idioma español.",
                                  alt:
                                    "Icon made by www.freepik.com from www.flaticon.com"
                                }
                              })
                            : estadisticas.language_code === "en"
                              ? _c("img", {
                                  attrs: {
                                    src: "../img/reino-unido.png",
                                    title: "Nivel idioma ingles.",
                                    alt:
                                      "Icon made by www.freepik.com from www.flaticon.com"
                                  }
                                })
                              : _vm._e(),
                          _vm._v("\n            Nivel:\n          ")
                        ]),
                        _vm._v(" "),
                        estadisticas.language_code === "es"
                          ? _c("span", { staticClass: "valor" }, [
                              _vm._v(_vm._s(_vm.levelES))
                            ])
                          : estadisticas.language_code === "en"
                            ? _c("span", { staticClass: "valor" }, [
                                _vm._v(_vm._s(_vm.levelEN))
                              ])
                            : _vm._e()
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "principal" }, [
                    _c("div", { staticClass: "ganadas" }, [
                      estadisticas.language_code === "es"
                        ? _c(
                            "div",
                            {
                              staticClass: "graphWin",
                              style: { height: _vm.valorStyleGanadasES + "px" },
                              attrs: { title: "Victorias" }
                            },
                            [
                              _c("span", { staticClass: "valor" }, [
                                _vm._v(_vm._s(_vm.valores_idiomas.ganadasES))
                              ])
                            ]
                          )
                        : estadisticas.language_code === "en"
                          ? _c(
                              "div",
                              {
                                staticClass: "graphWin",
                                style: {
                                  height: _vm.valorStyleGanadasEN + "px"
                                },
                                attrs: { title: "Victorias" }
                              },
                              [
                                _c("span", { staticClass: "valor" }, [
                                  _vm._v(_vm._s(_vm.valores_idiomas.ganadasEN))
                                ])
                              ]
                            )
                          : _vm._e(),
                      _vm._v(" "),
                      _vm._m(0, true)
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "perdidas" }, [
                      estadisticas.language_code === "es"
                        ? _c(
                            "div",
                            {
                              staticClass: "graphLose",
                              style: {
                                height: _vm.valorStylePerdidasES + "px"
                              },
                              attrs: { title: "Derrotas" }
                            },
                            [
                              _c("span", { staticClass: "valor" }, [
                                _vm._v(_vm._s(_vm.valores_idiomas.perdidasES))
                              ])
                            ]
                          )
                        : estadisticas.language_code === "en"
                          ? _c(
                              "div",
                              {
                                staticClass: "graphLose",
                                style: {
                                  height: _vm.valorStylePerdidasEN + "px"
                                },
                                attrs: { title: "Derrotas" }
                              },
                              [
                                _c("span", { staticClass: "valor" }, [
                                  _vm._v(_vm._s(_vm.valores_idiomas.perdidasEN))
                                ])
                              ]
                            )
                          : _vm._e(),
                      _vm._v(" "),
                      _vm._m(1, true)
                    ])
                  ])
                ]
              )
            })
          )
        : _vm._e(),
      _vm._v(" "),
      _vm.sin_datos == ""
        ? _c("div", { staticClass: "acciones" }, [
            _vm._v("\n    Actualizar Estadísticas:\n    "),
            _c(
              "button",
              {
                attrs: { title: "Actualizar Estadísticas" },
                on: { click: _vm.userStatistics }
              },
              [_c("i", { staticClass: "fas fa-sync-alt" })]
            )
          ])
        : _vm._e(),
      _vm._v(" "),
      _vm.sin_datos != ""
        ? _c("div", { staticClass: "mensaje" }, [
            _c("div", { staticClass: "sin_datos" }, [
              _vm._v(_vm._s(_vm.sin_datos))
            ])
          ])
        : _vm._e()
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "userStats", attrs: { id: "userWins" } }, [
      _c("span", { staticClass: "dato" }, [
        _c("i", {
          staticClass: "fas fa-user-graduate",
          attrs: { title: "Partidas Ganadas" }
        })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "userStats", attrs: { id: "userLost" } }, [
      _c("span", { staticClass: "dato" }, [
        _c("i", {
          staticClass: "far fa-thumbs-down",
          attrs: { title: "Partidas Perdidas" }
        })
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-06652f53", module.exports)
  }
}

/***/ }),
/* 165 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(166);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("76f59547", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-77c36763\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./DashboardContenedorComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-77c36763\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./DashboardContenedorComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 166 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(1)(false);
// imports


// module
exports.push([module.i, "\n.dashboard_contenedor[data-v-77c36763] {\n  display: grid;\n  margin: 10px 10% 10px 10%;\n  grid-template-columns: repeat(10, 1fr);\n  grid-template-rows: 0.5fr repeat(4, 1fr) 0.5fr;\n}\n@media screen and (max-width: 1450px) {\n.dashboard_contenedor[data-v-77c36763] {\n      grid-template-rows: repeat(6, 150px);\n}\n}\n.info_head[data-v-77c36763] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: row;\n          flex-direction: row;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -ms-flex-line-pack: center;\n      align-content: center;\n}\n.info_statistics[data-v-77c36763] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.info_statistics span[data-v-77c36763] {\n    margin: 5px 10px 5px 0;\n}\n.info_statistics i[data-v-77c36763] {\n    margin: 5px 10px 0 0;\n}\n.zona_estatica[data-v-77c36763] {\n  grid-column: 1/5;\n  grid-row: 2/6;\n  border: 1px solid red;\n  background-size: cover;\n  border-radius: 3px;\n}\n.zona_estatica img[data-v-77c36763] {\n    border-radius: 3px;\n    opacity: 0.8;\n    width: 100%;\n    height: 100%;\n}\n@media screen and (max-width: 655px) {\n.zona_estatica img[data-v-77c36763] {\n        height: auto;\n}\n}\n@media screen and (max-width: 1450px) {\n.zona_estatica[data-v-77c36763] {\n      grid-column: 3/9;\n      grid-row: 1/3;\n}\n}\n.zona_dinamica[data-v-77c36763] {\n  grid-column: 4/11;\n  grid-row: 1/7;\n  background: rgba(231, 231, 231, 0.3);\n  background-size: 100%;\n  border-radius: 10px;\n  display: grid;\n  grid-template-columns: repeat(13, 1fr);\n  grid-template-rows: 0.5fr 220px 300px;\n  grid-template-areas: \"neutro neutro head1 head1 head1 head1 head1 head1 head1 head1 head1 head1 head1\" \"neutro neutro comp1 comp1 comp1 comp1 comp1 comp1 comp1 comp1 comp1 comp1 comp1\" \"neutro neutro comp2 comp2 comp2 comp2 comp2 comp2 comp2 comp2 comp2 comp2 comp2\";\n}\n@media screen and (max-width: 1450px) {\n.zona_dinamica[data-v-77c36763] {\n      grid-template-rows: 90px 90px 150px 260px 260px;\n      grid-column: 1/11;\n      grid-row: 2/7;\n      grid-template-areas: \"neutro neutro neutro neutro neutro neutro neutro neutro neutro neutro neutro neutro neutro\" \"neutro neutro neutro neutro neutro neutro neutro neutro neutro neutro neutro neutro neutro\" \"head1 head1 head1 head1 head1 head1 head1 head1 head1 head1 head1 head1 head1\" \"comp1 comp1 comp1 comp1 comp1 comp1 comp1 comp1 comp1 comp1 comp1 comp1 comp1\" \"comp2 comp2 comp2 comp2 comp2 comp2 comp2 comp2 comp2 comp2 comp2 comp2 comp2\";\n}\n}\n@media screen and (max-width: 866px) {\n.zona_dinamica[data-v-77c36763] {\n      grid-template-rows: 90px 90px 150px 260px 780px;\n      grid-column: 1/11;\n      grid-row: 2/7;\n      grid-template-areas: \"neutro neutro neutro neutro neutro neutro neutro neutro neutro neutro neutro neutro neutro\" \"neutro neutro neutro neutro neutro neutro neutro neutro neutro neutro neutro neutro neutro\" \"head1 head1 head1 head1 head1 head1 head1 head1 head1 head1 head1 head1 head1\" \"comp1 comp1 comp1 comp1 comp1 comp1 comp1 comp1 comp1 comp1 comp1 comp1 comp1\" \"comp2 comp2 comp2 comp2 comp2 comp2 comp2 comp2 comp2 comp2 comp2 comp2 comp2\";\n}\n}\n.user_statistics[data-v-77c36763] {\n  grid-area: comp1;\n}\n.userRanking[data-v-77c36763] {\n  grid-area: comp2;\n}\n.head1[data-v-77c36763] {\n  font-family: Roboto, sans-serif;\n  font-size: 1.5rem;\n  font-weight: 900;\n  margin: auto 0 10px 0;\n  grid-area: head1;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.avatar[data-v-77c36763] {\n  width: 100px;\n  height: 100px;\n  margin-right: 10px;\n  border: 1px solid black;\n  border-radius: 10px;\n  padding: 5px;\n}\n.icon[data-v-77c36763] {\n  width: 26px;\n  height: 26px;\n  margin: 5px 10px 0 0;\n}\n", ""]);

// exports


/***/ }),
/* 167 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__DashboardContenedorComponent__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__DashboardContenedorComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__DashboardContenedorComponent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__RankingUsuarioComponent__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__RankingUsuarioComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__RankingUsuarioComponent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ModalWindowComponent__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ModalWindowComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__ModalWindowComponent__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  name: "dashboard-contenedor-component",
  props: {
    user: { required: true, type: Object },
    avatar: { required: true, type: String },
    statistics: { required: true, type: Array },
    usualopponents: { required: true, type: Array }
  },
  components: {
    DashboardContenedorComponent: __WEBPACK_IMPORTED_MODULE_0__DashboardContenedorComponent___default.a
  },
  data: function data() {
    return {
      c_user: null,
      c_avatar: null,
      c_statistics: null,
      c_usualopponents: null,
      c_nivelMedio: 0,
      c_nivelES: 0,
      c_nivelEN: 0,
      c_ganadas: 0,
      c_perdidas: 0,
      c_window: false,
      c_title: "",
      c_message: "",
      c_idg: null
    };
  },

  methods: {
    onGetData: function onGetData(nivel, ganadas, perdidas, nivelES, nivelEN) {
      this.c_nivelMedio = nivel;
      this.c_ganadas = ganadas;
      this.c_perdidas = perdidas;
      this.c_nivelES = nivelES;
      this.c_nivelEN = nivelEN;

      if (isNaN(this.c_nivelMedio)) {
        this.c_nivelMedio = 0;
      }
    },
    listenForBroadcast: function listenForBroadcast() {
      var _this = this;

      console.log("Escuchando canales: user" + this.c_user.id);
      var vm = this;
      Echo.private("user." + this.c_user.id).listen("AcceptedChallenge", function (e) {
        vm.c_title = "Confirmación de partida ";
        vm.c_message = "Generada partida " + e.gameId + "\nIdioma: " + e.lang + "\nOponente\n\tid: " + e.oppoId + "\n\tNombre: " + e.oppoName + "\n\tPaís: " + e.oppoCountry + "\n\tPulsa sobre esta ventana para acceder a la partida.";

        vm.c_idg = e.gameId;
        vm.c_window = true;
        Push.create(_this.c_title, {
          body: _this.c_message,
          icon: "/img/notify_success.png",
          timeout: 6000,
          onClick: function onClick() {
            this.gotoGame();
            this.window.close();
          }
        });
      });
    },
    gotoGame: function gotoGame(event, idg) {
      if (idg === undefined) {
        idg = this.c_idg;
      }
      var url = location.protocol + "//" + location.hostname + (location.port ? ":" + location.port : "");
      window.location.href = url + "/scrabble/game/" + idg;
    }
  },
  created: function created() {
    this.c_user = this.user;
    this.c_avatar = this.avatar;
    this.c_statistics = this.statistics;
    this.c_usualopponents = this.usualopponents;
    this.listenForBroadcast();
  },
  mounted: function mounted() {
    console.log("Componete dashboard-contenedor-component montado");
  }
});

/***/ }),
/* 168 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(169);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("6bdf6dc1", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-32e2715e\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./RankingUsuarioComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-32e2715e\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./RankingUsuarioComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 169 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(1)(false);
// imports


// module
exports.push([module.i, "\n.rankingContainer[data-v-32e2715e] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: start;\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n}\n@media screen and (max-width: 1450px) {\n.rankingContainer[data-v-32e2715e] {\n      -webkit-box-pack: start;\n          -ms-flex-pack: start;\n              justify-content: flex-start;\n      -webkit-box-align: center;\n          -ms-flex-align: center;\n              align-items: center;\n}\n}\n.ranking[data-v-32e2715e] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  margin: 5px 10px 5px 5px;\n}\n.rankingES[data-v-32e2715e],\n.rankingEN[data-v-32e2715e] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: row;\n          flex-direction: row;\n  margin: 5px 10px 5px 5px;\n  -webkit-box-pack: start;\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n}\n@media screen and (max-width: 866px) {\n.rankingES[data-v-32e2715e],\n    .rankingEN[data-v-32e2715e] {\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: normal;\n          -ms-flex-direction: column;\n              flex-direction: column;\n      -webkit-box-pack: center;\n          -ms-flex-pack: center;\n              justify-content: center;\n      -webkit-box-align: center;\n          -ms-flex-align: center;\n              align-items: center;\n}\n}\n.userInfo[data-v-32e2715e] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: row;\n          flex-direction: row;\n  margin: 5px 5px 5px 5px;\n  width: 100%;\n}\n.userRanked[data-v-32e2715e] {\n  display: grid;\n  grid-template-columns: 50px 150px;\n  grid-template-rows: 50px 50px;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  grid-template-areas: \"img rank\" \"img rank\";\n}\n.tituloH1[data-v-32e2715e] {\n  font-family: Roboto, sans-serif;\n  font-size: 1.3rem;\n  font-weight: 900;\n}\n.tituloH2[data-v-32e2715e] {\n  grid-area: titulo;\n  font-family: Roboto, sans-serif;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: row;\n          flex-direction: row;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.tituloH2 img[data-v-32e2715e] {\n    width: 25px;\n    height: 25px;\n    margin-left: 5px;\n}\n@media screen and (max-width: 1450px) {\n.tituloH2[data-v-32e2715e] {\n      margin-bottom: 8px;\n}\n}\n.avatar[data-v-32e2715e] {\n  grid-area: img;\n  width: 50px;\n  height: 50px;\n  margin: 3px 5px 3px 3px;\n  border: 1px solid black;\n  border-radius: 10px;\n  padding: 5px;\n}\n@media screen and (max-width: 1450px) {\n.avatar[data-v-32e2715e] {\n      margin: 3px 10px 3px 0;\n}\n}\n.infoUser[data-v-32e2715e] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  margin: auto 10px;\n  padding: auto;\n}\n", ""]);

// exports


/***/ }),
/* 170 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "ranking-usuario-component",
  props: ["user", "statistics", "usualopponents", "nivelES", "nivelEN"],
  data: function data() {
    return {
      c_user: null,
      c_statistics: null,
      c_usualopponents: null,
      c_rankingES: [],
      c_rankingEN: [],
      c_users: [],
      u_player: {},
      u_level: [{ en: null }, { es: null }],
      c_player: {}
    };
  },

  methods: {
    getLevelsUser: function getLevelsUser() {
      var levelEN = 0;
      var levelES = 0;
      this.c_statistics.forEach(function (element) {
        if (element.language_code == "en") {
          levelEN = element.level;
        }
        if (element.language_code == "es") {
          levelES = element.level;
        }
      });
      this.u_level["en"] = levelEN;
      this.u_level["es"] = levelES;
    },
    addUser: function addUser() {
      this.u_player = {
        p_id: this.c_user.id,
        p_name: this.c_user.name,
        p_avatar: this.c_user.avatar,
        p_level: this.u_level
      };
      this.c_users.push(this.u_player);
    },
    createUsers: function createUsers() {
      var _this = this;

      var id = null;
      var name = null;
      var avatar = null;
      var level = [{ en: null }, { es: null }];
      this.c_usualopponents.forEach(function (element) {
        id = element.id;
        name = element.name;
        avatar = element.avatar;
        level["en"] = element.level["en"];
        level["es"] = element.level["es"];
        _this.c_player = {
          p_id: id,
          p_name: name,
          p_avatar: avatar,
          p_level: level
        };
        _this.c_users.push(_this.c_player);
      });
      this.addUser();
      console.log(this.c_users);
    },
    createRankingES: function createRankingES() {
      var _this2 = this;

      var id, name, avatar, level, obj;
      this.c_users.forEach(function (element) {
        id = element.p_id;
        name = element.p_name;
        avatar = element.p_avatar;
        level = element.p_level["es"];
        obj = {
          p_id: id,
          p_name: name,
          p_avatar: avatar,
          p_level: level
        };
        _this2.c_rankingES.push(obj);
        _this2.c_rankingES.sort(function (a, b) {
          return a.p_level > b.p_level ? -1 : 1;
        });
        console.log(_this2.c_rankingES);
      });
    },
    createRankingEN: function createRankingEN() {
      var _this3 = this;

      var id, name, avatar, level, obj;
      this.c_users.forEach(function (element) {
        id = element.p_id;
        name = element.p_name;
        avatar = element.p_avatar;
        level = element.p_level["en"];
        obj = {
          p_id: id,
          p_name: name,
          p_avatar: avatar,
          p_level: level
        };
        _this3.c_rankingEN.push(obj);
        _this3.c_rankingEN.sort(function (a, b) {
          return a.p_level > b.p_level ? -1 : 1;
        });
        console.log(_this3.c_rankingEN);
      });
    },
    listenForBroadcast: function listenForBroadcast() {
      var _this4 = this;

      console.log("Escuchando canales: user" + this.c_user.id);
      Echo.private("user." + this.c_user.id).listen("AcceptedChallenge", function (e) {
        alert("Generada partida " + e.gameId + "\nIdioma: " + e.lang + "\nOponente\n\tid: " + e.oppoId + "\n\tNombre: " + e.oppoName + "\n\tPaís: " + e.oppoCountry + "\n\tAvatar: " + e.oppoAvatar);

        _this4.requestChallengeId = e.gameId;

        console.log("Recibido");
      });
    }
  },
  created: function created() {
    this.c_user = this.user;
    this.c_statistics = this.statistics;
    this.c_usualopponents = this.usualopponents;
    this.getLevelsUser();
    this.createUsers();
    this.createRankingES();
    this.createRankingEN();
    this.listenForBroadcast();
  },
  mounted: function mounted() {
    console.log("Componente ranking-usuario-component montado.");
  }
});

/***/ }),
/* 171 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "rankingContainer" }, [
    _vm._m(0),
    _vm._v(" "),
    _c("div", { staticClass: "ranking" }, [
      _vm.c_rankingES
        .map(function(e) {
          return e.p_level
        })
        .indexOf(0) == -1
        ? _c(
            "div",
            { staticClass: "rankingES" },
            [
              _c("div", { staticClass: "tituloH2" }, [
                _c("span", [_vm._v("Idioma")]),
                _vm._v(" "),
                _c("img", {
                  attrs: {
                    src: "../img/espana.png",
                    title: "Idioma español.",
                    alt:
                      "Icon made by http://www.freepik.com from http://www.flaticon.com"
                  }
                })
              ]),
              _vm._v(" "),
              _vm._l(_vm.c_rankingES, function(element, index) {
                return _c("div", { key: index, staticClass: "userInfo" }, [
                  _c("div", { staticClass: "userRanked" }, [
                    _c("img", {
                      staticClass: "avatar",
                      attrs: { src: element.p_avatar, alt: "Sin avatar" }
                    }),
                    _vm._v(" "),
                    _c("div", { staticClass: "infoUser" }, [
                      _c("span", [_vm._v(_vm._s(element.p_name))]),
                      _vm._v(" "),
                      _c("span", [_vm._v("Nivel " + _vm._s(element.p_level))]),
                      _vm._v(" "),
                      _c("span", [_vm._v("Puesto #" + _vm._s(index + 1))])
                    ])
                  ])
                ])
              })
            ],
            2
          )
        : _vm._e(),
      _vm._v(" "),
      _vm.c_rankingEN
        .map(function(e) {
          return e.p_level
        })
        .indexOf(0) == -1
        ? _c(
            "div",
            { staticClass: "rankingEN" },
            [
              _c("div", { staticClass: "tituloH2" }, [
                _c("span", [_vm._v("Idioma")]),
                _vm._v(" "),
                _c("img", {
                  attrs: {
                    src: "../img/reino-unido.png",
                    title: "Idioma Inglés.",
                    alt:
                      "Icon made by http://www.freepik.com from http://www.flaticon.com"
                  }
                })
              ]),
              _vm._v(" "),
              _vm._l(_vm.c_rankingEN, function(element, index) {
                return _c("div", { key: index, staticClass: "userInfo" }, [
                  _c("div", { staticClass: "userRanked" }, [
                    _c("img", {
                      staticClass: "avatar",
                      attrs: { src: element.p_avatar, alt: "Sin avatar" }
                    }),
                    _vm._v(" "),
                    _c("div", { staticClass: "infoUser" }, [
                      _c("span", [_vm._v(_vm._s(element.p_name))]),
                      _vm._v(" "),
                      _c("span", [_vm._v("Nivel " + _vm._s(element.p_level))]),
                      _vm._v(" "),
                      _c("span", [_vm._v("Puesto #" + _vm._s(index + 1))])
                    ])
                  ])
                ])
              })
            ],
            2
          )
        : _vm._e()
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "tituloH1" }, [
      _c("span", [_vm._v("CLASIFICACIÓN")])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-32e2715e", module.exports)
  }
}

/***/ }),
/* 172 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "dashboard_contenedor" }, [
    _c("div", { staticClass: "zona_estatica" }, [
      _c("img", {
        attrs: {
          src: "../img/blurbus-1528285.jpg",
          title: "Scrabble",
          alt: "FreeImages.com/Slivester Nuenenorl"
        }
      })
    ]),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "zona_dinamica" },
      [
        _c("div", { staticClass: "head1" }, [
          _c("div", { staticClass: "titulo_head" }, [_vm._v("Scrabble")]),
          _vm._v(" "),
          _c("div", { staticClass: "info_head" }, [
            _c("div", [
              _c("img", {
                staticClass: "avatar",
                attrs: { src: _vm.c_avatar, title: _vm.c_user.name }
              }),
              _vm._v(" "),
              _c("span", [_vm._v(_vm._s(_vm.c_user.name))])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "info_statistics" }, [
              _c("img", {
                staticClass: "icon",
                attrs: {
                  src: "../img/xp.png",
                  alt:
                    "Icon made by Alfredo Hernandez [https://www.flaticon.es/autores/alfredo-hernandez] from [https://www.flaticon.com] ",
                  title: "Nivel Jugador"
                }
              }),
              _vm._v(" "),
              _c("span", [_vm._v(_vm._s(_vm.c_nivelMedio))]),
              _vm._v(" "),
              _c("i", {
                staticClass: "fas fa-user-graduate",
                attrs: { title: "Partidas Ganadas" }
              }),
              _vm._v(" "),
              _c("span", [_vm._v(_vm._s(_vm.c_ganadas))]),
              _vm._v(" "),
              _c("i", {
                staticClass: "far fa-thumbs-down",
                attrs: { title: "Partidas Perdidas" }
              }),
              _vm._v(" "),
              _c("span", [_vm._v(_vm._s(_vm.c_perdidas))])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("estadisticas-usuario-component", {
          staticClass: "user_statistics",
          attrs: { user: _vm.c_user, statistics: _vm.c_statistics },
          on: { "recibir-datos": _vm.onGetData }
        }),
        _vm._v(" "),
        _c("ranking-usuario-component", {
          staticClass: "userRanking",
          attrs: {
            user: _vm.c_user,
            statistics: _vm.c_statistics,
            usualopponents: _vm.c_usualopponents
          }
        })
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-77c36763", module.exports)
  }
}

/***/ }),
/* 173 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(174)
}
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(176)
/* template */
var __vue_template__ = __webpack_require__(177)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-1f0fef3c"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/DashboardInfoChallengesComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1f0fef3c", Component.options)
  } else {
    hotAPI.reload("data-v-1f0fef3c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 174 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(175);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("03efca73", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1f0fef3c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./DashboardInfoChallengesComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1f0fef3c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./DashboardInfoChallengesComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 175 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(1)(false);
// imports


// module
exports.push([module.i, "\n.infoChallengesContenedor[data-v-1f0fef3c] {\n  display: grid;\n  grid-template-columns: 1fr 1fr;\n  grid-template-rows: 445px 430px;\n  grid-template-areas: \"finalizadas en_juego\" \"buscar buscar\";\n  grid-gap: 5px 5px;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: start;\n      -ms-flex-align: start;\n          align-items: flex-start;\n  width: 100%;\n  margin: 5px 0;\n}\n@media screen and (max-width: 1450px) {\n.infoChallengesContenedor[data-v-1f0fef3c] {\n      grid-template-columns: 1fr;\n      grid-template-rows: 425px 425px 425px;\n      grid-template-areas: \"finalizadas\" \"en_juego\" \"buscar\";\n}\n}\n.challengesContenedor[data-v-1f0fef3c],\n.gamesListContenedor[data-v-1f0fef3c],\n.userListContenedor[data-v-1f0fef3c] {\n  display: grid;\n  border-radius: 0.6rem;\n  font-family: Roboto, sans-serif;\n  font-size: 0.9rem;\n  line-height: 2rem;\n  color: #000;\n  width: 100%;\n  margin: 5px;\n}\n.challengesContenedor[data-v-1f0fef3c] {\n  grid-area: en_juego;\n  grid-template-rows: 80px 1fr;\n  grid-template-areas: \"head\" \"main\";\n}\n.gamesListContenedor[data-v-1f0fef3c] {\n  grid-area: finalizadas;\n  grid-template-rows: 80px 270px 60px;\n  grid-template-areas: \"head\" \"main\" \"navigation\";\n  height: 410px;\n}\n.userListContenedor[data-v-1f0fef3c] {\n  grid-area: buscar;\n  grid-template-rows: 80px 1fr;\n  grid-template-areas: \"head\" \"main\";\n  height: 410px;\n}\n.head[data-v-1f0fef3c] {\n  grid-area: head;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  border-top-right-radius: 10px;\n  border-top-left-radius: 10px;\n  border-bottom-right-radius: 3px;\n  border-bottom-left-radius: 3px;\n  background: rgba(81, 81, 82, 0.8);\n  margin: 15px 0;\n  color: #fff;\n  font-family: Roboto-Medium, sans-serif;\n  font-weight: bold;\n  font-size: 0.9rem;\n}\n.acciones ul[data-v-1f0fef3c] {\n  font-size: 0;\n  list-style-type: none;\n  height: 40px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  margin-top: 3px;\n  margin-bottom: 0.5rem !important;\n}\n.acciones ul li[data-v-1f0fef3c] {\n    font-family: Roboto, sans-serif;\n    font-size: 0.8rem;\n    font-weight: 400;\n    color: #f9f9f9;\n    padding: 8px;\n    display: inline-block;\n    position: relative;\n    margin: auto 5px 0;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.acciones ul li ul[data-v-1f0fef3c] {\n      display: none;\n}\n.acciones ul li[data-v-1f0fef3c]:hover {\n      cursor: pointer;\n      background-color: #f2f2f2;\n      color: #333;\n}\n.acciones ul li:hover ul[data-v-1f0fef3c] {\n        display: block;\n        width: 200px;\n        top: 45px;\n        left: -5px;\n        z-index: 2;\n        position: absolute;\n}\n.acciones ul li:hover ul li[data-v-1f0fef3c] {\n          display: block;\n          background-color: #e7e7e7;\n          color: #333;\n          width: 150px;\n          z-index: 2;\n          padding-left: 5px;\n          display: -webkit-box;\n          display: -ms-flexbox;\n          display: flex;\n          -webkit-box-pack: justify;\n              -ms-flex-pack: justify;\n                  justify-content: space-between;\n}\n.acciones ul li:hover ul li span[data-v-1f0fef3c] {\n            color: #f9f9f9;\n            background-color: #f03861;\n            padding: 2px 5px;\n            margin: 5px 10px;\n            text-align: center;\n            font-size: 0.8rem;\n            border-radius: 3px;\n}\n.acciones ul li:hover ul li[data-v-1f0fef3c]:hover {\n            background-color: #e0e0e0;\n}\n.acciones ul li:hover ul li:hover span[data-v-1f0fef3c] {\n              background-color: #ee204e;\n}\n.main[data-v-1f0fef3c] {\n  grid-area: main;\n  background: #fff;\n  border-radius: 3px;\n}\n.navigation[data-v-1f0fef3c] {\n  grid-area: navigation;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  background: #fff;\n  font-family: Roboto-Medium, sans-serif;\n  font-weight: bold;\n  font-size: 0.9rem;\n}\n.navigation span[data-v-1f0fef3c] {\n    margin: 0 10px;\n}\n.navigation span[data-v-1f0fef3c]:hover {\n    cursor: pointer;\n}\n.challengesList[data-v-1f0fef3c],\n.gamesList[data-v-1f0fef3c],\n.userList[data-v-1f0fef3c] {\n  width: 100%;\n  -webkit-backface-visibility: hidden;\n          backface-visibility: hidden;\n  border: 1px solid rgba(231, 231, 231, 0.3);\n  margin-bottom: 0 !important;\n}\n.challengesList li[data-v-1f0fef3c],\n  .gamesList li[data-v-1f0fef3c],\n  .userList li[data-v-1f0fef3c] {\n    height: 5.6rem;\n    line-height: 1.5;\n    display: block;\n    overflow: hidden;\n    z-index: 1;\n    position: relative;\n    border-bottom: 1px solid rgba(231, 231, 231, 0.3);\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.challengesList li span[data-v-1f0fef3c],\n    .gamesList li span[data-v-1f0fef3c],\n    .userList li span[data-v-1f0fef3c] {\n      margin-right: 10px;\n      margin-left: 10px;\n}\n.challengesList li span.row[data-v-1f0fef3c],\n    .gamesList li span.row[data-v-1f0fef3c],\n    .userList li span.row[data-v-1f0fef3c] {\n      display: inline-block;\n      vertical-align: middle;\n      position: relative;\n}\n.challengesList li span.row .title[data-v-1f0fef3c],\n      .gamesList li span.row .title[data-v-1f0fef3c],\n      .userList li span.row .title[data-v-1f0fef3c] {\n        font-family: Roboto-Medium, sans-serif;\n        font-size: 0.9rem;\n        color: black;\n        top: -0.2rem;\n        position: relative;\n        text-align: center;\n}\n.challengesList li span.row .content[data-v-1f0fef3c],\n      .gamesList li span.row .content[data-v-1f0fef3c],\n      .userList li span.row .content[data-v-1f0fef3c] {\n        text-align: center;\n        font-size: 0.9rem;\n}\n.challengesList li span.row.num[data-v-1f0fef3c],\n    .gamesList li span.row.num[data-v-1f0fef3c],\n    .userList li span.row.num[data-v-1f0fef3c] {\n      margin: auto;\n      font-family: Roboto-Medium, sans-serif;\n      font-size: 0.9rem;\n      color: black;\n      text-overflow: ellipsis;\n      overflow: hidden;\n      white-space: nowrap;\n}\n.challengesList li span.row.avat[data-v-1f0fef3c],\n    .gamesList li span.row.avat[data-v-1f0fef3c],\n    .userList li span.row.avat[data-v-1f0fef3c] {\n      width: 2.8rem;\n      height: 2.8rem;\n      position: relative;\n      margin-left: 0.4rem;\n      margin-right: 0.4rem;\n      background-color: #e2e3e4;\n}\n.challengesList li span.row.avat img[data-v-1f0fef3c],\n    .gamesList li span.row.avat img[data-v-1f0fef3c],\n    .userList li span.row.avat img[data-v-1f0fef3c] {\n      -webkit-backface-visibility: hidden;\n              backface-visibility: hidden;\n}\n.challengesList li span.row.id[data-v-1f0fef3c],\n    .gamesList li span.row.id[data-v-1f0fef3c],\n    .userList li span.row.id[data-v-1f0fef3c] {\n      margin: auto;\n      display: block;\n      line-height: 2rem;\n      font-family: Roboto-Medium, sans-serif;\n      color: black;\n      top: -0.2rem;\n      position: relative;\n}\n.challengesList li span.row.name[data-v-1f0fef3c],\n    .gamesList li span.row.name[data-v-1f0fef3c],\n    .userList li span.row.name[data-v-1f0fef3c] {\n      margin: auto;\n}\n.challengesList li span.row.name .name[data-v-1f0fef3c],\n      .gamesList li span.row.name .name[data-v-1f0fef3c],\n      .userList li span.row.name .name[data-v-1f0fef3c] {\n        display: block;\n        line-height: 2rem;\n        font-family: Roboto-Medium, sans-serif;\n        color: black;\n        top: -0.2rem;\n        position: relative;\n}\n.challengesList li span.row.email[data-v-1f0fef3c],\n    .gamesList li span.row.email[data-v-1f0fef3c],\n    .userList li span.row.email[data-v-1f0fef3c] {\n      margin: auto;\n      display: block;\n      line-height: 2rem;\n      font-family: Roboto-Medium, sans-serif;\n      color: black;\n      top: -0.2rem;\n      position: relative;\n}\n.challengesList li span.row.score[data-v-1f0fef3c],\n    .gamesList li span.row.score[data-v-1f0fef3c],\n    .userList li span.row.score[data-v-1f0fef3c] {\n      margin: auto;\n      display: block;\n      line-height: 2rem;\n      font-family: Roboto-Medium, sans-serif;\n      color: black;\n      top: -0.2rem;\n      position: relative;\n}\n.challengesList li span.row.vs[data-v-1f0fef3c],\n    .gamesList li span.row.vs[data-v-1f0fef3c],\n    .userList li span.row.vs[data-v-1f0fef3c] {\n      margin: auto;\n      font-family: Roboto-Medium, sans-serif;\n      font-size: 0.9rem;\n      color: black;\n      text-align: center;\n}\n.challengesList li span.row.state[data-v-1f0fef3c],\n    .gamesList li span.row.state[data-v-1f0fef3c],\n    .userList li span.row.state[data-v-1f0fef3c] {\n      margin: auto;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: normal;\n          -ms-flex-direction: column;\n              flex-direction: column;\n      -webkit-box-align: center;\n          -ms-flex-align: center;\n              align-items: center;\n      line-height: 2rem;\n      font-family: Roboto-Medium, sans-serif;\n      color: black;\n      top: -0.2rem;\n      position: relative;\n}\n.challengesList[data-v-1f0fef3c] {\n  height: 358px;\n  overflow: auto;\n}\n.challengesList a[data-v-1f0fef3c] {\n    display: grid;\n    grid-template-columns: 1fr 1fr 1fr 0.5fr 1fr 1fr 1fr 1fr;\n    grid-template-areas: \"player p_name p_puntos vs o_puntos o_name opponent state\";\n    justify-items: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    position: relative;\n    text-decoration: none;\n    outline: none;\n    cursor: pointer;\n}\n@media screen and (max-width: 555px) {\n.challengesList a[data-v-1f0fef3c] {\n        grid-template-columns: 1fr 1fr 0.5fr 1fr 1fr 1fr;\n        grid-template-areas: \"p_name p_puntos vs o_puntos o_name state\";\n}\n}\n.listGames a[data-v-1f0fef3c] {\n  display: grid;\n  grid-template-columns: 1fr 1fr 1fr 1fr 0.5fr 1fr 1fr 1fr;\n  grid-template-areas: \"lenguage p1_avatar p1_name p1_score vs_ p2_score p2_name p2_avatar\";\n  justify-items: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  position: relative;\n  height: 100%;\n  text-decoration: none;\n  outline: none;\n  cursor: pointer;\n}\n@media screen and (max-width: 555px) {\n.listGames a[data-v-1f0fef3c] {\n      grid-template-columns: 1fr 1fr 1fr 0.5fr 1fr 1fr;\n      grid-template-areas: \"lenguage p1_name p1_score vs_ p2_score p2_name\";\n}\n}\n.userList[data-v-1f0fef3c] {\n  height: 358px;\n  overflow: auto;\n}\n.userList li[data-v-1f0fef3c] {\n    display: grid;\n    grid-template-columns: 0.2fr 1fr 1fr 0.6fr;\n    grid-template-areas: \"id nombre email retar\";\n    width: 100%;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    position: relative;\n}\n.avatar .avat[data-v-1f0fef3c] {\n  width: 100%;\n  height: 100%;\n}\n.avatar img[data-v-1f0fef3c] {\n  width: 100%;\n  height: 100%;\n  display: block;\n  overflow: hidden;\n  border-radius: 2px;\n}\n#avatarPlayer[data-v-1f0fef3c] {\n  grid-area: player;\n}\n@media screen and (max-width: 555px) {\n#avatarPlayer[data-v-1f0fef3c] {\n      display: none;\n}\n}\n#namePlayer[data-v-1f0fef3c] {\n  grid-area: p_name;\n}\n#puntosPlayer[data-v-1f0fef3c] {\n  grid-area: p_puntos;\n}\n#vs[data-v-1f0fef3c] {\n  grid-area: vs;\n}\n#opponentPuntos[data-v-1f0fef3c] {\n  grid-area: o_puntos;\n}\n#opponentName[data-v-1f0fef3c] {\n  grid-area: o_name;\n}\n#opponentAvatar[data-v-1f0fef3c] {\n  grid-area: opponent;\n}\n@media screen and (max-width: 555px) {\n#opponentAvatar[data-v-1f0fef3c] {\n      display: none;\n}\n}\n#state[data-v-1f0fef3c] {\n  grid-area: state;\n}\n#lenguage[data-v-1f0fef3c] {\n  grid-area: lenguage;\n}\n#p1_avatar[data-v-1f0fef3c] {\n  grid-area: p1_avatar;\n}\n@media screen and (max-width: 555px) {\n#p1_avatar[data-v-1f0fef3c] {\n      display: none;\n}\n}\n#p1_name[data-v-1f0fef3c] {\n  grid-area: p1_name;\n}\n#p1_score[data-v-1f0fef3c] {\n  grid-area: p1_score;\n}\n#vs_[data-v-1f0fef3c] {\n  grid-area: vs_;\n}\n#p2_score[data-v-1f0fef3c] {\n  grid-area: p2_score;\n}\n#p2_name[data-v-1f0fef3c] {\n  grid-area: p2_name;\n}\n#p2_avatar[data-v-1f0fef3c] {\n  grid-area: p2_avatar;\n}\n@media screen and (max-width: 555px) {\n#p2_avatar[data-v-1f0fef3c] {\n      display: none;\n}\n}\n#idPlayer[data-v-1f0fef3c] {\n  grid-area: id;\n}\n#namePlyr[data-v-1f0fef3c] {\n  grid-area: nombre;\n}\n#emailPlayer[data-v-1f0fef3c] {\n  grid-area: email;\n}\n#retarPlayer[data-v-1f0fef3c] {\n  grid-area: retar;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n#retarPlayer .content a[data-v-1f0fef3c] {\n    text-decoration: none;\n    cursor: pointer;\n}\n#retarPlayer .content a span[data-v-1f0fef3c] {\n      color: #f9f9f9;\n      background-color: #f03861;\n      padding: 2px 5px;\n      margin: 5px 10px;\n      text-align: center;\n      font-size: 0.8rem;\n      border-radius: 3px;\n}\n.rojo[data-v-1f0fef3c] {\n  color: red;\n}\n.azul[data-v-1f0fef3c] {\n  color: blue;\n}\n.oculto[data-v-1f0fef3c] {\n  visibility: hidden;\n}\n.visible[data-v-1f0fef3c] {\n  visibility: visible;\n}\n", ""]);

// exports


/***/ }),
/* 176 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "dashboard-infochallenges-component",
  props: {
    user: { required: true, type: Object },
    avatar: { required: true, type: String },
    games: { required: true, type: Array }
  },
  data: function data() {
    return {
      c_user: null,
      c_userName: null,
      c_avatar: null,
      c_games: null,
      c_gamesList: null,
      c_infoGamesList: null,
      c_currentPage: null,
      c_firsPageURL: null,
      c_lastPageURL: null,
      c_nextPageURL: null,
      c_prevPageURL: null,
      c_lastPage: null,
      c_indexFrom: null,
      c_indexTo: null,
      c_total: null,
      c_ingles: "en",
      c_espanol: "es",
      c_primero: "<<",
      c_ultimo: ">>",
      c_siguiente: ">",
      c_anterior: "<",
      c_menor_igual: "<=",
      c_mayor_igual: ">=",
      c_nivel_menor_es: 0,
      c_nivel_menor_en: 0,
      c_nivel_mayor_es: 0,
      c_nivel_mayor_en: 0,
      c_sin_datos: "",
      c_buscarUser: null,
      c_listUsers: [],
      c_listUsersFiltered: [],
      c_search: "",
      c_requestChallengeId: null,
      c_title: "",
      c_message: ""
    };
  },

  computed: {
    filteredPlayers: function filteredPlayers() {
      var _this = this;

      return this.c_listUsersFiltered.filter(function (player) {
        return player.name.match(_this.c_search);
      });
    }
  },
  methods: {
    gotoGame: function gotoGame(idg) {
      console.log("id" + idg);
      if (idg === undefined) {
        idg = this.c_requestChallengeId;
      }
      var url = location.protocol + "//" + location.hostname + (location.port ? ":" + location.port : "");
      window.location.href = url + "/scrabble/game/" + idg;
    },
    newGame: function newGame(language) {
      var vm = this;
      var idioma = language;
      // solicita una partida en español
      return axios.post("/scrabble/challenge/request/" + idioma).then(function (response) {
        console.log("Reto creado:", response);
        vm.c_title = "Acción realizada";
        vm.c_message = "Reto Creado. Espera la confirmación";
        Push.create(vm.c_title, {
          body: vm.c_message,
          icon: "/img/notify_wait.png",
          timeout: 10000,
          onClick: function onClick() {
            vm.window.close();
          }
        });
      }).catch(function (error) {
        console.log("ERROR: " + error);
        vm.c_title = "Error";
        vm.c_message = error;
        Push.create(vm.c_title, {
          body: vm.c_message,
          icon: "/img/notify_error.png",
          timeout: 10000,
          onClick: function onClick() {
            vm.window.close();
          }
        });
      });
    },
    newChallengeLessLevel: function newChallengeLessLevel(language, level) {
      var vm = this;
      var idioma = language;
      var nivel = level * -1;
      // solicita una partida en español
      return axios.post("/scrabble/challenge/request/" + idioma + "/level/" + nivel).then(function (response) {
        console.log("Reto creado:", response);
        vm.c_nivel_menor_es = 0;
        vm.c_nivel_menor_en = 0;
        vm.c_title = "Acción realizada";
        vm.c_message = "Reto Creado. Espera la confirmación";
        Push.create(vm.c_title, {
          body: vm.c_message,
          icon: "/img/notify_wait.png",
          timeout: 10000,
          onClick: function onClick() {
            vm.window.close();
          }
        });
      }).catch(function (error) {
        console.log("ERROR: " + error);
        vm.c_title = "Error";
        vm.c_message = error;
        Push.create(vm.c_title, {
          body: vm.c_message,
          icon: "/img/notify_error.png",
          timeout: 10000,
          onClick: function onClick() {
            vm.window.close();
          }
        });
      });
    },
    newChallengeHigherLevel: function newChallengeHigherLevel(language, level) {
      var vm = this;
      var idioma = language;
      var nivel = level;
      // solicita una partida en español
      return axios.post("/scrabble/challenge/request/" + idioma + "/level/" + nivel).then(function (response) {
        console.log("Reto creado:", response);
        vm.c_nivel_mayor_es = 0;
        vm.c_nivel_mayor_en = 0;
        vm.c_title = "Acción realizada";
        vm.c_message = "Reto Creado. Espera la confirmación";
        Push.create(vm.c_title, {
          body: vm.c_message,
          icon: "/img/notify_wait.png",
          timeout: 10000,
          onClick: function onClick() {
            vm.window.close();
          }
        });
      }).catch(function (error) {
        console.log("ERROR: " + error);
        vm.c_title = "Error";
        vm.c_message = error;
        Push.create(vm.c_title, {
          body: vm.c_message,
          icon: "/img/notify_error.png",
          timeout: 10000,
          onClick: function onClick() {
            vm.window.close();
          }
        });
      });
    },
    newChallengeSelectUser: function newChallengeSelectUser(language, id) {
      var vm = this;
      var idioma = language;
      var _id = id;
      // solicita una partida en español
      return axios.post("/scrabble/challenge/request/" + idioma + "/against/" + _id).then(function (response) {
        console.log("Reto creado:", response);
        vm.c_title = "Acción realizada";
        vm.c_message = "Reto Creado. Espera la confirmación";
        Push.create(vm.c_title, {
          body: vm.c_message,
          icon: "/img/notify_wait.png",
          timeout: 10000,
          onClick: function onClick() {
            vm.window.close();
          }
        });
      }).catch(function (error) {
        console.log("ERROR: " + error);
        vm.c_title = "Error";
        vm.c_message = error;
        Push.create(vm.c_title, {
          body: vm.c_message,
          icon: "/img/notify_error.png",
          timeout: 10000,
          onClick: function onClick() {
            vm.window.close();
          }
        });
      });
    },
    userGames: function userGames() {
      var vm = this;
      // partidas del usuario paginadas, 3 por página
      return axios.get("/scrabble/user/games/3").then(function (response) {
        vm.c_infoGamesList = response.data;
        vm.c_gamesList = vm.c_infoGamesList.data; // lista de partidas del usuario
        vm.c_currentPage = vm.c_infoGamesList.current_page;
        vm.c_firsPageURL = vm.c_infoGamesList.first_page_url;
        vm.c_nextPageURL = vm.c_infoGamesList.next_page_url;
        vm.c_prevPageURL = vm.c_infoGamesList.prev_page_url;
        vm.c_lastPageURL = vm.c_infoGamesList.last_page_url;
        vm.c_lastPage = vm.c_infoGamesList.last_page;
        vm.c_indexFrom = vm.c_infoGamesList.from;
        vm.c_indexTo = vm.c_infoGamesList.to;
        vm.c_total = vm.c_infoGamesList.total;
        if (vm.c_gamesList.length == 0) {
          vm.c_sin_datos = vm.c_user.name + " no tiene partidas registradas.";
        }
      }).catch(function (error) {
        alert("ERROR: " + error);
        vm.c_title = "Error";
        vm.c_message = error;
        Push.create(vm.c_title, {
          body: vm.c_message,
          icon: "/img/notify_error.png",
          timeout: 10000,
          onClick: function onClick() {
            vm.window.close();
          }
        });
      });
    },
    userGamesPag: function userGamesPag(url) {
      var vm = this;
      var url = url;
      // partidas del usuario paginadas, 3 por página
      return axios.get(url).then(function (response) {
        vm.c_infoGamesList = response.data;
        vm.c_gamesList = vm.c_infoGamesList.data; // lista de partidas del usuario
        vm.c_currentPage = vm.c_infoGamesList.current_page;
        vm.c_firsPageURL = vm.c_infoGamesList.first_page_url;
        vm.c_nextPageURL = vm.c_infoGamesList.next_page_url;
        vm.c_prevPageURL = vm.c_infoGamesList.prev_page_url;
        vm.c_lastPageURL = vm.c_infoGamesList.last_page_url;
        vm.c_lastPage = vm.c_infoGamesList.last_page;
        vm.c_indexFrom = vm.c_infoGamesList.from;
        vm.c_indexTo = vm.c_infoGamesList.to;
        vm.c_total = vm.c_infoGamesList.total;

        if (vm.c_gamesList.length == 0) {
          vm.c_sin_datos = vm.c_user.name + " no tiene partidas en juego.";
        }
      }).catch(function (error) {
        alert("ERROR: " + error);
        vm.c_title = "Error";
        vm.c_message = error;
        Push.create(vm.c_title, {
          body: vm.c_message,
          icon: "/img/notify_error.png",
          timeout: 10000,
          onClick: function onClick() {
            vm.window.close();
          }
        });
      });
    },
    listUsers: function listUsers() {
      var vm = this;
      // solicita la lista de jugadores
      return axios.get("/scrabble/search/user/live", {
        params: {
          name: vm.c_buscarUser
        }
      }).then(function (response) {
        vm.c_listUsers = response.data;
        vm.c_listUsers.forEach(function (element) {
          if (element.name != vm.c_user.name) {
            vm.c_listUsersFiltered.push(element);
          }
        });
      }).catch(function (error) {
        alert("ERROR: " + error);
        vm.c_title = "Error";
        vm.c_message = error;
        Push.create(vm.c_title, {
          body: vm.c_message,
          icon: "/img/notify_error.png",
          timeout: 10000,
          onClick: function onClick() {
            vm.window.close();
          }
        });
      });
    }
    // listenForBroadcast: function() {
    //   var vm = this;
    //   //console.log("Escuchando canales: user" + this.c_user.id);
    //   Echo.private("user." + this.c_user.id).listen("AcceptedChallenge", e => {
    //     vm.c_message =
    //       "Generada partida " +
    //       e.gameId +
    //       "\nIdioma: " +
    //       e.lang +
    //       "\nOponente\n\tid: " +
    //       e.oppoId +
    //       "\n\tNombre: " +
    //       e.oppoName +
    //       "\n\tPaís: " +
    //       e.oppoCountry;

    //     vm.c_requestChallengeId = e.gameId;
    //     vm.c_title = "Reto Aceptado";
    //     vm.c_message = error;
    //     Push.create(vm.c_title, {
    //       body: vm.c_message,
    //       icon: "/img/notify_success.png",
    //       timeout: 10000,
    //       onClick: function() {
    //         vm.gotoGame();
    //         vm.window.close();
    //       }
    //     });
    //     //console.log("Recibido");
    //   });
    // }
  },
  created: function created() {
    this.c_user = this.user;
    this.c_userName = this.user.name;
    this.c_avatar = this.avatar;
    this.c_games = this.games;
    this.userGames();
    // this.listenForBroadcast();
    //console.log("RCID: " + this.c_requestChallengeId);
    this.listUsers();
  },
  mounted: function mounted() {
    console.log("Componente dashboard-infochallenges-component montado");
  },
  updated: function updated() {}
});

/***/ }),
/* 177 */
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "infoChallengesContenedor" }, [
      _c("div", { staticClass: "gamesListContenedor" }, [
        _c("div", { staticClass: "head" }, [
          _c("span", [
            _vm._v(
              "LISTADO DE PARTIDAS DE " +
                _vm._s(_vm._f("firstWord")(_vm.c_user.name))
            )
          ]),
          _vm._v(" "),
          _c("span", [_vm._v("Total: " + _vm._s(_vm.c_total))])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "main" }, [
          _vm.c_sin_datos != ""
            ? _c("span", { staticClass: "rojo" }, [
                _vm._v(_vm._s(_vm.c_sin_datos))
              ])
            : _vm._e(),
          _vm._v(" "),
          _vm.c_sin_datos == ""
            ? _c(
                "ul",
                { staticClass: "gamesList" },
                _vm._l(_vm.c_gamesList, function(game, index) {
                  return _c("li", { key: index, staticClass: "listGames" }, [
                    _c("a", [
                      _c(
                        "span",
                        { staticClass: "row avat", attrs: { id: "lenguage" } },
                        [
                          _c("div", { staticClass: "avatar" }, [
                            _c("img", {
                              attrs: {
                                src:
                                  game.language == "es"
                                    ? "../img/espana.png"
                                    : "../img/reino-unido.png",
                                alt:
                                  "Icon made by www.freepik.com from www.flaticon.com",
                                title: "Lenguage: " + game.language
                              }
                            })
                          ])
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "span",
                        { staticClass: "row avat", attrs: { id: "p1_avatar" } },
                        [
                          _c("div", { staticClass: "avatar" }, [
                            _c("img", {
                              attrs: {
                                src: game.player1.avatar,
                                alt: "Sin avatar",
                                title: game.player1.name
                              }
                            })
                          ])
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "span",
                        { staticClass: "row name", attrs: { id: "p1_name" } },
                        [
                          _c("span", { staticClass: "name" }, [
                            _vm._v(
                              _vm._s(_vm._f("firstWord")(game.player1.name))
                            )
                          ])
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "span",
                        { staticClass: "row score", attrs: { id: "p1_score" } },
                        [
                          _c("div", { staticClass: "title" }, [
                            _vm._v("Puntos")
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "content" }, [
                            _vm._v(_vm._s(game.player_1_score))
                          ])
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "span",
                        { staticClass: "row vs", attrs: { id: "vs_" } },
                        [_vm._v("vs")]
                      ),
                      _vm._v(" "),
                      _c(
                        "span",
                        { staticClass: "row score", attrs: { id: "p2_score" } },
                        [
                          _c("div", { staticClass: "title" }, [
                            _vm._v("Puntos")
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "content" }, [
                            _vm._v(_vm._s(game.player_2_score))
                          ])
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "span",
                        { staticClass: "row name", attrs: { id: "p2_name" } },
                        [
                          _c("span", { staticClass: "name" }, [
                            _vm._v(
                              _vm._s(_vm._f("firstWord")(game.player2.name))
                            )
                          ])
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "span",
                        { staticClass: "row avat", attrs: { id: "p2_avatar" } },
                        [
                          _c("div", { staticClass: "avatar" }, [
                            _c("img", {
                              attrs: {
                                src: game.player2.avatar,
                                alt: "Sin avatar",
                                title: game.player2.name
                              }
                            })
                          ])
                        ]
                      )
                    ])
                  ])
                })
              )
            : _vm._e()
        ]),
        _vm._v(" "),
        _vm.c_sin_datos == ""
          ? _c("div", { staticClass: "navigation" }, [
              _c(
                "span",
                { class: _vm.c_currentPage <= 1 ? "oculto" : "visible" },
                [
                  _c(
                    "a",
                    {
                      on: {
                        click: function($event) {
                          _vm.userGamesPag(_vm.c_firsPageURL)
                        }
                      }
                    },
                    [_vm._v(_vm._s(_vm.c_primero))]
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "span",
                { class: _vm.c_currentPage <= 1 ? "oculto" : "visible" },
                [
                  _c(
                    "a",
                    {
                      on: {
                        click: function($event) {
                          _vm.userGamesPag(_vm.c_prevPageURL)
                        }
                      }
                    },
                    [_vm._v(_vm._s(_vm.c_anterior))]
                  )
                ]
              ),
              _vm._v(" "),
              _c("span", [_vm._v(_vm._s(_vm.c_currentPage))]),
              _vm._v(" "),
              _c(
                "span",
                {
                  class:
                    _vm.c_currentPage >= _vm.c_lastPage ? "oculto" : "visible"
                },
                [
                  _c(
                    "a",
                    {
                      on: {
                        click: function($event) {
                          _vm.userGamesPag(_vm.c_nextPageURL)
                        }
                      }
                    },
                    [_vm._v(_vm._s(_vm.c_siguiente))]
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "span",
                {
                  class:
                    _vm.c_currentPage >= _vm.c_lastPage ? "oculto" : "visible"
                },
                [
                  _c(
                    "a",
                    {
                      on: {
                        click: function($event) {
                          _vm.userGamesPag(_vm.c_lastPageURL)
                        }
                      }
                    },
                    [_vm._v(_vm._s(_vm.c_ultimo))]
                  )
                ]
              )
            ])
          : _vm._e()
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "challengesContenedor" }, [
        _c("div", { staticClass: "head" }, [
          _c("span", [_vm._v("LISTADO DE PARTIDAS EN CURSO")]),
          _vm._v(" "),
          _c("div", { staticClass: "acciones" }, [
            _c("ul", { staticClass: "nav-items" }, [
              _c("li", { staticClass: "nav-item" }, [
                _vm._v("\n              Aleatoria\n              "),
                _c("i", { staticClass: "fas fa-angle-down" }),
                _vm._v(" "),
                _c("ul", [
                  _c(
                    "li",
                    {
                      on: {
                        click: function($event) {
                          _vm.newGame(_vm.c_espanol)
                        }
                      }
                    },
                    [
                      _vm._v("\n                  Español\n                  "),
                      _c("span", [_vm._v(_vm._s(_vm.c_espanol))])
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "li",
                    {
                      on: {
                        click: function($event) {
                          _vm.newGame(_vm.c_ingles)
                        }
                      }
                    },
                    [
                      _vm._v("\n                  Ingles\n                  "),
                      _c("span", [_vm._v(_vm._s(_vm.c_ingles))])
                    ]
                  )
                ])
              ]),
              _vm._v(" "),
              _c("li", { staticClass: "nav-item" }, [
                _vm._v("\n              Nuevo Reto\n              "),
                _c("i", { staticClass: "fas fa-angle-down" }),
                _vm._v(" "),
                _c("ul", [
                  _c("li", [
                    _c(
                      "a",
                      {
                        on: {
                          click: function($event) {
                            _vm.newChallengeLessLevel(
                              _vm.c_espanol,
                              _vm.c_nivel_menor_es
                            )
                          }
                        }
                      },
                      [
                        _c("span", [_vm._v(_vm._s(_vm.c_espanol))]),
                        _vm._v(
                          "\n                    Nivel " +
                            _vm._s(_vm.c_menor_igual) +
                            " " +
                            _vm._s(_vm.c_nivel_menor_es) +
                            "\n                  "
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _vm.c_nivel_menor_es > 0
                      ? _c("i", {
                          staticClass: "far fa-minus-square",
                          on: {
                            click: function($event) {
                              _vm.c_nivel_menor_es--
                            }
                          }
                        })
                      : _vm._e(),
                    _vm._v(" "),
                    _c("i", {
                      staticClass: "far fa-plus-square",
                      on: {
                        click: function($event) {
                          _vm.c_nivel_menor_es++
                        }
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _c(
                      "a",
                      {
                        on: {
                          click: function($event) {
                            _vm.newChallengeLessLevel(
                              _vm.c_ingles,
                              _vm.c_nivel_menor_en
                            )
                          }
                        }
                      },
                      [
                        _c("span", [_vm._v(_vm._s(_vm.c_ingles))]),
                        _vm._v(
                          "\n                    Nivel " +
                            _vm._s(_vm.c_menor_igual) +
                            " " +
                            _vm._s(_vm.c_nivel_menor_en) +
                            "\n                  "
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _vm.c_nivel_menor_en > 0
                      ? _c("i", {
                          staticClass: "far fa-minus-square",
                          on: {
                            click: function($event) {
                              _vm.c_nivel_menor_en--
                            }
                          }
                        })
                      : _vm._e(),
                    _vm._v(" "),
                    _c("i", {
                      staticClass: "far fa-plus-square",
                      on: {
                        click: function($event) {
                          _vm.c_nivel_menor_en++
                        }
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _c(
                      "a",
                      {
                        on: {
                          click: function($event) {
                            _vm.newChallengeHigherLevel(
                              _vm.c_spanol,
                              _vm.c_nivel_mayor_es
                            )
                          }
                        }
                      },
                      [
                        _c("span", [_vm._v(_vm._s(_vm.c_espanol))]),
                        _vm._v(
                          "\n                    Nivel " +
                            _vm._s(_vm.c_mayor_igual) +
                            " " +
                            _vm._s(_vm.c_nivel_mayor_es) +
                            "\n                  "
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _vm.c_nivel_mayor_es > 0
                      ? _c("i", {
                          staticClass: "far fa-minus-square",
                          on: {
                            click: function($event) {
                              _vm.c_nivel_mayor_es--
                            }
                          }
                        })
                      : _vm._e(),
                    _vm._v(" "),
                    _c("i", {
                      staticClass: "far fa-plus-square",
                      on: {
                        click: function($event) {
                          _vm.c_nivel_mayor_es++
                        }
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _c(
                      "a",
                      {
                        on: {
                          click: function($event) {
                            _vm.newChallengeHigherLevel(
                              _vm.c_ingles,
                              _vm.c_nivel_mayor_en
                            )
                          }
                        }
                      },
                      [
                        _c("span", [_vm._v(_vm._s(_vm.c_ingles))]),
                        _vm._v(
                          "\n                    Nivel " +
                            _vm._s(_vm.c_mayor_igual) +
                            " " +
                            _vm._s(_vm.c_nivel_mayor_en) +
                            "\n                  "
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _vm.c_nivel_mayor_en > 0
                      ? _c("i", {
                          staticClass: "far fa-minus-square",
                          on: {
                            click: function($event) {
                              _vm.c_nivel_mayor_en--
                            }
                          }
                        })
                      : _vm._e(),
                    _vm._v(" "),
                    _c("i", {
                      staticClass: "far fa-plus-square",
                      on: {
                        click: function($event) {
                          _vm.c_nivel_mayor_en++
                        }
                      }
                    })
                  ])
                ])
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "main" }, [
          _vm.c_sin_datos != ""
            ? _c("span", { staticClass: "rojo" }, [
                _vm._v(_vm._s(_vm.c_sin_datos))
              ])
            : _vm._e(),
          _vm._v(" "),
          _vm.c_sin_datos == ""
            ? _c(
                "ul",
                { staticClass: "challengesList" },
                _vm._l(_vm.c_games, function(game, index) {
                  return _c("li", { key: index }, [
                    _c(
                      "a",
                      {
                        attrs: { target: "_blanc" },
                        on: {
                          click: function($event) {
                            _vm.gotoGame(game.id)
                          }
                        }
                      },
                      [
                        _c(
                          "span",
                          {
                            staticClass: "row avat",
                            attrs: { id: "avatarPlayer" }
                          },
                          [
                            _c("div", { staticClass: "avatar" }, [
                              _c("img", {
                                attrs: {
                                  src: _vm.c_avatar,
                                  title: _vm.c_user.name
                                }
                              })
                            ])
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            staticClass: "row name",
                            attrs: { id: "namePlayer" }
                          },
                          [
                            _c("span", { staticClass: "name" }, [
                              _vm._v(
                                _vm._s(_vm._f("firstWord")(_vm.c_user.name))
                              )
                            ])
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            staticClass: "row score",
                            attrs: { id: "puntosPlayer" }
                          },
                          [
                            _c("div", { staticClass: "title" }, [
                              _vm._v("Puntos")
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "content" }, [
                              _vm._v(_vm._s(game.player_score))
                            ])
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "row vs", attrs: { id: "vs" } },
                          [_vm._v("vs")]
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            staticClass: "row score",
                            attrs: { id: "opponentPuntos" }
                          },
                          [
                            _c("div", { staticClass: "title" }, [
                              _vm._v("Puntos")
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "content" }, [
                              _vm._v(_vm._s(game.opponent_score))
                            ])
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            staticClass: "row name",
                            attrs: { id: "opponentName" }
                          },
                          [
                            _c("span", { staticClass: "name" }, [
                              _vm._v(
                                _vm._s(
                                  _vm._f("firstWord")(game.opponent[0].name)
                                )
                              )
                            ])
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            staticClass: "row avat",
                            attrs: { id: "opponentAvatar" }
                          },
                          [
                            _c("div", { staticClass: "avatar" }, [
                              _c("img", {
                                attrs: {
                                  src: game.opponent[0].avatar,
                                  alt: "Sin avatar",
                                  title: game.opponent[0].name
                                }
                              })
                            ])
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "row state", attrs: { id: "state" } },
                          [
                            _c("div", { staticClass: "title" }, [
                              _vm._v("Turno")
                            ]),
                            _vm._v(" "),
                            game.state == 0
                              ? _c("div", { staticClass: "content" }, [
                                  _vm._v("Confirmando")
                                ])
                              : game.state == 1
                                ? _c("div", { staticClass: "content" }, [
                                    _vm._v(
                                      _vm._s(
                                        _vm._f("firstWord")(_vm.c_user.name)
                                      )
                                    )
                                  ])
                                : game.state == 2
                                  ? _c("div", { staticClass: "content" }, [
                                      _vm._v(
                                        _vm._s(
                                          _vm._f("firstWord")(
                                            game.opponent[0].name
                                          )
                                        )
                                      )
                                    ])
                                  : _vm._e()
                          ]
                        )
                      ]
                    )
                  ])
                })
              )
            : _vm._e()
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "userListContenedor" }, [
        _c("div", { staticClass: "head" }, [
          _c("span", [_vm._v("LISTADO DE JUGADORES REGISTRADOS")]),
          _vm._v(" "),
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.c_search,
                expression: "c_search"
              }
            ],
            attrs: { type: "search", placeholder: "Buscar Jugador" },
            domProps: { value: _vm.c_search },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.c_search = $event.target.value
              }
            }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "main" }, [
          _vm.c_sin_datos != ""
            ? _c("span", { staticClass: "rojo" }, [
                _vm._v(_vm._s(_vm.c_sin_datos))
              ])
            : _vm._e(),
          _vm._v(" "),
          _vm.c_sin_datos == ""
            ? _c(
                "ul",
                { staticClass: "userList" },
                _vm._l(_vm.filteredPlayers, function(plyr) {
                  return _c(
                    "li",
                    { key: plyr.id, staticClass: "listUserInfo" },
                    [
                      _c(
                        "span",
                        { staticClass: "row id", attrs: { id: "idPlayer" } },
                        [
                          _c("span", { staticClass: "name" }, [
                            _vm._v(_vm._s(plyr.id))
                          ])
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "span",
                        { staticClass: "row name", attrs: { id: "namePlyr" } },
                        [
                          _c("span", { staticClass: "name" }, [
                            _vm._v(_vm._s(plyr.name))
                          ])
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "span",
                        {
                          staticClass: "row email",
                          attrs: { id: "emailPlayer" }
                        },
                        [
                          _c("div", { staticClass: "content" }, [
                            _vm._v(_vm._s(plyr.email))
                          ])
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "span",
                        {
                          staticClass: "row retar",
                          attrs: { id: "retarPlayer" }
                        },
                        [
                          _c("div", { staticClass: "title" }, [
                            _vm._v("Retar")
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "content" }, [
                            _c(
                              "a",
                              {
                                on: {
                                  click: function($event) {
                                    _vm.newChallengeSelectUser(
                                      _vm.c_espanol,
                                      plyr.id
                                    )
                                  }
                                }
                              },
                              [_c("span", [_vm._v(_vm._s(_vm.c_espanol))])]
                            ),
                            _vm._v(" "),
                            _c(
                              "a",
                              {
                                on: {
                                  click: function($event) {
                                    _vm.newChallengeSelectUser(
                                      _vm.c_ingles,
                                      plyr.id
                                    )
                                  }
                                }
                              },
                              [_c("span", [_vm._v(_vm._s(_vm.c_ingles))])]
                            )
                          ])
                        ]
                      )
                    ]
                  )
                })
              )
            : _vm._e()
        ])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-1f0fef3c", module.exports)
  }
}

/***/ })
/******/ ]);