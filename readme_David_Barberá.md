1 - https://upload.wikimedia.org/wikipedia/commons/8/8e/Plateau_de_Scrabble.svg
Este archivo está bajo la licencia Creative Commons Attribution-Share Alike 3.0 Unported

2 - https://images.freeimages.com/images/large-previews/d53/blurbus-1528285.jpg


3 - https://upload.wikimedia.org/wikipedia/commons/thumb/8/8f/Macys-sign-largest-store.jpg/512px-Macys-sign-largest-store.jpg

Enriquecornejo at English Wikipedia [GFDL (http://www.gnu.org/copyleft/fdl.html) 
or CC BY-SA 3.0(https://creativecommons.org/licenses/by-sa/3.0)]

4 - https://cdn.pixabay.com/photo/2015/10/25/22/52/facebook-1006474_960_720.jpg

CC0 Creative Commons 
Gratis para usos comerciales 
No es necesario reconocimiento 

5 - https://cdn.pixabay.com/photo/2015/10/26/12/43/twitter-1007075_960_720.jpg

CC0 Creative Commons 
Gratis para usos comerciales 
No es necesario reconocimiento 

6 - https://cdn.pixabay.com/photo/2015/10/26/12/42/instagram-1007070_960_720.jpg

CC0 Creative Commons 
Gratis para usos comerciales 
No es necesario reconocimiento 

7 - https://cdn.pixabay.com/photo/2017/09/27/01/49/game-2790713_960_720.jpg

CC0 Creative Commons 
Gratis para usos comerciales 
No es necesario reconocimiento 

8 - https://cdn.pixabay.com/photo/2014/11/30/19/39/press-552026_960_720.jpg

CC0 Creative Commons 
Gratis para usos comerciales 
No es necesario reconocimiento 

9 - https://cdn.pixabay.com/photo/2016/07/15/19/44/old-print-press-1520124_960_720.jpg

CC0 Creative Commons 
Gratis para usos comerciales 
No es necesario reconocimiento

10 - https://cdn.pixabay.com/photo/2014/11/20/19/57/letters-539709_960_720.jpg

Pixabay License
Gratis para usos comerciales
No es necesario reconocimiento

11 - https://es.freeimages.com/photo/blurbus-1528285

Licencia de contenido de FreeImages.com Licencia
Who owns the content? All of the licensed content is owned by either FreeImages.com or the artists who supply the content. All rights not expressly granted in this agreement are reserved by FreeImages.com and the content suppliers.
Attribution:
        1-Do I need to include a photo credit? You do not need to include a photo credit for commercial use, but if you are using content for editorial purposes, you must include the following credit adjacent to the content or in audio/visual production credits: “FreeImages.com/Slivester Nuenenorl.”
        Do I need to include an audio and/or video credit? Yes, if technically feasible, you must include the following credit in audio or audio/visual productions: “FreeImages.com/Slivester Nuenenorl.”

12 - https://www.flaticon.es/icono-gratis/union-europea_197615

Free License (with attribution)This license allows you to use for free any of Flaticon contents for your projects as long as they are attributed to their author in the definitive project

13 - https://www.flaticon.es/icono-gratis/espana_197593

Free License (with attribution)This license allows you to use for free any of Flaticon contents for your projects as long as they are attributed to their author in the definitive project

14 - https://www.flaticon.es/icono-gratis/reino-unido_197374

Free License (with attribution)This license allows you to use for free any of Flaticon contents for your projects as long as they are attributed to their author in the definitive project

15 - https://www.flaticon.es/icono-gratis/alemania_197571

Free License (with attribution)This license allows you to use for free any of Flaticon contents for your projects as long as they are attributed to their author in the definitive project

16 - https://www.flaticon.es/icono-gratis/francia_197560

Free License (with attribution)This license allows you to use for free any of Flaticon contents for your projects as long as they are attributed to their author in the definitive project

17 - https://www.flaticon.es/icono-gratis/xp_459554#term=xp&page=1&position=1

Free License (with attribution)This license allows you to use for free any of Flaticon contents for your projects as long as they are attributed to their author in the definitive project

18 - https://www.textures.com/preview/woodfine0009/21780

Imagen free.

19 - https://pngtree.com/freepng/default-avatar_762420.html

Imagen free.

20 - https://www.iconfinder.com/icons/2682803/attention_erro_exclamation_mark_warn_warning_weather_icon

Imagen free.

21 - https://www.iconfinder.com/icons/299045/error_sign_icon

Imagen free.

22 - https://www.iconfinder.com/icons/1930264/check_complete_done_green_success_valid_icon

Imagen free.

23 - https://www.iconfinder.com/icons/3126605/add_to_favorite_official_verify_icon

Imagen free.

24 - http://www.iconarchive.com/show/bag-o-tiles-icons-by-barkerbaggies.html

CC Attribution-Share Alike 4.0: You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use 

25 - https://www.iconfinder.com/icons/416372/champion_cup_sports_winner_icon

Imagen free.

