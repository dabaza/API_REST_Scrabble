$(function() {    

    var spmComponedor = $("#componedor"); // referimos elemento del li "componedor"
    var imgComponedor = $("#imgComponedor"); // referimos el elemento img del componedor
    var spmEntintador = $("#entintador"); // referimos elemento del li "entintador"
    var imgEntintador = $("#imgEntintador"); // referimos el elemento img del entintador
    var spmTirador = $("#tirador"); // referimos elemento del li "tirador"
    var imgTirador = $("#imgTirador"); // referimos el elemento img del tirador
    var imgEuropa11 = $("#imgEuropa11"); // referimos el elemento img de mapa de europa
    var spnVenecia = $("#venecia");
    var spnParis = $("#paris");
    var spnBrujas = $("#brujas");
    var spnSegovia = $("#segovia");
    var spnValencia = $("#valencia");
    var spnWestminster = $("#westminster");

    // comprobamos si se accede a info scrabble para posicionar el scroll en la posición deseada.
    var URLactual = window.location.toString();
    var sitio = URLactual.substr(17,8);    
    if (sitio == "scrabble") {
        $('html, body').animate({ scrollTop: 750 }, 'slow');
        return false;
    }    
    
    // manejador del evento scroll de la ventana que fija la barra de navegación
    // en un momento determinado
    $(window).scroll(function() {
        console.log(this);
        if ($(this).scrollTop() >= 750) {
            $("header").addClass("fijarCabecera");
            $(".miLogo").addClass("miLogoPequeño").removeClass("miLogo");
            $("#gutenberg").addClass("gutenbergScroll");
            $("#scrabbleInfo").addClass("gutenbergScroll");
        } else {
            $("header").removeClass("fijarCabecera");
            $(".miLogoPequeño").addClass("miLogo").removeClass("miLogoPequeño");
            $("#gutenberg").removeClass("gutenbergScroll");
            $("#scrabbleInfo").removeClass("gutenbergScroll");
        }
    })

    // manejador del evento click de los enlaces del menú referentes a info imprenta
    // localiza la posición del destino en el dom y posiciona la página en su posición
    // se realiza ajuste para que el título de la sección no quede debajo del menú de navegación
    $('a[href^="/#"]').click(function() {
        var destino = $(this.hash);
        if (destino.length == 0) {
          destino = $('a[name="' + this.hash.substr(1) + '"]');
        }
        if (destino.length == 0) {
          destino = $('html');
        }
        
        $('html, body').animate({ scrollTop: (destino.offset().top) - 130 }, 'slow');
        return false;
      });  
      // cuando se pulsa el enlace desde otra ruta establecida.
      $('a[href^="./scrabble"]').click(function() {
        location.href= "/scrabble";
        
        $('html, body').animate({ scrollTop: 750 }, 'slow');
        return false;
      });

    // manjejador del evento hover en el li del componedor
    spmComponedor.hover(function() { 
        imgComponedor.attr("src", "img/08b_Trabajadores_imprenta_1.png"); 
    }, function() {
        imgComponedor.attr("src", "img/08_Trabajadores_imprenta_1.png");
    });
    // manjejador del evento hover la imagen del componedor
    imgComponedor.hover(function() {        
        $(this).attr("src", "img/08b_Trabajadores_imprenta_1.png");
    },function(){       
        $(this).attr("src", "img/08_Trabajadores_imprenta_1.png ");
    }); 

    spmEntintador.hover(function () {
        imgEntintador.attr("src", "img/09b_Trabajadores_imprenta_2.jpg");
    }, function() {
        imgEntintador.attr("src", "img/09_Trabajadores_imprenta_2.jpg");
    })

    imgEntintador.hover(function() {
        $(this).attr("src", "img/09b_Trabajadores_imprenta_2.jpg");
    }, function() {
        $(this).attr("src", "img/09_Trabajadores_imprenta_2.jpg");
    })

    spmTirador.hover(function() {
        imgTirador.attr("src", "img/10b_Trabajadores_imprenta_3.jpg");
    }, function() {
        imgTirador.attr("src", "img/10_Trabajadores_imprenta_3.jpg");
    })

    imgTirador.hover(function() {
        $(this).attr("src", "img/10b_Trabajadores_imprenta_3.jpg");
    }, function() {
        $(this).attr("src", "img/10_Trabajadores_imprenta_3.jpg");
    })

    imgEuropa11.hover(function() {
        $(this).attr("src", "img/11_b_Europa_e_imprenta.png")
    }, function() {
        $(this).attr("src", "img/11_transparente.png")
    });

    spnVenecia.hover(function() {
        $(imgEuropa11).attr("src", "img/11_venecia.png")
    }, function() {
        $(imgEuropa11).attr("src", "img/11_transparente.png")
    });

    spnParis.hover(function() {
        $(imgEuropa11).attr("src", "img/11_paris.png")
    }, function() {
        $(imgEuropa11).attr("src", "img/11_transparente.png")
    });

    spnBrujas.hover(function() {
        $(imgEuropa11).attr("src", "img/11_brujas.png")
    }, function() {
        $(imgEuropa11).attr("src", "img/11_transparente.png")
    });

    spnSegovia.hover(function() {
        $(imgEuropa11).attr("src", "img/11_segovia.png")
    }, function() {
        $(imgEuropa11).attr("src", "img/11_transparente.png")
    });

    spnValencia.hover(function() {
        $(imgEuropa11).attr("src", "img/11_valencia.png")
    }, function() {
        $(imgEuropa11).attr("src", "img/11_transparente.png")
    });

    spnWestminster.hover(function() {
        $(imgEuropa11).attr("src", "img/11_westminster.png")
    }, function() {
        $(imgEuropa11).attr("src", "img/11_transparente.png")
    });

    for (let i = 2; i < 6; i++) {
        $("#img1"+i).hover(function() {
            $(this).attr("class", "imgLink");
        }, function () {
            $(this).removeAttr("class");
        });
    }    

    // manejador del evento click del elemento subir que realiza scroll al inicio de la página
    $("#subir").click(function () { //referimos el elemento          
        $('html, body').animate({scrollTop:750}, 'slow'); //seleccionamos etiquetas,clase o identificador destino, creamos animación hacia top de la página.
        return false;        
    });
    
});
