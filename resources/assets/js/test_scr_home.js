/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import Echo from 'laravel-echo';
window.io = require('socket.io-client');

// Have this in case you stop running your laravel echo server
if (typeof io !== 'undefined') {
  window.Echo = new Echo({
    broadcaster: 'socket.io',
    host: window.location.hostname + ':6001',
  });
}

Vue.component(
  'dashboard-test-component',
  require('./components/DashboardTestComponent.vue')
);
Vue.component(
  'datos-usuario-component',
  require('./components/DatosUsuarioComponent.vue')
);
Vue.component(
  'estadisticas-usuario-component',
  require('./components/EstadisticasUsuarioComponent.vue')
);
Vue.component(
  'dashboard-contenedor-component',
  require('./components/DashboardContenedorComponent.vue')
);
Vue.component(
  'ranking-usuario-component',
  require('./components/RankingUsuarioComponent.vue')
);
Vue.component(
  'dashboard-infochallenges-component',
  require('./components/DashboardInfoChallengesComponent.vue')
);
Vue.component(
  'modal-window-component',
  require('./components/ModalWindowComponent.vue')
);
Vue.component('footer-component', require('./components/FooterComponent.vue'));
Vue.filter('fechaES', function(value) {
  var fecha = value;
  fecha = fecha
    .substring(0, 10)
    .split('-')
    .reverse()
    .join('/');
  return fecha;
});

Vue.filter('firstWord', function(value) {
  if (!value) {
    return '';
  } else {
    var userName = [];
    var name = '';
    if (value.indexOf(' ') != -1) {
      userName = value.split(' ');
      name = userName[0];
    } else {
      name = value;
    }
    return name;
  }
});

const register = new Vue({
  el: '#app',
});
