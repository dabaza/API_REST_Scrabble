import Echo from 'laravel-echo';
window.io = require('socket.io-client');

// Have this in case you stop running your laravel echo server
if (typeof io !== 'undefined') {
  window.Echo = new Echo({
    broadcaster: 'socket.io',
    host: window.location.hostname + ':6001',
  });
}

Vue.component(
  'tableboard-test-component',
  require('./components/TableboardTestComponent.vue')
);
Vue.component(
  'tableboard-user-component',
  require('./components/TableboardUserComponent.vue')
);
Vue.component(
  'datos-juego-component',
  require('./components/DatosJuegoComponent.vue')
);
Vue.component(
  'game-zone-component',
  require('./components/GameZoneComponent.vue')
);
Vue.component(
  'modal-window-component',
  require('./components/ModalWindowComponent.vue')
);
Vue.component('footer-component', require('./components/FooterComponent.vue'));
const register = new Vue({
  el: '#app',
});
