@extends('layouts.app',
    ['title' => 'Login', 'css_files' => ['test1', 'estilos', 'miLogInStyles'],
    'js_files' => ['main', 'login_logout_component']])

@section('content')
{{-- Menu navegadion --}}
{{-- <div class="row fixed-top">
    <div class="col-sm-12 col-md-12 col-lg-12 co-xl-12">
        <nav id="menuNav" class="navbar fixed-top navbar-expand-xl navbar-light sacar" role="navigation">
            <a class="navbar-brand" href="../" role="link" aria-label="Ir al Home"><img class="miLogoPequeño" src="{{ asset('img/Logo.jpeg') }}"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto" role="list">
                    <li class="nav-item mr-2" role="list-item"><a class="nav-link" id="linkGutenberg" href="../" role="link" aria-label="Ir a Información Imprenta">Gutenberg</a></li>
                    <li class="nav-item mr-2" role="list-item"><a class="nav-link" id="linkScrabble" href="../scrabble" role="link" aria-label="Ir a Información Scrabble">Scrabble. Info</a></li>
                    <li class="nav-item mr-2 active" role="list-item"><a class="nav-link" id="linkLogin" href="" role="link" aria-label="Enlace actual"><i class="fas fa-user-circle"></i> Login</a></li>
                    <li class="nav-item mr-2" role="list-item"><a class="nav-link" id="linkRegistro" href="./register" role="link" aria-label="Ir a registrar usuario"><i class="fas fa-address-card"></i> Registro</a></li>
                </ul>
            </div>
        </nav>
    </div>
</div> --}}
{{-- Menu navegadion --}}
{{-- Sección principal --}}
<section role="main">
    <div class="container">
        {{-- formulario login dark --}}
        <div class="card card-login mx-auto text-center bg-dark">
            <div class="card-header mx-auto bg-dark">
                <span>
                    <img src="{{asset('/img/letters-539709_640.jpg')}}" class="w-75" alt="Logo">
                </span>
                <br/>
                <span class="logo_title mt-5">Login Dashboard</span>
            </div>
            <div class="card-body">
                @if ($errors->isNotEmpty())
                <div class="alert alert-danger mt-2" role="alert">
                    <div>
                        <button type="button" data-dismiss="alert" aria-label="Cerrar alerta" class="close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4>Errores en el login de usuario</h4>
                        @if ($errors->has('email'))
                            <p><strong>email:&nbsp;</strong>{{ $errors->first('email') }}</p>
                        @endif
                    </div>
                </div>
                @endif
                <form action="{{ route('login') }}" method="post" role="form" aria-label="Formulario login">
                    @csrf
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="far fa-envelope"></i>
                            </span>
                        </div>
                        <input type="text" id="email" name="email" class="form-control" placeholder="Correo Electrónico" value="{{ old('email') }}" aria-label="Intoduce dirección email" autofocus="" required>
                    </div>

                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                        </div>
                        <input type="password" name="password" class="form-control" placeholder="Password" aria-label="Introduce contraseña" required>
                    </div>

                    <div class="form-group text-left">
                    <span>
                        <a role="link" id="reset" href="./password/reset" aria-label="Pulsar para recuperar contraseña" data-toggle="tooltip" data-placement="bottom" title="¿Olvidaste la contraseña?">Recuperar contraseña</a>
                    </span>
                    </div>

                    <div class="form-group">
                        <input type="submit" name="btn" value="Login" class="btn btn-outline-danger float-right login_btn" role="button" aria-label="Enviar datos">
                    </div>

                </form>
            </div>
        </div>{{-- formulario login dark --}}

         {{-- formulario --}}
        {{-- <div class="row justify-content-center align-items-center mt-3">
            <div class="col-12">
                <div class="justify-content-center align-items-center">
                    <div class="contenedor-login">
                        <form action="{{ route('login') }}" method="POST" role="form" aria-label="Formulario login">
                            @csrf
                            <h1 class="my-4">Formulario Login</h1>
                            <fieldset>
                                <legend>Datos Credencial</legend>
                                <i class="far fa-envelope"></i>
                                <div role="group" aria-label="Email" class="form-label-group">
                                    <input type="email" name="email" id="email" class="form-control form-control-lg" aria-describedby="emailHelp" placeholder="Introduce dirección de correo" value="{{ old('email') }}" required autofocus>
                                    <label for="email" class="lead">Dirección de Correo</label>
                                    <small id="emailHelp" class="form-text text-muted">Nunca compartiremos tu dirección de correo con terceros.</small>
                                </div>
                                <i class="fas fa-key"></i>
                                <div role="group" aria-label="Contraseña" class="form-label-group">
                                    <input type="password" name="password" id="password" class="form-control form-control-lg" placeholder="Contraseña" required>
                                    <label for="pass" class="lead">Contraseña</label>
                                </div>
                            </fieldset>

                            <button id="botonEnviar" class="btn btn-primary mt-4" type="submit" role="button" aria-label="Login">Enviar</button>
                            <button id="botonRegistro" class="btn btn-warning mt-4" type="button" role="button" aria-label="Ir a Registro">Registrar Usuario</button>
                            <button id="botonPass" class="btn btn-secondary mt-4" type="button" role="button" aria-label="Resetear contraseña">Solicitar Contraseña</button>
                        </form>
                    </div>
                    @if ($errors->isNotEmpty())
                        <div class="alert alert-danger mt-2">
                            <div>
                                <button type="button" data-dismiss="alert" aria-label="Close" class="close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h4>Error modo 1</h4>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            <div>
                                <h4>Error modo 2</h4>
                                @if ($errors->has('email'))
                                    <p><strong>email:</strong>{{ $errors->first('email') }}</p>
                                @endif
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>formulario --}}
</div>
</section> {{-- Sección principal --}}
@endsection
