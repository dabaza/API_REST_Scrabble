@extends('layouts.app',
    ['title' => 'Reinicio contraseña', 'css_files' => ['estilos', 'miEmailResetStyles','test_scr_login'],
    'js_files' => ['main','test_scr_reset']])

@section('content')
{{-- Menu navegadion --}}
<div class="row fixed-top">
    <div class="col-sm-12 col-md-12 col-lg-12 co-xl-12">
        <nav id="menuNav" class="navbar fixed-top navbar-expand-xl navbar-light sacar" role="navigation">
            <a class="navbar-brand" href="../../../" role="link" aria-label="Ir al Home"><img class="miLogoPequeño" src="{{asset('/img/Logo.jpeg')}}"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto" role="list">
                    <li class="nav-item" role="list-item"><a class="nav-link" id="linkGutenberg" href="/../../../" role="link" aria-label="Ir a Información Imprenta">Gutenberg</a></li>
                    <li class="nav-item" role="list-item"><a class="nav-link" href="/../../scrabble" role="link" aria-label="Ir a Información Scrabble">Scrabble. Info</a></li>
                    <li class="nav-item" role="list-item"><a class="nav-link" href="/../../scrabble/login" role="link" aria-label="Enlace actual"><i class="fas fa-user-circle"></i> Login</a></li>
                    <li class="nav-item" role="list-item"><a class="nav-link" href="/../../scrabble/register" role="link" aria-label="Ir a registrar usuario"><i class="fas fa-address-card"></i> Registro</a></li>
                </ul>
            </div>
        </nav>
    </div>
</div>{{-- Menu navegadion --}}

<section role="main">
    <div class="container">
        <div class="card card-login mx-auto text-center bg-dark">
            <div class="card-header mx-auto bg-dark">
                <span>
                    <img src="{{asset('/img/letters-539709_640.jpg')}}" class="w-75" alt="Logo formulario">
                </span>
                <br/>
                <span class="logo_title mt-5">Reinicio Contraseña</span>
            </div>
            <div class="card-body">
                    @if ($errors->isNotEmpty())
                    <div class="alert alert-danger mt-2" role="alert">
                        <div>
                            <button type="button" data-dismiss="alert" aria-label="Cerrar alerta" class="close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4>Errores en el login de usuario</h4>
                            @if ($errors->has('email'))
                                <p><strong>email:&nbsp;</strong>{{ $errors->first('email') }}</p>
                            @endif
                        </div>
                    </div>
                    @endif
                    <form method="POST" action="{{ route('password.email') }}" role="form">
                        @csrf <!-- por razones educativas está desactivado -->
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="far fa-envelope"></i>
                                </span>
                            </div>
                            <input id="email" type="email" name="email" class="form-control"  aria-label="Intoduce dirección email" placeholder="Correo electrónico" data-toggle="tooltip" data-placement="right" title="Se enviará un enlace a la dirección indicada para acceder al formulario de reinicio de contraseña" autofocus="" required>
                            {{-- <div class="tooltip bs-tooltip-top" role="tooltip">
                                <div class="arrow"></div>
                                <div class="tooltip-inner">
                                    Se enviará un enlace a la dirección indicada para acceder al formulario de reinicio de contraseña
                                </div>
                            </div> --}}
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-outline-danger float-right login_btn" role="button" aria-label="Enviar enlace">
                                Enviar enlace
                            </button>
                        </div>{{-- Menu navegadion --}}
                        {{-- <div class="row fixed-top">
                            <div class="col-sm-12 col-md-12 col-lg-12 co-xl-12">
                                <nav id="menuNav" class="navbar fixed-top navbar-expand-xl navbar-light sacar" role="navigation">
                                    <a class="navbar-brand" href="../" role="link" aria-label="Ir al Home"><img class="miLogoPequeño" src="../img/Logo.jpeg"></a>
                                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav"
                                            aria-expanded="false" aria-label="Toggle navigation">
                                            <span class="navbar-toggler-icon"></span>
                                    </button>
                                    <div class="collapse navbar-collapse" id="navbarNav">
                                        <ul class="navbar-nav ml-auto" role="list">
                                            <li class="nav-item" role="list-item"><a class="nav-link" id="linkGutenberg" href="../" role="link" aria-label="Ir a Información Imprenta">Gutenberg</a></li>
                                            <li class="nav-item" role="list-item"><a class="nav-link" href="../scrabble" role="link" aria-label="Ir a Información Scrabble">Scrabble. Info</a></li>
                                            <li class="nav-item active" role="list-item"><a class="nav-link" href="" role="link" aria-label="Enlace actual"><i class="fas fa-user-circle"></i> Login</a></li>
                                            <li class="nav-item" role="list-item"><a class="nav-link" href="./register" role="link" aria-label="Ir a registrar usuario"><i class="fas fa-address-card"></i> Registro</a></li>
                                        </ul>
                                    </div>
                                </nav>
                            </div>
                        </div> --}}
                        {{-- Menu navegadion --}}
                    </form>
                </div>
        </div>
        @if (session('status'))
            <div class="informacion">
                {{ session('status') }}
            </div>
        @endif
    </div>
</section>

{{--
    @if ($errors->isNotEmpty())
        <div class="error">
            <div>
                <h4>Error modo 1</h4>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            <div>
                <h4>Error modo 2</h4>
                @if ($errors->has('email'))
                    <p><strong>email:</strong>{{ $errors->first('email') }}</p>
                @endif
            </div>
        </div>
    @endif --}}

@endsection
