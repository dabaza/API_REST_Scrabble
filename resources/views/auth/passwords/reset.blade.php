<!-- el fichero css de login me vale para el registro -->
@extends('layouts.app',
    ['title' => 'Reinicio contraseña', 'css_files' => ['estilos', 'miEmailResetStyles','test_scr_login'],
    'js_files' => ['test_scr_reset']])

@section('content')
{{-- Menu navegadion --}}
<div class="row fixed-top">
    <div class="col-sm-12 col-md-12 col-lg-12 co-xl-12">
        <nav id="menuNav" class="navbar fixed-top navbar-expand-xl navbar-light sacar" role="navigation">
            <a class="navbar-brand" href="../../../" role="link" aria-label="Ir al Home"><img class="miLogoPequeño" src="{{asset('/img/Logo.jpeg')}}"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto" role="list">
                    <li class="nav-item" role="list-item"><a class="nav-link" id="linkGutenberg" href="/../../../" role="link" aria-label="Ir a Información Imprenta">Gutenberg</a></li>
                    <li class="nav-item" role="list-item"><a class="nav-link" href="/../../scrabble" role="link" aria-label="Ir a Información Scrabble">Scrabble. Info</a></li>
                    <li class="nav-item" role="list-item"><a class="nav-link" href="/../../scrabble/login" role="link" aria-label="Enlace actual"><i class="fas fa-user-circle"></i> Login</a></li>
                    <li class="nav-item" role="list-item"><a class="nav-link" href="/../../scrabble/register" role="link" aria-label="Ir a registrar usuario"><i class="fas fa-address-card"></i> Registro</a></li>
                </ul>
            </div>
        </nav>
    </div>
</div>{{-- Menu navegadion --}}
<section role="main">
    <div class="container">
        <div class="card card-login mx-auto text-center bg-dark">
            <div class="card-header mx-auto bg-dark">
                <span>
                    <img src="{{asset('/img/letters-539709_640.jpg')}}" class="w-75" alt="Logo">
                </span>
                <br/>
                <span class="logo_title mt-5">Cambiar Contraseña</span>
            </div>
            <div class="card-body">
                @if ($errors->isNotEmpty())
                    <div class="alert alert-danger mt-2" role="alert">
                        <div>
                            <button type="button" data-dismiss="alert" aria-label="Cerrar alerta" class="close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        <h4>Errores formulario reinicio contraseña</h4>
                            @if ($errors->has('email'))
                                <p><strong>Email:</strong>{{ $errors->first('email') }}</p>
                            @endif
                            @if ($errors->has('password'))
                                <p><strong>Password:</strong>{{ $errors->first('password') }}</p>
                            @endif
                        </div>
                    </div>
                @endif
                <form method="POST" action="{{ route('password.request') }}" role="form">
                    @csrf <!-- por razones educativas está desactivado -->
                    <input type="hidden" name="token" value="{{ $token }}">
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="far fa-envelope"></i>
                            </span>
                        </div>
                        <input id="email" type="email" name="email" class="form-control" value="{{ $email or old('email') }}" placeholder="Correo Electrónico" aria-label="Correo electronico" required autofocus>
                        &nbsp;<span class="requerido">(*)</span>
                    </div>
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-key"></i>
                            </span>
                        </div>
                        <input id="password" type="password" name="password" class="form-control" placeholder="Nueva contraseña" aria-label="Password" required>
                        &nbsp;<span class="requerido">(*)</span>
                    </div>
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="far fa-check-circle"></i>
                            </span>
                        </div>
                        <input id="password-confirm" type="password" name="password_confirmation" class="form-control" placeholder="Confirmar contraseña" aria-label="Confirmacion de password" required>
                        &nbsp;<span class="requerido">(*)</span>
                    </div>
                    <button type="submit" class="btn btn-outline-danger float-right login_btn" role="button" aria-label="Confirmar datos">
                        Aceptar
                    </button>

                </form>
            </div>
        </div>
    </div>
</section>

    {{-- <div class="form">
        <form method="POST" action="{{ route('password.request') }}" role="form">
            @csrf <!-- por razones educativas está desactivado -->

            <input type="hidden" name="token" value="{{ $token }}">

            <label for="email">Correo electrónico</label>
            <input id="email" type="email" name="email" value="{{ $email or old('email') }}" required autofocus>
            <br>
            <label for="password">Contraseña</label>
            <input id="password" type="password" name="password" required>
            <br>
            <label for="password-confirm">Confirma contraseña</label>
            <input id="password-confirm" type="password" name="password_confirmation" required>
            <br>
            <button type="submit">
                Reinicia contraseña
            </button>

        </form>
    </div>
    @if ($errors->isNotEmpty())
        <div class="error">
            <div>
                <h4>Error modo 1</h4>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            <div>
                <h4>Error modo 2</h4>
                @if ($errors->has('email'))
                    <p><strong>email:</strong>{{ $errors->first('email') }}</p>
                @endif
                @if ($errors->has('password'))
                    <p><strong>password:</strong>{{ $errors->first('password') }}</p>
                @endif
            </div>
        </div>
    @endif --}}
@endsection
