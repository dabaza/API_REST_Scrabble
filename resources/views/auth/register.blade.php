<!-- el fichero css de login me vale para el registro -->
@extends('layouts.app',
    ['title' => 'Registro', 'css_files' => ['test_scr_login', 'test1', 'estilos', 'miRegisterStyles'],
    'js_files' => ['main', 'login_logout_component']])

@section('content')
{{-- Menú navegadion --}}
<div class="row fixed-top">
    <div class="col-sm-12 col-md-12 col-lg-12 co-xl-12">
        <nav id="menuNav" class="navbar fixed-top navbar-expand-xl navbar-light sacar">
            <a class="navbar-brand" href="../"><img class="miLogoPequeño" src="../img/Logo.jpeg"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item mr-2"><a class="nav-link" href="../" id="linkGutenberg">Gutenberg</a></li>
                    <li class="nav-item mr-2"><a class="nav-link" id="linkScrabble" href="../scrabble">Scrabble. Info</a></li>
                    <li class="nav-item mr-2"><a class="nav-link" id="linkLogin" href="./login"><i class="fas fa-user-circle"></i> Login</a></li>
                    <li class="nav-item mr-2 active"><a class="nav-link" id="linkRegistro" href=""><i class="fas fa-address-card"></i> Registro</a></li>
                </ul>
            </div>
        </nav>
    </div>
</div>
{{-- Menú navegadion --}}
{{-- contenedor --}}
<div class="container">
    {{-- menú página --}}
    {{-- <div class="row jusrify-conten-center align-items-center mt-5">
        <div class="col-12">
            <div id="menuRegister" class="d-flex flex-row justify-content-center">
                <span class="izquierda mr-sm-1 mr-md-3 mr-lg-3 mr-xl-3 shadow-lg">
                    <a v-on:click="automatic_register" class="lead" href="">Registro automático</a>
                </span>
                <span class="derecha  mr-sm-1 mr-md-1 mr-lg-3 mr-xl-3 shadow-lg">
                    <a v-on:click="wrong_register" class="lead" href="">Registro erróneo</a>
                </span>
            </div>
        </div>
    </div> --}}
    {{-- menú página --}}
     {{-- formulario --}}
    <div class="card card-login mx-auto text-center bg-dark">
        <div class="card-header mx-auto bg-dark">
            <span>
                <img src="{{asset('/img/letters-539709_640.jpg')}}" class="w-75" alt="Logo">
            </span>
            <br/>
            <span class="logo_title mt-5">Registro de Usuario</span>
        </div>
        <div class="card-body">
            @if ($errors->isNotEmpty())
            <div>
                <h4>Errores en el registro de usuario</h4>
                @if ($errors->has('name'))
                    <p><strong>Nombre:</strong>{{ $errors->first('name') }}</p>
                @endif
                @if ($errors->has('email'))
                    <p><strong>Correo:</strong>{{ $errors->first('email') }}</p>
                @endif
                @if ($errors->has('password'))
                    <p><strong>Contraseña:</strong>{{ $errors->first('password') }}</p>
                @endif
                @if ($errors->has('country'))
                    <p><strong>País:</strong>{{ $errors->first('country') }}</p>
                @endif
            </div>
            @endif
                <form method="POST" action="{{ route('register') }}" role="form">
                    @csrf <!-- por razones educativas está desactivado -->
                    {{-- <i class="fas fa-signature"></i> --}}
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-user-tie"></i>
                            </span>
                        </div>
                        <input id="name" type="text" name="name" class="form-control" value="{{ old('name') }}" placeholder="Indica tu nombre" aria-label="Tu nombre" autofocus="" required>&nbsp;<span class="requerido">(*)</span>

                    </div>
                    {{-- <i class="far fa-envelope"></i> --}}
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="far fa-envelope"></i>
                            </span>
                        </div>
                        <input id="email" type="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="Correo electrónico" required>&nbsp;<span class="requerido">(*)</span>
                    </div>
                    {{-- <i class="fas fa-key"></i> --}}
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-key"></i>
                            </span>
                        </div>
                        <input id="password" type="password" name="password" class="form-control" placeholder="Contraseña" required>&nbsp;<span class="requerido">(*)</span>
                    </div>
                    {{-- <i class="far fa-check-circle"></i> --}}
                    <div class="input-group form-group">
                        <!-- es obligatorio que el name sea XXXX_confirmation, donde XXXX es el
                                nombre del campo a confirmar -->
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="far fa-check-circle"></i>
                            </span>
                        </div>
                        <input id="password-confirm" type="password" name="password_confirmation" placeholder="Confirma contraseña" class="form-control" required>&nbsp;<span class="requerido">(*)</span>
                    </div>
                    {{-- <i class="far fa-flag"></i> --}}
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="far fa-flag"></i>
                            </span>
                        </div>
                        <input id="country" type="text" name="country" class="form-control" placeholder="País" required>&nbsp;<span class="requerido">(*)</span>
                    </div>
                    <div class="input-group form-group">
                        <small class="form-text text-white">(*) Campos obligatorios</small>
                    </div>
                    <button type="submit" class="btn btn-outline-danger float-right login_btn lead">
                        Registrar Usuario
                    </button>
                </form>

                {{-- @if ($errors->isNotEmpty())
                <div class="modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Error modo 1</h5>
                                <button type="button" data-dismiss="modal" aria-label="Close" class="close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-secondary">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="alert alert-danger mt-2" role="alert">
                        <div>
                            <button type="button" data-dismiss="alert" aria-label="Close" class="close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4>Error modo 1</h4>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        <hr>
                        <div>
                            <h4>Error modo 2</h4>
                            @if ($errors->has('name'))
                                <p><strong>Nombre:</strong>{{ $errors->first('name') }}</p>
                            @endif
                            @if ($errors->has('email'))
                                <p><strong>Correo:</strong>{{ $errors->first('email') }}</p>
                            @endif
                            @if ($errors->has('password'))
                                <p><strong>Contraseña:</strong>{{ $errors->first('password') }}</p>
                            @endif
                            @if ($errors->has('country'))
                                <p><strong>País:</strong>{{ $errors->first('country') }}</p>
                            @endif
                        </div>

                    </div>
                @endif--}}

        </div>
    </div>
</div>
{{-- contenedor --}}
@endsection
