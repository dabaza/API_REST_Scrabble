<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('css/errorStyles.css') }}" rel="stylesheet">
    <title>Error 401</title>
</head>
<body>
    <p class="info-area">No está autorizado para ver esta página. Prueba a loguearte</p>
    <section class="error-container">
        <span><span>4</span></span>
        <span>0</span>
        <span><span>1</span></span>
    </section>
    <div class="link-container">
        <a href="/" class="more-link">Volver a la página de inicio</a>
    </div>
</body>
</html>