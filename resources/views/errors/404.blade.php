<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('css/errorStyles.css') }}" rel="stylesheet">
    <title>Error 404</title>
</head>
<body>
    <h1>Página de Error 404</h1>
    <p class="info-area">Página no encontrada</p>
    <section class="error-container">
        <span><span>4</span></span>
        <span>0</span>
        <span><span>4</span></span>
    </section>
    <div class="link-container">
        <a href="/" class="more-link">Volver a la página de inicio</a>
    </div>
</body>
</html>