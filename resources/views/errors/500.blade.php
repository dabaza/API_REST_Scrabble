<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('css/errorStyles.css') }}" rel="stylesheet">
    <title>Error 500</title>
</head>
<body>
    <p></p>
    <p class="info-area">Error interno del servido</p>
    <section class="error-container">
        <span><span>5</span></span>
        <span>0</span>
        <span><span>0</span></span>
    </section>
    <div class="link-container">
        <a href="/" class="more-link">Volver a la página de inicio</a>
    </div>
</body>
</html>