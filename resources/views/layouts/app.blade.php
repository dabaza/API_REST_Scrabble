<!DOCTYPE html>
<!-- obtiene de la configuracion (app.php) el idioma por defecto -->
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token para evitar ataques de petición de sitios cruzados -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Obtiene del fichero /config/app.php la variable name, en caso contrario
         usa Laravel. Lo une con el valor de la variable title que se le pasa desde
         el template, con la directiva extends -->
    <title>{{ config('app.name', 'Laravel') . '. '}} {{ $title or '' }}</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <!-- Carga los estilos css -->
    <!-- Estilos generales como los de bootstrap -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Estilos generales de prueba -->
    <link href="{{ asset('css/test1.css') }}" rel="stylesheet">
    <!-- Estilos especificos -->
    @foreach ($css_files as $file)
        <link href="{{ asset('css/' . $file . '.css') }}" rel="stylesheet">
    @endforeach
</head>
<body>
    <div id="app" class="w-100"> <!-- contenedor para trabajo con Vue -->


        <div id="contInfoImprenta" class="container-fluid" data-spy="affix" data-offset-top="100">
            <!-- barra navegación página-->
            <div id="head" class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 co-xl-12">
                    <nav id="menuNav" class="navbar fixed-top navbar-expand-xl navbar-light">
                        <a class="navbar-brand ml-3" href="./"><img class="miLogoPequeño" src="{{ asset('img/Logo.jpeg') }}">{{ session('status') }}</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav"
                                aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item mr-2">
                                <a class="nav-link" id="linkGutenberg" href="/">Gutenberg</a>
                                </li>
                                <li class="nav-item mr-2">
                                <a class="nav-link quitar" id="linkTrabajos" href="#trabajosImprenta">Trabajos imprenta</a>
                                </li>
                                <li class="nav-item mr-2">
                                <a class="nav-link quitar" id="linkDifusion" href="#difusionImprenta">Difusión Imprenta</a>
                                </li>
                                <li class="nav-item mr-2">
                                <a class="nav-link quitar" id="linkLibros" href="#primerosLibros">Primeros libros</a>
                                </li>
                                <li class="nav-item mr-2">
                                <a class="nav-link quitar" id="linkImprentaValencia" href="#imprentaValenciana">Imprenta valenciana</a>
                                </li>
                                <li class="nav-item mr-2">
                                <a class="nav-link" id="linkScrabble" href="/scrabble">Scrabble. Info</a>
                                </li>
                                @if (!Auth::check())
                                <li class="nav-item mr-2">
                                <a class="nav-link" id="linkLogin" href="/scrabble/login">
                                    <i class="fas fa-user-circle"></i> Login
                                </a>
                                </li>
                                <li class="nav-item mr-2">
                                <a class="nav-link" id="linkRegistro" href="/scrabble/register">
                                    <i class="fas fa-address-card"></i> Registro
                                </a>
                                </li>
                                @endif
                                @if (Auth::check())
                                <li class="nav-item mr-2">
                                <a class="nav-link" id="linkDashboard" href="/scrabble/dashboard">
                                    <i class="fas fa-tachometer-alt"></i>
                                   {{ auth()->user()->name }}
                                </a>
                                </li>
                                <li class="nav-item mr-2">
                                <a
                                    href="#"
                                    class="nav-link"
                                    id="linkLogout"
                                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                                >
                                    <i class="fas fa-sign-out-alt"></i> Logout
                                </a>
                                </li>
                                @endif
                            </ul>
                            <!--<login-logout-component></login-logout-component>-->
                            {{-- <ul class="navbar-nav ml-auto">
                                <li class="nav-item mr-2"><a class="nav-link" id="linkGutenberg" href="./">Gutenberg</a></li>
                                <li class="nav-item mr-2"><a class="nav-link" id="linkTrabajos" href="#trabajosImprenta">Trabajos imprenta</a></li>
                                <li class="nav-item mr-2"><a class="nav-link" id="linkDifusion" href="#difusionImprenta">Difusión Imprenta</a></li>
                                <li class="nav-item mr-2"><a class="nav-link" id="linkLibros" href="#primerosLibros">Primeros libros</a></li>
                                <li class="nav-item mr-2"><a class="nav-link" id="linkImprentaValencia" href="#imprentaValenciana">Imprenta valenciana</a></li>
                                <li class="nav-item mr-2"><a class="nav-link" id="linkScrabble" href="./scrabble">Scrabble. Info</a></li>
                                <li class="nav-item mr-2"><a class="nav-link" id="linkLogin" href="./scrabble/login"><i class="fas fa-user-circle"></i> Login</a></li>
                                <li class="nav-item mr-2"><a class="nav-link" id="linkRegistro" href="./scrabble/register"><i class="fas fa-address-card"></i> Registro</a></li>
                            </ul> --}}
                        </div>
                    </nav>
                </div>
            </div><!-- fin barra navegación página -->

            @yield('content')
        </div>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
        <span id="subir" class="ir-arriba fas fa-angle-double-up sacar"></span>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <!-- Scripts especificos -->
    @foreach ($js_files as $file)
        <script src="{{ asset('js/' . $file . '.js') }}"></script>
    @endforeach

    @yield('internal_script')
</body>
</html>
