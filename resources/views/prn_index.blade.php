@extends('layouts.app', ['title' => 'Info Imprenta', 'css_files' => ['estilos', 'test1', 'animaciones'], 'js_files' => ['main','login_logout_component']])

@section('content')
    <header id="cabecera" class="container-fluid" data-spy="affix" data-offset-top="100">
        <!-- {{ config('app.name', 'Laravel') . '. ' }} {{ $title or '' }} -->
        <!-- imagen cabecera -->
        <div class="contenedor-cabecera">
            <img class="imgCabecera" src="img/imgCabecera.jpeg">
            <div class="header-caption">
                <h1>Imprenta</h1>
            </div>
        </div>


    </header>

    <div class="row">
        <div class="co-xl-1 order-xl-1">

        </div>
        <div class="col-sm-12 col-md-12 col-lg-1 co-xl-2 order-md-2 order-sm-2 order-xl-2 order-lg-2">

        </div>
        <div class="col-sm-12 col-md-12 col-lg-10 co-xl-6 order-sm-1 order-xl-3 order-lg-3">
            <!-- inicio seccion difusion imprenta -->
            <div id="gutenberg" class="gutenberg animation">
                <h2 class="mt-5 mb-3">Johannes Gensfleisch zur Laden zum Gutenberg</h2>

                <div class="textoGutenberg">
                    <div id="imgGuntenberg" class="float-left pl-2">
                        <img id="img01" src="img/01_Gutenberg.jpg" class="animation" title="See page for author [Public domain], via Wikimedia Commons">
                        <span></span>
                    </div>
                    <p>
                        Gutenberg nació en Maguncia, Alemania alrededor de 1400 en la casa paterna llamada zum Gutenberg. Su
                        apellido verdadero es Gensfleisch (en dialecto alemán renano este apellido tiene semejanza,
                        si es que no significa, «carne de ganso», por lo que el inventor de la imprenta en Occidente prefirió
                        usar el apellido por el cual es conocido). Hijo del comerciante Federico Gensfleisch, que adoptaría
                        posteriormente hacia 1410 el apellido zum Gutenberg, y de Else Wyrich, hija de un tendero.
                    </p>
                    <p>
                        Conocedor del arte de la fundición del oro, se destacó como herrero para el obispado de su ciudad. La familia se
                        trasladó a Eltville am Rhein, ahora en el Estado de Hesse, donde Else había heredado una finca. Debió haber
                        estudiado en la Universidad de Erfurt, en donde está registrado en 1419 el nombre de Johannes de Alta Villa
                        (Eltvilla). Ese año murió su padre. Nada más se conoce de Gutenberg hasta que en 1434 residió como platero en Estrasburgo,
                        donde cinco años después se vio envuelto en un proceso, que demuestra de forma indudable, que Gutenberg había formado una
                        sociedad con Hanz Riffe para desarrollar ciertos procedimientos secretos. En 1438 entraron como asociados Andrés Heilman y
                        Andreas Dritzehen (sus herederos fueron los reclamantes) y en el expediente judicial se mencionan los términos de prensa,
                        formas e impresión.
                    </p>
                    <p>
                        De regreso a Maguncia, formó una nueva sociedad con Johann Fust, quien le da un préstamo con el que, en 1449,
                        publicó el Misal de Constanza, primer libro tipográfico del mundo occidental. Recientes publicaciones, en cambio,
                        aseguran que este misal no pudo imprimirse antes de 1473 debido a la confección de su papel, por lo que no debió
                        ser obra de Gutenberg. En 1452, Gutenberg da comienzo a la edición de la Biblia de 42 líneas (también conocida como
                        Biblia de Gutenberg). En 1455, Gutenberg carecía de solvencia económica para devolver el préstamo que le había concedido
                        Fust, por lo que se disolvió la unión y Gutenberg se vio en la penuria (incluso tuvo que difundir el secreto de montar
                        imprentas para poder subsistir).
                    </p>
                    <p>
                        Johannes Gutenberg murió arruinado en Maguncia, Alemania el 3 de febrero de 1468. A pesar de la oscuridad de sus últimos años
                        de vida, siempre será reconocido como el inventor de la imprenta moderna.
                    </p>
                    </details>
                </div>
                <div class="clearfix"></div>
                <h3 class="mt-4 mb-3">¿Que es lo que inventó?</h3>
                <p>
                    El nombre de Gutenberg lo asociamos a la invención de la imprenta, pero mucho antes que él ya se imprimía sobre pergamino o papel.
                    Un breve recorrido histórico nos indica que:
                </p>
                <div id="carouselInvento" class="carousel slide carousel-fade shadow-lg p-2 mb-5 rounded" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselInvento" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselInvento" data-slide-to="1"></li>
                        <li data-target="#carouselInvento" data-slide-to="2"></li>
                        <li data-target="#carouselInvento" data-slide-to="3"></li>
                        <li data-target="#carouselInvento" data-slide-to="4"></li>
                        <li data-target="#carouselInvento" data-slide-to="5"></li>
                        <li data-target="#carouselInvento" data-slide-to="6"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img id="img34" class="d-block w-100" src="img/pompeii-3677352_1920.jpg" title="CC0 Creative Commons">
                            <div class="carousel-caption d-none d-md-block">
                                <h4>Diapositiva 1</h4>
                                <h5>Grabado romano</h5>
                                <p>2000 años antes de Gutenberg, en Roma se imprimían carteles con signos grabados en arcilla.</p>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="contenedorImg">
                                <img id="img03" class="d-block w-100" src="img/03_Tipos_chinos_en_madera.jpg" title="BabelStone [CC BY-SA 3.0 (https://creativecommons.org/licenses/by-sa/3.0)], from Wikimedia Commons">
                                <div class="carousel-caption d-none d-md-block">
                                    <h4>Diapositiva 2</h4>
                                    <h5>Plancha con grabado en madera</h5>
                                    <p>1400 años antes de Gutenberg, en China se imprimían carteles utilizando signos grabados en madera.</p>
                                </div>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="contenedorImg">
                                <img id="img02" class="d-block w-100" src="img/512px-Jingangjing.jpg" title="English: The colophon, at the inner end, reads: Reverently [caused to be] made for universal free distribution by Wang Jie on behalf of his two parents on the 13th of the 4th moon of the 9th year of Xiantong [i.e. 11th May, CE 868 ]. [Public domain], via Wikimedia Commons">
                                <div class="carousel-caption d-none d-md-block">
                                    <h4>Diapositiva 3</h4>
                                    <h5>El Sutra del diamante British Library</h5>
                                    <p>
                                        Así en el año 686 se imprimió una publicación que se llamó “El sutra del diamante”,
                                        con signos grabados en una única madera.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="contenedorImg">
                                <img id="img15" class="d-block w-100" src="img/04_Fundidor_de_tipos_de_Gutenberg.png">
                                <div class="carousel-caption d-none d-md-block">
                                    <h4>Diapositiva 4</h4>
                                    <h5>Fundidor manual</h5>
                                    <p>
                                        Pero los moldes de madera tenían un problema: pronto se desgastaban y se echaban a perder. La gran idea de Gutenberg
                                        fue moldear piezas de metal para cada letra
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="contenedorImg">
                                <img id="img05" class="d-block w-100" src="img/05_tipos_moviles_metal_gutenberg.jpeg" title="CC0 Creative Commons">
                                <div class="carousel-caption d-none d-md-block">
                                    <h4>Diapositiva 5</h4>
                                    <h5>Letras individuales</h5>
                                    <p>
                                        Creando de tipos de letras de metal individuales para la imprenta.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="contenedorImg">
                                <img id="img06" class="d-block w-100" src="img/old-print-press-1520124_1920.jpg" title="CC0 Creative Commons">
                                <div class="carousel-caption d-none d-md-block">
                                    <h4>Diapositiva 6</h4>
                                    <h5>Composición de texto letra a letra</h5>
                                    <p>
                                        Después sólo quedaba componer en una cajita, letra a letra, el texto que se quería imprimir, cajita que se manchaba
                                        con unos tampones entintados.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="contenedorImg">
                                <img id="img07" class="d-block w-100" src="img/press-552026_1920.jpg" title="CC0 Creative Commons">
                                <div class="carousel-caption d-none d-md-block">
                                    <h4>Diapositiva 7</h4>
                                    <h5>Réplica de la prensa de Gutenber</h5>
                                    <p>
                                        Finalmente, sobre las letras metálicas entintadas se colocaba el papel y se presionaba con una prensa.
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>
                    <a class="carousel-control-prev" href="#carouselInvento" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselInvento" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                <p>
                    En resumen, Gutenberg confeccionó un abecedario con letras y signos de plomo. Su idea fue eficaz porque la perfeccionó con:
                    <ul>
                        <li>Letras móviles</li>
                        <li>Molde de metal</li>
                        <li>Fundidor de tipos o aparato de fundición manual</li>
                        <li>Aleación especial de metales para fabricar los móviles (plomo, antimonio y bismuto)</li>
                        <li>Prensa de madera anclada al suelo y al techo</li>
                        <li>Tinta de imprimir en un determinado papel</li>
                    </ul>
                </p>
            </div>
            <!-- fin seccion difusion imprenta -->
            <!-- inicio seccion trabajos imprenta -->
            <div id="trabajosImprenta" class="animation">
                <h2 class="mt-5 mb-3">Los trabajos en una Imprenta del siglo XV</h2>
                <p>Por eso, en una imprenta se necesitaban fundamentalmente 3 personas:</p>
                <div class="card-deck">
                    <div class="card border-0" style="background-color: whitesmoke;">
                        <img id="imgComponedor" src="img/08_Trabajadores_imprenta_1.png" alt="" class="card-img-top animation" title="Autor desconocido [Dominio Público], via Wikimedia Commons">
                        <div class="card-body">
                            <h5 class="card-title animation">El Componedor</h5>
                            <p class="card-text animation">Realiza el trabajo más delicado. A medida que lee el manuscrito coloca en una cajita, una a una, todas las piezas de metal con letras y espacios que forman una línea. Debe hacerlo en orden inverso. Y cajita a cajita, confecciona toda una página.</p>
                        </div>
                    </div>
                    <div class="card border-0" style="background-color: whitesmoke;">
                        <img id="imgEntintador" class="card-img-top animation" src="img/09_Trabajadores_imprenta_2.jpg" alt="" title="Daniel Chodowiecki [Dominio Público]">
                        <div class="card-body">
                            <h5 class="card-title animation">El Entintador</h5>
                            <p class="card-text animation">Encargado de entintar la superficie de letras que ha elaborado el componedor. Par ello utiliza dos tampones semiesféricos impregnados de tinta, uno en cada mano.</p>
                        </div>
                    </div>
                    <div class="card border-0" style="background-color: whitesmoke;">
                        <img id="imgTirador" class="card-img-top animation" src="img/10_Trabajadores_imprenta_3.jpg" title="Este material está en dominio público en los demás países donde el derecho de autor se extiende por 70 años (o menos) tras la muerte del autor.">
                        <div class="card-body">
                            <h5 class="card-title animation">El Tirador</h5>
                            <p class="card-text animation">Coloca papel sobre la superficie de letras entintadas y acciona la palanca que hace bajar la prensa sobre los tipos metálicos que colocó el componedor, de manera que quedan marcadas en el papel.</p>
                        </div>
                    </div>
                </div>
                <br><br>
                <p>
                    Video en el que se muestra el proceso completo:
                </p>
                <br>
                <div class="row">
                    <div class="col-xl-2"></div>
                    <div class="col-xl-8 shadow-lg p-0 rounded">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item w-80" src="https://www.youtube.com/embed/LidCmy9ADZ4"
                            allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="col-xl-2"></div>
                </div>
            </div>
            <!-- fin seccion trabajos imprenta -->
            <!-- inicio difusión de la idea -->
            <div id="difusionImprenta">
                <h2 class="animation mt-5 mb-3">Difusión de la idea</h2>
                {{-- <div class="d-flex flex-row justify-contente-between align-items-center"> --}}
                    <div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 d-lg-none d-xl-none">
                                <p>
                                    La idea de Gutenberg de las letras individuales grabadas en metal se difunde por Europa Comienza en <span id="mainz">MAINZ (hacia 1450).</span>
                                </p>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 d-lg-none d-xl-none listaHorizontal">
                                    <p>
                                        Van apareciendo las principales ciudades en donde se instalan imprentas:
                                        <ul>
                                            <li><span id="venecia">1469 Venecia (Italia)</span></li>
                                            <li><span id="paris">1470 París (Francia)</span></li>
                                            <li><span id="brujas">1471 Brujas (Holanda)</span></li>
                                            <li><span id="segovia">1472 Segovia (España)</span></li>
                                            <li><span id="valencia">1474 Valencia (España)</span></li>
                                            <li><span id="westminster">1476 Westminster (Gran Bretaña)</span></li>
                                        </ul>
                                    </p>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 d-lg-none d-xl-none">
                                <div class="animation">
                                    <div class="cajaFlex" id="cajaFlex25">
                                        <div class='miImgsXL p-0 shadow-lg rounded' id="img11">
                                            <img id="imgEuropa11" src="img/11_transparente.png">
                                            <figcaption id="XL">Primeras imprentas de Europa</figcaption>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="d-none d-lg-block col-lg-6">
                                <p>
                                    La idea de Gutenberg de las letras individuales grabadas en metal se difunde por Europa Comienza en <span id="mainz_2">MAINZ (hacia 1450).</span>
                                </p>
                                <p>
                                    Van apareciendo las principales ciudades en donde se instalan imprentas:
                                    <ul>
                                        <li><span id="venecia_2">1469 Venecia (Italia)</span></li>
                                        <li><span id="paris_2">1470 París (Francia)</span></li>
                                        <li><span id="brujas_2">1471 Brujas (Holanda)</span></li>
                                        <li><span id="segovia_2">1472 Segovia (España)</span></li>
                                        <li><span id="valencia_2">1474 Valencia (España)</span></li>
                                        <li><span id="westminster_2">1476 Westminster (Gran Bretaña)</span></li>
                                    </ul>
                                </p>
                            </div>
                            <div class="d-none d-lg-block col-lg-6">
                                <div class="animationLeft ml-3">
                                    <div class="cajaFlex" id="cajaFlex25">
                                        <div class='miImgsXL p-0 shadow-lg rounded' id="img11">
                                            <img id="imgEuropa11_2" src="img/11_transparente.png">
                                            <figcaption id="XL">Primeras imprentas de Europa</figcaption>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                {{-- </div> --}}
            </div>
            <!-- fin difusión de la idea -->
            <div class="clearfix"></div>
            <!-- inicio primeros libros -->
            <div id="primerosLibros">
                <h2 class="mt-5 mb-3 animation">Primeros Libros en España</h2>
                <p class="animation">
                    El primer libro impreso en España fue el El sinodal de Aguilafuente en 1472 en Segovia.
                </p>
                <p class="animation">
                    Por su parte, los tres primeros impresos en València con el procedimiento de Gutenberg fueron:
                </p>
                <div class="card-deck mt-3">
                    <div class="card border-0" style="background-color: whitesmoke;">
                        <a href="http://bivaldi.gva.es/es/catalogo_imagenes/grupo.cmd?path=1003553" target="blanc" title="Pulsa para abrir archivo digital">
                            <img id="img12" class="card-img-top animationRight" src="img/12_les_trobes.jpg" alt="Pulsa para abrir archivo digital" title="Bonifaci Ferrer [Domino Público], via Wikimedia Commons">
                        </a>
                        <div class="card-body">
                        <h5 class="card-title animationRight">Obres o trobes en laors de la Verge Maria - 1474</h5>
                        <p class="card-text animationRight">Publicadas en Valencià en 1474, reimpresas con una introducción y noticias biogràficas de sus autores.</p>
                        </div>
                    </div>
                    <div class="card border-0" style="background-color: whitesmoke;">
                            <a href="http://bibliotecadigital.rah.es/dgbrah/es/catalogo_imagenes/grupo.cmd?path=1023031" target="blanc" title="Pulsa para abrir archivo digital">
                                <img id="img13"  class="card-img-top animation" src="img/13_el_comprensorium.jpg">
                            </a>
                        <div class="card-body">
                        <h5 class="card-title animation">Comprehensorium - 1475</h5>
                        <p class="card-text animation">El 23 de febrero aparece en Valencia el libro Comprehensorium, la primera obra editada en España siguiendo las técnicas de impresión de Gutenberg.</p>
                        </div>
                    </div>
                    <div class="card border-0" style="background-color: whitesmoke;">
                        <a href="http://bivaldi.gva.es/es/catalogo_imagenes/grupo.cmd?path=1001292" target="blanc" title="Pulsa para abrir archivo digital">
                            <img id="img14" class="card-img-top animationLeft" src="img/14_valencian_bible.jpg">
                        </a>
                        <div class="card-body">
                        <h5 class="card-title animationLeft">Biblia valenciana – 1478</h5>
                        <p class="card-text">Fue traducida por Bonifacio Ferrer entre 1477 y 1478, como atestigua el colofón de la obra:</p>
                        <p class="card-text text-muted font-italic animation">
                            <small class="text-muted">
                                Acaba la Biblia, molt vera e catholica, treta de una Biblia del noble mossen Berenguer Vives de Boïl, cavaller, la qual fon trelladada de aquella propia que fon arromançada, en lo Monestir de Portaceli, de lengua latina en la nostra valenciana, per lo molt reverend micer Bonifaci Ferrer
                            </small>
                        </p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- fin primeros libros -->
            <!-- inicio lugares emblemáticos de la imprenta valenciana -->
                <div id="imprentaValenciana" class="animation">
                    <h2 class="mt-5 mb-3">Lugares emblemáticos de la Imprenta Valenciana</h2>
                    <p>
                        Lugares emblemáticos de las primeras imprentas en València con el procedimiento de Gutenberg.
                    </p>
                    <div class="accordion mb-5 pb-5" id="lugaresEmblematicos">
                        <div class="card">
                          <div class="card-header" id="headingOne">
                            <h2 class="mb-0" role="heading" aria-label="Lugar emblematico El Moli de Rovella">
                              <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" role="button">
                                El Molí de Rovella
                              </button>
                            </h2>
                          </div>

                          <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#lugaresEmblematicos" aria-expanded="true">
                            <div class="card-body" style="background-color: whitesmoke;">
                                <div class="row">
                                    <div class="col-md-6">
                                      <div class="card border-0">
                                        <div class="card-body" style="background-color: whitesmoke;">
                                          <p class="card-text">El Molí de Rovella albergó la imprenta más importante de Valencia de los siglos XVI al XVII y ya aparecía en el Llibre del Repartiment. Es Berenguer Revell, uno de los nobles entre los que Jaume I distribuyó las tierras recién conquistadas, quien aparece en el Llibre del Repartiment recibiendo el Molino. Y de ahí procede su nombre. El molino debió heredarlo su mujer o su hija, por lo que comenzó a denominarse Na Revella, que acabó derivando en Na Rovella.</p>
                                          <p class="card-text">Construido sobre la Sèquia Mare de Rovella, el viejo molino se encontraba inicialmente fuera de las murallas, en plena huerta. En el siglo XIV, cuando se construye la segunda defensa de Valencia(por la actual calle Guillem de Castro), el Molí de Rovella fue el único que se encontraba dentro de la ciudad amurallada.</p>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="card border-0">
                                        <div class="card-body" style="background-color: whitesmoke;">
                                            <div class="card border-0 rounded" style="background-color:whitesmoke;">
                                                <img id="img04" class="animation card-img" src="img/15_Valencia_Mercado_Central.jpg">
                                                <div class="text"></div>
                                                <div class="card-img-overlay">
                                                  <h5 class="card-title font-weight-bold">Mercado Central de Valencia</h5>
                                                  <p class="card-text" style="color:mediumblue">En la confluencia de las actuales calles Barón de Cárcer y Pie de la Cruz estuvo ubicada la
                                                    primera imprenta en València.</p>
                                                </div>
                                              </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                            </div>
                          </div>
                        </div>
                        <div class="card">
                          <div class="card-header" id="headingTwo">
                            <h2 class="mb-0" role="heading" aria-label="Lugar emblematico, el monasterio de Santa Maria del Puig">
                              <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" role="button">
                                El Monasterio de Santa María del Puig
                              </button>
                            </h2>
                          </div>
                          <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#lugaresEmblematicos" aria-expanded="false">
                            <div class="card-body" style="background-color: whitesmoke;">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="card border-0">
                                        <div class="card-body" style="background-color: whitesmoke;">
                                            <p class="card-text">Declarado Monumento Histórico-Artístico Nacional (Bien de Interés Cultural) en 1969, se trata de un edificio religioso renacentista con cuatro torres como elementos defensivos. Su construcción se inició 1237 y fue erigido por orden de Jaime I en conmemoración de la batalla que permitió la conquista de Valencia a los musulmanes. En su interior se pueden visitar los claustros, el salón Real, de uso exclusivo de los monarcas españoles en sus visitas a Valencia, el salón Gótico de Jaume I, donde se puede admirar la reproducción de la espada del rey y una sección de facsímiles, y el Salón de la Cerámica con numerosas piezas de cerámica romanas, íberas.</p>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="card border-0">
                                        <div class="card-body border-0" style="background-color: whitesmoke;">
                                                <div class="card border-0 rounded" style="background-color:whitesmoke;">
                                                        <img id="img16" class="card-img animation" src="img/16_El_Puig_Monasterio.jpg" title="Espencat [GFDL (http://www.gnu.org/copyleft/fdl.html), CC-BY-SA-3.0 (http://creativecommons.org/licenses/by-sa/3.0/) or Public domain], from Wikimedia Commons">
                                                    <div class="card-img-overlay">
                                                        <h5 class="card-title font-weight-bold">Monasterio de Santa María del Puig</h5>
                                                        <p class="card-text" style="color:mediumblue">Alberga el Museo de la Imprenta y contiene una réplica exacta de la imprenta utilizada Gutenberg y que se conserva en Maguncia.</p>
                                                    </div>
                                                </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                          </div>
                        </div>
                        <div class="card">
                          <div class="card-header" id="headingThree">
                            <h2 class="mb-0" role="heading" aria-label="Lugar emblematico Imprenta de Palamart">
                              <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree" role="button">
                                    Imprenta de Palmart
                              </button>
                            </h2>
                          </div>
                          <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#lugaresEmblematicos" aria-expanded="false">
                            <div class="card-body" style="background-color: whitesmoke;">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="card border-0">
                                            <div class="card-body" style="background-color: whitesmoke;">
                                                <p class="card-text">Mossen Lambert Palmart, impresor alemán nacido en Colonia (Alemania) supuestamente en 1440. Impresor de Obres o trobes en lahors de la Verge Maria considerada la primera obra literaria impresa en España. </p>
                                                <p class="card-text">Se trasladó a Valencia para dirigir el taller de imprenta del comerciante alemán Jacobo Vitzlán, que representaba a la familia de los Ravensburg.</p>
                                                <p class="card-text">De este taller se hizo cargo como maestro impresor, que en el año 1474 (dos años después de Juan Párix) se imprimió uno de los primeros incunables españoles «Les Trobes en lahors de la Verge Maria», considerado como el primer libro literario impreso en España.</p>
                                                <p class="card-text">También es conocido por ser uno de los primeros impresores en utilizar caracteres móviles, así como fundidor de tipos. Consta documentalmente que, en 1493, vendió cierto número de punzones de letra o matrices. Dándole un nivel por encima de la mayoría de impresores. Alfonso Fernández de Córdoba, alumno y colaborador de Palmart en la impresión de algunas obras, aprendió de éste la técnica de la fundición.</p>
                                            </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="card border-0">
                                            <div class="card-body border-0" style="background-color:whitesmoke;">
                                                    <div class="card border-0" style="background-color:whitesmoke;">
                                                        <div class="d-flex justify-content-between" style="background-color:whitesmoke;">
                                                            <img id="img17" class="animation card-img" src="img/17_portal_valldigna_1.jpg">
                                                            <img id="img18" class="animation card-img" src="img/17_portal_valldigna_2.jpg">
                                                        </div>

                                                        <div class="card-img-overlay">
                                                            <h5 class="card-title font-weight-bold text-right">Imprenta de Lampart</h5>
                                                            <p class="card-text" style="color:mediumblue">Junto al Portal de la Valladigna se situaron los talleres de imprenta de donde salieron: “Trobes en loors de la Verge María”, “Comprehensorium” y la “Biblia valenciana” .</p>
                                                        </div>
                                                    </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                          </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingFour">
                                <h2 class="mb-0" role="heading" aria-label="Lugar emblematico, Imprenta de Patricio Mey">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour" role="button">
                                        Imprenta de Patricio Mey
                                </button>
                                </h2>
                            </div>
                            <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#lugaresEmblematicos" aria-expanded="false">
                                <div class="card-body" style="background-color: whitesmoke;">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="card border-0">
                                                <div class="card-body" style="background-color: whitesmoke;">
                                                    <p class="card-text">Los Mey constituyen una familia de impresores que desarrollaron su actividad en Valencia casi de forma exclusiva desde mediados del siglo XVI hasta bien entrado el XVII. El primer impresor fue el flamenco Juan Mey que estuvo activo entre 1543 y 1555 en Valencia, salvo un breve período que permaneció en Alcalá de Henares. A la muerte de Juan Mey se hizo cargo de la imprenta su viuda, Jerónima de Gales, que continuó con el taller hasta su nuevo matrimonio con el también impresor Pedro de Huete. A la muerte de éste, Jerónima con Pedro Patricio, hijo suyo habido con Juan Mey, comenzó una nueva etapa hasta que falleció en 1587, momento en el que quedó al frente de la imprenta Pedro Patricio que la dirigió entre 1588 y 1623. Se encuentra por primera vez a Pedro Patricio en 1581 junto a su madre, como beneficiarios de la subvención que los Jurados habían concedido a Juan Mey.</p>
                                                    <p class="card-text">Entre sus impresiones más importantes están, sobre todo, las ediciones sexta y séptima del Quijote y la de las obras de Tirso de Molina. Imprimió importantes textos de contenido histórico como la Crónica General de España, de 1604, o los Anales del Reino de Valencia, de 1613. Produjo también gran cantidad de obras de carácter legal lo que le ha valido el título de uno de los mejores impresores valencianos.</p>
                                                </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="card border-0">
                                                <div class="card-body" style="background-color:whitesmoke;">
                                                        <div class="card border-0" style="background-color:whitesmoke;">
                                                            <div class="d-flex justify-content-between" style="background-color:whitesmoke;">
                                                                <img id="img19" class="animation card-img" src="img/19b_calle_san_vicente_Patricio_May.png">
                                                                <img id="img20" class="animation card-img" src="img/19_calle_san_vicente_patricio_mey_2.jpg">
                                                            </div>

                                                            <div class="card-img-overlay">
                                                                <h5 class="card-title font-weight-bold text-right">Imprenta de Patricio Mey</h5>
                                                                <p class="card-text" style="color:palevioletred">En el número 3 de la calle San Vicente se imprimió la segunda edición de “Don Quijote de la Mancha”.</p>
                                                            </div>
                                                        </div>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                      </div>
                </div>
            <!-- fin lugares emblemáticos de la imprenta valenciana -->
            <div class="col-sm-12 col-md-12 col-lg-1 co-xl-2 order-md-3 order-sm-3 order-xl-4 order-lg-4">

            </div>
            <div class="co-xl-1 order-xl-5">

            </div>
        </div>

    </div>
    <!-- <span id="subir" class="ir-arriba fas fa-angle-double-up"></span> -->
    <!-- <a href="#" class="goUp" id="subir">
        <img src="img/flecha3.png" title="Ir a inicio" width="50px" height="50px">
    </a> -->

    {{-- <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form> --}}


@endsection