@extends('layouts.app',
    ['title' => 'Dashboard', 'css_files' => ['estilos','dashboard','test_scr_dashboard'],
    'js_files' => ['main', 'push', 'test_scr_home']])

@section('content')

<div id="contenedor">
    @php
    if (is_null($user->avatar)) $avatar = "";
    else $avatar = $user->avatar;
    // echo $user['name'];
    @endphp
    {{-- componentes de head --}}
    <div id="head">

        </div> {{-- fin componentes de head --}}
    {{-- Componentes de usuario --}}
    <div id="user">
        <datos-usuario-component
            :user="{{ json_encode($user) }}"
            :avatar="{{ json_encode($avatar) }}">
        </datos-usuario-component>
    </div>{{-- fin Componentes de menu --}}
    {{-- componentes de main --}}
    <div id="main">
        <dashboard-contenedor-component
            :user="{{ json_encode($user) }}"
            :avatar="{{ json_encode($avatar) }}"
            :statistics="{{ json_encode($statistics) }}"
            :usualopponents="{{ json_encode($usualopponents)}}"
        ></dashboard-contenedor-component>
    </div> {{-- fin componentes de main --}}

    <!-- componentes de infoChallenge -->
    <div class="infochallenge">
        <dashboard-infochallenges-component
            :user="{{ json_encode($user) }}"
            :avatar="{{ json_encode($avatar) }}"
            :games="{{json_encode($games)}}"
        >
        </dashboard-infochallenges-component>
    </div><!-- fin componentes de infoChallenge -->
    <!-- componentes de footer -->
    <div id="footer">
        <footer-component></footer-component>
    </div><!-- fin componentes de footer -->


    <!-- form oculto para realizar el logout via POST de manera síncrona. Podría haber utilizado la función post
        que está definida en test_helpers -->
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
</div>



@endsection
