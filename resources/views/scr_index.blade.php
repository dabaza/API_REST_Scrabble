@extends('layouts.app', ['title' => 'Info Scrabble', 'css_files' => ['estilos', 'test1', 'animaciones'], 'js_files' => ['main','login_logout_component']])

    @section('content')
    <header id="cabecera" class="container-fluid" data-spy="affix" data-offset-top="100">
        <!-- {{ config('app.name', 'Laravel') . '. ' }} {{ $title or '' }} -->
        <!-- imagen cabecera -->
        <img class="imgCabecera" src="img/game-2790713_1920.jpg" title="CC0 Creative Commons. Gratis para usos comerciales, No es necesario reconocimiento">

    </header>
        <div class="row">
        <div class="co-xl-1 order-xl-1">

        </div>
        <div class="col-sm-12 col-md-12 col-lg-1 co-xl-2 order-md-2 order-sm-2 order-xl-2 order-lg-2">

        </div>
        <div class="col-sm-12 col-md-12 col-lg-10 co-xl-6 order-md-1 order-sm-1 order-xl-3 order-lg-3">
            <!-- inicio seccion info scrabble -->
            <div id="scrabbleInfo" class="scrabbleInfo">
                <h2 class="mt-5 mb-3">¿Que es Scrabble?</h2>
                <div id="imgScrabble" class="left">
                    <img id="img22" src="img/blurbus-1528285-639x425.jpg" title="https://images.freeimages.com/images/large-previews/d53/blurbus-1528285.jpg">
                    <span></span>
                </div>
                <p>
                    Scrabble es un juego de mesa en el cual cada jugador intenta ganar más puntos mediante la construcción de palabras
                    sobre un tablero de 15x15 casillas. Las palabras pueden formarse horizontal o verticalmente y se pueden cruzar
                    siempre y cuando aparezcan en el diccionario estándar.
                </p>
                <p>
                    También es conocido como Literati, Alfapet, Funworder, Skip-A-Cross, Scramble, Spelofun, Palabras Cruzadas,
                    Intelect y Word for Word. El juego se vende en 121 países, en 29 idiomas distintos.
                </p>
                <div class="clearfix"></div>
                <h3 class="mt-4 mb-3">Características</h3>
                <p>
                    El juego se realiza entre 2, 3 o 4 jugadores (en general se juega de a 2), sobre un tablero de 15x15 casillas,
                    en las que cada jugador coloca sus fichas. Cada jugador recibe un número específico de fichas (o letras) extraídas
                    aleatoriamente. Las letras se encuentran numeradas con su respectivo valor, obteniéndose por cada palabra formada una
                    puntuación que depende tanto del valor de las letras empleadas como de la posición de dichas letras dentro del tablero.
                </p>
                <p>
                    En total hay 100 fichas, 98 marcadas con letras y dos en blanco (sin puntos, actúan como comodines usándose para reemplazar letras).
                    Según su frecuencia de aparición, las letras tienen más o menos puntos, siempre las de mayor frecuencia valen menos.
                    El tablero tiene también casillas de premiación, que multiplican el número de puntos concedidos:
                    las casillas rojo oscuro son de "triple palabra", las rosas "doble palabra", azul oscuro "triple letra" y celeste "doble letra".
                    El casillero central se marca con una estrella y cuenta como casilla de doble palabra, es obligatorio que el juego comience utilizando esta casilla.
                </p>
                <div class="cajaFlex mt-4" id="cajaFlex21">
                    <div class="cont">
                        <div class="div-img d-none d-lg-block">
                            <img id="img21" class="img" src="img/Tablero_de_Scrabble.svg" title="Pypaertv [GFDL (http://www.gnu.org/copyleft/fdl.html) or CC BY-SA 3.0 (https://creativecommons.org/licenses/by-sa/3.0)], from Wikimedia Commons">
                            <div class="text">
                                <img src="img/celda_roja.jpg" width="15px" height="15px"> Valor triple de palabra. <br>
                                <img src="img/celda_rosa.jpg" width="15px" height="15px"> Valor doble de palabra. <br>
                                <img src="img/celda_azul.jpg" width="15px" height="15px"> Valor triple de letra. <br>
                                <img src="img/celda_celeste.jpg" width="15px" height="15px"> Valor doble de letra. <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- fin seccion info scrabble -->
            <!-- inicio seccion historia scrabble -->
            <div id="scrabbleHistoria">
                <h2 class="mt-5 mb-3">Historia del Scrabble</h2>
                <p>
                    En 1938 Alfred Mosher Butts, residente de Poughkeepsie, creó el juego como una variante de un juego anterior suyo llamado Lexiko.
                </p>
                <p>
                    En medio de la Gran Depresión en 1938, un arquitecto desempleado de Nueva York llamado Alfred Mosher Butts decidió crear un
                    juego de mesa. Butts descubrió que hay tres tipos de juegos: juegos de movimiento, juegos de números, y juegos de palabras.
                    Juegos de movimiento son corrector y el ajedrez, juegos de números son Sudoku y Bingo, y los juegos de palabras son los
                    anagramas y crucigramas. Butts se concentró en la última categoría que involucra tanto a las habilidades como al azar.
                    Más tarde, el juego cambió de nombre a Criss-Cross.
                </p>



                <h3 class="mt-4 mb-3">Lexico</h3>
                <p>
                    Lexiko se jugaba sin tablero de juego, y los jugadores anotaban puntos sobre la base de la longitud de las palabras que iban
                    formando. Se obtenían puntos adicionales por utilizar letras poco comunes (B, F, H, M, P, V, W, Y) y un mayor número de puntos
                    adicionales por utilizar las letras más inusuales (J, K, Q, X, Z).
                </p>
                <p>
                    La manera como Butts calculó la frecuencia de aparición de las letras en su lengua y el correspondiente valor de cada una de
                    ellas fue mediante un riguroso análisis de la portada del New York Times. Llegó a la conclusión de que el juego resultaba muy
                    fácil si se incluían muchas "S", así que redujo el número a 4.
                </p>
                <p>
                    A lo largo de los años siguientes, la mecánica del juego cambió paulatinamente. Por ejemplo, en un momento dado, la primera
                    palabra del juego debía colocarse en la esquina superior izquierda del tablero. Sin embargo, muchas de las características del
                    juego original de Butts se han conservado hasta nuestros días, por ejemplo, el tablero de 15 x 15 casillas o el atril de 7
                    fichas, así como el número de fichas de cada letra y los valores de las mismas, que se han mantenido igual desde 1938 hasta hoy.
                </p>
                <p>
                    Pero con Criss-Crosswords le fue denegada la patente para el juego y varios fabricantes rechazaron su propuesta.
                    Durante un tiempo, Butts consideró la idea de fabricar y comercializar el juego con sus propios medios, pero pronto se
                    dio cuenta de que no tenía espíritu de empresario y volvió a su trabajo como arquitecto. Con la entrada de Estados Unidos
                    a la Segunda Guerra Mundial, Butts abandonó el desarrollo del juego hasta 1948.
                </p>
                <h3 class="mt-4 mb-3">Primeros pasos</h3>
                <p>
                    James Brunot, amigo de Butts y uno de los primeros en recibir uno de los juegos Criss-Crosswords que Butts había fabricado con
                    sus propias manos protagonizó un hito en la historia del juego. Brunot y su esposa creían firmemente en el juego y decidieron
                    arriesgarse a comercializarlo. James Brunot no sólo creía en el juego, sino que disponía del tiempo y del espíritu empresarial necesarios
                    para embarcarse en esta aventura comercial. Así que Butts y Brunot llegaron a un acuerdo: a cambio de dar la autorización a Brunot para
                    fabricar el juego, Butts recibiría un porcentaje de las ganancias de cada juego vendido.
                </p>
                <h3 class="mt-4 mb-3">Nacimiento del Scrabble</h3>
                <p>
                    Los Brunot decidieron que el juego necesitaba pequeñas modificaciones, por lo que reorganizaron la distribución de casillas
                    con premio y simplificaron las reglas, que resultaban demasiado largas y complejas. Al mismo tiempo, empezaron a pensar en un
                    nuevo nombre y solicitaron una patente, la que se les concedió el 1 de diciembre de 1948. Pocos días después, tras largas
                    consideraciones, decidieron llamar Scrabble al juego (esa palabra ya existía en inglés) y consiguieron registrarlo el 16 de
                    diciembre de 1948.
                </p>
                <p>
                    Durante 1949, el primer año de producción del juego por parte de los Brunot, montaron y vendieron 2251 juegos,
                    teniendo un déficit de US$ 450. En los años siguientes, continuaron luchando y trabajando duro para comercializar el juego.
                    En 1952 siguió la tendencia deficitaria, por lo que empezaron a pensar en abandonar el proyecto y Brunot se tomó unas
                    vacaciones para considerar el futuro de su empresa.
                </p>
                <p>
                    Al volver, se encontró con una sorpresa: una avalancha de pedidos, debido a que los que lo habían adquirido lo recomendaban a
                    sus amigos y conocidos. Había llegado el momento de trasladarse a un local más grande, así que se mudaron a un colegio
                    abandonado cercano a su casa de Connecticut. En el último trimestre de 1952 se vendieron 37.000 unidades de Scrabble
                </p>
                <p>
                    Ese mismo año, Jack Strauss, presidente de Marcy's de Nueva Yorck (Los grandes almacenes más importantes del mundo), aprendió a jugar
                    al Scrabble. El juego le entusiasmó de tal manera que al volver a Nueva York pidió a su Departamento de Juegos que le
                    mandaran unas cuantas unidades. No existen testimonios de cómo sucedió todo, pero el hecho es que su Departamento de Juegos
                    tuvo que confesar que no vendían Scrabble. Macy's no sólo empezó a vender Scrabble, sino que apoyó una campaña de promoción del
                    juego que cautivó a miles de personas.
                </p>
                <div class="cajaFlex mt-4" id="cajaFlex24">
                    <div class="cont">
                        <div class="div-img d-none d-lg-block">
                            <img id="img24" title="Enriquecornejo at English Wikipedia [GFDL (http://www.gnu.org/copyleft/fdl.html) or CC BY-SA 3.0 (https://creativecommons.org/licenses/by-sa/3.0)]" class="img" src="img/Macys-sign-largest-store.jpg">
                            <div class="text">
                                Grandes Almacenes Macy's de Nueva York
                            </div>
                        </div>
                    </div>
                </div>
                <h3 class="mt-4 mb-3">Actualidad</h3>
                <p>
                    En 1953, Brunot se dio cuenta de que no podía satisfacer la extraordinaria demanda de Scrabble, así que cedió la licencia
                    de fabricación a Selchow and Righter, en esa época el fabricante de juegos líder en Estados Unidos. Durante tres años,
                    se tuvo que racionar el servicio de Scrabble a las tiendas, ya que la demanda no dejaba de aumentar y el fabricante no podía
                    satisfacerla.
                </p>
                <p>
                    Mientras, la fiebre de Scrabble se extendió hasta Australia en 1953. Ese mismo año, J. W. Spear & Sons lanzaron el juego
                    en Gran Bretaña, donde se convirtió en un éxito de ventas inmediato. Brunot acabó vendiendo los derechos de Scrabble en
                    1968 a Spear's para todo el mundo excepto para los Estados Unidos, Canadá y Australia (unos años más tarde consiguieron
                    también los derechos para este último país). Los derechos quedaron repartidos de esta manera hasta la actualidad.
                </p>
                <p>
                    En 1991 tuvo lugar el primer campeonato mundial (en inglés) de Scrabble en Londres, y el segundo se organizó en Nueva York en
                    1993. En español, se celebró el Primer Campeonato Internacional, en Madrid, en el año 1997. En 1998 se convocó al Segundo
                    Campeonato Internacional en la Ciudad de México D.F. El XIII campeonato mundial en español se realizó en Venezuela (en la Isla
                    de Margarita) en octubre de 2009.
                </p>
                <p>
                    Hay 3 campeonatos del mundo de Scrabble, en español, en inglés y en francés.
                </p>

            </div>
            <!-- fin seccion historia scrabble -->
            <!-- inicio seccion como se juega -->
            <div id="jugarScrabble">
                <h2 class="mt-5 mb-3">Jugar a Scrabble</h2>
                <div class="row mb-5 pb-5">
                    <div class="col-lg-4 col-xl-4">
                      <div class="list-group" id="list-tab-jugar" role="tablist">
                        <a class="list-group-item list-group-item-action active d-xs-inline d-lg-block" id="list-primeraPalabra-list" data-toggle="list" data-target="#primera-palabra" role="tab" aria-controls="list-primeraPalabra-list">Colocar la primera palabra</a>
                        <a class="list-group-item list-group-item-action" id="list-cuentaPuntos-list" data-toggle="list" data-target="#cuenta-puntos" role="tab" aria-controls="list-cuentaPuntos-list">Cuentar tus puntos</a>
                        <a class="list-group-item list-group-item-action d-xs-inline d-lg-block" id="list-extraeNuevos-list" data-toggle="list" data-target="#extrae-nuevos" role="tab" aria-controls="list-extraeNuevos-list">Extraer nuevos azulejos</a>
                        <a class="list-group-item list-group-item-action" id="list-convinaPalabras-list" data-toggle="list" data-target="#convina-palabras" role="tab" aria-controls="list-convinaPalabras-list">Construir sobre las palabras de otros jugadores</a>
                        <a class="list-group-item list-group-item-action" id="list-mejorPuntuacion-list" data-toggle="list" data-target="#mejor-puntuacion" role="tab" aria-controls="list-mejorPuntuacion-list">Obtener la mejor puntuación con tus azulejos</a>
                        <a class="list-group-item list-group-item-action" id="list-debateJugadores-list" data-toggle="list" data-target="#debate-jugadores" role="tab" aria-controls="list-debateJugadores-list">Debatir acerca de una palabra</a>
                        <a class="list-group-item list-group-item-action" id="list-intercambaAzulejos-list" data-toggle="list" data-target="#intercambia-azulejos" role="tab" aria-controls="list-intercambaAzulejos-list">Intercambiar azulejos no deseados</a>
                      </div>
                    </div>

                    <div class="col-lg-8 col-xl-8">
                      <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="primera-palabra" role="tabpanel" aria-labelledby="list-primeraPalabra-list">
                            <p>
                                El jugador que escogió que ganó en el sorteo de turno debe colocar la primera palabra. La palabra debe tener al menos dos azulejos y debe colocarse a lo largo del cuadrado de estrella en el centro del tablero. La palabra se puede colocar en sentido vertical u horizontal, pero no diagonal.
                            </p>
                            <p>
                                Al calcular el primer puntaje de la palabra, ten en cuenta que el jugador que coloca la primera palabra obtiene el doble del puntaje total debido a que la estrella cuenta como un cuadrado de mayor valor (un bono de palabra doble).
                            </p>
                        </div>
                        <div class="tab-pane fade" id="cuenta-puntos" role="tabpanel" aria-labelledby="list-cuentaPuntos-list">
                            <p>
                                Después de colocar una letra, asegúrate de contar tus puntos. Suma los puntos de la esquina superior derecha de cada uno de los azulejos que has puesto. Si colocaste un azulejo en un cuadrado de mayor valor, ajusta tu puntaje como se indica en dicho cuadrado.  Por ejemplo, si colocas una palabra sobre un cuadrado que diga “Palabra doble”, entonces debes duplicar el valor total de la palabra. Si colocas un azulejo sobre un cuadrado que diga “Letra doble”, entonces debes duplicar el valor de ese azulejo de letra solo al calcular tu puntaje.
                            </p>
                        </div>
                        <div class="tab-pane fade" id="extrae-nuevos" role="tabpanel" aria-labelledby="list-extraeNuevos-list">
                            <p>
                                Después de cada turno, tendrás que extraer tantos azulejos nuevos como los que acabas de jugar. Por ejemplo, si jugaste tres de tus azulejos para formar una palabra durante tu turno, entonces tendrás que sacar tres más al terminar. Coloca estos azulejos nuevos en tu estante y pasa la bolsa al siguiente jugador.
                            </p>
                        </div>
                        <div class="tab-pane fade" id="convina-palabras" role="tabpanel" aria-labelledby="list-convinaPalabras-list">
                            <p>
                                En tu siguiente turno, tendrás que formar tu palabra a partir de las palabras que tus oponentes han formado. Eso significa que no puedes crear una palabra independiente en el tablero. Todos los azulejos deben estar conectados.
                            </p>
                            <p>
                                A medida que construyas sobre las palabras que tus oponentes han formado, asegúrate de considerar todos los azulejos conectados. Tu adición al tablero debe crear al menos una palabra nueva, pero si la conectas a otros  azulejos (de otras direcciones) entonces debes asegurarte de crear palabras válidas con esas conexiones.
                            </p>
                        </div>
                        <div class="tab-pane fade" id="mejor-puntuacion" role="tabpanel" aria-labelledby="list-mejorPuntuacion-list">
                            <p>
                                Es una buena idea considerar varias jugadas durante cada uno de tus turnos y hacer la jugada que te hará ganar más puntos. Busca oportunidades para incorporar cuadrados de mayor valor y letras de mayor puntaje como “Z” y “Q” a tus jugadas. Los cuadrados de mayor valor disponibles incluyen:
                                <ul>
                                    <li>
                                        <span>“El puntaje de letra doble”:</span> significa que una letra colocada en este cuadrado obtendrá el doble del número
                                        de puntos que se muestra en la letra.
                                    </li>
                                    <li>
                                        <span>“El puntaje de palabra doble”:</span> significa que una palabra formada que incluya una letra colocada en este cuadrado
                                        obtendrá el doble del número de puntos.
                                    </li>
                                    <li>
                                        <span>“El puntaje de letra triple”:</span> significa que una letra colocada en este cuadrado obtendrá tres veces el número de puntos
                                        que se muestra en la letra.
                                    </li>
                                    <li>
                                        <span>“El puntaje de palabra triple”:</span> significa que una palabra formada que incluya una letra colocada en este cuadrado obtendrá el
                                        triple del número de puntos.
                                    </li>
                                </ul>
                            </p>
                        </div>
                        <div class="tab-pane fade" id="debate-jugadores" role="tabpanel" aria-labelledby="list-debateJugadores-list">
                            <p>
                                Si crees que un jugador ha formado una palabra que no existe o que otro jugador ha deletreado mal una palabra, entonces puedes desafiar a ese jugador. Cuando desafíes a un jugador, debes buscar la palabra en el diccionario.
                            </p>
                            <p>
                                Si la palabra está en el diccionario y el jugador la ha deletreado correctamente, entonces la palabra permanecerá y el jugador obtendrá los puntos. El desafiador pierde su turno. De lo contrario, si la palabra no está en el diccionario o el jugador la ha deletreado de manera incorrecta, entonces el jugador debe quitar la palabra del tablero. El jugador no ganará puntos y perderá ese turno.
                            </p>
                        </div>
                        <div class="tab-pane fade" id="intercambia-azulejos" role="tabpanel" aria-labelledby="list-intercambaAzulejos-list">
                            <p>
                                En algún punto durante el juego, puedes decidir que deseas intercambiar algunos o todos tus azulejos por nuevos. Puedes usar un turno para obtener nuevos azulejos. Simplemente devuelve los azulejos que ya no deseas a la bolsa, agita la bolsa y extrae el número de azulejos que devolviste. Solo ten en cuenta que no puedes formar una palabra después de extraer nuevos azulejos, ya que esta operación contará como un turno
                            </p>
                        </div>
                      </div>
                    </div>
                  </div>
                {{-- <ol>
                    <li>
                        <span>Coloca la primera palabra.</span>
                    </li>
                    <li>
                        <span>Cuenta tus puntos.</span>
                    </li>
                    <li>
                        <span>Extrae nuevos azulejos.</span>
                    </li>
                    <li>
                        <span>Construye sobre las palabras de otros jugadores.</span>
                    </li>
                    <li>
                        <span>Usa tus azulejos para obtener el puntaje más alto posible en cada turno.</span>
                    </li>
                    <li>
                        <span>Desafía a otros jugadores a debatir acerca de una palabra.</span>
                    </li>
                    <li>
                        <span>Intercambia los azulejos que no desees.</span>
                    </li>
                </ol> --}}






            </div>
             <!-- fin seccion como se juega -->

            <div class="col-sm-12 col-md-12 col-lg-1 co-xl-2 order-md-3 order-sm-3 order-xl-4 order-lg-4">

            </div>
            <div class="co-xl-1 order-xl-5">

            </div>
        </div>
        <!-- <span id="subir" class="ir-arriba fas fa-angle-double-up"></span> -->
        <!-- <a href="#" class="goUp" id="subir">
            <img src="img/flecha3.png" title="Ir a inicio" width="50px" height="50px">
        </a> -->
    @endsection

