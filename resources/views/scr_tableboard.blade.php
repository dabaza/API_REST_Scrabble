@extends('layouts.app',
    ['title' => 'Tableboard', 'css_files' => ['test_scr_tableboard', 'estilos', 'tableboard'],
    'js_files' => ['push', 'test_scr_tableboard']])

@section('content')
    <div class="contenedor_tableboard">
        <div class="contenido">
            {{-- <tableboard-test-component
                :user="{{ json_encode($user) }}"
                :opponent="{{ json_encode($opponent) }}"
                :game="{{ json_encode($game) }}">
            </tableboard-test-component> --}}
            <tableboard-user-component
                :user="{{ json_encode($user) }}"
                :opponent="{{ json_encode($opponent) }}"
                :game="{{ json_encode($game) }}">
            </tableboard-user-component>
        </div>
        <div id="footer">
            <footer-component></footer-component>
        </div>
    </div>


    <!-- form oculto para realizar el logout via POST de manera síncrona. Podría haber utilizado la función post
        que está definida en test_helpers -->
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>

@endsection